﻿#if !UNITY_ENGINE

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        private static Dictionary<Color, Pen> DrawRectanglePens = new Dictionary<Color, Pen>();
        private static Dictionary<Color, Brush> DrawRectangleBrushes = new Dictionary<Color, Brush>();
        public static void DrawLine(this Graphics graphics, Pen pen, Point start, Point end)
        {
            // Turn off antialiasing
            SmoothingMode smoothingMode = graphics.SmoothingMode;
            //graphics.SmoothingMode = SmoothingMode.AntiAlias;
            //graphics.CompositingQuality = CompositingQuality.HighQuality;
            //graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            graphics.DrawLines(pen, new Point[]
            {
                start, end
            });

            // Revert back
            graphics.SmoothingMode = smoothingMode;
        }
        [Flags]
        public enum DrawRectangleStyle
        {
            INNER = 1,
            OUTER = 2,
        }
        public static void DrawRectangle(this Graphics graphics, Rectangle rectangle, Color color)
        {
            if (!DrawRectangleBrushes.TryGetValue(color, out Brush brush))
            {
                brush = new SolidBrush(color);
                DrawRectangleBrushes.Add(color, brush);
            }

            // Turn off antialiasing
            SmoothingMode smoothingMode = graphics.SmoothingMode;

            graphics.FillRectangle(brush, rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);

            // Revert back
            graphics.SmoothingMode = smoothingMode;
        }
        public static void DrawBorder(this Graphics graphics, Color color, Rectangle rectangle, int width, DrawRectangleStyle style = DrawRectangleStyle.INNER)
        {
            if (!DrawRectangleBrushes.TryGetValue(color, out Brush brush))
            {
                brush = new SolidBrush(color);
                DrawRectangleBrushes.Add(color, brush);
            }
            DrawBorder(graphics, brush, rectangle, width, style);
        }
        public static void DrawBorder(this Graphics graphics, Brush brush, Rectangle rectangle, int width, DrawRectangleStyle style = DrawRectangleStyle.INNER)
        {
            // Turn off antialiasing
            SmoothingMode smoothingMode = graphics.SmoothingMode;

            //param_graphics.SmoothingMode = SmoothingMode.AntiAlias;
            //param_graphics.CompositingMode = CompositingMode.SourceCopy;
            //param_graphics.CompositingQuality = CompositingQuality.AssumeLinear;

            //
            //                             ###############
            //                             ###############
            // ###########   +---------+   ##           ##
            // ###########   |         |   ##           ##
            // ##       ##   |  REC    |   ##           ##
            // ## INNER ##   |   TAN   |   ##   OUTER   ##
            // ##       ##   |    GLE  |   ##           ##
            // ###########   |         |   ##           ##
            // ###########   +---------+   ##           ##
            //                             ###############
            //                             ###############
            //
            //

            if (style.HasFlag(DrawRectangleStyle.INNER))
            {
                graphics.FillRectangle(brush, rectangle.X, rectangle.Y, rectangle.Width, width);
                graphics.FillRectangle(brush, rectangle.X + rectangle.Width - width, rectangle.Y, width, rectangle.Height);
                graphics.FillRectangle(brush, rectangle.X, rectangle.Y + rectangle.Height - width, rectangle.Width, width);
                graphics.FillRectangle(brush, rectangle.X, rectangle.Y, width, rectangle.Height);
            }

            if (style.HasFlag(DrawRectangleStyle.OUTER))
            {
                graphics.FillRectangle(brush, rectangle.X - width, rectangle.Y - width, rectangle.Width + width * 2, width);
                graphics.FillRectangle(brush, rectangle.X + rectangle.Width, rectangle.Y, width, rectangle.Height);
                graphics.FillRectangle(brush, rectangle.X - width, rectangle.Y + rectangle.Height, rectangle.Width + width * 2, width);
                graphics.FillRectangle(brush, rectangle.X - width, rectangle.Y, width, rectangle.Height);
            }

            // Revert back
            graphics.SmoothingMode = smoothingMode;
        }
        //public static void DrawBorder(this Graphics param_graphics, Brush param_brush, Rectangle param_rectangle, int param_borderTopAndBottom, int param_borderLeftAndRight)
        //{
        //    throw new NotImplementedException();
        //}
        //public static void DrawBorder(this Graphics param_graphics, Brush param_brush, Rectangle param_rectangle, int param_borderTop, int param_borderRight, int param_borderBottom, int param_borderLeft)
        //{
        //    throw new NotImplementedException();
        //}
        //public static void DrawBorder(this Graphics param_graphics, Brush param_brush, Rectangle param_outer, Rectangle param_inner)
        //{
        //    throw new NotImplementedException();
        //}
        //public static void DrawBorder(this Graphics param_graphics, Pen param_pen, Rectangle param_rectangle)
        //{
        //    float shrinkAmount = param_pen.Width / 2;
        //    param_graphics.DrawRectangle(
        //        param_pen,
        //        param_rectangle.X + shrinkAmount,   // move half a pen-width to the right
        //        param_rectangle.Y + shrinkAmount,   // move half a pen-width to the down
        //        param_rectangle.Width - param_pen.Width,   // shrink width with one pen-width
        //        param_rectangle.Height - param_pen.Width); // shrink height with one pen-width
        //}
    }
}


#endif