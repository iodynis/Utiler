﻿using System;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
		/// <summary>
		/// Converts a base64 string to a bytes array.
		/// </summary>
		/// <param name="string">Base64 string.</param>
		/// <returns>Bytes array.</returns>
        public static byte[] Base64ToBytes(string @string)
        {
            return Convert.FromBase64String(@string);
        }
        public static byte[] BytesExtractByStartAndLength(byte[] bytes, long start, long length)
        {
            return BytesExtractByStartAndEnd(bytes, start, start + length - 1);
        }
        public static byte[] BytesExtractByStartAndEnd(byte[] bytes, long start)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException(nameof(bytes), "Byte array cannot be null.");
			}
			return BytesExtractByStartAndEnd(bytes, start, bytes.Length - 1);
		}
        public static byte[] BytesExtractByStartAndEnd(byte[] bytes, long start, long end)
        {
            if (bytes == null)
            {
				throw new ArgumentNullException(nameof(bytes), "Byte array cannot be null.");
            }
	        if (start < 0)
	        {
		        throw new ArgumentOutOfRangeException(nameof(start), "Array starting index cannot be negative.");
	        }
	        if (start >= bytes.Length)
	        {
		        throw new ArgumentOutOfRangeException(nameof(start), "Array starting index cannot be equal to or greater than array length.");
	        }
	        if (end < 0)
	        {
		        throw new ArgumentOutOfRangeException(nameof(end), "Array ending index cannot be negative.");
	        }
	        if (end >= bytes.Length)
	        {
		        throw new ArgumentOutOfRangeException(nameof(end), "Array ending index cannot be equal to or greater than array length.");
	        }
            // Extract
            long count = end - start + 1;
            byte[] subbytes = new byte[count];
            for (int index = 0; index < count; index++)
            {
                subbytes[index] = bytes[start + index];
            }
            return subbytes;
        }
        public static long BytesReadByte(byte[] bytes, int index)
        {
	        if (bytes == null)
	        {
		        throw new ArgumentNullException(nameof(bytes), "Array cannot be null.");
	        }
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative.");
			}
			if (bytes.Length <= index)
			{
				throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be equal to or greater than array length.");
			}
            return bytes[index];
        }
        public static long BytesReadShort(byte[] bytes, int index)
        {
	        if (bytes == null)
	        {
		        throw new ArgumentNullException(nameof(bytes), "Array cannot be null.");
	        }
	        if (index < 0)
	        {
		        throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative.");
	        }
	        if (bytes.Length <= index + 1)
	        {
		        throw new ArgumentOutOfRangeException(nameof(index), "Index + 1 cannot be equal to or greater than array length.");
	        }
            return
                ((short)bytes[index] << 8) +
                ((short)bytes[index + 1]);
        }
        public static int BytesReadInt(byte[] bytes, int index)
        {
	        if (bytes == null)
	        {
		        throw new ArgumentNullException(nameof(bytes), "Array cannot be null.");
	        }
	        if (index < 0)
	        {
		        throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative.");
	        }
	        if (bytes.Length <= index + 3)
	        {
		        throw new ArgumentOutOfRangeException(nameof(index), "Index + 3 cannot be equal to or greater than array length.");
	        }
            return
                ((int)bytes[index] << 24) +
                ((int)bytes[index + 1] << 16) +
                ((int)bytes[index + 2] << 8) +
                ((int)bytes[index + 3]);
        }
        public static long BytesReadLong(byte[] bytes, int index)
        {
	        if (bytes == null)
	        {
		        throw new ArgumentNullException(nameof(bytes), "Array cannot be null.");
	        }
	        if (index < 0)
	        {
		        throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative.");
	        }
	        if (bytes.Length <= index + 7)
	        {
		        throw new ArgumentOutOfRangeException(nameof(index), "Index + 7 cannot be equal to or greater than array length.");
	        }
            return
                ((long)bytes[index] << 56) +
                ((long)bytes[index + 1] << 48) +
                ((long)bytes[index + 2] << 40) +
                ((long)bytes[index + 3] << 32) +
                ((long)bytes[index + 4] << 24) +
                ((long)bytes[index + 5] << 16) +
                ((long)bytes[index + 6] << 8) +
                ((long)bytes[index + 7]);
        }
        public static byte[] Xor(byte[] one, byte[] two)
        {
	        if (one == null)
	        {
		        throw new ArgumentNullException(nameof(one), "Array cannot be null.");
	        }
	        if (two == null)
	        {
		        throw new ArgumentNullException(nameof(two), "Array cannot be null.");
	        }
            if (one.Length != two.Length)
            {
	            throw new ArgumentException($"First array length of {one.Length} is not equal to second array length {two.Length}.");
            }
            byte[] bytes = new byte[one.Length];
            for (int index = 0; index < bytes.Length; index++)
            {
                bytes[index] = (byte)(one[index] ^ two[index]);
            }
            return bytes;
        }
        public static byte[] GetBytes(string @string)
        {
            if (@string == null)
            {
                throw new ArgumentNullException(nameof(@string), "String cannot be null.");
            }
            if (@string == "")
            {
                return new byte[0];
            }
            char[] chars = @string.ToCharArray();
            byte[] bytes = new byte[@string.Length * sizeof(char)];
            Buffer.BlockCopy(chars, 0, bytes, 0, bytes.Length);
            return bytes;
        }
        public static short ReadLittleEndianShort(this byte[] bytes, int offset)
        {
            return (short)(bytes[offset] + (bytes[offset + 1] << 8));
        }
        public static void WriteLittleEndianShort(this byte[] bytes, short offset, int value)
        {
            bytes[offset]     = (byte)( value        & 0xFF);
            bytes[offset + 1] = (byte)((value >> 8)  & 0xFF);
        }
        public static int ReadLittleEndianInteger(this byte[] bytes, int offset)
        {
            return bytes[offset] + (bytes[offset + 1] << 8) + (bytes[offset + 2] << 16) + (bytes[offset + 3] << 24);
        }
        public static void WriteLittleEndianInteger(this byte[] bytes, int offset, int value)
        {
            bytes[offset]     = (byte)( value        & 0xFF);
            bytes[offset + 1] = (byte)((value >> 8)  & 0xFF);
            bytes[offset + 2] = (byte)((value >> 16) & 0xFF);
            bytes[offset + 3] = (byte)((value >> 24) & 0xFF);
        }
        public static int ReadBigEndianInteger(this byte[] bytes, int offset)
        {
            return bytes[offset + 3] + (bytes[offset + 2] << 8) + (bytes[offset + 1] << 16) + (bytes[offset] << 24);
        }
        public static void WriteBigEndianInteger(this byte[] bytes, int offset, int value)
        {
            bytes[offset]     = (byte)((value >> 24) & 0xFF);
            bytes[offset + 1] = (byte)((value >> 16) & 0xFF);
            bytes[offset + 2] = (byte)((value >> 8)  & 0xFF);
            bytes[offset + 3] = (byte)( value        & 0xFF);
        }
        public static uint ReadBigEndianUnsignedInteger(this byte[] bytes, int offset)
        {
            return ((uint)bytes[offset + 3]) + (((uint)bytes[offset + 2]) << 8) + (((uint)bytes[offset + 1]) << 16) + (((uint)bytes[offset]) << 24);
        }
        public static void WriteBigEndianUnsignedInteger(this byte[] bytes, int offset, uint value)
        {
            bytes[offset]     = (byte)((value >> 24) & 0xFF);
            bytes[offset + 1] = (byte)((value >> 16) & 0xFF);
            bytes[offset + 2] = (byte)((value >> 8)  & 0xFF);
            bytes[offset + 3] = (byte)( value        & 0xFF);
        }
    }
}
