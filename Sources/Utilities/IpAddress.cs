﻿using System;
using System.Net;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        public static uint IpAddressToUint(string address)
        {
            if (string.IsNullOrEmpty(address))
            {
                return 0;
            }
            IPAddress.TryParse(address, out IPAddress ipAddress);
            return IpAddressToUint(ipAddress);
        }
        public static uint IpAddressToUint(IPAddress address)
        {
            if (address == null)
            {
                return 0;
            }
            byte[] bytes = address.GetAddressBytes();
            byte temp = bytes[0];
            bytes[0] = bytes[3];
            bytes[3] = temp;
            temp = bytes[1];
            bytes[1] = bytes[2];
            bytes[2] = temp;
            return BitConverter.ToUInt32(bytes, 0);
        }
        public static string IpAddressToString(uint address)
        {
            return ((address & 0xFF000000) >> 24) + "." + ((address & 0x00FF0000) >> 16) + "." + ((address & 0x0000FF00) >> 8) + "." + (address & 0x000000FF);
            //return (param_address & 0x000000FF) + "." + ((param_address & 0x0000FF00) >> 8) + "." + ((param_address & 0x00FF0000) >> 16) + "." + ((param_address & 0xFF000000) >> 24);
        }
        public static string IpAddressToString(IPAddress address)
        {
            if (address == null)
            {
                return null;
            }
            return address.ToString();
        }
        public static IPAddress IpAddressToObject(uint address)
        {
            if (address == 0)
            {
                return null;
            }
            return IPAddress.Parse(IpAddressToString(address));
        }
        public static IPAddress IpAddressToObject(string address)
        {
            if (string.IsNullOrEmpty(address))
            {
                return null;
            }
            return IPAddress.Parse(address);
        }
    }
}
