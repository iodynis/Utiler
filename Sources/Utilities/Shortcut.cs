﻿#if !UNITY_ENGINE

using IWshRuntimeLibrary;
using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        [DllImport("shell32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern void SHChangeNotify(uint wEventId, uint uFlags, IntPtr dwItem1, IntPtr dwItem2);
        public static void Associate(string path, string extension, string description = null)
        {
            string name = FileGetName(path);

            // +--------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+
            // |           |   Windows    |   Windows    |   Windows    |Windows NT| Windows | Windows | Windows | Windows | Windows | Windows | Windows | Windows |  Windows | Windows |
            // |           |     95       |      98      |     Me       |    4.0   |  2000   |   XP    |  2003   |  Vista  |  2008   |    7    | 2008 R2 |    8    |   8.1    |   10    |
            // +--------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+
            // |PlatformID | Win32Windows | Win32Windows | Win32Windows | Win32NT  | Win32NT | Win32NT | Win32NT | Win32NT | Win32NT | Win32NT | Win32NT | Win32NT | Win32NT  | Win32NT |
            // +--------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+
            // |Major      |              |              |              |          |         |         |         |         |         |         |         |         |          |         |
            // | version   |      4       |      4       |      4       |    4     |    5    |    5    |    5    |    6    |    6    |    6    |    6    |    6    |     6    |   10    |
            // +--------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+
            // |Minor      |              |              |              |          |         |         |         |         |         |         |         |         |          |         |
            // | version   |      0       |     10       |     90       |    0     |    0    |    1    |    2    |    0    |    0    |    1    |    1    |    2    |     3    |    0    |
            // +--------------------------------------------------------------------------------------------------------------------------------------------------------------+---------+

            // Windows 7, 8, 8.1
            if (Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version.Major == 6 /* Vista, 2008, 7, 2008R2, 8, 8.1 */)
            {
                try
                {
                    //Registry.ClassesRoot.CreateSubKey(extension).SetValue("", name);
                    //using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(progID))
                    //{
                    //    if (description != null)
                    //        key.SetValue("", description);
                    //    if (icon != null)
                    //    key.CreateSubKey("DefaultIcon").SetValue("", ToShortPathName(icon));
                    //    if (application != null)
                    //    key.CreateSubKey(@"Shell\Open\Command").SetValue("",
                    //    ToShortPathName(application) + " \"%1\"");
                    //}
                    RegistryKey BaseKey;
                    RegistryKey OpenMethod;
                    RegistryKey Shell;
                    RegistryKey CurrentUser;

                    BaseKey = Registry.ClassesRoot.CreateSubKey(extension);
                    BaseKey.SetValue("", name);

                    OpenMethod = Registry.ClassesRoot.CreateSubKey(name);
                    if (description != null)
                    {
                        OpenMethod.SetValue("", description);
                    }
                    OpenMethod.CreateSubKey("DefaultIcon").SetValue("", "\"" + path + "\",0");
                    Shell = OpenMethod.CreateSubKey("Shell");
                    Shell.CreateSubKey("edit").CreateSubKey("command").SetValue("", "\"" + path + "\"" + " \"%1\"");
                    Shell.CreateSubKey("open").CreateSubKey("command").SetValue("", "\"" + path + "\"" + " \"%1\"");
                    BaseKey.Close();
                    OpenMethod.Close();
                    Shell.Close();

                    //CurrentUser = Registry.CurrentUser.CreateSubKey(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\" + Extension);
                    //CurrentUser = CurrentUser.OpenSubKey("UserChoice", RegistryKeyPermissionCheck.ReadWriteSubTree, System.Security.AccessControl.RegistryRights.FullControl);
                    //CurrentUser.SetValue("Progid", KeyName, RegistryValueKind.String);
                    //CurrentUser.Close();
                    CurrentUser = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\" + extension, true);
                    CurrentUser.DeleteSubKey("UserChoice", false);
                    CurrentUser.Close();
                }
                catch
                {
                    ;
                }
            }
            // Windows 10
            if (Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version.Major == 7 /* 10 */)
            {
                try
                {
                    using (RegistryKey User_Classes = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Classes\\", true))
                    using (RegistryKey User_Ext = User_Classes.CreateSubKey("." + extension))
                    using (RegistryKey User_AutoFile = User_Classes.CreateSubKey(extension + "_auto_file"))
                    using (RegistryKey User_AutoFile_Command = User_AutoFile.CreateSubKey("shell").CreateSubKey("open").CreateSubKey("command"))
                    using (RegistryKey ApplicationAssociationToasts = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\ApplicationAssociationToasts\\", true))
                    using (RegistryKey User_Classes_Applications = User_Classes.CreateSubKey("Applications"))
                    using (RegistryKey User_Classes_Applications_Exe = User_Classes_Applications.CreateSubKey(name))
                    using (RegistryKey User_Application_Command = User_Classes_Applications_Exe.CreateSubKey("shell").CreateSubKey("open").CreateSubKey("command"))
                    using (RegistryKey User_Explorer = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\FileExts\\." + extension))
                    using (RegistryKey User_Choice = User_Explorer.OpenSubKey("UserChoice"))
                    {
                        User_Ext.SetValue("", extension + "_auto_file", RegistryValueKind.String);
                        User_Classes.SetValue("", extension + "_auto_file", RegistryValueKind.String);
                        User_Classes.CreateSubKey(extension + "_auto_file");
                        User_AutoFile_Command.SetValue("", "\"" + path + "\"" + " \"%1\"");
                        ApplicationAssociationToasts.SetValue(extension + "_auto_file_." + extension, 0);
                        ApplicationAssociationToasts.SetValue(@"Applications\" + name + "_." + extension, 0);
                        User_Application_Command.SetValue("", "\"" + path + "\"" + " \"%1\"");
                        User_Explorer.CreateSubKey("OpenWithList").SetValue("a", name);
                        User_Explorer.CreateSubKey("OpenWithProgids").SetValue(name + "_auto_file", "0");
                        if (User_Choice != null)
                        {
                            User_Explorer.DeleteSubKey("UserChoice");
                        }
                        User_Explorer.CreateSubKey("UserChoice").SetValue("ProgId", @"Applications\" + name);
                    }
                    SHChangeNotify(0x08000000, 0x0000, IntPtr.Zero, IntPtr.Zero);
                }
                catch
                {
                    ;
                }
            }
        }
        public static void PinToDesktop(string path = null, string name = null, bool unpin = false)
        {
            if (path == null)
            {
                path = ExecutablePath;
            }
            if (name == null)
            {
                name = FileGetName(path);
            }
            string directoryDesktop = DirectoryPathSlashIt(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory));
            string pathShortcut = directoryDesktop + name + ".lnk";
            if (unpin)
            {
                if (System.IO.File.Exists(pathShortcut))
                {
                    FileDelete(pathShortcut);
                }
            }
            else
            {
                WshShell shell = new WshShell();
                WshShortcut shortcut = (WshShortcut)shell.CreateShortcut(pathShortcut);
                shortcut.TargetPath = path; //The exe file this shortcut executes when double clicked
                shortcut.IconLocation = path + ",0"; //Sets the icon of the shortcut to the exe`s icon
                shortcut.WorkingDirectory = path; //The working directory for the exe
                shortcut.Arguments = ""; //The arguments used when executing the exe
                shortcut.Save(); //Creates the shortcut
            }
        }
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true, BestFitMapping = false, ThrowOnUnmappableChar = true)]
        internal static extern IntPtr LoadLibrary(string lpLibFileName);
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true, BestFitMapping = false, ThrowOnUnmappableChar = true)]
        internal static extern int LoadString(IntPtr hInstance, uint wID, StringBuilder lpBuffer, int nBufferMax);
        public static bool PinToTaskbar(string path, bool unpin = false)
        {
            //string directoryAppdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            //string directoryTaskBar = DirectoryPathSlashIt(Path.Combine(directoryAppdata, "/Roaming/Microsoft/Internet Explorer/Quick Launch/User Pinned/TaskBar/"));

            if (!System.IO.File.Exists(path))
            {
                throw new FileNotFoundException(path);
            }

            // Windows 7, 8, 8.1
            if (Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version.Major == 6 /* Vista, 2008, 7, 2008R2, 8, 8.1 */)
            {
                int MAX_PATH = 255;
                var actionIndex = unpin ? 5387 : 5386; // 5386 is the DLL index for "Pin to Tas&kbar", ref. http://www.win7dll.info/shell32_dll.html

                StringBuilder szPinToStartLocalized = new StringBuilder(MAX_PATH);
                IntPtr hShell32 = LoadLibrary("Shell32.dll");
                LoadString(hShell32, (uint)actionIndex, szPinToStartLocalized, MAX_PATH);
                string localizedVerb = szPinToStartLocalized.ToString();

                string pathDirectory = Path.GetDirectoryName(path);
                string pathFileName = Path.GetFileName(path);

                // create the shell application object
                dynamic shellApplication = Activator.CreateInstance(Type.GetTypeFromProgID("Shell.Application"));
                dynamic directory = shellApplication.NameSpace(pathDirectory);
                dynamic link = directory.ParseName(pathFileName);

                dynamic verbs = link.Verbs();
                for (int i = 0; i < verbs.Count(); i++)
                {
                    dynamic verb = verbs.Item(i);
                    if (verb.Name.Equals(localizedVerb))
                    {
                        verb.DoIt();
                        return true;
                    }
                }
            }
            // Windows 10
            if (Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version.Major == 7 /* 10 */)
            {
                throw new NotImplementedException();
            }
            return false;
        }
        public static bool PinToStartMenu(string path, string startMenuDirectoryName = null, bool unpin = false)
        {
            // Pin to start menu
            if (startMenuDirectoryName == null)
            {
                //string directoryAppdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                //string directoryTaskBar = DirectoryPathSlashIt(Path.Combine(directoryAppdata, "/Roaming/Microsoft/Internet Explorer/Quick Launch/User Pinned/TaskBar/"));

                if (!System.IO.File.Exists(path))
                {
                    throw new FileNotFoundException(path);
                }

                // Windows 7, 8, 8.1
                if (Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version.Major == 6 /* Vista, 2008, 7, 2008R2, 8, 8.1 */)
                {
                    int MAX_PATH = 255;
                    var actionIndex = unpin ? 5382 : 5381; // 5381 is the DLL index for "Pin to Start Men&u", ref. http://www.win7dll.info/shell32_dll.html
                    //var actionIndex = unpin ? 51394 : 51201; // 51201 is the DLL index for "Pin to Start Men&u", ref. http://www.win7dll.info/shell32_dll.html

                    StringBuilder szPinToStartLocalized = new StringBuilder(MAX_PATH);
                    IntPtr hShell32 = LoadLibrary("Shell32.dll");
                    LoadString(hShell32, (uint)actionIndex, szPinToStartLocalized, MAX_PATH);
                    string localizedVerb = szPinToStartLocalized.ToString();

                    string pathDirectory = Path.GetDirectoryName(path);
                    string pathFileName = Path.GetFileName(path);

                    // create the shell application object
                    dynamic shellApplication = Activator.CreateInstance(Type.GetTypeFromProgID("Shell.Application"));
                    dynamic directory = shellApplication.NameSpace(pathDirectory);
                    dynamic link = directory.ParseName(pathFileName);

                    dynamic verbs = link.Verbs();
                    for (int i = 0; i < verbs.Count(); i++)
                    {
                        dynamic verb = verbs.Item(i);
                        if (verb.Name.Equals(localizedVerb))
                        {
                            verb.DoIt();
                            return true;
                        }
                    }
                }
                // Windows 10
                if (Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version.Major == 7 /* 10 */)
                {
                    throw new NotImplementedException();
                }
                return false;
            }
            // Create start menu folder
            else
            {
                string startMenuDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Programs);
                string shortcutName = FileGetNameWithoutExtension(path) + ".lnk";
                string shortcutPath = Path.Combine(startMenuDirectory, startMenuDirectoryName, shortcutName);
                string shortcutDirectory = FileGetDirectory(shortcutPath);

                // Unpin
                if (unpin)
                {
                    // Delete the file
                    FileDelete(shortcutPath);

                    // Delete directories while they are empty
                    while (DirectoryGetDirectories(shortcutDirectory).Count == 0 && DirectoryGetFiles(shortcutDirectory).Length == 0)
                    {
                        // Make a pair of comparable paths
                        string comparableShortcutDirectory = Path.GetFullPath(shortcutPath);
                        string comparableStartMenuDirectory = Path.GetFullPath(startMenuDirectory);

                        // If directory is the Start Menu one or upper -- break
                        if (comparableShortcutDirectory == comparableStartMenuDirectory || comparableShortcutDirectory.Length < comparableStartMenuDirectory.Length)
                        {
                            break;
                        }

                        // Delete current directory and move one step up
                        DirectoryDelete(shortcutDirectory);
                        shortcutDirectory = PathGetLeftPart(shortcutDirectory, -1);
                    }


                    if (startMenuDirectoryName != "")
                    {
                        DirectoryDelete(shortcutDirectory);
                    }
                    else
                    {
                        FileDelete(shortcutPath);
                    }
                }
                // Pin
                else
                {
                    PathCreate(shortcutDirectory);

                    WshShell shell = new WshShell();
                    WshShortcut shortcut = (WshShortcut)shell.CreateShortcut(shortcutPath);
                    shortcut.TargetPath = path;
                    shortcut.IconLocation = path + ",0";
                    shortcut.WorkingDirectory = path;
                    shortcut.Arguments = "";
                    shortcut.Description = "";
                    shortcut.Save();
                }
                return true;
            }
        }
    }
}


#endif