﻿using Iodynis.Libraries.Utility.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        private static string _ExecutablePath;
        public static string ExecutablePath
        {
            get
            {
                if (_ExecutablePath == null)
                {
                    Assembly assembly = Assembly.GetEntryAssembly();
                    if (assembly == null)
                    {
                        _ExecutablePath = "./";
                    }
                    else
                    {
                        _ExecutablePath = assembly.Location;
                    }
                    _ExecutablePath = FilePathSlashIt(Path.GetFullPath(_ExecutablePath));
                }
                return _ExecutablePath;
            }
        }
        private static string _ExecutableDirectory;
        public static string ExecutableDirectory
        {
            get
            {
                if (_ExecutableDirectory == null)
                {
                    _ExecutableDirectory = DirectoryPathSlashIt(Path.GetDirectoryName(ExecutablePath));
                }
                return _ExecutableDirectory;
            }
        }
        private static string _ExecutableFileName;
        public static string ExecutableFileName
        {
            get
            {
                if (_ExecutableFileName == null)
                {
                    _ExecutableFileName = Path.GetFileName(ExecutablePath);
                }
                return _ExecutableFileName;
            }
        }
        private static string _ExecutableFileNameWithoutExtension;
        public static string ExecutableFileNameWithoutExtension
        {
            get
            {
                if (_ExecutableFileNameWithoutExtension == null)
                {
                    _ExecutableFileNameWithoutExtension = Path.GetFileNameWithoutExtension(ExecutablePath);
                }
                return _ExecutableFileNameWithoutExtension;
            }
        }
        public static void DirectoryCreate(string path)
        {
            //Directory.CreateDirectory(param_path);
            //try
            //{
            //    Directory.CreateDirectory(param_path);
            //}
            //catch
            //{
            //    ;
            //}

            // Full path
            string pathFull = Path.GetFullPath(path);
            // Individual path parts
            List<string> parts = new List<string>(pathFull.Split(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar }));
            // Terminate path parts with slash, deal with . and ..
            for (int i = 0; i < parts.Count; i++)
            {
                if (parts[i] == "" || parts[i] == ".")
                {
                    parts.RemoveAt(i--);
                    continue;
                }
                if (parts[i] == "..")
                {
                    parts.RemoveAt(i--);
                    parts.RemoveAt(i--);
                    continue;
                }
                parts[i] += Path.DirectorySeparatorChar;
            }
            // Full paths
            string[] paths = new string[parts.Count];
            for (int i = 0; i < paths.Length; i++)
            {
                paths[i] = Path.Combine(parts.GetRange(0, i + 1).ToArray());
            }
            // Create directories
            for (int i = 0; i < paths.Length; i++)
            {
                if (!Directory.Exists(paths[i]))
                {
                    Directory.CreateDirectory(paths[i]);
                }
            }
        }
        /// <summary>
        /// Creates a uniquely named, zero-byte file on disk in the system temp directory and returns the full path to that directory.
        /// </summary>
        public static string FileCreateTemporary()
        {
            return Path.GetTempFileName();
        }
        /// <summary>
        /// Creates a uniquely named, zero-byte file on disk in the specified directory and returns the full path to that directory.
        /// </summary>
        public static string FileCreateTemporary(string directory)
        {
            string file;

            // Try until there is no such file yet
            do
            {
                string fileRandom = Path.GetRandomFileName();
                file = Path.Combine(directory, fileRandom);
            }
            /* do */ while (File.Exists(file));

            // Create the file
            File.Create(file);

            return file;
        }
        /// <summary>
        /// Creates a uniquely named directory on disk in the system temp directory and returns the full path to that directory.
        /// </summary>
        /// <returns></returns>
        public static string DirectoryCreateTemporary()
        {
            string directory;
            string directoryTemp = Path.GetTempPath();

            // Try until there is no such directory yet
            do
            {
                string directoryRandom = Path.GetRandomFileName();
                directory = Path.Combine(directoryTemp, directoryRandom);
            }
            /* do */ while (Directory.Exists(directory));

            // Create the directory
            Directory.CreateDirectory(directory);

            return directory;
        }
        /// <summary>
        /// Rename the directory.
        /// </summary>
        /// <param name="directory">The path to the directory to rename.</param>
        /// <param name="name">The new name for the directory.</param>
        /// <param name="overwrite">Should at first delete the destination directory with the specified name if it already exists.</param>
        public static void DirectoryRename(string directory, string name, bool overwrite = false)
        {
            string directoryTo = Path.Combine(Path.GetDirectoryName(directory), name);
            DirectoryMove(directory, directoryTo, overwrite);
        }
        /// <summary>
        /// Rename the directory.
        /// </summary>
        /// <param name="directoryFrom">The current path to the directory.</param>
        /// <param name="directoryTo">The new path for the directory.</param>
        /// <param name="overwrite">Should at first delete the destination directory if it already exists.</param>
        public static void DirectoryMove(string directoryFrom, string directoryTo, bool overwrite = false)
        {
            if (DirectoryExists(directoryTo))
            {
                if (overwrite)
                {
                    DirectoryDelete(directoryTo);
                }
                else
                {
                    ;
                }
            }
            DirectoryCopy(directoryFrom, directoryTo);
            DirectoryDelete(directoryFrom);
            //try
            //{
            //    Directory.Move(param_diractoryFrom, param_directoryTo);
            //}
            ////catch (AccessViolationException exception)
            ////{
            ////    ;
            ////}
        }
        public static void DirectoryDeleteAllEmptyDirectoriesRecursively(string directory)
        {
            List<string> subDirectories = Directory.GetDirectories(directory).ToList();
            // Call itself on all subDirectories
            foreach (string subDirectory in subDirectories)
            {
                DirectoryDeleteAllEmptyDirectoriesRecursively(subDirectory);
            }
            // Delete if empty
            if (Directory.GetFiles(directory).Length == 0 && Directory.GetDirectories(directory).Length == 0)
            {
                Directory.Delete(directory);
            }
        }
        /// <summary>
        /// Deletes all files inside, then deletes all directories inside, then deletes the specified directory itself.
        /// </summary>
        /// <param name="directory">Directory to delete</param>
        public static void DirectoryDelete(string directory)
        {
            if (!Directory.Exists(directory))
            {
                return;
            }
            try
            {
                Directory.Delete(directory, true);
            }
            catch
            {
                try
                {
                    List<Process> processes = Win32Processes.GetProcessesLockingFile(directory);
                    foreach (Process process in processes)
                    {
                        process.Kill();
                    }
                    Directory.Delete(directory, true);
                }
                catch
                {
                    string[] files = DirectoryGetFiles(directory, true);
                    foreach (string file in files)
                    {
                        FileDelete(file);
                    }
                    List<string> directories = DirectoryGetDirectories(directory, true);
                    for (int i = directories.Count - 1; i >= 0; i--)
                    {
                        if (Directory.Exists(directories[i]))
                        {
                            try
                            {
                                Directory.Delete(directories[i], true);
                            }
                            catch
                            {
                                ;
                            }
                        }
                    }
                    try
                    {
                        Directory.Delete(directory, true);
                    }
                    catch
                    {
                        ;
                    }
                }
            }
            //}
        }
        /// <summary>
        /// Deletes all files inside, then deletes all directories inside.
        /// </summary>
        /// <param name="directory">Directory to clear</param>
        public static void DirectoryClear(string directory)
        {
            if (!Directory.Exists(directory))
            {
                return;
            }
            List<string> files = DirectoryGetFiles(directory, null, true);
            foreach (string file in files)
            {
                FileDelete(file);
            }
            List<string> directories = DirectoryGetDirectories(directory, true);
            for (int i = directories.Count - 1; i >= 0; i--)
            {
                if (Directory.Exists(directories[i]))
                {
                    Directory.Delete(directories[i], true);
                }
            }
        }
        public static bool DirectoryIsWritable(string directory)
        {
            try
            {
                string path = Path.Combine(directory, $"test.if.directory.is.writable.{Path.GetRandomFileName().RemoveCharacters(' ', '.', '_')}.tmp");
                while (File.Exists(path))
                {
                    path = Path.Combine(directory, $"test.if.directory.is.writable.{Path.GetRandomFileName().RemoveCharacters(' ', '.', '_')}.tmp");
                }
                using (File.Create(path)) { }
                File.Delete(path);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static List<string> DirectoryGetDirectories(string directory, bool recursively = false)
        {
            List<string> directories = new List<string>();
            DirectoryGetDirectories(directory, recursively, directories);
            return directories;
        }
        private static void DirectoryGetDirectories(string directory, bool recursively, List<string> directories)
        {
            string[] subdirectories = Directory.GetDirectories(directory);
            for (int index = 0; index < subdirectories.Length; index++)
            {
                directories.Add(DirectoryPathSlashIt(subdirectories[index]));
            }

            if (recursively)
            {
                for (int i = 0; i < subdirectories.Length; i++)
                {
                    DirectoryGetDirectories(subdirectories[i], true, directories);
                }
            }
        }
        /// <summary>
        /// Split path into parts divided by / and \.
        /// </summary>
        /// <param name="path">Path to split.</param>
        /// <returns>Individual path parts that will represent the initial path if joined with / or \.</returns>
        public static List<string> PathGetParts(string path)
        {
            string[] parts = path.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
            return new List<string>(parts);
        }
        /// <summary>
        /// Get the  directories sequence for the path.
        /// Useful for checking and creating all the directories in the path before creating the file.
        /// For example for "C:/Program Files/Program.exe" it will be [C:/, C:/Program Files/], for "C:/Program Files/Program/" it will be [C:/, C:/Program Files/, C:/Program Files/Program/].
        /// </summary>
        /// <param name="path">Path to get the directories sequence for.</param>
        /// <returns>Directories sequence for the path provided.</returns>
        public static List<string> PathGetDirectorySequence(string path)
        {
            List<string> subpaths = new List<string>();
            for (int index = 0; index < path.Length; index++)
            {
                if (path[index] == '/' || path[index] == '\\')
                {
                    subpaths.Add(path.Substring(0, index + 1));
                }
            }
            return subpaths;
        }
        /// <summary>
        /// Get path part.
        /// </summary>
        /// <param name="path">Path.</param>
        /// <param name="index">Index of the part.
        /// index &gt; 0 will give part counting from the start of the path;
        /// index = 0 throws an error;
        /// index &lt; 0 will give part counting from the end of the path.
        /// For example for "C:/Program Files/Program.exe":
        /// "C:" is 1 and -3,
        /// "Program Files" is 2 and -2
        /// "Program.exe" is 3 and -1
        /// Default index is -1.</param>
        /// <returns>Name of a directory in the path.</returns>
        public static string PathGetPart(string path, int index)
        {
            string[] parts = path.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
            if (index > 0)
            {
                if (index > parts.Length)
                {
                    return "";
                }
                return parts[index - 1];
            }
            if (index < 0)
            {
                if (-index > parts.Length)
                {
                    return "";
                }
                return parts[parts.Length + index];
            }
            //if (param_index == 0)
            //{
            //    return "";
            //}
            throw new Exception("PathGetPart does not support index = 0.");
        }
        /// <summary>
        /// Get left part of the path.
        /// </summary>
        /// <param name="path">Path.</param>
        /// <param name="count">Index of the breaking point.
        /// Index &gt; 0 will count from the end of the path to its start
        /// Index = 0 will give path itself
        /// Index &lt; 0 will count from the start of the path to its end</param>
        /// <returns>Left part of the path.</returns>
        public static string PathGetLeftPart(string path, int count)
        {
            if (count < 0)
            {
                int index = path.Length - 1;
                // Find first delimiter in the last delimiter group
                while (index > 0 && (path[index] == '/' || path[index] == '\\'))
                {
                    index--;
                }
                bool wasDelimiter;
                bool isDelimiter = false;

                while (true)
                {
                    // Check index out of boundaries
                    if (index < 0)
                    {
                        return "";
                    }
                    // Delimiter flags
                    wasDelimiter = isDelimiter;
                    isDelimiter = (path[index] == '/' || path[index] == '\\');
                    // Do math
                    if (isDelimiter && !wasDelimiter)
                    {
                        count++;
                        if (count >= 0)
                        {
                            break;
                        }
                    }
                    // Move to next symbol
                    index--;
                }
                return path.Substring(0, index + 1);
            }
            if (count > 0)
            {
                int index = 0;
                bool wasDelimiter;
                bool isDelimiter = false;

                while (true)
                {
                    // Check index out of boundaries
                    if (index >= path.Length)
                    {
                        return path;
                    }
                    // Delimiter flags
                    wasDelimiter = isDelimiter;
                    isDelimiter = (path[index] == '/' || path[index] == '\\');
                    // Do math
                    if (!isDelimiter && wasDelimiter)
                    {
                        count--;
                        if (count <= 0)
                        {
                            break;
                        }
                    }
                    // Move to next symbol
                    index++;
                }
                return path.Substring(0, index);
            }
            return path;
        }
        /// <summary>
        /// Get right part of the path.
        /// </summary>
        /// <param name="path">Path.</param>
        /// <param name="count">Index of the breaking point.
        /// Index &gt; 0 will count from the end of the path to its start.
        /// Index = 0 will give path itself.
        /// Index &lt; 0 will count from the start of the path to its end.</param>
        /// <returns>Right part of the path.</returns>
        public static string PathGetRightPart(string path, int count)
        {
            if (count > 0)
            {
                int index = path.Length - 1;
                // Skip the last slashes if any
                while (index >= 0 && (path[index] == '/' || path[index] == '\\'))
                {
                    index--;
                }
                bool wasDelimiter;
                bool isDelimiter = false;

                while (true)
                {
                    // Check index out of boundaries
                    if (index < 0)
                    {
                        return path;
                    }
                    // Delimiter flags
                    wasDelimiter = isDelimiter;
                    isDelimiter = (path[index] == '/' || path[index] == '\\');
                    // Do math
                    if (isDelimiter && !wasDelimiter)
                    {
                        count--;
                        if (count <= 0)
                        {
                            break;
                        }
                    }
                    // Move to next symbol
                    index--;
                }
                return path.Substring(index + 1, path.Length - index - 1);
            }
            if (count < 0)
            {
                int index = 0;
                bool wasDelimiter;
                bool isDelimiter = false;

                while (true)
                {
                    // Check index out of boundaries
                    if (index >= path.Length)
                    {
                        return "";
                    }
                    // Delimiter flags
                    wasDelimiter = isDelimiter;
                    isDelimiter = (path[index] == '/' || path[index] == '\\');
                    // Do math
                    if (!isDelimiter && wasDelimiter)
                    {
                        count++;
                        if (count >= 0)
                        {
                            break;
                        }
                    }
                    // Move to next symbol
                    index++;
                }
                return path.Substring(index, path.Length - index);
            }
            return path;
            //if (param_count < 0)
            //{
            //    int index = 0;
            //    while (param_count < 0 && index < param_path.Length)
            //    {
            //        if ((param_path[index] == '/' || param_path[index] == '\\') && (index == 0 || (param_path[index - 1] != '/' && param_path[index - 1] != '\\')))
            //        {
            //            param_count++;
            //        }
            //        index++;
            //    }
            //    return param_path.Substring(param_path.Length - index, index);
            //}
            //if (param_count > 0)
            //{
            //    int index = param_path.Length - 1;
            //    while (param_count > 0 && index >= 0)
            //    {
            //        if ((param_path[index] == '/' || param_path[index] == '\\') && (index == 0 || (param_path[index - 1] != '/' && param_path[index - 1] != '\\')))
            //        {
            //            param_count--;
            //        }
            //        index--;
            //    }
            //    return param_path.Substring(param_path.Length - index, index);
            //}

            //string[] parts = param_path.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
            //if (param_index >= 0)
            //{
            //    if (param_index >= parts.Length)
            //    {
            //        return "";
            //    }
            //    return parts[param_index];
            //}
            //if (param_index < 0)
            //{
            //    if (-param_index > parts.Length)
            //    {
            //        return "";
            //    }
            //    return parts[parts.Length + param_index];
            //}
            //throw new Exception($"{nameof(Util)}.{nameof(PathGetRightPart)} does not support index = 0.");
            //throw new Exception("Should never happen.");
        }
        public static string[] DirectoryGetFiles(string directory, bool recursively = true)
        {
	        if (directory == null)
	        {
		        throw new ArgumentNullException(nameof(directory), "Directory path cannot be null.");
	        }
            if (!recursively)
            {
                return Directory.GetFiles(directory);
            }

            Stack<string> directories = new Stack<string>() { directory };
            Stack<string[]> filesInDirectories = new Stack<string[]>();

            long filesCount = 0;
            while (directories.Count > 0)
            {
                string directoryCurrent = directories.Pop();
                directories.Push(Directory.GetDirectories(directoryCurrent));
                string[] filesInDirectory = Directory.GetFiles(directoryCurrent);
                filesCount += filesInDirectory.LongLength;
                filesInDirectories.Add(filesInDirectory);
            }

            string[] files = new string[filesCount];
            for (long counter = 0; counter < filesCount;)
            {
                string[] filesInDirectory = filesInDirectories.Pop();
                for (long index = 0; index < filesInDirectory.LongLength; index++, counter++)
                {
                    files[counter] = filesInDirectory[index];
                }
            }

            return files;
        }
        private static void DirectoryGetFiles(string directory, List<string> files)
        {
            files.AddRange(Directory.GetFiles(directory));
            foreach (string subDirectory in Directory.GetDirectories(directory))
            {
                DirectoryGetFiles(subDirectory, files);
            }
        }
        public static string DirectoryGetFile(string directory, string file)
        {
            string path = Path.Combine(directory, file);
            if (File.Exists(path))
            {
                return path;
            }
            List<string> subDirectories = DirectoryGetDirectories(directory, true);
            foreach (string subDirectory in subDirectories)
            {
                path = Path.Combine(subDirectory, file);
                if (File.Exists(path))
                {
                    return path;
                }
            }
            return null;
        }
        public static List<string> DirectoryGetFiles(string directory, string regex, string extensionInclude, string extensionsExclude)
        {
            return DirectoryGetFiles(directory, regex, false, new string[] { extensionInclude }, new string[] { extensionsExclude });
        }
        public static List<string> DirectoryGetFiles(string directory, string regex, string extensionInclude, string[] extensionsExclude = null)
        {
            return DirectoryGetFiles(directory, regex, false, new string[] { extensionInclude }, extensionsExclude);
        }
        public static List<string> DirectoryGetFiles(string directory, string regex, string[] extensionsInclude, string extensionExclude)
        {
            return DirectoryGetFiles(directory, regex, false, extensionsInclude, new string[] { extensionExclude });
        }
        public static List<string> DirectoryGetFiles(string directory, string regex, string[] extensionsInclude = null, string[] extensionsExclude = null)
        {
            return DirectoryGetFiles(directory, regex, false, extensionsInclude, extensionsExclude);
        }

        public static List<string> DirectoryGetFiles(string directory, string regex, bool recursively, string extensionInclude, string extensionExclude)
        {
            return DirectoryGetFiles(directory, regex, recursively, new string[] { extensionInclude }, new string[] { extensionExclude });
        }
        public static List<string> DirectoryGetFiles(string directory, string regex, bool recursively, string extensionInclude, string[] extensionsExclude = null)
        {
            return DirectoryGetFiles(directory, regex, recursively, new string[] { extensionInclude }, extensionsExclude);
        }
        public static List<string> DirectoryGetFiles(string directory, string regex, bool recursively, string[] extensionsInclude, string extensionExclude)
        {
            return DirectoryGetFiles(directory, regex, recursively, extensionsInclude, new string[] { extensionExclude });
        }
        public static List<string> DirectoryGetFiles(string directory, string regex, bool recursively, string[] extensionsInclude = null, string[] extensionsExclude = null)
        {
            List<string> files = new List<string>();
            extensionsInclude = extensionsInclude ?? new string[0];
            extensionsExclude = extensionsExclude ?? new string[0];

            if (recursively)
            {
                List<string> subDirectories = DirectoryGetDirectories(directory, true);
                foreach (string subDirectory in subDirectories)
                {
                    DirectoryGetFiles(subDirectory, regex, extensionsInclude, extensionsExclude, files);
                    for (int i = 0; i < files.Count; i++)
                    {
                        files[i] = FilePathSlashIt(files[i]);
                    }
                }
            }
            else
            {
                DirectoryGetFiles(directory, regex, extensionsInclude, extensionsExclude, files);
                for (int i = 0; i < files.Count; i++)
                {
                    files[i] = FilePathSlashIt(files[i]);
                }
            }
            return files;
        }
        private static void DirectoryGetFiles(string directory, string regexString, string[] extensionsInclude, string[] extensionsExclude, List<string> files)
        {
             DirectoryGetFiles(directory, regexString != null ? new Regex(regexString, RegexOptions.Compiled | RegexOptions.IgnoreCase) : null, extensionsInclude, extensionsExclude, files);
        }
        private static void DirectoryGetFiles(string directory, Regex regex, string[] extensionsInclude, string[] extensionsExclude, List<string> files)
        {
            if (!Directory.Exists(directory))
            {
                return;
            }
            List<string> filesInDirectory = Directory.GetFiles(directory).Select(lambda_file => FilePathSlashIt(lambda_file)).ToList();
            // Regex
            if (regex != null)
            {
                for (int i = 0; i < filesInDirectory.Count; i++)
                {
                    string filename = Path.GetFileName(filesInDirectory[i]);
                    if (!regex.IsMatch(filename))
                    {
                        filesInDirectory.RemoveAt(i--);
                    }
                }
            }
            // Extensions to include
            if (extensionsInclude != null && extensionsInclude.Length != 0)
            {
                for (int i = 0; i < filesInDirectory.Count; i++)
                {
                    bool extensionFound = false;
                    for (int j = 0; j < extensionsInclude.Length; j++)
                    {
                        if (filesInDirectory[i].EndsWith(extensionsInclude[j]))
                        {
                            extensionFound = true;
                            break;
                        }
                    }
                    if (!extensionFound)
                    {
                        filesInDirectory.RemoveAt(i--);
                    }
                    //string extension = Path.GetExtension(files[i]);
                    //if (!param_extensionsInclude.Contains(extension))
                    //{
                    //    files.RemoveAt(i--);
                    //}
                }
            }
            // Extensions to exclude
            if (extensionsExclude != null && extensionsExclude.Length != 0)
            {
                for (int i = 0; i < filesInDirectory.Count; i++)
                {
                    for (int j = 0; j < extensionsExclude.Length; j++)
                    {
                        if (filesInDirectory[i].EndsWith(extensionsExclude[j]))
                        {
                            filesInDirectory.RemoveAt(i--);
                            break;
                        }
                    }
                    //string extension = Path.GetExtension(files[i]);
                    //if (param_extensionsExclude.Contains(extension))
                    //{
                    //    files.RemoveAt(i--);
                    //}
                }
            }
            files.AddRange(filesInDirectory);

            //// Subdirectories
            //string[] directories = Directory.GetDirectories(directory);
            //for (int i = 0; i < directories.Length; i++)
            //{
            //    DirectoryGetFiles(directories[i], regex, extensionsInclude, extensionsExclude, files);
            //}
        }
        public static List<string> DirectoryGetFilesExact(string directory, string name, bool checkRootDirectoryToo)
        {
            List<string> paths = new List<string>();

            if (checkRootDirectoryToo)
            {
                string path = Path.Combine(directory, name);
                if (File.Exists(path))
                {
                    paths.Add(path);
                }
            }

            foreach (string subDirectory in DirectoryGetDirectories(directory, true))
            {
                string path = Path.Combine(subDirectory, name);
                if (File.Exists(path))
                {
                    paths.Add(path);
                }
            }

            return paths;
        }
        /// <summary>
        /// Copies content of the directory to a new place
        /// </summary>
        /// <param name="source">Directory to copy its content from</param>
        /// <param name="destination">Directory to paste content to</param>
        public static void DirectoryCopy(string source, string destination)
        {
	        if (source == null)
	        {
		        throw new ArgumentNullException(nameof(source), "Source directory path cannot be null.");
	        }
	        if (destination == null)
	        {
		        throw new ArgumentNullException(nameof(destination), "Destination directory path cannot be null.");
	        }

            source = Path.GetFullPath(source);
            destination = Path.GetFullPath(destination);

            // Get the subDirectories for the specified directory.
            DirectoryInfo directoryInfo = new DirectoryInfo(source);
            DirectoryInfo[] subDirectoryInfos = directoryInfo.GetDirectories();

            if (!directoryInfo.Exists)
            {
                throw new DirectoryNotFoundException($"Source directory does not exist or could not be found: {source}");
            }

            // If the destination directory doesn't exist, create it.
            DirectoryCreate(destination);
            //if (!Directory.Exists(param_destination))
            //{
            //    DirectoryCreate(param_destination);
            //}

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = directoryInfo.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destination, file.Name);
                file.CopyTo(temppath, true);
            }

            foreach (DirectoryInfo subDirectoryInfo in subDirectoryInfos)
            {
                string temppath = Path.Combine(destination, subDirectoryInfo.Name);
                DirectoryCopy(subDirectoryInfo.FullName, temppath);
            }
        }
        public static bool DirectoryIsEmpty(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "Directory path cannot be null.");
	        }

            return !Directory.EnumerateFileSystemEntries(path).Any();
        }
        public static string DirectoryGetName(string path)
        {
            return PathGetRightPart(path, 1);
        }
        /// <summary>
        /// Split the path of the directory specified into the path of its parent directory and the name of the specified directory.
        /// </summary>
        /// <param name="path">Path to the directory.</param>
        /// <param name="trailingSlash">Keep the trialing slash of the directory.</param>
        /// <returns>Path to the parent directory and the name of the specified directory.</returns>
        public static (string Directory, string Name) DirectoryGetParentAndName(string path, bool trailingSlash = true)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "Directory path cannot be null.");
	        }
            if (path.Length == 0)
            {
                return ("", "");
            }
            if (path.Length == 1)
            {
                if (path == "/" || path == "\\")
                {
                    return ("", "");
                }
                else
                {
                    return ("", path);
                }
            }

            // Should ignore the last slash
            char characterLast = path[path.Length - 1];
            bool characterLastIsSlash = characterLast == '\\' || characterLast == '/';
            int start = characterLastIsSlash ? path.Length - 2 : path.Length - 1;
	        int indexOfLastSlash = path.LastIndexOfAny(new char[] { '/', '\\' }, start, path.Length - 1);

	        // If there is no slash
	        if (indexOfLastSlash < 0)
	        {
		        return ("", characterLastIsSlash ? path.Substring(0, path.Length - 1) : path);
	        }

            string parentName = path.Substring(0, indexOfLastSlash + (trailingSlash ? 1 : 0));
            string directoryName = path.Substring(indexOfLastSlash + 1, path.Length - (indexOfLastSlash + 1) - (characterLastIsSlash ? 1 : 0));
            return (parentName, directoryName);
        }
        public static bool DirectoryExists(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "Directory path cannot be null.");
	        }

            return Directory.Exists(path);
        }
        /// <summary>
        /// Change slashes from \ to /, optionally end the path with a /.
        /// </summary>
        /// <param name="directory">Path to the directory.</param>
        /// <param name="trailSlash">End the path with a /.</param>
        /// <returns>Path to the directory.</returns>
        public static string DirectoryPathSlashIt(string directory, bool trailSlash = true)
        {
	        if (directory == null)
	        {
		        throw new ArgumentNullException(nameof(directory), "Directory path cannot be null.");
	        }

            directory = directory.Replace('\\', '/');
            if (trailSlash)
            {
                if (!directory.EndsWith("/"))
                {
                    directory += "/";
                }
            }
            else
            {
                if (directory.EndsWith("/"))
                {
                    directory = directory.TrimEnd('/');
                }
            }
            return directory;
        }
        /// <summary>
        /// Change slashes from \ to /.
        /// </summary>
        /// <param name="file">Path to the file.</param>
        /// <returns>Path to the file.</returns>
        public static string FilePathSlashIt(string file)
        {
	        if (file == null)
	        {
		        throw new ArgumentNullException(nameof(file), "File path cannot be null.");
	        }

            file = file.Replace('\\', '/');
            return file;
        }
        /// <summary>
        /// Change slashes from \ to /.
        /// </summary>
        /// <param name="file">Path.</param>
        /// <returns>Path.</returns>
        public static string PathSlashIt(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "Path cannot be null.");
	        }

            path = path.Replace('\\', '/');
            return path;
        }
        public static string FileGetName(string file)
        {
			if (file == null)
			{
				throw new ArgumentNullException(nameof(file), "File path cannot be null.");
			}

			int indexOfLastSlash = file.LastIndexOfAny(new char[] { '/', '\\' });
			// If there is no slash
			if (indexOfLastSlash < 0)
			{
				return file;
			}

			int filenameStartIndex = indexOfLastSlash + 1;
			// If the last character is a slash
			if (filenameStartIndex >= file.Length)
			{
				return "";
			}

			string fileName = file.Substring(filenameStartIndex);
            return fileName;
        }
        public static string FileGetDirectory(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }
            if (path.Length == 0)
            {
                return "";
            }
            char characterLast = path[path.Length - 1];
            if (characterLast == '\\' /* directory */ || characterLast == '/' /* directory */)
            {
                return path;
            }

	        int indexOfLastSlash = path.LastIndexOfAny(new char[] { '/', '\\' });
	        // If there is no slash
	        if (indexOfLastSlash < 0)
	        {
		        return "";
	        }

            string directoryName = path.Substring(0, indexOfLastSlash + 1);
            return directoryName;
        }
        public static (string Directory, string Name) FileGetDirectoryAndName(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }
            if (path.Length == 0)
            {
                return ("", "");
            }
            char characterLast = path[path.Length - 1];
            if (characterLast == '\\' /* directory */ || characterLast == '/' /* directory */)
            {
                return (path, "");
            }

	        int indexOfLastSlash = path.LastIndexOfAny(new char[] { '/', '\\' });
	        // If there is no slash
	        if (indexOfLastSlash < 0)
	        {
		        return ("", path);
	        }

            string directoryName = path.Substring(0, indexOfLastSlash + 1);
            string fileName = path.Substring(indexOfLastSlash + 1);
            return (directoryName, fileName);
        }
        public static (string Directory, string NameWithoutExtension, string Extension) FileGetDirectoryAndNameAndExtension(string path, int count = 1)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }
            if (path.Length == 0)
            {
                return ("", "", "");
            }
            char characterLast = path[path.Length - 1];
            if (characterLast == '\\' /* directory */ || characterLast == '/' /* directory */)
            {
                return (path, "", "");
            }
            if (count < 0)
            {
                throw new ArgumentException("Count cannot be negative.", nameof(count));
            }

            string directory, nameWithExtension, nameWithoutExtension, extension;

	        int indexOfLastSlash = path.LastIndexOfAny(new char[] { '/', '\\' });
	        // If there is no slash
	        if (indexOfLastSlash < 0)
	        {
		        directory = "";
                nameWithExtension = path;
	        }
            else
            {
                directory = path.Substring(0, indexOfLastSlash + 1);
                nameWithExtension = path.Substring(indexOfLastSlash + 1);
            }

            int counter = 0;
            int indexOfTheDot = -1;
            for (int index = nameWithExtension.Length - 1; index >= 0; index--)
            {
                if (nameWithExtension[index] == '.')
                {
                    indexOfTheDot = index;
                    counter++;
                }
                if (counter == count)
                {
                    break;
                }
            }
	        // If there are no dots
            if (indexOfTheDot < 0)
            {
                nameWithoutExtension = nameWithExtension;
                extension = "";
            }
            else
            {
                nameWithoutExtension = nameWithExtension.Substring(0, indexOfTheDot);
                extension = nameWithExtension.Substring(indexOfTheDot);
            }

            return (directory, nameWithoutExtension, extension);
        }
        public static string FileGetNameWithoutExtension(string path, int count = 1)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }
            if (count < 0)
            {
                throw new ArgumentException("Count cannot be negative.", nameof(count));
            }
            if (path.Length == 0)
            {
                return "";
            }
            char characterLast = path[path.Length - 1];
            if (characterLast == '\\' /* directory */ || characterLast == '/' /* directory */)
            {
                return "";
            }
            if (count == 0)
            {
                return Path.GetFileName(path);
            }

            int indexOfLastSlash = path.LastIndexOfAny(new char[] { '/', '\\' });
	        int filenameStartIndex = indexOfLastSlash + 1;

			// Using the last slash get the filename
            string fileName = (indexOfLastSlash >= 0 && 0 <= filenameStartIndex && filenameStartIndex < path.Length) ? path.Substring(filenameStartIndex) : path;
            int counter = 0;
            int indexOfTheDot = -1;
            for (int index = fileName.Length - 1; index >= 0; index--)
            {
                if (fileName[index] == '.')
                {
                    indexOfTheDot = index;
                    counter++;
                }
                if (counter == count)
                {
                    break;
                }
            }
	        // If there are no dots
            if (indexOfTheDot < 0)
            {
                return fileName;
            }

            fileName = fileName.Substring(0, indexOfTheDot);
            return fileName;
        }
        /// <summary>
        /// Get the filename extension without the dot.
        /// Extension is considered to be after the last dot.
        /// Extension itself contains no dots.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <returns></returns>
        public static string FileGetExtension(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }
            if (path.Length == 0)
            {
                return "";
            }
            char characterLast = path[path.Length - 1];
            if (characterLast == '\\' /* directory */ || characterLast == '/' /* directory */ || characterLast == '.' /* . or .. */)
            {
                return "";
            }

            int indexOfLastSlash = path.LastIndexOfAny(new char[] { '/', '\\' });
	        int filenameStartIndex = indexOfLastSlash + 1;
			//// If last symbol is a slash
			//if (filenameStartIndex == param_path.Length)
			//{
			//	return "";
			//}
			// Using the last slash get the filename
            string fileName = (indexOfLastSlash >= 0 && 0 <= filenameStartIndex && filenameStartIndex < path.Length) ? path.Substring(filenameStartIndex) : path;

            int indexOfLastDot = fileName.LastIndexOf('.');
	        // If there is no dot (and no extension therefore)
            if (indexOfLastDot < 0)
            {
                return "";
            }

	        int extensionStartIndex = indexOfLastDot + 1;
	        // If the last character of the path is a dot
			if (extensionStartIndex >= fileName.Length)
			{
				return "";
			}

            string extension = fileName.Substring(extensionStartIndex);
            return extension;
        }
        /// <summary>
        /// Removes the \/"*:?&lt;&gt;| symbols.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetFileNameSafe(string name)
        {
            return name
                .Replace("\\", "")
                .Replace("/", "")
                .Replace("\"", "")
                .Replace("*", "")
                .Replace(":", "")
                .Replace("?", "")
                .Replace("<", "")
                .Replace(">", "")
                .Replace("|", "");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="IndexOutOfRangeException"></exception>
        public static string PathStripExtensions(string path, int count = 1)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }

            if (count < 0)
            {
                throw new IndexOutOfRangeException($"{count} < 0");
            }
            if (count == 0)
            {
                return path;
            }

            int indexOfLastDot = path.Length;
            for (int index = path.Length - 1; index >= 0; index--)
            {
                if (path[index] == '/' || path[index] == '\\')
                {
                    break;
                }
                if (path[index] == '.')
                {
                    indexOfLastDot = index;

                    if (--count <= 0)
                    {
                        break;
                    }
                }
            }

            return path.Substring(0, indexOfLastDot);
        }
        public static byte[] FileReadBytes(string path)
        {
            using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 4096, FileOptions.SequentialScan))
            {
                byte[] bytes = new byte[fileStream.Length];
                fileStream.Read(bytes, 0, fileStream.Length);
                return bytes;
            }
        }
        public static string FileReadString(string path, Encoding encoding = null)
        {
            using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 4096, FileOptions.SequentialScan))
            using (StreamReader streamReader = new StreamReader(fileStream, encoding ?? Encoding.UTF8))
            {
                return streamReader.ReadToEnd();
            }
        }
        public static string[] FileReadLines(string path, Encoding encoding = null)
        {
            return FileReadString(path, encoding).Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
        }
        //public static string FileGetAbsolutePath(string param_path)
        //{
        //    return FileSlashIt(Path.GetFullPath(param_path));
        //}
        //public static string FileGetRelativePath(string param_path)
        //{
        //    return FileSlashIt(Path.GetFullPath(param_path));
        //}
        public static void PathCreate(string path, bool createOnlyDirectories = false)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path), "Path cannot be null.");
            }

            string pathSlashed = PathSlashIt(path);
            string directory = pathSlashed.EndsWith("/") ? pathSlashed : PathGetLeftPart(pathSlashed, -1);
            // Create directory
            DirectoryCreate(directory);
            // Create file
            if (!createOnlyDirectories && !pathSlashed.EndsWith("/"))
            {
                FileCreate(pathSlashed);
            }
        }
        public static string PathGetAbsolutePath(string path)
        {
            if (path == null)
            {
                throw new Exception("Path is null.");
            }

            return Path.GetFullPath(path).Replace('\\', '/');// + ((param_path.EndsWith("\\") || param_path.EndsWith("/")) ? "/" : "");
        }
        public static string PathGetRelativePath(string pathMain, string pathReferenced)
        {
            if (pathMain == null)
            {
                throw new Exception("Main path is null.");
            }
            if (pathReferenced == null)
            {
                throw new Exception("Referenced path is null.");
            }

            string pathMainAbsolute = PathGetAbsolutePath(pathMain);
            string pathReferencedAbsolute = PathGetAbsolutePath(pathReferenced);

            // If different drive letters
            if (pathMainAbsolute.Length >= 2 && pathMainAbsolute[1] == ':' &&
                pathReferencedAbsolute.Length >= 2 && pathReferencedAbsolute[1] == ':' &&
                pathMainAbsolute[0] != pathReferencedAbsolute[0])
            {
                return pathMainAbsolute;
            }

            List<string> pathMainParts = PathGetParts(pathMainAbsolute);
            List<string> pathReferencedParts = PathGetParts(pathReferencedAbsolute);

            // For reference path we need only directories -- remove file name if there is one
            string filenameReferenced = "";
            if (!pathReferencedAbsolute.EndsWith("/"))
            {
                filenameReferenced = pathReferencedParts[pathReferencedParts.Count - 1];
                pathReferencedParts.RemoveLast();
            }
            string filenameMain = "";
            if (!pathMainAbsolute.EndsWith("/"))
            {
                filenameMain = pathMainParts[pathMainParts.Count - 1];
                pathMainParts.RemoveLast();
            }

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(".");
            int index = 0;
            while (index < pathMainParts.Count && index < pathReferencedParts.Count && pathMainParts[index] == pathReferencedParts[index])
            {
                index++;
            }
            for (int indexReferenced = index; indexReferenced < pathReferencedParts.Count; indexReferenced++)
            {
                stringBuilder.Append("/..");
            }
            for (int indexMain = index; indexMain < pathMainParts.Count; indexMain++)
            {
                stringBuilder.Append('/').Append(pathMainParts[indexMain]);
            }
            //if (pathMainAbsolute.EndsWith("/"))
            //{
            //    stringBuilder.Append("/");
            //}

            stringBuilder.Append('/').Append(filenameMain);

            //if (index >= pathOneParts.Count)
            //{
            //    while (index < pathOneParts.Count)
            //    {
            //        stringBuilder.Append("/..");
            //        index++;
            //    }
            //    if (pathOneAbsolute.EndsWith("/"))
            //    {
            //        stringBuilder.Append("/");
            //    }
            //    //return (pathTwoParts.Count - pathOneParts.Count + (pathTwoAbsolute.EndsWith("/") ? 0 /* directory */ : 1 /* file */)).Times("../");
            //}
            //else if (index >= pathTwoParts.Count)
            //{
            //    while (index < pathOneParts.Count)
            //    {
            //        stringBuilder.Append("/" + pathOneParts[index]);
            //        index++;
            //    }
            //    if (pathOneAbsolute.EndsWith("/"))
            //    {
            //        stringBuilder.Append("/");
            //    }
            //}
            //else
            //{
            //}

            //for (int i = 0; i < pathOneParts.Count && i < pathOneParts.Count; i++)

            //return DirectorySlashIt(Path.GetFullPath(param_path));

            return stringBuilder.ToString();
        }
        //public static void DirectoryCreate(string param_path)
        //{
        //    if (File.Exists(param_path))
        //    {
        //        return;
        //    }
        //    string directory = Path.GetDirectoryName((Path.GetFullPath(param_path)));
        //    DirectoryCreate(directory);
        //    File.Create(param_path);
        //}
        public static void FileCreate(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }

            if (File.Exists(path))
            {
                return;
            }
            string directory = Path.GetDirectoryName((Path.GetFullPath(path)));
            DirectoryCreate(directory);
            FileStream fileStream = File.Create(path);
			fileStream.Close();
        }
        public static void FileDelete(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }

            if (!File.Exists(path))
            {
                return;
            }
            File.SetAttributes(path, FileAttributes.Normal);
            File.Delete(path);
        }
        public static void FileOpen(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }

            if (!File.Exists(path))
            {
                return;
            }

            Process.Start(Path.GetFullPath(path));
        }
        public static void DirectoryShowInExplorer(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "Directory path cannot be null.");
	        }

            if (!Directory.Exists(path))
            {
                return;
            }

            Process.Start("explorer.exe", Path.GetFullPath(path));
        }
        public static void FileShowInExplorer(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }

            if (!File.Exists(path))
            {
                return;
            }

            Process.Start("explorer.exe", String.Format("/select,\"{0}\"", Path.GetFullPath(path)));
        }
        public static void FileCopy(string source, string destination, int attempts = 1)
        {
	        if (source == null)
	        {
		        throw new ArgumentNullException(nameof(source), "Source file path cannot be null.");
	        }
	        if (destination == null)
	        {
		        throw new ArgumentNullException(nameof(destination), "Destination file path cannot be null.");
	        }

            source = Path.GetFullPath(source);
            destination = Path.GetFullPath(destination);

            if (source == destination)
            {
                return;
            }

            DirectoryCreate(Path.GetDirectoryName(destination));
            if (File.Exists(destination))
            {
                File.Delete(destination);
            }

            // Make multiple attempts
            while (attempts-- > 0)
            {
                try
                {
                    // Try to copy
                    File.Copy(source, destination);
                    // Copy succeeded, done
                    break;
                }
                catch
                {
                    // Check if any attempts left
                    if (attempts == 0)
                    {
                        throw;
                    }
                    // Give the harddrive and os some time
                    Thread.Sleep(100);
                }
            }
        }
        public static void FileMove(string source, string destination)
        {
	        if (source == null)
	        {
		        throw new ArgumentNullException(nameof(source), "Source file path cannot be null.");
	        }
	        if (destination == null)
	        {
		        throw new ArgumentNullException(nameof(destination), "Destination file path cannot be null.");
	        }

            source = Path.GetFullPath(source);
            destination = Path.GetFullPath(destination);

            if (source == destination)
            {
                return;
            }

            DirectoryCreate(Path.GetDirectoryName(destination));
            if (File.Exists(destination))
            {
                File.Delete(destination);
            }
            File.Move(source, destination);
        }
        public static long FileSize(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }

            if (!File.Exists(path))
            {
                return 0;
            }
            FileInfo fileInfo = new FileInfo(path);
            long size = fileInfo.Length;
            return size;
        }
        public static string FileSizeFormat(long size, string[] units = null)
        {
	        if (size < 0)
	        {
		        throw new ArgumentException("File size cannot be negative.", nameof(size));
	        }

            units = units ?? new string[] { "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };
            if (size < (1 << 10))
            {
                return size + units[0];
            }
            else if (size < (1 << 20))
            {
                return (size / (1 << 10)) + units[1];
            }
            else if (size < (1 << 30))
            {
                return (size / (1 << 20)) + units[2];
            }
            else if (size < (1L << 40))
            {
                return (size / (1 << 30)) + units[3];
            }
            else if (size < (1L << 50))
            {
                return (size / (1L << 40)) + units[4];
            }
            else if (size < (1L << 60))
            {
                return (size / (1L << 50)) + units[5];
            }
			else
			{
				return (size / (1L << 60)) + units[6];
			}
		}
        public static bool FileIsBinary(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }

            if (!File.Exists(path))
            {
                throw new FileNotFoundException(path);
            }
            int length = 4096;
            char[] buffer = new char[length];
            int binaryScore = 0;
            char symbol;

            // Read first N characters
            StreamReader streamReader = new StreamReader(path, Encoding.UTF8);
            length = streamReader.ReadBlock(buffer, 0, length);
            streamReader.Close();
            streamReader.Dispose();

            // Check every character for being a control character
            for (int i = 0; i < length; i++)
            {
                symbol = buffer[i];
                if (symbol < 0x20 && symbol != 0x09 /* TAB */ && symbol != 0x0D /* CR */ && symbol != 0x0A /* LF */)
                {
                    binaryScore += 1;
                }
            }

            // 1 or more control symbol per 100 ordinary symbols means that file seems to be binary
            return binaryScore > length / 100;
        }
        public static bool FileIsEmpty(string path)
        {
	        if (path == null)
	        {
		        throw new ArgumentNullException(nameof(path), "File path cannot be null.");
	        }

            int length = 1;
            char[] buffer = new char[length];

            // Read first character
            StreamReader streamReader = new StreamReader(path, Encoding.UTF8);
            length = streamReader.ReadBlock(buffer, 0, length);
            streamReader.Close();
            streamReader.Dispose();

            return length == 1;
        }
        public static long PathGetFreeSpace(string path)
        {
            try
            {
                string letter = Path.GetPathRoot(path);
                return DriveInfo.GetDrives().Single(_driveInfo => _driveInfo.IsReady && _driveInfo.Name == letter).AvailableFreeSpace;
            }
            catch (Exception exception)
            {
                throw new Exception("Could not get free space for drive ", exception);
            }
        }
        //public static string BytesHashSha256Hex(byte[] param_bytes)
        //{
        //    return BytesToHex(FileHashSha256(param_bytes));
        //}
        //public static byte[] BytesHashSha256(byte[] param_bytes)
        //{
        //    // Use input string to calculate SHA256 hash
        //    using (System.Security.Cryptography.SHA256 sha256 = System.Security.Cryptography.SHA256.Create())
        //    {
        //        byte[] hash = sha256.ComputeHash(param_bytes);
        //        return hash;
        //    }
        //}
        //public static string FileHashMd5Hex(string param_path)
        //{
        //    return BytesToHex(FileHashMd5(param_path));
        //}
        //public static string FileHashSha1Hex(string param_path)
        //{
        //    if (!File.Exists(param_path))
        //    {
        //        return "";
        //    }
        //    // Use input string to calculate MD5 hash
        //    using (System.Security.Cryptography.SHA1 sha1 = System.Security.Cryptography.SHA1.Create())
        //    {
        //        byte[] bytes = File.ReadAllBytes(param_path);
        //        byte[] hash = sha1.ComputeHash(bytes);
        //        return BytesToHex(hash);
        //    }
        //}
        //public static string FileHashSha256Hex(string param_path)
        //{
        //    if (!File.Exists(param_path))
        //    {
        //        return "";
        //    }
        //    // Use input string to calculate MD5 hash
        //    using (System.Security.Cryptography.SHA256 sha2 = System.Security.Cryptography.SHA256.Create())
        //    {
        //        byte[] bytes = File.ReadAllBytes(param_path);
        //        byte[] hash = sha2.ComputeHash(bytes);
        //        return BytesToHex(hash);
        //    }
        //}
        //public static string FileSignature(string param_path)
        //{
        //    if (!File.Exists(param_path))
        //    {
        //        return "";
        //    }
        //    // Use input string to calculate MD5 hash
        //    using (System.Security.Cryptography.SHA256 sha2 = System.Security.Cryptography.SHA256.Create())
        //    {
        //        byte[] bytes = File.ReadAllBytes(param_path);
        //        byte[] hash = sha2.ComputeHash(bytes);
        //        return BytesToHex(hash);
        //    }
        //}
    }
}
