﻿using System;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        /// <summary>
        /// Check if two floats are equal. See <see cref="https://floating-point-gui.de/errors/comparison/"/>.
        /// </summary>
        /// <param name="one">First.</param>
        /// <param name="two">Second.</param>
        /// <param name="epsilon">Minimal difference.</param>
        /// <returns>If floats are equal within epsilon.</returns>
        public static bool Equals(float one, float two, float epsilon = Single.Epsilon)
        {
            // Shortcut, handles infinities
		    if (one == two)
            {
                return true;
            }
		    float diff = Math.Abs(one - two);

			// one or two is zero or both are extremely close to it, relative error is less meaningful here
		    if (one == 0 || two == 0 || diff < Single.MinValue)
            {
			    return diff < (epsilon * Single.MinValue);
		    }

            // Use relative error
		    float absA = Math.Abs(one);
            float absB = Math.Abs(two);
			return diff / Math.Min(absA + absB, Single.MaxValue) < epsilon;
	    }
        /// <summary>
        /// Check if two floats are equal.
        /// </summary>
        /// <param name="one">First.</param>
        /// <param name="two">Second.</param>
        /// <returns>If floats are bitwise equal.</returns>
        public static bool Equals(float one, float two)
        {
            return BitConverter.DoubleToInt64Bits(one) == BitConverter.DoubleToInt64Bits(two);
	    }
        /// <summary>
        /// Check if the looped value is inside the provided range.
        /// </summary>
        /// <param name="value">The value to check.</param>
        /// <param name="loopStart">The start of the loop. In most usecases it is 0.</param>
        /// <param name="loopEnd">The end of the loop.</param>
        /// <param name="rangeStart">The start of the range. If greater than the end of the range then the range is considered to be looped.</param>
        /// <param name="rangeEnd">The end of the range. If lesser than the start of the range then the range is considered to be looped.</param>
        /// <returns></returns>
        public static bool IsInRange(ref float value, float loopStart, float loopEnd, float rangeStart, float rangeEnd)
        {
            if (loopStart == loopEnd)
            {
                return true;
            }

            if (loopStart > loopEnd)
            {
                Swap(ref loopStart, ref loopEnd);
            }

            float loopLength = loopEnd - loopStart;

            float valueInLoop = Mathematics.Modulo(value - loopStart, loopLength) + loopStart;
            float rangeStartInLoop = Mathematics.Modulo(rangeStart - loopStart, loopLength) + loopStart;
            float rangeEndInLoop = Mathematics.Modulo(rangeEnd - loopStart, loopLength) + loopStart;

            if (rangeStartInLoop < rangeEndInLoop)
            {
                return rangeStartInLoop <= valueInLoop && valueInLoop <= rangeEndInLoop;
            }
            else
            {
                return valueInLoop <= rangeEndInLoop || rangeStartInLoop <= valueInLoop;
            }
        }

        /// <inheritdoc cref="Mathematics.Modulo(float, float)"/>
        public static float Modulo(ref float value, float divisor)
        {
            return Mathematics.Modulo(value, divisor);
        }
    }
}
