﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        /// <summary>
        /// Convert the hex representation of the color to the color itself.
        /// </summary>
        /// <param name="hex">Color in RRGGBB, AARRGGBB, #RRGGBB or #AARRGGBB format.</param>
        /// <returns>The corresponding color.</returns>
        public static Color ColorFromHex(string hex)
        {
            if (hex == null)
            {
                throw new ArgumentNullException(nameof(hex), "Color cannot be null.");
            }
            // Format RRGGBB
            if (hex.Length == 6)
            {
                string red   = hex.Substring(0, 2);
                string green = hex.Substring(2, 2);
                string blue  = hex.Substring(4, 2);

                return Color.FromArgb(255, Convert.ToInt32(red, 16), Convert.ToInt32(green, 16), Convert.ToInt32(blue, 16));
            }
            // Format #RRGGBB
            else if (hex.Length == 7)
            {
                if (hex[0] != '#')
                {
                    throw new Exception($"Color {hex} is invalid.");
                }
                string red   = hex.Substring(1, 2);
                string green = hex.Substring(3, 2);
                string blue  = hex.Substring(5, 2);

                return Color.FromArgb(255, Convert.ToInt32(red, 16), Convert.ToInt32(green, 16), Convert.ToInt32(blue, 16));
            }
            // Format AARRGGBB
            else if (hex.Length == 8)
            {
                string alpha   = hex.Substring(0, 2);
                string red   = hex.Substring(2, 2);
                string green = hex.Substring(4, 2);
                string blue  = hex.Substring(6, 2);

                return Color.FromArgb(Convert.ToInt32(alpha, 16), Convert.ToInt32(red, 16), Convert.ToInt32(green, 16), Convert.ToInt32(blue, 16));
            }
            // Format #AARRGGBB
            else if (hex.Length == 9)
            {
                if (hex[0] != '#')
                {
                    throw new Exception($"Color {hex} is invalid.");
                }
                string alpha   = hex.Substring(1, 2);
                string red   = hex.Substring(3, 2);
                string green = hex.Substring(5, 2);
                string blue  = hex.Substring(7, 2);

                return Color.FromArgb(Convert.ToInt32(alpha, 16), Convert.ToInt32(red, 16), Convert.ToInt32(green, 16), Convert.ToInt32(blue, 16));
            }

            // Unknown format
            throw new Exception($"Color {hex} is invalid.");
        }
        /// <summary>
        /// Produce a number of colors complementary to the specified one.
        /// </summary>
        /// <param name="hue">Hue in [0-360) degree range</param>
        /// <param name="saturation">Saturation in [0, 1] range</param>
        /// <param name="value">Value in [0, 1] range</param>
        /// <param name="count">Count of colors to produce.</param>
        /// <returns></returns>
        public static List<Color> ColorsFromHsvComplementary(float hue, float saturation, float value, int count)
        {
            return ColorsFromHsvComplementary(1, hue, saturation, value, count);
        }
        /// <summary>
        /// Produce a number of colors complementary to the specified one.
        /// </summary>
        /// <param name="alpha">Alpha in [0, 1] range</param>
        /// <param name="hue">Hue in [0-360) degree range</param>
        /// <param name="saturation">Saturation in [0, 1] range</param>
        /// <param name="value">Value in [0, 1] range</param>
        /// <param name="count">Count of colors to produce.</param>
        /// <returns></returns>
        public static List<Color> ColorsFromHsvComplementary(float alpha, float hue, float saturation, float value, int count)
        {
            List<Color> colors = new List<Color>();
            float hueComplementaryIncrement = 360f / count;
            float hueComplementary = hue;
            for (int index = 0; index < count; index++)
            {
                colors.Add(ColorFromHsv(alpha, hueComplementary, saturation, value));
                hueComplementary += hueComplementaryIncrement;
                if (hueComplementary >= 360)
                {
                    hueComplementary -= 360;
                }
            }
            return colors;
        }
        /// <summary>
        /// Create a RGB color from HSV one.
        /// </summary>
        /// <param name="hue">Hue in [0-360) degree range</param>
        /// <param name="saturation">Saturation in [0, 1] range</param>
        /// <param name="value">Value in [0, 1] range</param>
        public static Color ColorFromHsv(float hue, float saturation, float value)
        {
            return ColorFromHsv(1, hue, saturation, value);
        }
        /// <summary>
        /// Create an ARGB color from AHSV one.
        /// </summary>
        /// <param name="alpha">Alpha in [0, 1] range</param>
        /// <param name="hue">Hue in [0-360) degree range</param>
        /// <param name="saturation">Saturation in [0, 1] range</param>
        /// <param name="value">Value in [0, 1] range</param>
        /// <returns></returns>
        public static Color ColorFromHsv(float alpha, float hue, float saturation, float value)
        {
            if (alpha < 0)
            {
                throw new ArgumentException("Alpha is negative.", nameof(alpha));
            }
            if (alpha > 1)
            {
                throw new ArgumentException("Alpha is greater than 1.", nameof(alpha));
            }
            if (hue < 0)
            {
                throw new ArgumentException("Hue is negative.", nameof(hue));
            }
            if (hue >= 360)
            {
                throw new ArgumentException("Hue is greater or equal to 360.", nameof(hue));
            }
            if (saturation < 0)
            {
                throw new ArgumentException("Saturation is negative.", nameof(saturation));
            }
            if (saturation > 1)
            {
                throw new ArgumentException("Saturation is greater than 1.", nameof(saturation));
            }
            if (value < 0)
            {
                throw new ArgumentException("Value is negative.", nameof(value));
            }
            if (value > 1)
            {
                throw new ArgumentException("Value is greater than 1.", nameof(value));
            }

            float chroma = saturation * value;
            float hueSector = hue / 60;
            float x = chroma * (1 - Math.Abs(hueSector % 2 - 1)); // Intermediate value used for computing the RGB model
            float red = 0, green = 0, blue = 0;
            float smallestRgbComponent = value - chroma; // The RGB component with the smalles value

            // 6 sectors
            if (hueSector <= 1)
            {
                red = chroma;
                green = x;
            }
            else if (hueSector <= 2)
            {
                red = x;
                green = chroma;
            }
            else if (hueSector <= 3)
            {
                green = chroma;
                blue = x;
            }
            else if (hueSector <= 4)
            {
                green = x;
                blue = chroma;
            }
            else if (hueSector <= 5)
            {
                red = x;
                blue = chroma;
            }
            else
            {
                red = chroma;
                blue = x;
            }

            return Color.FromArgb(
                (int)(alpha              * 255),
                (int)((red   + smallestRgbComponent) * 255),
                (int)((green + smallestRgbComponent) * 255),
                (int)((blue  + smallestRgbComponent) * 255));
        }
    }
}
