﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        #if !UNITY_ENGINE
        private static Dictionary<object, Dictionary<string, Dictionary<Action, Delegate>>> ObjectToEventToActionToDelegate = new Dictionary<object, Dictionary<string, Dictionary<Action, Delegate>>>();
        public static void Subscribe(object @object, string eventName, Action action)
        {
            EventInfo eventInfo = @object.GetType().GetEvent(eventName);
            Subscribe(@object, eventInfo, action);
        }
        public static void Subscribe(object @object, EventInfo eventInfo, Action action)
        {
            // Prepare the event handler type
            Type eventHandlerType = eventInfo.EventHandlerType;
            MethodInfo eventHandlerInvokeMethod = eventHandlerType.GetMethod("Invoke");
            ParameterInfo[] eventHandlerInvokeMethodParameters = eventHandlerInvokeMethod.GetParameters();
            Type[] eventHandlerInvokeMethodParameterTypes = new Type[eventHandlerInvokeMethodParameters.Length];
            for (int i = 0; i < eventHandlerInvokeMethodParameters.Length; i++)
            {
                eventHandlerInvokeMethodParameterTypes[i] = eventHandlerInvokeMethodParameters[i].ParameterType.IsByRef
                    ? eventHandlerInvokeMethodParameters[i].ParameterType.GetElementType()
                    : eventHandlerInvokeMethodParameters[i].ParameterType;
            }
            string className = "Handler";
            string methodName = eventInfo.Name + "Handler";

            // Preapare to generate IL
            AssemblyName assemblyName = new AssemblyName();
            assemblyName.Name = "DynamicEventHandler";
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule(assemblyName.Name);
            TypeBuilder typeBuilder = moduleBuilder.DefineType(className, TypeAttributes.Class | TypeAttributes.Public);

            //DynamicMethod dynamicMethod = new DynamicMethod();
            //dynamicMethod.

            // Build a method with arguments appropriate for the event handler and make it call the action
            //MethodBuilder methodBuilder = typeBuilder.DefineMethod(methodName, MethodAttributes.Public | MethodAttributes.Static, eventHandlerInvokeMethod.ReturnType, eventHandlerInvokeMethodParameterTypes);
            //ILGenerator il = methodBuilder.GetILGenerator();
            //LocalBuilder localBuilderAction = il.DeclareLocal(typeof(Action));

            //il.Emit(OpCodes.Ldarg, action.Method);
            //il.Emit(OpCodes.Callvirt, );
            ////il.Emit(OpCodes.Ldnull);
            //il.Emit(OpCodes.Nop);
            //il.Emit(OpCodes.Ret);

            //var mi = typeof(Action).GetMethod("Invoke");
            //il.Emit(OpCodes.Ldarg_3);
            //il.Emit(OpCodes.st);
            //il.Emit(OpCodes.Callvirt, mi);
            ////il.Emit(OpCodes.Ldnull);
            //il.Emit(OpCodes.Ret);

            //il.Emit(OpCodes.Call, typeof(Util).GetMethod("Nya"));
            //il.Emit(OpCodes.Ret);

            Type type = typeBuilder.CreateType();
            MethodInfo eventHandler = type.GetMethod(methodName);

            // Create the event handler and subscribe
            Delegate @delegate = Delegate.CreateDelegate(eventHandlerType, eventHandler);
            eventInfo.AddEventHandler(@object, @delegate);
        }
        public static void Unsubscribe(object @object, string eventName, Action action)
        {
            EventInfo eventInfo = @object.GetType().GetEvent(eventName);
            Unsubscribe(@object, eventInfo, action);
        }
        public static void Unsubscribe(object @object, EventInfo eventInfo, Action action)
        {
            if (!ObjectToEventToActionToDelegate.TryGetValue(@object, out Dictionary<string, Dictionary<Action, Delegate>> eventToActionToDelegate))
            {
                return;
            }
            if (!eventToActionToDelegate.TryGetValue(eventInfo.Name, out Dictionary<Action, Delegate> actionToDelegate))
            {
                return;
            }
            if (!actionToDelegate.TryGetValue(action, out Delegate @delegate))
            {
                return;
            }

            // Remove event handler
            eventInfo.RemoveEventHandler(@object, @delegate);

            actionToDelegate.Remove(action);
            if (actionToDelegate.Count == 0)
            {
                eventToActionToDelegate.Remove(eventInfo.Name);
            }
            if (eventToActionToDelegate.Count == 0)
            {
                ObjectToEventToActionToDelegate.Remove(@object);
            }
        }
        #endif
    }
}
