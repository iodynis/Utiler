﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Security.Permissions;
using System.Security.Principal;
using Iodynis.Libraries.Utility.Extensions;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        public static void Swap<T>(ref T one, ref T two)
        {
            T temp = one;
            one = two;
            two = temp;
        }
        public static void AddEventHandler(object @object, string eventName, Delegate eventHandler)
        {
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }
            if (eventName == null)
            {
                throw new ArgumentNullException(nameof(eventName));
            }
            if (eventHandler == null)
            {
                throw new ArgumentNullException(nameof(eventHandler));
            }

            Type type = @object.GetType();
            EventInfo eventInfo = type.GetEvent(eventName);
            if (eventInfo == null)
            {
                throw new ArgumentException( string.Format("Event '{0}' is not defined by type '{1}'", eventName, type.Name));
            }

            eventInfo.AddEventHandler(@object, eventHandler);
        }
	    /// <summary>
	    /// Read configuration file as a list of pairs "variable value" and "variable array"
	    /// </summary>
	    /// <param name="path">Path to the file to read</param>
	    /// <param name="values">Dictionary of values</param>
	    /// <param name="arrays">Dictionary of arrays</param>
	    /// <exception cref="ArgumentNullException"></exception>
	    public static void FileReadConfiguration(string path, Dictionary<string, string> values, Dictionary<string, List<string>> arrays)
        {
            if (values == null)
            {
	            throw new ArgumentNullException(nameof(values), "Values dictionary cannot be null.");
            }
            if (arrays == null)
            {
	            throw new ArgumentNullException(nameof(arrays), "Array dictionary cannot be null.");
            }

            // Read the file to memory
            string pathFull = Path.GetFullPath(path);
            if (!File.Exists(pathFull))
            {
                throw new FileNotFoundException("File " + pathFull + " does not exist.");
            }
            string text = FileReadString(pathFull) + "\r\n" /* A new line at the end of the file for convenient parsing of the last line */;

            // Check if ⌠ and ⌡ are balanced
            {
                int countOpen = text.Count('⌠');
                int countClose = text.Count('⌡');
                if (countOpen != countClose)
                {
                    throw new Exception("Symbols ⌠ and ⌡ are unbalanced, there are " + countOpen + " of opening ⌠ and " + countClose + " of closing ⌡. Please make those amounts equal by adding or removing some of the symbols.");
                }
            }

            // Prepare to read symbol by symbol
            List<List<string>> lines = new List<List<string>>();

            StringBuilder buffer = new StringBuilder();
            int depth = 0;
            // Human-readable parameters
            int humanLine = 1;
            int humanPosition = 1;
            //int indexPreviousDoubleQuote = -1;
            bool isInsideDoubleQuotes = false;
            bool isInsideComment = false;
            bool isInsideValue = false;
            //bool isNewLine = true;
            bool doFlush = false;

            // Read symbol-wise
            for (int index = 0; index < text.Length; index++)
            {
                // Count lines
                if (text[index] == '\n')
                {
                    humanLine++;
                    humanPosition = 1;
                }
                // Count characters
                else
                {
                    humanPosition++;
                }
                if (doFlush)
                {
                    if (buffer.Length != 0)
                    {
                        lines.Last().Add(buffer.ToString());
                        buffer.Clear();
                    }
                    doFlush = false;
                }
                if (isInsideComment)
                {
                    if (text[index] == '\r' || text[index] == '\n')
                    {
                        isInsideComment = false;
                    }
                    continue;
                }
                if (text[index] == '⌠')
                {
                    buffer.Append(text[index]);
                    depth++;
                    continue;
                }
                if (text[index] == '⌡')
                {
                    buffer.Append(text[index]);
                    depth--;
                    continue;
                }
                if (depth != 0)
                {
                    buffer.Append(text[index]);
                    continue;
                }

                // Here we get only if not in comment nor in replacer

                // Comment starts
                if (text[index] == '#' && !isInsideDoubleQuotes)
                {
                    // Value ends
                    if (isInsideValue)
                    {
                        isInsideValue = false;
                        doFlush = true;
                    }

                    isInsideComment = true;
                    //continue;
                }
                else if (text[index] == '"')
                {
                    // Value cannot contain double quotes (should wrap it in double quotes to do that)
                    if (isInsideValue)
                    {
                        throw new Exception("Incorrect syntax. A double quote at line " + humanLine + " and position at " + humanPosition + " is inside a value (or next to it). Please, wrap the value in double quotes. Don't forget to escape the double quotes that are inside the value by doubling them (substitute \" with \"\").");
                    }
                    // Double-quoted string starts
                    if (!isInsideDoubleQuotes)
                    {
                        isInsideDoubleQuotes = true;
                        //continue;
                    }
                    else
                    {
                        // Escaped double quote
                        if (index + 1 < text.Length && text[index + 1] == '"')
                        {
                            buffer.Append('"');
                            index++;
                            humanPosition++;
                            //continue;
                        }
                        // Double-quoted string ends
                        else
                        {
                            doFlush = true;
                            isInsideDoubleQuotes = false;
                            //continue;
                        }
                    }
                }
                // White space delimiter
                else if (!isInsideDoubleQuotes && (text[index] == ' ' || text[index] == '\t' || text[index] == '\r' || text[index] == '\n'))
                {
                    isInsideValue = false;
                    doFlush = true;
                    //continue;
                }
                // Any other symbol
                else
                {
                    if (isInsideDoubleQuotes)
                    {
                        buffer.Append(text[index]);
                    }
                    else
                    {
                        if (index == 0 || text[index - 1] == '\r' || text[index - 1] == '\n')
                        {
                            lines.Add(new List<string>());
                        }
                        if (!isInsideValue)
                        {
                            isInsideValue = true;
                            // Key-value starts
                            //if (index == 0 || (text[index - 1] != ' ' && text[index - 1] != '\t'))
                            buffer.Append(text[index]);
                        }
                        else
                        {
                            buffer.Append(text[index]);
                        }
                    }
                }
            }
            foreach (List<string> line in lines)
            {
                if (line.Count == 0)
                {
                    continue;
                }
                if (line.Count == 1)
                {
                    if (!values.ContainsKey(line[0]))
                    {
                        values.Add(line[0], "");
                    }
                    else
                    {
                        values[line[0]] = "";
                    }
                }
                else if (line.Count == 2)
                {
                    if (!values.ContainsKey(line[0]))
                    {
                        values.Add(line[0], line[1]);
                    }
                    else
                    {
                        values[line[0]] = line[1];
                    }
                }
                else
                {
                    if (!arrays.ContainsKey(line[0]))
                    {
                        arrays.Add(line[0], line.Slice(1));
                    }
                    else
                    {
                        arrays[line[0]] = line.Slice(1);
                    }
                }
            }
                //    if (text[i] != ' ' && text[i] != '\t')
                //    {
                //        if (isInsideComment)
                //        {
                //            isInsideComment = false;
                //        }
                //        continue;
                //    }

                //    // If any other symbol
                //    {
                //        if (isNewLine)
                //        {

                //        }
                //    }


                //    {
                //        string line = stringBuilder.ToString().Trim(Util.SpaceCharacters);
                //        stringBuilder.Clear();

                //        if (line.Length == 0)
                //        {
                //            continue;
                //        }

                //        string[] parts = line.Split(new char[]{ ' ', '\t' }, 2);

                //        string key = parts[0].Trim(Util.SpaceCharacters);
                //        string value = parts.Length > 1 ? parts[1].Trim(Util.SpaceCharacters) : "";

                //        if (dictionary.ContainsKey(key))
                //        {
                //            dictionary[key] = value;
                //        }
                //        else
                //        {
                //            dictionary.Add(key, value);
                //        }
                //        isNewLine = true;
                //    }
                //    else
                //    {
                //        stringBuilder.Append(text[i]);
                //    }
                //}
                //// Check that ⌠ and ⌡ are balanced
                //if (depth != 0)
                //{
                //    throw new Exception("⌠ and ⌡ are unbalanced.");
                //}

                //return dictionary;
            //}
        }

	    /// <summary>
	    /// Read configuration file as a list of pairs "variable value"
	    /// </summary>
	    /// <param name="path">Path to the file to read</param>
	    /// <exception cref="FileNotFoundException"></exception>
	    /// <returns>Dictionary of values</returns>
	    public static Dictionary<string, string> FileReadConfiguration(string path)
        {
            string pathFull = Path.GetFullPath(path);
            if (!File.Exists(pathFull))
            {
                throw new FileNotFoundException($"File {pathFull} does not exist.");
            }

            string text = FileReadString(pathFull) + "\r\n" /* HACK: Add a new line so the last line one will be parsed no matter what */;

            StringBuilder stringBuilder = new StringBuilder();
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            int depth = 0;
            bool isInsideDoubleQuotes = false;
            bool isInsideComment = false;

            for (int i = 0; i < text.Length; i++)
            {
                if (isInsideComment && text[i] != '\r' && text[i] != '\n')
                {
                    ;
                }
                else if (text[i] == '⌠')
                {
                    stringBuilder.Append(text[i]);
                    depth++;
                }
                else if (text[i] == '⌡')
                {
                    stringBuilder.Append(text[i]);
                    depth--;
                }
                else if (depth != 0)
                {
                    stringBuilder.Append(text[i]);
                }
                else if (isInsideDoubleQuotes)
                {
                    if (text[i] == '"')
                    {
                        // "" sequence inside double quote block
                        if (i + 1 < text.Length && text[i + 1] == '"')
                        {
                            stringBuilder.Append(text[i]);
                            i++;
                        }
                        // Closing double quote
                        else
                        {
                            isInsideDoubleQuotes = false;
                        }
                    }
                    else
                    {
                        stringBuilder.Append(text[i]);
                    }
                }
                // Opening double quote
                else if (text[i] == '"')
                {
                    isInsideDoubleQuotes = true;
                }
                // Comment line
                else if (text[i] == '#' && stringBuilder.ToString().Trim(SpaceCharacters).Length == 0)
                {
                    isInsideComment = true;
                }
                else if (text[i] == '\r' || text[i] == '\n')
                {
                    if (isInsideComment)
                    {
                        isInsideComment = false;
                        stringBuilder.Clear();
                        continue;
                    }

                    string line = stringBuilder.ToString().Trim(SpaceCharacters);
                    stringBuilder.Clear();

                    if (line.Length == 0)
                    {
                        continue;
                    }

                    string[] parts = line.Split(new char[]{ ' ', '\t' }, 2);

                    string key = parts[0].Trim(Utiler.SpaceCharacters);
                    string value = parts.Length > 1 ? parts[1].Trim(SpaceCharacters) : "";

                    if (dictionary.ContainsKey(key))
                    {
                        dictionary[key] = value;
                    }
                    else
                    {
                        dictionary.Add(key, value);
                    }
                }
                else
                {
                    stringBuilder.Append(text[i]);
                }
            }
            // Check that ⌠ and ⌡ are balanced
            if (depth != 0)
            {
                throw new Exception("⌠ and ⌡ are unbalanced.");
            }

            return dictionary;
        }
        //public static string ToCamelCase(string param_string)
        //{
        //    if (String.IsNullOrEmpty(param_string))
        //    {
        //        return param_string;
        //    }
        //    StringBuilder stringBuilder = new StringBuilder();
        //    //bool previousCharWasInUpperCase = true;
        //    bool convertToLower = true;
        //    stringBuilder.Append(Char.ToUpperInvariant(param_string[0]));
        //    for (int i = 1; i < param_string.Length; i++)
        //    {
        //        if (Char.IsLetter(param_string[i]) && Char.IsUpper(param_string[i]))
        //        {
        //            if (convertToLower)
        //            {
        //                if (i + 1 < param_string.Length && Char.IsLetter(param_string[i + 1]) && Char.IsLower(param_string[i + 1]))
        //                {
        //                    stringBuilder.Append(param_string[i]);
        //                }
        //                else
        //                {
        //                    stringBuilder.Append(Char.ToLower(param_string[i]));
        //                }
        //            }
        //            else
        //            {
        //                stringBuilder.Append(param_string[i]);
        //                convertToLower = true;
        //            }
        //        }
        //        else
        //        {
        //            stringBuilder.Append(param_string[i]);
        //            convertToLower = false;
        //        }
        //    }
        //    //for (int i = 1; i < param_string.Length; i++)
        //    //{
        //    //    if (i != 1 && Char.IsLetter(param_string[i]) && Char.IsUpper(param_string[i]))
        //    //    {
        //    //        stringBuilder.Append(Char.ToLower(param_string[i - 1]));
        //    //    }
        //    //    else
        //    //    {
        //    //        stringBuilder.Append(param_string[i - 1]);
        //    //    }
        //    //}
        //    //stringBuilder.Append(param_string[param_string.Length - 1]);
        //    return stringBuilder.ToString();
        //}
        public static void ProcessKill(string name)
        {
            foreach (Process process in Process.GetProcessesByName(name))
            {
			    try
			    {
				    process.Kill();
			    }
			    catch (Exception)
			    {
				    ;
			    }
            }
        }
        //public static bool IsInDesignMode()
        //{
        //    return Process.GetCurrentProcess().ProcessName == "devenv";
        //}
        //public static bool MergeIEnumerables<T1, T2>(T1 param_one, T2 param_two)
        //{
        //    ((IEnumerable<T1>)param_one).
        //}

        #if !UNITY_ENGINE
        public static bool IsRunningAsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
        public static void RestartAsAdministrator()
        {
            try
            {
                Process processCurrent = Process.GetCurrentProcess();
                Process process = new Process();
                process.StartInfo = processCurrent.StartInfo;
                process.StartInfo.Arguments = Environment.CommandLine.Substring(Environment.GetCommandLineArgs()[0].Length + 3 /* Cut the first argument (the program name) and 3 characters: the opening quote, the closing quote and the space */);
                //String.Join(" ", Environment
                //    .GetCommandLineArgs()
                //    .Skip(1) /* Skip the "C:/.../Program.exe" executable path as it comes first and is not the argument we are looking for */
                //    .Select(_argument => _argument.ContainsAny(new char[] { ' ', '\t', '\r', '\n' }) ? "\"" + _argument + "\"" : _argument) /* Enquote arguments that have space characters */
                //    );
                process.StartInfo.FileName = processCurrent.MainModule.FileName;
                process.StartInfo.WorkingDirectory = Path.GetDirectoryName(processCurrent.MainModule.FileName);

                // Elevate privileges
                process.StartInfo.UseShellExecute = true; /* Required for UAC to work */
                process.StartInfo.Verb = "runas";

                process.Start();
            }
            catch (Win32Exception exception) when (exception.NativeErrorCode == 1223 /* Cancelled */)
            {
                return;
            }
            catch (Exception exception)
            {
                throw new Exception($"Failed to restart process as administrator.", exception);
            }
        }
        #endif

        // TODO: Swap lists. The first one should be immutable
        // Bring first list to have same items as the second one
        public static bool Enlist<T>(IList<T> one, IList<T> two)
        {
            bool changesDone = false;
            for (int i = 0; i < one.Count; i++)
            {
                if (!two.Contains(one[i]))
                {
                    one.RemoveAt(i--);
                    changesDone = true;
                }
            }
            for (int i = 0; i < two.Count; i++)
            {
                if (!one.Contains(one[i]))
                {
                    one.Add(one[i]);
                    changesDone = true;
                }
            }
            return changesDone;

            //for (int i = 0; i < players.Count; i++)
            //{
            //    if (!listBoxPlayersOnline.Items.Contains(players[i]) && players[i].IsOnline.HasValue && players[i].IsOnline.Value)
            //    {
            //        listBoxPlayersOnline.Items.Add(players[i]);
            //    }
            //}
            //for (int i = 0; i < listBoxPlayersOnline.Items.Count; i++)
            //{
            //    if (!players.Contains(listBoxPlayersOnline.Items[i] as Player) || !players[i].IsOnline.HasValue || !players[i].IsOnline.Value)
            //    {
            //        listBoxPlayersOnline.Items.RemoveAt(i--);
            //    }
            //}
        }
    }
}
