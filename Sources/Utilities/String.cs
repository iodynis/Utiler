﻿using System;
using System.Linq;
using System.Text;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        public static readonly char[] SpaceCharacters = { ' ', '\t', '\r', '\n' };
		/// <summary>
		/// Converts an UTF8 string to a hex string.
		/// </summary>
		/// <param name="utf8">String in UTF8.</param>
		/// <returns>Hex string.</returns>
        public static string Utf8ToHex(string utf8)
        {
			if (utf8 == null)
			{
				throw new ArgumentNullException(nameof(utf8), "String cannot be null.");
			}
            return BytesToHex(Encoding.UTF8.GetBytes(utf8));
        }
		/// <summary>
		/// Converts a hex string to an UTF8 string.
		/// </summary>
		/// <param name="hex">Hax string.</param>
		/// <returns>UTF8 string.</returns>
        public static string HexToUtf8(string hex)
        {
	        if (hex == null)
	        {
		        throw new ArgumentNullException(nameof(hex), "String cannot be null.");
	        }
            return Encoding.UTF8.GetString(HexToBytes(hex));
        }
		/// <summary>
		/// Converts a bytes array to a hex representation string.
		/// </summary>
		/// <param name="bytes">Byte array.</param>
		/// <returns>Hex string.</returns>
        public static string BytesToHex(byte[] bytes)
        {
	        if (bytes == null)
	        {
		        throw new ArgumentNullException(nameof(bytes), "Array cannot be null.");
	        }
            // Convert the byte array to hexadecimal string
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                stringBuilder.Append(bytes[i].ToString("X2"));
            }
            return stringBuilder.ToString();
        }
		/// <summary>
		/// Converts a bytes array to a hex representation string.
		/// </summary>
		/// <param name="bytes">Byte array.</param>
		/// <returns>Hex string.</returns>
        public static string ByteToHex(byte @byte)
        {
            return @byte.ToString("X2");
        }
        /// <summary>
        /// Converts a hex string to a bytes array.
        /// </summary>
        /// <param name="hex">Hex string.</param>
        /// <returns>Bytes array.</returns>
        public static byte[] HexToBytes(string hex)
        {
            if (hex == null)
            {
                throw new ArgumentNullException(nameof(hex), "String cannot be null.");
            }
            return Enumerable.Range(0, hex.Length / 2).Select(_index => Convert.ToByte(hex.Substring(_index * 2, 2), 16)).ToArray();
        }
		/// <summary>
		/// Converts a bytes array to a base64 representation string.
		/// </summary>
		/// <param name="bytes">Byte array.</param>
		/// <returns>Base64 string.</returns>
        public static string BytesToBase64(byte[] bytes)
        {
	        if (bytes == null)
	        {
		        throw new ArgumentNullException(nameof(bytes), "Array cannot be null.");
	        }
            return Convert.ToBase64String(bytes);
        }
        public static string GetString(byte[] bytes)
        {
            if (bytes == null)
            {
                throw new ArgumentNullException(nameof(bytes), "Byte array cannot be null.");
            }
            if (bytes.Length == 0)
            {
                return "";
            }
            char[] chars = new char[bytes.Length / sizeof(char)];
            Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
