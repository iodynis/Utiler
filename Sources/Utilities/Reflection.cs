﻿using System;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        [MethodImplAttribute(MethodImplOptions.NoInlining)]
        public static bool IsCallerInstanceOf(object @object)
        {
            StackFrame stackFrame = new StackFrame(2);
            MethodBase callerMethod = stackFrame.GetMethod();
            Type callingClass = callerMethod.DeclaringType;
            return callingClass == @object.GetType();
        }
        [MethodImplAttribute(MethodImplOptions.NoInlining)]
        public static bool IsCallerInstanceOf(Type type)
        {
            StackFrame stackFrame = new StackFrame(2);
            MethodBase callerMethod = stackFrame.GetMethod();
            Type callingClass = callerMethod.DeclaringType;
            return callingClass == type;
        }
        /// <summary>
        /// Compiles a property getter.
        /// </summary>
        /// <typeparam name="TObject">Object type.</typeparam>
        /// <typeparam name="TProperty">Property type.</typeparam>
        /// <param name="propertyName">Property name.</param>
        /// <returns>Property getter.</returns>
        public static Func<TObject, TProperty> GetPropertyGetter<TObject, TProperty>(string propertyName)
        {
            ParameterExpression parameterObject = Expression.Parameter(typeof(TObject), "value");
            Expression expression = Expression.Property(parameterObject, propertyName);
            Func<TObject, TProperty> getter = Expression.Lambda<Func<TObject, TProperty>>(expression, parameterObject).Compile();

            return getter;
        }
        /// <summary>
        /// Compiles a property setter.
        /// </summary>
        /// <typeparam name="TObject">Object type.</typeparam>
        /// <typeparam name="TProperty">Property type.</typeparam>
        /// <param name="propertyName">Property name.</param>
        /// <returns>Property setter.</returns>
        public static Action<TObject, TProperty> GetPropertySetter<TObject, TProperty>(string propertyName)
        {
            ParameterExpression parameterObject = Expression.Parameter(typeof(TObject));
            ParameterExpression parameterPropertyValue = Expression.Parameter(typeof(TProperty), propertyName);
            MemberExpression expression = Expression.Property(parameterObject, propertyName);
            Action<TObject, TProperty> setter = Expression.Lambda<Action<TObject, TProperty>>
            (
                Expression.Assign(expression, parameterPropertyValue), parameterObject, parameterPropertyValue
            ).Compile();

            return setter;
        }
        public static MethodInfo GetMethod<T>(Expression<Action<T>> lambda)
        {
            MethodCallExpression call = lambda.Body as MethodCallExpression;
            if (call == null)
            {
                throw new ArgumentException($"Expression body was expected to be a method call, but was '{lambda.Body}' ({call.GetType()})");
            }
            return call.Method;
        }
    }
}
