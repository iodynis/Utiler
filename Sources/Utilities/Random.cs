﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Iodynis.Libraries.Utility
{
    public static partial class Utiler
    {
        private static Random RandomDefault = new Random((int)DateTime.Now.Ticks);
        //private static Random RandomPersistent = new Random((int)DateTime.Now.Ticks);

        /// <summary>
        /// Get a random integer in the interval of [<paramref name="min"/>, <paramref name="max"/>].
        /// </summary>
        /// <param name="min">Lower interval bound.</param>
        /// <param name="max">Higher interval bound.</param>
        /// <returns></returns>
        public static int RandomInteger(int min, int max)
        {
            int sureMin = min < max ? min : max;
            int sureMax = min < max ? max : min;
            return RandomDefault.Next(sureMin, sureMax == int.MaxValue ? int.MaxValue : sureMax + 1);
        }
        /// <summary>
        /// Get a random integer in the interval of [0, <paramref name="max"/>].
        /// </summary>
        /// <param name="max">Higher interval bound.</param>
        /// <returns></returns>
        public static int RandomInteger(int max)
        {
            return RandomDefault.Next(0, max == int.MaxValue ? int.MaxValue : max + 1);
        }
        /// <summary>
        /// Get a random float in the interval of [<paramref name="min"/>, <paramref name="max"/>).
        /// </summary>
        /// <param name="min">Lower interval bound.</param>
        /// <param name="max">Higher interval bound.</param>
        /// <returns></returns>
        public static float RandomFloat(float min, float max)
        {
            float sureMin = min < max ? min : max;
            float sureMax = min < max ? max : min;
            return sureMin + (sureMax - sureMin) * (float)RandomDefault.NextDouble();
        }
        /// <summary>
        /// Get a random float in the interval of [0, <paramref name="max"/>].
        /// </summary>
        /// <param name="max">Higher interval bound.</param>
        /// <returns></returns>
        public static float RandomFloat(float max)
        {
            return max * (float)RandomDefault.NextDouble();
        }
        /// <summary>
        /// Get a random float in the interval of [0, 1].
        /// </summary>
        /// <returns></returns>
        public static float RandomFloat()
        {
            return (float)RandomDefault.NextDouble();
        }
        /// <summary>
        /// Get a random double in the interval of [<paramref name="min"/>, <paramref name="max"/>].
        /// </summary>
        /// <param name="min">Lower interval bound.</param>
        /// <param name="max">Higher interval bound.</param>
        /// <returns></returns>
        public static double RandomDouble(double min, double max)
        {
            double sureMin = min < max ? min : max;
            double sureMax = min < max ? max : min;
            return sureMin + (sureMax - sureMin) * RandomDefault.NextDouble();
        }
        /// <summary>
        /// Get a random double in the interval of [0, <paramref name="max"/>].
        /// </summary>
        /// <param name="max">Higher interval bound.</param>
        /// <returns></returns>
        public static double RandomDouble(double max)
        {
            return max * RandomDefault.NextDouble();
        }
        /// <summary>
        /// Get a random double in the interval of [0, 1].
        /// </summary>
        /// <returns></returns>
        public static double RandomDouble()
        {
            return RandomDefault.NextDouble();
        }
        /// <summary>
        /// Get a random value from the provided array of values.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="values">Array of values.</param>
        /// <returns></returns>
        public static T Random<T>(params T[] values)
        {
            int index = RandomInteger(0, values.Length - 1);
            return values[index];
        }
    }
}
