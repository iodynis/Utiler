﻿#if !UNITY_ENGINE

using System.Drawing;
using System.IO;
using Iodynis.Libraries.Utility.Extensions;

namespace Iodynis.Libraries.Utility
{
    public class Bitmap9
    {
        public Bitmap BitmapTopLeft;
        public Bitmap BitmapTop;
        public Bitmap BitmapTopRight;
        public Bitmap BitmapRight;
        public Bitmap BitmapBottomRight;
        public Bitmap BitmapBottom;
        public Bitmap BitmapBottomLeft;
        public Bitmap BitmapLeft;
        public Bitmap BitmapCenter;
        public Rectangle TextRectangle;

        private Rectangle BitmapTopRectangle;
        private Rectangle BitmapRightRectangle;
        private Rectangle BitmapBottomRectangle;
        private Rectangle BitmapLeftRectangle;
        private Rectangle BitmapCenterRectangle;

        public int WidthLeft    { get; private set; }
        public int WidthCenter  { get; private set; }
        public int WidthRight   { get; private set; }
        public int HeightTop    { get; private set; }
        public int HeightCenter { get; private set; }
        public int HeightBottom { get; private set; }
        private int WidthLeftAndRight  { get; set; }
        private int HeightTopAndBottom { get; set; }

        public Bitmap9(Bitmap bitmap)
        {
            // Top black bar
            int topStart = -1;
            int topEnd = -1;
            for (int index = 0; index < bitmap.Width; index++)
            {
                if (bitmap.GetPixel(index, 0).EqualsInArgb(Color.Black))
                {
                    if (topStart < 0 && topEnd < 0)
                    {
                        topStart = index;
                    }
                }
                else
                {
                    if (topStart >= 0 && topEnd < 0)
                    {
                        topEnd = index;
                    }
                }
            }
            if (topStart < 0)
            {
                topStart = 0;
            }
            if (topEnd < 0)
            {
                topEnd = bitmap.Width;
            }

            // Bottom black bar
            int bottomStart = -1;
            int bottomEnd = -1;
            for (int index = 0; index < bitmap.Width; index++)
            {
                if (bitmap.GetPixel(index, bitmap.Height - 1).EqualsInArgb(Color.Black))
                {
                    if (bottomStart < 0 && bottomEnd < 0)
                    {
                        bottomStart = index;
                    }
                }
                else
                {
                    if (bottomStart >= 0 && bottomEnd < 0)
                    {
                        bottomEnd = index;
                    }
                }
            }
            if (bottomStart < 0)
            {
                bottomStart = 0;
            }
            if (bottomEnd < 0)
            {
                bottomEnd = bitmap.Width;
            }

            // Left black bar
            int leftStart = -1;
            int leftEnd = -1;
            for (int index = 0; index < bitmap.Height; index++)
            {
                if (bitmap.GetPixel(0, index).EqualsInArgb(Color.Black))
                {
                    if (leftStart < 0 && leftEnd < 0)
                    {
                        leftStart = index;
                    }
                }
                else
                {
                    if (leftStart >= 0 && leftEnd < 0)
                    {
                        leftEnd = index;
                    }
                }
            }
            if (leftStart < 0)
            {
                leftStart = 0;
            }
            if (leftEnd < 0)
            {
                leftEnd = bitmap.Height;
            }

            // Right black bar
            int rightStart = -1;
            int rightEnd = -1;
            for (int index = 0; index < bitmap.Height; index++)
            {
                if (bitmap.GetPixel(bitmap.Width - 1, index).EqualsInArgb(Color.Black))
                {
                    if (rightStart < 0 && rightEnd < 0)
                    {
                        rightStart = index;
                    }
                }
                else
                {
                    if (rightStart >= 0 && rightEnd < 0)
                    {
                        rightEnd = index;
                    }
                }
            }
            if (rightStart < 0)
            {
                rightStart = 0;
            }
            if (rightEnd < 0)
            {
                rightEnd = bitmap.Height;
            }

            WidthLeft    = topStart - 1 /* Black bars */;
            WidthCenter  = topEnd - topStart;
            WidthRight   = bitmap.Width - topEnd - 1 /* Black bars */;
            HeightTop    = leftStart - 1 /* Black bars */;
            HeightCenter = leftEnd - leftStart;
            HeightBottom = bitmap.Height - leftEnd - 1 /* Black bars */;

            WidthLeftAndRight = WidthLeft + WidthRight;
            HeightTopAndBottom = HeightTop + HeightBottom;

            int xLeft   = 1;
            int xCenter = topStart;
            int xRight  = topEnd;
            int yTop    = 1;
            int yCenter = leftStart;
            int yBottom = leftEnd;

            BitmapTopLeft     = bitmap.Clone(new Rectangle(xLeft,   yTop,    WidthLeft,   HeightTop),    bitmap.PixelFormat);
            BitmapTopRight    = bitmap.Clone(new Rectangle(xRight,  yTop,    WidthRight,  HeightTop),    bitmap.PixelFormat);
            BitmapBottomRight = bitmap.Clone(new Rectangle(xRight,  yBottom, WidthRight,  HeightBottom), bitmap.PixelFormat);
            BitmapBottomLeft  = bitmap.Clone(new Rectangle(xLeft,   yBottom, WidthLeft,   HeightBottom), bitmap.PixelFormat);
            BitmapTop         = Enborder(bitmap.Clone(new Rectangle(xCenter, yTop,    WidthCenter, HeightTop),    bitmap.PixelFormat));
            BitmapRight       = Enborder(bitmap.Clone(new Rectangle(xRight,  yCenter, WidthRight,  HeightCenter), bitmap.PixelFormat));
            BitmapBottom      = Enborder(bitmap.Clone(new Rectangle(xCenter, yBottom, WidthCenter, HeightBottom), bitmap.PixelFormat));
            BitmapLeft        = Enborder(bitmap.Clone(new Rectangle(xLeft,   yCenter, WidthLeft,   HeightCenter), bitmap.PixelFormat));
            BitmapCenter      = Enborder(bitmap.Clone(new Rectangle(xCenter, yCenter, WidthCenter, HeightCenter), bitmap.PixelFormat));

            BitmapTopRectangle    = new Rectangle(1, 1, BitmapTop.Width    - 2, BitmapTop.Height    - 2);
            BitmapRightRectangle  = new Rectangle(1, 1, BitmapRight.Width  - 2, BitmapRight.Height  - 2);
            BitmapBottomRectangle = new Rectangle(1, 1, BitmapBottom.Width - 2, BitmapBottom.Height - 2);
            BitmapLeftRectangle   = new Rectangle(1, 1, BitmapLeft.Width   - 2, BitmapLeft.Height   - 2);
            BitmapCenterRectangle = new Rectangle(1, 1, BitmapCenter.Width - 2, BitmapCenter.Height - 2);

            TextRectangle = new Rectangle(bottomStart, rightStart, bottomEnd - bottomStart, rightEnd - rightStart);
        }
        public void Draw(Graphics graphics, int x, int y, int width, int height)
        {
            Draw(graphics, new Rectangle(x, y, width, height));
        }
        public void Draw(Graphics graphics, Rectangle rectangle)
        {
            int centerWidth  = rectangle.Width  - WidthLeftAndRight;
            int centerHeight = rectangle.Height - HeightTopAndBottom;
            int centerLeft   = rectangle.Left   + WidthLeft;
            int centerRight  = rectangle.Right  - WidthRight;
            int centerTop    = rectangle.Top    + HeightTop;
            int centerBottom = rectangle.Bottom - HeightBottom;

            // Corners
            if (WidthLeft  > 0 && HeightTop    > 0) graphics.DrawImage(BitmapTopLeft,     rectangle.Left, rectangle.Top, BitmapTopLeft.Width,     BitmapTopLeft.Height);
            if (WidthRight > 0 && HeightTop    > 0) graphics.DrawImage(BitmapTopRight,    centerRight,    rectangle.Top, BitmapTopRight.Width,    BitmapTopRight.Height);
            if (WidthRight > 0 && HeightBottom > 0) graphics.DrawImage(BitmapBottomRight, centerRight,    centerBottom,  BitmapBottomRight.Width, BitmapBottomRight.Height);
            if (WidthLeft  > 0 && HeightBottom > 0) graphics.DrawImage(BitmapBottomLeft,  rectangle.Left, centerBottom,  BitmapBottomLeft.Width,  BitmapBottomLeft.Height);
            // Edges
            if (                  HeightTop    > 0) graphics.DrawImage(BitmapTop,    new Rectangle(centerLeft,     rectangle.Top, centerWidth, HeightTop),    BitmapTopRectangle,    GraphicsUnit.Pixel);
            if (WidthRight > 0                    ) graphics.DrawImage(BitmapRight,  new Rectangle(centerRight,    centerTop,     WidthRight,  centerHeight), BitmapRightRectangle,  GraphicsUnit.Pixel);
            if (                  HeightBottom > 0) graphics.DrawImage(BitmapBottom, new Rectangle(centerLeft,     centerBottom,  centerWidth, HeightBottom), BitmapBottomRectangle, GraphicsUnit.Pixel);
            if (WidthLeft > 0                     ) graphics.DrawImage(BitmapLeft,   new Rectangle(rectangle.Left, centerTop,     WidthLeft,   centerHeight), BitmapLeftRectangle,   GraphicsUnit.Pixel);
            // Center
            graphics.DrawImage(BitmapCenter, new Rectangle(centerLeft, centerTop, centerWidth, centerHeight), BitmapCenterRectangle, GraphicsUnit.Pixel);
        }
        public static Bitmap9 FromFile(string path, bool useEmbeddedColorManagement = true)
        {
            Bitmap bitmap = new Bitmap(Image.FromFile(path, useEmbeddedColorManagement));
            return new Bitmap9(bitmap);
        }
        public static Bitmap9 FromStream(Stream stream, bool useEmbeddedColorManagement = true)
        {
            Bitmap bitmap = new Bitmap(Image.FromStream(stream, useEmbeddedColorManagement));
            return new Bitmap9(bitmap);
        }
        private Bitmap Enborder(Bitmap bitmap)
        {
            Bitmap bitmapWithBorder = new Bitmap(bitmap.Width + 2, bitmap.Height + 2, bitmap.PixelFormat);
            using (Graphics graphics = Graphics.FromImage(bitmapWithBorder))
            {
                // Center
                graphics.DrawImage(bitmap, 1, 1, bitmap.Width, bitmap.Height);
                // Enge left
                graphics.DrawImage(bitmap, new Rectangle(0, 1, 1, bitmap.Height), new Rectangle(0, 0, 1, bitmap.Height), GraphicsUnit.Pixel);
                // Edge right
                graphics.DrawImage(bitmap, new Rectangle(bitmapWithBorder.Width - 1,  1, 1, bitmap.Height), new Rectangle(bitmap.Width - 1, 0, 1, bitmap.Height), GraphicsUnit.Pixel);
                // Edge top
                graphics.DrawImage(bitmap, new Rectangle(1, 0, bitmap.Width,  1), new Rectangle(0, 0, bitmap.Width,  1), GraphicsUnit.Pixel);
                // Edge bottom
                graphics.DrawImage(bitmap, new Rectangle(1, bitmapWithBorder.Height - 1, bitmap.Width, 1), new Rectangle(0, bitmap.Height - 1, bitmap.Width, 1), GraphicsUnit.Pixel);
            }
            // Corners
            bitmapWithBorder.SetPixel(0, 0, bitmap.GetPixel(0, 0));
            bitmapWithBorder.SetPixel(bitmapWithBorder.Width - 1, 0, bitmap.GetPixel(bitmap.Width - 1, 0));
            bitmapWithBorder.SetPixel(0, bitmapWithBorder.Height - 1, bitmap.GetPixel(0, bitmap.Height - 1));
            bitmapWithBorder.SetPixel(bitmapWithBorder.Width - 1, bitmapWithBorder.Height - 1, bitmap.GetPixel(bitmap.Width - 1, bitmap.Height - 1));

            return bitmapWithBorder;
        }
    }
}

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static void DrawImage(this Graphics graphics, Bitmap9 bitmap, int x, int y, int width, int height)
        {
            bitmap.Draw(graphics, x, y, width, height);
        }
        public static void DrawImage(this Graphics graphics, Bitmap9 bitmap, Rectangle rectangle)
        {
            bitmap.Draw(graphics, rectangle);
        }
    }
}

#endif