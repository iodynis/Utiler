﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Utility
{
    public class ControlAdapter<T> : INotifyPropertyChanged where T : class
    {
        public event PropertyChangedEventHandler PropertyChanged;
        //private Delegate field_eventHandler;
        //private event EventHandler field_event;
        private readonly string[] field_eventNames;
        private readonly Delegate[] field_eventDelegates;
        private readonly Func<T, string> field_toString;
        private string field_string;
        public string String
        {
            get
            {
                return field_string ?? (field_string = ToString());
            }
        }
        private readonly T field_object;
        public T Object
        {
            get
            {
                return field_object;
            }
        }
        public ControlAdapter(T @object, Func<T, string> toString = null, string[] eventNames = null)
        {
            if (@object == null)
            {
                throw new NullReferenceException("Control adapter does not accept null objects.");
            }
            field_object = @object;
            field_toString = toString;
            field_eventNames = eventNames ?? new string[0];
            field_eventDelegates = new Delegate[field_eventNames.Length];

            Attach();
        }
        ~ControlAdapter()
        {
            Detach();
        }
        //private delegate void EventHandler(object param_sender, object param_value);
        //private delegate void Fuck(object param_sender, object param_value);

        private void Handle()//object param_sender, object param_value)
        {
			PropertyChanged?.Invoke(null, null);
		}
        static private Delegate Create(EventInfo eventInfo, Action action)
        {
            Type handlerType = eventInfo.EventHandlerType;
            ParameterInfo[] eventParameters = handlerType.GetMethod("Invoke").GetParameters();

            //lambda: (object x0, EventArgs x1) => d()
            IEnumerable<ParameterExpression> parameters = eventParameters.Select(p=>Expression.Parameter(p.ParameterType,"x"));
            MethodCallExpression body = Expression.Call(Expression.Constant(action),action.GetType().GetMethod("Invoke"));
            LambdaExpression lambda = Expression.Lambda(body,parameters.ToArray());
            return Delegate.CreateDelegate(handlerType, lambda.Compile(), "Invoke", false);
        }
        private void Attach()
        {
            Type type = field_object.GetType();
            for (int eventIndex = 0; eventIndex < field_eventNames.Length; eventIndex++)
            {
                string eventName = field_eventNames[eventIndex];
                EventInfo eventInfo = type.GetEvent(eventName);
                if (eventInfo == null)
                {
                    continue;
                }
                field_eventDelegates[eventIndex] = Create(eventInfo, Handle);
                eventInfo.AddEventHandler(field_object, field_eventDelegates[eventIndex]);
            }
        }

        private void Detach()
        {
	        if (field_eventNames == null)
	        {
		        return;
	        }

            Type type = field_object.GetType();
            for (int eventIndex = 0; eventIndex < field_eventNames.Length; eventIndex++)
            {
                string eventName = field_eventNames[eventIndex];
                EventInfo eventInfo = type.GetEvent(eventName);
                if (eventInfo == null)
                {
                    continue;
                }
                eventInfo.RemoveEventHandler(field_object, field_eventDelegates[eventIndex]);
            }
        }

        public override string ToString()
        {
            return field_toString != null ? field_toString.Invoke(Object) : field_object.ToString();
        }
    }

}
