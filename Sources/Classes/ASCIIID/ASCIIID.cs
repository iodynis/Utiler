﻿using System;
using System.Linq;

namespace Iodynis.Libraries.Utility
{
    public class ASCIIID
    {
        private char[] Characters;
        private int[] SymbolToValue;
        private int StringLength;

        /// <summary>
        /// Symbols used for the string representation of the IDs.
        /// </summary>
        public readonly string Symbols;

        public ASCIIID(string symbols, int longsCount)
        {
            foreach (char symbol in symbols)
            {
                if ((symbol & 0xFF00) != 0)
                {
                    throw new Exception($"Symbol {symbol} is not ASCII. Only ASCII symbols are allowed.");
                }
            }

            Symbols = symbols;
            Characters = Symbols.ToArray();
            StringLength = GetLength(longsCount);

            SymbolToValue = new int[256];
            for (int i = 0; i < 256; i++)
            {
                SymbolToValue[i] = (char)0;
            }
            for (int i = 0; i < Characters.Length; i++)
            {
                char symbol = Characters[i];
                SymbolToValue[symbol] = i;
            }
        }

        /// <summary>
        /// Get the length of the string that can encode the CUID.
        /// </summary>
        /// <param name="longsCount">How many longs do represent the CUID.</param>
        /// <returns>The minimal string length that may encode the CUID.</returns>
        private int GetLength(int longsCount)
        {
            int length = 0;

            uint value = uint.MaxValue;
            while (value != 0)
            {
                length++;
                value = value / (uint)Characters.Length;
            }

            // For now, just strore the encoded longs seperately
            length *= longsCount;

            return length;
        }

        public string Convert(long id)
        {
            char[] symbols = new char[StringLength];

            for (int symbolsIndex = symbols.Length - 1; symbolsIndex >= 0; symbolsIndex--)
            {
                symbols[symbolsIndex] = (char)(id % Characters.Length);
                id = id / Characters.Length;
            }

            string @string = new string(symbols);
            return @string;
        }

        public long Convert(string @string)
        {
            if (@string.Length != StringLength)
            {
                throw new Exception($"CUID {@string} is of invalid length, should be exactly {StringLength} symbols");
            }

            long id = 0;

            for (int stringIndex = @string.Length - 1; stringIndex >= 0; stringIndex++)
            {
                id *= Characters.Length;
                id += SymbolToValue[@string[stringIndex]];
            }

            return id;
        }
    }
}
