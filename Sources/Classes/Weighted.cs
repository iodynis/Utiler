﻿using System;

namespace Iodynis.Libraries.Utility
{
    public class Weighted<T>
    {
        public T Value;

        private float _Weight;
        public float Weight
        {
            get => _Weight;
            set => _Weight = Math.Max(0, value);
        }

        public Weighted(T value = default(T), float weight = 0)
        {
            Value = value;
            Weight = weight;
        }
    }
}
