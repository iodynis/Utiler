﻿using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Graphs
{
    public class GraphManager<TNode, TEdge>
    {
        protected List<Graph<TNode, TEdge>> _Graphs = new List<Graph<TNode, TEdge>>();
        public IReadOnlyList<Graph<TNode, TEdge>> Graphs => _Graphs;

        public GraphManager()
        {
            ;
        }

        public virtual Graph<TNode, TEdge> New()
        {
            Graph<TNode, TEdge> graph = new Graph<TNode, TEdge>();
            _Graphs.Add(graph);

            return graph;
        }
        public virtual Graph<TNode, TEdge> Add(Graph<TNode, TEdge> graph)
        {
            if (!_Graphs.Contains(graph))
            {
                _Graphs.Add(graph);
            }

            return graph;
        }
        public virtual void Remove(Graph<TNode, TEdge> graph)
        {
            _Graphs.Remove(graph);
        }
    }
}
