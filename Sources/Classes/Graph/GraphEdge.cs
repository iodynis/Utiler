﻿namespace Iodynis.Libraries.Utility.Graphs
{
    public class GraphEdge<TNode, TEdge>
    {
        public virtual GraphNode<TNode, TEdge> Output { get; protected set; }
        public virtual GraphNode<TNode, TEdge> Input { get; protected set; }

        public TEdge Value { get; set; }

        public GraphEdge(GraphNode<TNode, TEdge> output, GraphNode<TNode, TEdge> input)
            : this(default, output, input) { }

        public GraphEdge(TEdge value, GraphNode<TNode, TEdge> output, GraphNode<TNode, TEdge> input)
        {
            Value = value;
            Output = output;
            Input = input;
        }
    }
}
