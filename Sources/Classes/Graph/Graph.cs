﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Graphs
{
    public class Graph<TNode, TConnection>
    {
        protected List<GraphSubgraph<TNode, TConnection>> _Subgraphs = new List<GraphSubgraph<TNode, TConnection>>();
        public IReadOnlyList<GraphSubgraph<TNode, TConnection>> Subgraphs => _Subgraphs;

        public Graph()
        {
            ;
        }

        public GraphSubgraph<TNode, TConnection> New(string name = null)
        {
            GraphSubgraph<TNode, TConnection> subgraph = new GraphSubgraph<TNode, TConnection>(this, name);
            _Subgraphs.Add(subgraph);

            return subgraph;
        }

        public virtual bool Has(string name)
        {
            return Get(name) != null;
        }

        public virtual bool Has(GraphSubgraph<TNode, TConnection> subgraph)
        {
            return subgraph.Graph == this;
        }

        public virtual GraphSubgraph<TNode, TConnection> Get(string name)
        {
            for (int subgraphIndex = 0; subgraphIndex < Subgraphs.Count; subgraphIndex++)
            {
                if (Subgraphs[subgraphIndex].Name == name)
                {
                    return Subgraphs[subgraphIndex];
                }
            }

            return null;
        }

        public virtual GraphSubgraph<TNode, TConnection> Add(GraphSubgraph<TNode, TConnection> subgraph)
        {
            if (subgraph == null)
            {
                new ArgumentNullException(nameof(subgraph));
            }
            if (_Subgraphs.Contains(subgraph))
            {
                return subgraph;
            }
            if (subgraph.Name != null && Has(subgraph.Name))
            {
                new ArgumentException($"Subgraph with the name {subgraph.Name} is already presented in the graph.", nameof(subgraph));
            }

            if (subgraph.Graph != null && subgraph.Graph != this)
            {
                subgraph.Graph.Remove(subgraph);
            }

            _Subgraphs.Add(subgraph);
            subgraph.Graph = this;

            return subgraph;
        }

        public virtual GraphSubgraph<TNode, TConnection> Remove(GraphSubgraph<TNode, TConnection> subgraph)
        {
            if (_Subgraphs.Remove(subgraph))
            {
                subgraph.Graph = null;
                return subgraph;
            }

            return null;
        }
    }
}
