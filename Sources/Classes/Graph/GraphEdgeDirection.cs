﻿using System;

namespace Iodynis.Libraries.Utility.Graphs
{
    /// <summary>
    /// Direction of the graph connection.
    /// </summary>
    [Flags]
    public enum GraphEdgeDirection
    {
        /// <summary>
        /// Direction is not set.
        /// </summary>
        NONE   = 0,

        /// <summary>
        /// .
        /// </summary>
        INPUT  = 1,

        /// <summary>
        /// .
        /// </summary>
        OUTPUT = 2,

        /// <summary>
        /// Both directions.
        /// </summary>
        BOTH   = OUTPUT | INPUT,
    }
}
