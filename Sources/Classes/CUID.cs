﻿using Iodynis.Libraries.Utility.Extensions;
using System;
using System.Linq;
using System.Threading;

namespace Iodynis.Libraries.Utility
{
    /// <summary>
    /// Custom Unique ID.
    /// </summary>
    public static class CUID
    {
        private static Random IdRandom = new Random();
        private static int IdCounter = 0;

        /// <summary>
        /// Create a unique ID.
        /// </summary>
        /// <param name="reverse">True to bring the most unique, randomized and changing part to first bytes. False to leave it on the right.</param>
        /// <returns>A new unique ID.</returns>
        public static ulong New(bool reverse = false)
        {
            ulong id = (ulong)(DateTime.UtcNow.Ticks << 12) /* Cycles in 16 years */ + (ulong)(Interlocked.Increment(ref IdCounter) << 12) + (ulong)(IdRandom.Next() & 0xFFF);
            return reverse ? id.Reverse() : id;
        }
    }
}
