﻿using System;

namespace Iodynis.Libraries.Utility
{
    /// <summary>
    /// Postponed action.
    /// </summary>
    public class ActionPostponed
    {
        /// <summary>
        /// The action to invoke on the <see cref="InvokeMaybe"/> and <see cref="InvokeForce"/> calls.
        /// </summary>
        public readonly Action Action;
        /// <summary>
        /// If should invoke the <see cref="Action"/> when the <see cref="InvokeMaybe"/> is called.
        /// </summary>
        public bool ShouldInvoke;

        /// <summary>
        /// Postponed action.
        /// </summary>
        /// <param name="action">Action to invoke.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public ActionPostponed(Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            Action = action;
            ShouldInvoke = false;
        }

        /// <summary>
        /// Reset the <see cref="ShouldInvoke"/> flag so that the next call to the <see cref="InvokeMaybe"/> won't invoke the <see cref="Action"/>.
        /// </summary>
        public void Reset()
        {
            ShouldInvoke = false;
        }

        /// <summary>
        /// Raise the <see cref="ShouldInvoke"/> flag to run the <see cref="Action"/> the next time the <see cref="InvokeMaybe"/> is called.
        /// </summary>
        public void InvokeLater()
        {
            ShouldInvoke = true;
        }

        /// <summary>
        /// Invoke the <see cref="Action"/> if the <see cref="ShouldInvoke"/> flag is raised and reset the flag.
        /// </summary>
        public void InvokeMaybe()
        {
            if (!ShouldInvoke)
            {
                return;
            }

            ShouldInvoke = false;
            Action.Invoke();
        }

        /// <summary>
        /// Invoke the <see cref="Action"/> independently of the <see cref="ShouldInvoke"/> flag state and reset the flag.
        /// </summary>
        public void InvokeForce()
        {
            ShouldInvoke = false;
            Action.Invoke();
        }
    }
}
