﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Utility.ProgramArguments
{
    public class ProgramArgument
    {
        public readonly Enum Enum;
        public readonly string Key;
        public readonly IReadOnlyList<string> Values;
        public ProgramArgumentStatus Status;
        public ProgramArgument(Enum keyEnum, string keyString, ProgramArgumentStatus status, params string[] values)
        {
            Enum = keyEnum;
            Key = keyString;
            Values = new List<string>(values);
            Status = status;
        }
        public ProgramArgument(Enum keyEnum, string keyString, ProgramArgumentStatus status, IEnumerable<string> values)
        {
            Enum = keyEnum;
            Key = keyString;
            Values = new List<string>(values);
            Status = status;
        }
        public ProgramArgument(string key, ProgramArgumentStatus status, params string[] values)
            : this(default(Enum), key, status, values) { }
        public ProgramArgument(string key, ProgramArgumentStatus status, IEnumerable<string> values)
            : this(default(Enum), key, status, values) { }

        public override string ToString()
        {
            return $"{Key} {String.Join(" ", Values.Select(_value => $"\"{_value}\""))}";
        }
    }
}
