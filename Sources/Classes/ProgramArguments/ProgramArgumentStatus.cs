﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Utility.ProgramArguments
{
    [Flags]
    public enum ProgramArgumentStatus
    {
        OK                           = 0,
        ERROR_KEY_UNKNOWN            = 1 << 0,
        ERROR_KEY_DUPLICATE          = 1 << 1,
        ERROR_KEY_INCOMPATIBLE       = 1 << 2,
        ERROR_VALUES_INCORRECT_COUNT = 1 << 3,
    }
}
