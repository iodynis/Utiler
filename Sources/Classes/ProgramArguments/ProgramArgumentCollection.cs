﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Utility.ProgramArguments
{
    public class ProgramArgumentCollection
    {
        /// <summary>
        /// All arguments.
        /// </summary>
        public readonly List<ProgramArgument> Arguments;
        /// <summary>
        /// All arguments by key.
        /// Only arguments that have a key are presented, refer to <see cref="Arguments"/> property for the list of all arguments.
        /// </summary>
        public readonly Dictionary<Enum, List<ProgramArgument>> ArgumentsByKey;
        /// <summary>
        /// List of all arguments in the front.
        /// </summary>
        public readonly List<string> ArgumentsNose;
        /// <summary>
        /// List of all arguments in the tail.
        /// </summary>
        public readonly List<string> ArgumentsTail;
        /// <summary>
        /// List of all unknown arguments.
        /// </summary>
        public readonly List<ProgramArgument> ArgumentsUnknown;
        /// <summary>
        /// Unknown arguments by key.
        /// Only arguments that have a key are presented, refer to <see cref="ArgumentsUnknown"/> property for the list of all unknown arguments.
        /// </summary>
        public readonly Dictionary<Enum, List<ProgramArgument>> ArgumentsUnknownByKey;
        /// <summary>
        /// List of all incompatible arguments.
        /// </summary>
        public readonly List<ProgramArgument> ArgumentsIncompatible;
        /// <summary>
        /// Incompatible arguments by key.
        /// Only arguments that have a key are presented, refer to <see cref="ArgumentsIncompatible"/> property for the list of all incompatible arguments.
        /// </summary>
        public readonly Dictionary<Enum, List<ProgramArgument>> ArgumentsIncompatibleByKey;
        /// <summary>
        /// List of all duplicated arguments.
        /// </summary>
        public readonly List<ProgramArgument> ArgumentsDuplicated;
        /// <summary>
        /// Duplicated arguments by key.
        /// </summary>
        public readonly Dictionary<Enum, List<ProgramArgument>> ArgumentsDuplicatedByKey;
        /// <summary>
        /// List of all arguments that have incorrect values.
        /// </summary>
        public readonly List<ProgramArgument> ArgumentsIncorrectValuesCount;
        /// <summary>
        /// Arguments that have incorrect values by key.
        /// Only arguments that have a key are presented, refer to <see cref="ArgumentsIncorrectValuesCount"/> property for the list of all incorrect arguments.
        /// </summary>
        public readonly Dictionary<Enum, List<ProgramArgument>> ArgumentsIncorrectValuesCountByKey;

        /// <summary>
        /// If all provided arguments are Ok.
        /// No unknown, incompatible, duplicated arguments and no arguments with incorrect values.
        /// </summary>
        public readonly bool Ok;
        /// <summary>
        /// If no arguments were provided.
        /// </summary>
        public readonly bool HasNoArguments;

        internal ProgramArgumentCollection(IEnumerable<ProgramArgument> arguments, IEnumerable<string> argumentsNose, IEnumerable<string> argumentsTail)
        {
            arguments = arguments ?? new ProgramArgument[0];
            argumentsNose = argumentsNose ?? new string[0];
            argumentsTail = argumentsTail ?? new string[0];

            Arguments                     = arguments.Where(_argument => _argument.Status == ProgramArgumentStatus.OK).ToList();
            ArgumentsUnknown              = arguments.Where(_argument => (_argument.Status & ProgramArgumentStatus.ERROR_KEY_UNKNOWN           ) != 0).ToList();
            ArgumentsIncompatible         = arguments.Where(_argument => (_argument.Status & ProgramArgumentStatus.ERROR_KEY_INCOMPATIBLE      ) != 0).ToList();
            ArgumentsDuplicated           = arguments.Where(_argument => (_argument.Status & ProgramArgumentStatus.ERROR_KEY_DUPLICATE         ) != 0).ToList();
            ArgumentsIncorrectValuesCount = arguments.Where(_argument => (_argument.Status & ProgramArgumentStatus.ERROR_VALUES_INCORRECT_COUNT) != 0).ToList();
            ArgumentsNose = new List<string>(argumentsNose);
            ArgumentsTail = new List<string>(argumentsTail);

            ArgumentsByKey = new Dictionary<Enum, List<ProgramArgument>>();
            foreach (ProgramArgument argument in arguments)
            {
                if (argument.Enum == null)
                {
                    continue;
                }
                if (!ArgumentsByKey.TryGetValue(argument.Enum, out List<ProgramArgument> list))
                {
                    list = new List<ProgramArgument>();
                    ArgumentsByKey.Add(argument.Enum, list);
                }

                list.Add(argument);
            }

            ArgumentsUnknownByKey = new Dictionary<Enum, List<ProgramArgument>>();
            foreach (ProgramArgument argument in ArgumentsUnknown)
            {
                if (argument.Enum == null)
                {
                    continue;
                }
                ArgumentsUnknownByKey.Add(argument.Enum, ArgumentsByKey[argument.Enum]);
            }

            ArgumentsIncompatibleByKey = new Dictionary<Enum, List<ProgramArgument>>();
            foreach (ProgramArgument argument in ArgumentsIncompatible)
            {
                if (argument.Enum == null)
                {
                    continue;
                }
                ArgumentsIncompatibleByKey.Add(argument.Enum, ArgumentsByKey[argument.Enum]);
            }

            ArgumentsDuplicatedByKey = new Dictionary<Enum, List<ProgramArgument>>();
            foreach (ProgramArgument argument in ArgumentsDuplicated)
            {
                if (argument.Enum == null)
                {
                    continue;
                }
                if (!ArgumentsDuplicatedByKey.TryGetValue(argument.Enum, out List<ProgramArgument> argumentsDuplicated))
                {
                    argumentsDuplicated = new List<ProgramArgument>();
                }
                argumentsDuplicated.AddRange(ArgumentsByKey[argument.Enum]);
            }

            ArgumentsIncorrectValuesCountByKey = new Dictionary<Enum, List<ProgramArgument>>();
            foreach (ProgramArgument argument in ArgumentsIncorrectValuesCount)
            {
                if (argument.Enum == null)
                {
                    continue;
                }
                ArgumentsIncorrectValuesCountByKey.Add(argument.Enum, ArgumentsByKey[argument.Enum]);
            }

            Ok = ArgumentsUnknown.Count == 0 && ArgumentsIncompatible.Count == 0 && ArgumentsDuplicated.Count == 0 && ArgumentsIncorrectValuesCount.Count == 0;
            HasNoArguments = ArgumentsByKey.Count == 0 && ArgumentsNose.Count == 0 && ArgumentsTail.Count == 0;
        }

        public bool HasKey(Enum @enum)
        {
            return ArgumentsByKey.ContainsKey(@enum);
        }
        public ProgramArgument GetArgument(Enum @enum, int indexArgument)
        {
            if (@enum == null)
            {
                return null;
            }
            if (!ArgumentsByKey.TryGetValue(@enum, out List<ProgramArgument> arguments))
            {
                return null;
            }
            if (indexArgument < 0 || arguments.Count <= indexArgument)
            {
                return null;
            }
            return arguments[indexArgument];
        }
        public string GetValue(Enum @enum, int indexValue)
        {
            ProgramArgument argument = GetArgument(@enum, 0);
            if (argument == null)
            {
                return null;
            }
            if (indexValue < 0 || argument.Values.Count <= indexValue)
            {
                return null;
            }
            return argument.Values[indexValue];
        }
        public string GetValue(Enum @enum, int indexArgument, int indexValue)
        {
            ProgramArgument argument = GetArgument(@enum, indexArgument);
            if (argument == null)
            {
                return null;
            }
            if (indexValue < 0 || argument.Values.Count <= indexValue)
            {
                return null;
            }
            return argument.Values[indexValue];
        }
    }
}