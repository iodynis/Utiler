﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Utility.ProgramArguments
{
    public class ProgramArgumentRule
    {
        public readonly Enum Key;
        public readonly bool AllowMultiple;
        public readonly List<Enum> RequiredKeys;
        public readonly List<Enum> CompatibleKeys;
        public readonly List<Enum> IncompatibleKeys;
        public readonly IReadOnlyList<int> ValuesCount;
        public readonly int ValuesCountMin;
        public readonly int ValuesCountMax;
        public ProgramArgumentRule(Enum key)
        {
            Key = key;
            AllowMultiple = false;
            IncompatibleKeys = new List<Enum>();
            CompatibleKeys   = new List<Enum>();
            RequiredKeys     = new List<Enum>();
            ValuesCount = new List<int>();
            ValuesCountMin = -1;
            ValuesCountMax = -1;
        }
        public ProgramArgumentRule(Enum key, int valuesCount, IEnumerable<Enum> incompatibleArguments = null, IEnumerable<Enum> compatibleArguments = null, IEnumerable<Enum> requiredArguments = null, bool allowMultiple = false)
        {
            Key = key;
            AllowMultiple = allowMultiple;
            IncompatibleKeys = incompatibleArguments != null ? new List<Enum>(incompatibleArguments) : new List<Enum>();
            CompatibleKeys   = compatibleArguments   != null ? new List<Enum>(compatibleArguments)   : new List<Enum>();
            RequiredKeys     = requiredArguments     != null ? new List<Enum>(requiredArguments)     : new List<Enum>();
            ValuesCount = new List<int>() { valuesCount };
            ValuesCountMin = -1;
            ValuesCountMax = -1;
        }
        public ProgramArgumentRule(Enum key, IEnumerable<int> valuesCount, IEnumerable<Enum> incompatibleArguments = null, IEnumerable<Enum> compatibleArguments = null, IEnumerable<Enum> requiredArguments = null, bool allowMultiple = false)
        {
            Key = key;
            AllowMultiple = allowMultiple;
            IncompatibleKeys = incompatibleArguments != null ? new List<Enum>(incompatibleArguments) : new List<Enum>();
            CompatibleKeys   = compatibleArguments   != null ? new List<Enum>(compatibleArguments)   : new List<Enum>();
            RequiredKeys     = requiredArguments     != null ? new List<Enum>(requiredArguments)     : new List<Enum>();
            ValuesCount = valuesCount != null ? new List<int>(valuesCount) : new List<int>();
            ValuesCountMin = -1;
            ValuesCountMax = -1;
        }
        public ProgramArgumentRule(Enum key, int valuesCountMin, int valuesCountMax, IEnumerable<Enum> incompatibleArguments = null, IEnumerable<Enum> compatibleArguments = null, IEnumerable<Enum> requiredArguments = null, bool allowMultiple = false)
        {
            Key = key;
            AllowMultiple = allowMultiple;
            IncompatibleKeys = incompatibleArguments != null ? new List<Enum>(incompatibleArguments) : new List<Enum>();
            CompatibleKeys   = compatibleArguments   != null ? new List<Enum>(compatibleArguments)   : new List<Enum>();
            RequiredKeys     = requiredArguments     != null ? new List<Enum>(requiredArguments)     : new List<Enum>();
            ValuesCount = new List<int>();
            ValuesCountMin = valuesCountMin;
            ValuesCountMax = valuesCountMax;
        }
        public bool IsValuesCountOk(int count)
        {
            if (ValuesCountMin >= 0 && count < ValuesCountMin)
            {
                return false;
            }
            if (ValuesCountMax >= 0 && count > ValuesCountMax)
            {
                return false;
            }
            if (ValuesCount.Count > 0 && !ValuesCount.Contains(count))
            {
                return false;
            }
            return true;
        }
    }
}