﻿#if !UNITY_ENGINE

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        public enum ExcelBackgroundPattern
        {
            NONE,
            GRAY125,
            SOLID,
        }
    }
}

#endif