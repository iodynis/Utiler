﻿#if !UNITY_ENGINE

using System;

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        public enum ExcelBorderLineStyle
        {
            /// <summary>
            /// -
            /// </summary>
            NONE,

            /// <summary>
            /// thin
            /// </summary>
            THIN,
            /// <summary>
            /// medium
            /// </summary>
            MEDIUM,
            /// <summary>
            /// thick
            /// </summary>
            THICK,

            /// <summary>
            /// hair
            /// </summary>
            HAIR,
            /// <summary>
            /// dotted
            /// </summary>
            DOTTED,
            /// <summary>
            /// dashed
            /// </summary>
            DASHED,
            /// <summary>
            /// dashDot
            /// </summary>
            DASH_DOT,
            /// <summary>
            /// dashDotDot
            /// </summary>
            DASH_DOT_DOT,

            /// <summary>
            /// slantDashDot
            /// </summary>
            SLANT_DASH_DOT,
            /// <summary>
            /// mediumDashed
            /// </summary>
            MEDIUM_DASHED,
            /// <summary>
            /// mediumDashDot
            /// </summary>
            MEDIUM_DASH_DOT,
            /// <summary>
            /// mediumDashDotDot
            /// </summary>
            MEDIUM_DASH_DOT_DOT,

            /// <summary>
            /// double
            /// </summary>
            DOUBLE,
        }
        private static string ExcelBorderStyleToString(ExcelBorderLineStyle style)
        {
            switch (style)
            {
                case ExcelBorderLineStyle.THIN:
                    return "thin";
                case ExcelBorderLineStyle.MEDIUM:
                    return "medium";
                case ExcelBorderLineStyle.THICK:
                    return "thick";
                case ExcelBorderLineStyle.HAIR:
                    return "hair";
                case ExcelBorderLineStyle.DOUBLE:
                    return "double";
                case ExcelBorderLineStyle.DOTTED:
                    return "dotted";
                case ExcelBorderLineStyle.DASHED:
                    return "dashed";
                case ExcelBorderLineStyle.DASH_DOT:
                    return "dashDot";
                case ExcelBorderLineStyle.DASH_DOT_DOT:
                    return "dashDotDot";
                case ExcelBorderLineStyle.SLANT_DASH_DOT:
                    return "slantDashDot";
                case ExcelBorderLineStyle.MEDIUM_DASHED:
                    return "mediumDashed";
                case ExcelBorderLineStyle.MEDIUM_DASH_DOT:
                    return "mediumDashDot";
                case ExcelBorderLineStyle.MEDIUM_DASH_DOT_DOT:
                    return "mediumDashDotDot";
                default:
                    throw new Exception($"Border style {style} is not supported.");
            }
        }
    }
}

#endif