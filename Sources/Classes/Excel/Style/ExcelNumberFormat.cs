﻿#if !UNITY_ENGINE

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        internal class ExcelNumberFormat : ExcelSharedItem
        {
            internal string FormatCode;
            internal ExcelNumberFormat(string formatCode)
            {
                FormatCode = formatCode;
            }
        }
    }
}

#endif