﻿#if !UNITY_ENGINE

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using Iodynis.Libraries.Utility.Extensions;

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        public class ExcelBook
        {
            public string CreatorName;

            //internal readonly Dictionary<string, int> SharedStringToIndex = new Dictionary<string, int>();
            //internal readonly List<ExcelString>       SharedStrings = new List<ExcelString>();
            internal static readonly IReadOnlyList<ExcelNumberFormat> BuiltInNumberFormats;
            internal readonly Dictionary<string, ExcelSharedString> StringToSharedString = new Dictionary<string, ExcelSharedString>();

            internal readonly List<ExcelNumberFormat>     SharedNumberFormats = new List<ExcelNumberFormat>();
            internal readonly List<ExcelSharedFont>       SharedFonts = new List<ExcelSharedFont>();
            internal readonly List<ExcelSharedBackground> SharedBackgrounds = new List<ExcelSharedBackground>();
            internal readonly List<ExcelSharedBorder>     SharedBorders = new List<ExcelSharedBorder>();
            internal readonly List<ExcelSharedStyle>      SharedStyles = new List<ExcelSharedStyle>();

            // These are used when writing a file
            internal readonly List<ExcelNumberFormat>     SortedSharedNumberFormats = new List<ExcelNumberFormat>();
            internal readonly List<ExcelSharedString>     SortedSharedStrings = new List<ExcelSharedString>();
            internal readonly List<ExcelSharedFont>       SortedSharedFonts = new List<ExcelSharedFont>();
            internal readonly List<ExcelSharedBackground> SortedSharedBackgrounds = new List<ExcelSharedBackground>();
            internal readonly List<ExcelSharedBorder>     SortedSharedBorders = new List<ExcelSharedBorder>();
            internal readonly List<ExcelSharedStyle>      SortedSharedStyles = new List<ExcelSharedStyle>();

            internal ExcelSharedFont SharedFontDefault { get; }
            internal ExcelSharedBackground SharedBackgroundDefaultNone { get; }
            internal ExcelSharedBackground SharedBackgroundDefaultGray125 { get; }
            internal ExcelSharedBorder SharedBorderDefaultNone { get; }
            internal ExcelSharedStyle SharedStyleDefault { get; }

            private List<ExcelSheet> _Sheets = new List<ExcelSheet>();
            public IReadOnlyList<ExcelSheet> Sheets
            {
                get
                {
                    return _Sheets;
                }
            }

            private ExcelSharedFont Font { get; set; }

            static ExcelBook()
            {
                List<ExcelNumberFormat> builtInNumberFormats = new List<ExcelNumberFormat>();

                builtInNumberFormats.Add(new ExcelNumberFormat("General"));
                builtInNumberFormats.Add(new ExcelNumberFormat("0"));
                builtInNumberFormats.Add(new ExcelNumberFormat("0.00"));
                builtInNumberFormats.Add(new ExcelNumberFormat("#,##0"));
                builtInNumberFormats.Add(new ExcelNumberFormat("#,##0.00"));
                builtInNumberFormats.Add(new ExcelNumberFormat("0%"));
                builtInNumberFormats.Add(new ExcelNumberFormat("0.00%"));
                builtInNumberFormats.Add(new ExcelNumberFormat("0.00E+00"));
                builtInNumberFormats.Add(new ExcelNumberFormat("# ?/?"));
                builtInNumberFormats.Add(new ExcelNumberFormat("# ??/??"));
                builtInNumberFormats.Add(new ExcelNumberFormat("d/m/yyyy"));
                builtInNumberFormats.Add(new ExcelNumberFormat("d-mmm-yy"));
                builtInNumberFormats.Add(new ExcelNumberFormat("d-mmm"));
                builtInNumberFormats.Add(new ExcelNumberFormat("mmm-yy"));
                builtInNumberFormats.Add(new ExcelNumberFormat("h:mm tt"));
                builtInNumberFormats.Add(new ExcelNumberFormat("h:mm:ss tt"));
                builtInNumberFormats.Add(new ExcelNumberFormat("H:mm"));
                builtInNumberFormats.Add(new ExcelNumberFormat("H:mm:ss"));
                builtInNumberFormats.Add(new ExcelNumberFormat("m/d/yyyy H:mm"));
                builtInNumberFormats.Add(new ExcelNumberFormat("#,##0 ;(#,##0)"));
                builtInNumberFormats.Add(new ExcelNumberFormat("#,##0 ;[Red](#,##0)"));
                builtInNumberFormats.Add(new ExcelNumberFormat("#,##0.00;(#,##0.00)"));
                builtInNumberFormats.Add(new ExcelNumberFormat("#,##0.00;[Red](#,##0.00)"));
                builtInNumberFormats.Add(new ExcelNumberFormat("mm:ss"));
                builtInNumberFormats.Add(new ExcelNumberFormat("[h]:mm:ss"));
                builtInNumberFormats.Add(new ExcelNumberFormat("mmss.0"));
                builtInNumberFormats.Add(new ExcelNumberFormat("##0.0E+0"));
                builtInNumberFormats.Add(new ExcelNumberFormat("@"));

                //builtInNumberFormats.Add(new ExcelNumberFormat(0, "General"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(1, "0"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(2, "0.00"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(3, "#,##0"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(4, "#,##0.00"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(9, "0%"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(10, "0.00%"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(11, "0.00E+00"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(12, "# ?/?"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(13, "# ??/??"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(14, "d/m/yyyy"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(15, "d-mmm-yy"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(16, "d-mmm"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(17, "mmm-yy"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(18, "h:mm tt"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(19, "h:mm:ss tt"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(20, "H:mm"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(21, "H:mm:ss"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(22, "m/d/yyyy H:mm"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(37, "#,##0 ;(#,##0)"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(38, "#,##0 ;[Red](#,##0)"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(39, "#,##0.00;(#,##0.00)"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(40, "#,##0.00;[Red](#,##0.00)"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(45, "mm:ss"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(46, "[h]:mm:ss"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(47, "mmss.0"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(48, "##0.0E+0"));
                //builtInNumberFormats.Add(new ExcelNumberFormat(49, "@"));

                BuiltInNumberFormats = builtInNumberFormats;
            }
            public ExcelBook()
            {
                SharedFontDefault  = new ExcelSharedFont("Times New Roman", 12, Color.Black, FontStyle.Regular, TextAlignmentHorizontal.LEFT, TextAlignmentVertical.TOP, false);
                SharedBackgroundDefaultNone = new ExcelSharedBackground(ExcelBackgroundPattern.NONE);
                SharedBackgroundDefaultGray125 = new ExcelSharedBackground(ExcelBackgroundPattern.GRAY125);
                SharedBorderDefaultNone = new ExcelSharedBorder();
                SharedStyleDefault = new ExcelSharedStyle(SharedFontDefault, SharedBackgroundDefaultNone, SharedBorderDefaultNone);

                // This hack will ensure that the defaults won't be deleted
                SharedFontDefault.Counter++;
                SharedBackgroundDefaultNone.Counter++;
                SharedBackgroundDefaultGray125.Counter++;
                SharedBorderDefaultNone.Counter++;
                SharedStyleDefault.Counter++;

                //ExcelSharedBackground gray = new ExcelSharedBackground(Color.Transparent, ExcelBackgroundPattern.GRAY125);
                //gray.Counter++;
                //SharedBackgrounds.Add(gray);

                SharedFonts.Add(SharedFontDefault);
                SharedBackgrounds.Add(SharedBackgroundDefaultNone);
                SharedBackgrounds.Add(SharedBackgroundDefaultGray125);
                SharedBorders.Add(SharedBorderDefaultNone);
                SharedStyles.Add(SharedStyleDefault);
            }
            public ExcelSheet SheetAdd()
            {
                return SheetAdd(_Sheets.Count);
            }
            public ExcelSheet SheetAdd(int position)
            {
                ExcelSheet sheet = new ExcelSheet(this);
                _Sheets.Insert(position, sheet);

                return sheet;
            }
            public void SheetRemove(int position)
            {
                _Sheets.RemoveAt(position);
            }
            public void SheetRemove(ExcelSheet sheet)
            {
                _Sheets.Remove(sheet);
            }

            private float _MaximumDigitWidth = 0;
            internal float MaximumDigitWidth
            {
                get
                {
                    if (_MaximumDigitWidth <= 0)
                    {
                        _MaximumDigitWidth = new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" }.Select(_digit => MeasureStringInPixels(_digit, SharedFontDefault.Font, Int32.MaxValue).Width).Max();
                        //_ColumnWidthPixelsInOneUnit = Excel.MeasureStringInPixels("12345678901234567890123456789012345678901234567890", SharedFontDefault.Font, Int32.MaxValue).Width / 50f);
                    }
                    return _MaximumDigitWidth;
                }
            }
            public void Save(string path)
            {
                ResetTemp();
                //UpdateSharedStrings();
                //UpdateSharedBorders();

                Dictionary<string, string> contents = new Dictionary<string, string>();

                // Process worksheets first as shared string and styles depend on them
                // ./xl/worksheets/*
                for (int index = 0; index < Sheets.Count; index++)
                {
                    contents.Add($"xl\\worksheets\\_rels\\sheet{index + 1}.xml.rels", GetFileContentForSheetRels(index));
                    contents.Add($"xl\\worksheets\\sheet{index + 1}.xml", GetFileContentForSheet(index));
                }

                // ./*
                contents.Add("[Content_Types].xml", GetFileContentForContentTypes());
                // ./_rel/*
                contents.Add("_rels\\.rels", GetFileContentForRels());
                // ./docProps/*
                contents.Add("docProps\\app.xml", GetFileContentForApp());
                contents.Add("docProps\\core.xml", GetFileContentForCore());
                // ./xl/*
                contents.Add("xl\\sharedStrings.xml", GetFileContentForSharedStrings());
                contents.Add("xl\\styles.xml", GetFileContentForStyles());
                contents.Add("xl\\workbook.xml", GetFileContentForBook());
                // ./xl/_rel/*
                contents.Add("xl\\_rels\\workbook.xml.rels", GetFileContentForBookRels());
                // ./xl/theme/*
                contents.Add("xl\\theme\\theme1.xml", GetFileContentForTheme());

                byte[] bytes = Utiler.Zip(contents);
                File.WriteAllBytes(path, bytes);
            }
            private void ResetTemp()
            {
                // Reset arrays
                SortedSharedStrings.Clear();
                SortedSharedNumberFormats.Clear();
                SortedSharedFonts.Clear();
                SortedSharedBackgrounds.Clear();
                SortedSharedBorders.Clear();
                SortedSharedStyles.Clear();

                // Reset indexes
                foreach (KeyValuePair<string, ExcelSharedString> StringAndSharedString in StringToSharedString)
                {
                    StringAndSharedString.Value.IndexInTempArray = -1;
                }
                foreach (ExcelNumberFormat numberFormat in SharedNumberFormats)
                {
                    numberFormat.IndexInTempArray = -1;
                }
                foreach (ExcelSharedFont font in SharedFonts)
                {
                    font.IndexInTempArray = -1;
                }
                foreach (ExcelSharedBackground background in SharedBackgrounds)
                {
                    background.IndexInTempArray = -1;
                }
                foreach (ExcelSharedBorder border in SharedBorders)
                {
                    border.IndexInTempArray = -1;
                }
                foreach (ExcelSharedStyle style in SharedStyles)
                {
                    style.IndexInTempArray = -1;
                }

                // Fill the defaults
                SortedSharedFonts.Add(SharedFontDefault);
                SortedSharedBackgrounds.Add(SharedBackgroundDefaultNone);
                SortedSharedBackgrounds.Add(SharedBackgroundDefaultGray125);
                SortedSharedBorders.Add(SharedBorderDefaultNone);
                SortedSharedStyles.Add(SharedStyleDefault);

                // Fill the defaults indexes
                SharedFontDefault.IndexInTempArray = 0;
                SharedBackgroundDefaultNone.IndexInTempArray = 0;
                SharedBackgroundDefaultGray125.IndexInTempArray = 1;
                SharedBorderDefaultNone.IndexInTempArray = 0;
                SharedStyleDefault.IndexInTempArray = 0;
            }

            #region Files
            private string GetFileContentForSheet(int index)
            {
                return GetFileContentForSheet(Sheets[index]);
            }
            private string GetFileContentForSheet(ExcelSheet sheet)
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
			        "<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" mc:Ignorable=\"x14ac\" xmlns:x14ac=\"http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac\">\r\n" +
			        $"\t<dimension ref=\"{(sheet.ColumnMin == sheet.ColumnMax && sheet.RowMin == sheet.RowMax ? ExcelCell.GetAlphanumericIndex(sheet.RowMin, sheet.ColumnMin) : $"{ExcelCell.GetAlphanumericIndex(sheet.RowMin, sheet.ColumnMin)}:{ExcelCell.GetAlphanumericIndex(sheet.RowMax, sheet.ColumnMax)}")}\"/>\r\n" +
			        "\t<sheetViews>\r\n" +
			        "\t\t<sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\r\n" +
			        "\t</sheetViews>\r\n"
                );

                // ...
                stringBuilder.Append($"\t<sheetFormatPr defaultColWidth=\"{sheet.WidthDefault}\" defaultRowHeight=\"{sheet.HeightDefault}\" x14ac:dyDescent=\"0.25\">\r\n");
                stringBuilder.Append("\t</sheetFormatPr>\r\n");

                // Columns
                if (sheet.Widths.Count != 0)
                {
                    stringBuilder.Append("\t<cols>\r\n");
                    if (sheet.Widths.Count > 0)
                    {
                        // Column widths
                        List<int> keys = sheet.Widths.Keys.ToList();
                        keys.Sort();

                        int sharedWidthColumnStartIndex = keys[0] + 1;
                        int sharedWidthColumnPreviousIndex = sharedWidthColumnStartIndex;
                        float sharedWidth = -1;

                        for (int keyIndex = 0; keyIndex < keys.Count; keyIndex++)
                        {
                            int columnIndex = keys[keyIndex] + 1;
                            float width = sheet.Widths[keys[keyIndex]];

                            // If width changed or column index changed jumped forward
                            if (columnIndex != sharedWidthColumnPreviousIndex + 1 || width != sharedWidth)
                            {
                                if (sharedWidthColumnStartIndex >= 0 && sharedWidth >= 0)
                                {
                                    stringBuilder.Append($"\t\t<col min=\"{sharedWidthColumnStartIndex}\" max=\"{sharedWidthColumnPreviousIndex}\" width=\"{sharedWidth}\" customWidth=\"true\"/>\r\n");
                                }
                                if (keyIndex == keys.Count - 1)
                                {
                                    stringBuilder.Append($"\t\t<col min=\"{columnIndex}\" max=\"{columnIndex}\" width=\"{width}\" customWidth=\"true\"/>\r\n");
                                }

                                sharedWidthColumnStartIndex = columnIndex;
                                sharedWidth = width;
                            }
                            else
                            {
                                if (keyIndex == keys.Count - 1)
                                {
                                    stringBuilder.Append($"\t\t<col min=\"{sharedWidthColumnStartIndex}\" max=\"{columnIndex}\" width=\"{sharedWidth}\" customWidth=\"true\"/>\r\n");
                                }
                            }
                            sharedWidthColumnPreviousIndex = columnIndex;
                        }
                    }
                    //foreach (KeyValuePair<int, float> columnAndWidth in sheet.Widths)
                    //{
                    //    int columnIndex = columnAndWidth.Key + 1;
                    //    float width = columnAndWidth.Value;
                    //    stringBuilder.Append($"\t\t<col min=\"{columnIndex}\" max=\"{columnIndex}\" width=\"{width}\" customWidth=\"1\"/>\r\n");
                    //}
                    stringBuilder.Append("\t</cols>\r\n");
                }

                stringBuilder.Append("\t<sheetData>\r\n");

                // Cells
                List<int> rowIndexes = sheet.RowToColumnToCell.Keys.ToList();
                rowIndexes.Sort();
                foreach (int rowIndex in rowIndexes)
                //foreach (KeyValuePair<int, Dictionary<int, ExcelCell>> indexAndRow in sheet.RowToColumnToCell)
                {
                    //int rowIndex = indexAndRow.Key;
                    Dictionary<int, ExcelCell> row = sheet.RowToColumnToCell[rowIndex];
                    if (!sheet.Height.TryGetValue(rowIndex, out float height))
                    {
                        height = -1;
                    }
                    stringBuilder.Append($"\t\t<row r=\"{rowIndex + 1}\"{(height > 0 ? $" ht=\"{height}\" customHeight=\"true\"" : "")}>\r\n"); // spans="1:31"
                    List<int> columnIndexes = row.Keys.ToList();
                    columnIndexes.Sort();
                    foreach (int columnIndex in columnIndexes)
                    //foreach (KeyValuePair<int, ExcelCell> indexAndCell in indexAndRow.Value)
                    {
                        //int column = indexAndCell.Key;
                        //ExcelCell cell = indexAndCell.Value;
                        ExcelCell cell = row[columnIndex];

                        // Get style id
                        if (cell.SharedStyle.IndexInTempArray < 0)
                        {
                            cell.SharedStyle.IndexInTempArray = SortedSharedStyles.Count;
                            SortedSharedStyles.Add(cell.SharedStyle);
                            if (cell.SharedStyle.Font.IndexInTempArray < 0)
                            {
                                cell.SharedStyle.Font.IndexInTempArray = SortedSharedFonts.Count;
                                SortedSharedFonts.Add(cell.SharedStyle.Font);
                            }
                            if (cell.SharedStyle.Background.IndexInTempArray < 0)
                            {
                                cell.SharedStyle.Background.IndexInTempArray = SortedSharedBackgrounds.Count;
                                SortedSharedBackgrounds.Add(cell.SharedStyle.Background);
                            }
                            if (cell.SharedStyle.Border.IndexInTempArray < 0)
                            {
                                cell.SharedStyle.Border.IndexInTempArray = SortedSharedBorders.Count;
                                SortedSharedBorders.Add(cell.SharedStyle.Border);

                                //.... Read Vertical and Horizontal borders directly ?..
                                //No, shared borders are exactly that, plus sharing same borders among multiple cells
                            }
                        }
                        // If cell got text
                        if (!String.IsNullOrEmpty(cell.Text))
                        {
                            // Get text id
                            if (cell.SharedText.IndexInTempArray < 0)
                            {
                                cell.SharedText.IndexInTempArray = SortedSharedStrings.Count;
                                SortedSharedStrings.Add(cell.SharedText);
                            }
                            stringBuilder.Append($"\t\t\t<c r=\"{cell.AlphanumericIndex}\" t=\"s\" s=\"{cell.SharedStyle.IndexInTempArray}\">\r\n\t\t\t\t<v>{cell.SharedText.IndexInTempArray}</v>\r\n\t\t\t</c>\r\n");
                        }
                        // If cell is empty
                        else
                        {
                            stringBuilder.Append($"\t\t\t<c r=\"{cell.AlphanumericIndex}\" s=\"{cell.SharedStyle.IndexInTempArray}\"></c>\r\n");
                        }
                    }
                    stringBuilder.Append("\t\t</row>\r\n");
                }
                stringBuilder.Append("\t</sheetData>\r\n");
                if (sheet.Merges.Count != 0)
                {
                    stringBuilder.Append($"\t<mergeCells count=\"{sheet.Merges.Count}\">\r\n");
                    // Merges
			        foreach (ExcelRange merge in sheet.Merges)
                    {
                        stringBuilder.Append($"\t\t<mergeCell ref=\"{merge.AlphanumericIndexStart}:{merge.AlphanumericIndexEnd}\"/>\r\n");
                    }
                    stringBuilder.Append("\t</mergeCells>\r\n");
                }
                stringBuilder.Append(
			        "\t<pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\r\n" +
			        //$"\t<pageSetup r:Id=\"1\" orientation=\"{sheet.OrientationIsPortrait.Ternary("portrait", "landscape")}\" paperSize=\"8\"/>\r\n" +
			        "</worksheet>"
                );

                return stringBuilder.ToString();
            }
            private string GetFileContentForSheetRels(int index)
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
                    "<Relationships xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\">\r\n" +
                    //"\t<Relationship Id=\"rId1\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/printerSettings\" Target=\"../printerSettings/printerSettings1.bin\"/>\r\n" +
                    //"\t<Relationship Id=\"rId2\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/drawing\" Target=\"../drawings/drawing1.xml\"/>\r\n" +
                    "</Relationships>"
                );

                return stringBuilder.ToString();
            }
            private string GetFileContentForSheetRels(ExcelSheet sheet)
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
			        "<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" mc:Ignorable=\"x14ac\" xmlns:x14ac=\"http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac\">\r\n" +
			        "\t<dimension ref=\"A1\"/>\r\n" +
			        "\t<sheetViews>\r\n" +
			        "\t\t<sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\r\n" +
			        "\t</sheetViews>\r\n" +
			        "\t<sheetFormatPr defaultRowHeight=\"15\" x14ac:dyDescent=\"0.25\"/>\r\n" +
			        "\t<sheetData/>\r\n" +
			        "\t<pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\r\n" +
			        "</worksheet>"
                );

                return stringBuilder.ToString();
            }
            private string GetFileContentForTheme()
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
                    "<a:theme xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\" name=\"Тема Office\">\r\n" +
                    "\t<a:themeElements>\r\n" +
                    "\t\t<a:clrScheme name=\"Стандартная\">\r\n" +
                    "\t\t\t<a:dk1>\r\n" +
                    "\t\t\t\t<a:sysClr val=\"windowText\" lastClr=\"000000\"/>\r\n" +
                    "\t\t\t</a:dk1>\r\n" +
                    "\t\t\t<a:lt1>\r\n" +
                    "\t\t\t\t<a:sysClr val=\"window\" lastClr=\"FFFFFF\"/>\r\n" +
                    "\t\t\t</a:lt1>\r\n" +
                    "\t\t\t<a:dk2>\r\n" +
                    "\t\t\t\t<a:srgbClr val=\"1F497D\"/>\r\n" +
                    "\t\t\t</a:dk2>\r\n" +
                    "\t\t\t<a:lt2>\r\n" +
                    "\t\t\t\t<a:srgbClr val=\"EEECE1\"/>\r\n" +
                    "\t\t\t</a:lt2>\r\n" +
                    "\t\t\t<a:accent1>\r\n" +
                    "\t\t\t\t<a:srgbClr val=\"4F81BD\"/>\r\n" +
                    "\t\t\t</a:accent1>\r\n" +
                    "\t\t\t<a:accent2>\r\n" +
                    "\t\t\t\t<a:srgbClr val=\"C0504D\"/>\r\n" +
                    "\t\t\t</a:accent2>\r\n" +
                    "\t\t\t<a:accent3>\r\n" +
                    "\t\t\t\t<a:srgbClr val=\"9BBB59\"/>\r\n" +
                    "\t\t\t</a:accent3>\r\n" +
                    "\t\t\t<a:accent4>\r\n" +
                    "\t\t\t\t<a:srgbClr val=\"8064A2\"/>\r\n" +
                    "\t\t\t</a:accent4>\r\n" +
                    "\t\t\t<a:accent5>\r\n" +
                    "\t\t\t\t<a:srgbClr val=\"4BACC6\"/>\r\n" +
                    "\t\t\t</a:accent5>\r\n" +
                    "\t\t\t<a:accent6>\r\n" +
                    "\t\t\t\t<a:srgbClr val=\"F79646\"/>\r\n" +
                    "\t\t\t</a:accent6>\r\n" +
                    "\t\t\t<a:hlink>\r\n" +
                    "\t\t\t\t<a:srgbClr val=\"0000FF\"/>\r\n" +
                    "\t\t\t</a:hlink>\r\n" +
                    "\t\t\t<a:folHlink>\r\n" +
                    "\t\t\t\t<a:srgbClr val=\"800080\"/>\r\n" +
                    "\t\t\t</a:folHlink>\r\n" +
                    "\t\t</a:clrScheme>\r\n" +
                    "\t\t<a:fontScheme name=\"Стандартная\">\r\n" +
                    "\t\t\t<a:majorFont>\r\n" +
                    "\t\t\t\t<a:latin typeface=\"Cambria\"/>\r\n" +
                    "\t\t\t\t<a:ea typeface=\"\"/>\r\n" +
                    "\t\t\t\t<a:cs typeface=\"\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Jpan\" typeface=\"ＭＳ Ｐゴシック\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Hang\" typeface=\"맑은 고딕\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Hans\" typeface=\"宋体\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Hant\" typeface=\"新細明體\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Arab\" typeface=\"Times New Roman\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Hebr\" typeface=\"Times New Roman\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Thai\" typeface=\"Tahoma\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Ethi\" typeface=\"Nyala\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Beng\" typeface=\"Vrinda\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Gujr\" typeface=\"Shruti\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Khmr\" typeface=\"MoolBoran\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Knda\" typeface=\"Tunga\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Guru\" typeface=\"Raavi\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Cans\" typeface=\"Euphemia\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Cher\" typeface=\"Plantagenet Cherokee\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Yiii\" typeface=\"Microsoft Yi Baiti\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Tibt\" typeface=\"Microsoft Himalaya\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Thaa\" typeface=\"MV Boli\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Deva\" typeface=\"Mangal\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Telu\" typeface=\"Gautami\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Taml\" typeface=\"Latha\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Syrc\" typeface=\"Estrangelo Edessa\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Orya\" typeface=\"Kalinga\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Mlym\" typeface=\"Kartika\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Laoo\" typeface=\"DokChampa\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Sinh\" typeface=\"Iskoola Pota\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Mong\" typeface=\"Mongolian Baiti\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Viet\" typeface=\"Times New Roman\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Uigh\" typeface=\"Microsoft Uighur\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Geor\" typeface=\"Sylfaen\"/>\r\n" +
                    "\t\t\t</a:majorFont>\r\n" +
                    "\t\t\t<a:minorFont>\r\n" +
                    "\t\t\t\t<a:latin typeface=\"Calibri\"/>\r\n" +
                    "\t\t\t\t<a:ea typeface=\"\"/>\r\n" +
                    "\t\t\t\t<a:cs typeface=\"\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Jpan\" typeface=\"ＭＳ Ｐゴシック\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Hang\" typeface=\"맑은 고딕\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Hans\" typeface=\"宋体\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Hant\" typeface=\"新細明體\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Arab\" typeface=\"Arial\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Hebr\" typeface=\"Arial\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Thai\" typeface=\"Tahoma\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Ethi\" typeface=\"Nyala\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Beng\" typeface=\"Vrinda\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Gujr\" typeface=\"Shruti\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Khmr\" typeface=\"DaunPenh\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Knda\" typeface=\"Tunga\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Guru\" typeface=\"Raavi\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Cans\" typeface=\"Euphemia\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Cher\" typeface=\"Plantagenet Cherokee\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Yiii\" typeface=\"Microsoft Yi Baiti\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Tibt\" typeface=\"Microsoft Himalaya\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Thaa\" typeface=\"MV Boli\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Deva\" typeface=\"Mangal\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Telu\" typeface=\"Gautami\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Taml\" typeface=\"Latha\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Syrc\" typeface=\"Estrangelo Edessa\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Orya\" typeface=\"Kalinga\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Mlym\" typeface=\"Kartika\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Laoo\" typeface=\"DokChampa\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Sinh\" typeface=\"Iskoola Pota\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Mong\" typeface=\"Mongolian Baiti\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Viet\" typeface=\"Arial\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Uigh\" typeface=\"Microsoft Uighur\"/>\r\n" +
                    "\t\t\t\t<a:font script=\"Geor\" typeface=\"Sylfaen\"/>\r\n" +
                    "\t\t\t</a:minorFont>\r\n" +
                    "\t\t</a:fontScheme>\r\n" +
                    "\t\t<a:fmtScheme name=\"Стандартная\">\r\n" +
                    "\t\t\t<a:fillStyleLst>\r\n" +
                    "\t\t\t\t<a:solidFill>\r\n" +
                    "\t\t\t\t\t<a:schemeClr val=\"phClr\"/>\r\n" +
                    "\t\t\t\t</a:solidFill>\r\n" +
                    "\t\t\t\t<a:gradFill rotWithShape=\"1\">\r\n" +
                    "\t\t\t\t\t<a:gsLst>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"0\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:tint val=\"50000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:satMod val=\"300000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"35000\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:tint val=\"37000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:satMod val=\"300000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"100000\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:tint val=\"15000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:satMod val=\"350000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t</a:gsLst>\r\n" +
                    "\t\t\t\t\t<a:lin ang=\"16200000\" scaled=\"1\"/>\r\n" +
                    "\t\t\t\t</a:gradFill>\r\n" +
                    "\t\t\t\t<a:gradFill rotWithShape=\"1\">\r\n" +
                    "\t\t\t\t\t<a:gsLst>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"0\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:shade val=\"51000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:satMod val=\"130000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"80000\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:shade val=\"93000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:satMod val=\"130000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"100000\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:shade val=\"94000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:satMod val=\"135000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t</a:gsLst>\r\n" +
                    "\t\t\t\t\t<a:lin ang=\"16200000\" scaled=\"0\"/>\r\n" +
                    "\t\t\t\t</a:gradFill>\r\n" +
                    "\t\t\t</a:fillStyleLst>\r\n" +
                    "\t\t\t<a:lnStyleLst>\r\n" +
                    "\t\t\t\t<a:ln w=\"9525\" cap=\"flat\" cmpd=\"sng\" algn=\"ctr\">\r\n" +
                    "\t\t\t\t\t<a:solidFill>\r\n" +
                    "\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t<a:shade val=\"95000\"/>\r\n" +
                    "\t\t\t\t\t\t\t<a:satMod val=\"105000\"/>\r\n" +
                    "\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t</a:solidFill>\r\n" +
                    "\t\t\t\t\t<a:prstDash val=\"solid\"/>\r\n" +
                    "\t\t\t\t</a:ln>\r\n" +
                    "\t\t\t\t<a:ln w=\"25400\" cap=\"flat\" cmpd=\"sng\" algn=\"ctr\">\r\n" +
                    "\t\t\t\t\t<a:solidFill>\r\n" +
                    "\t\t\t\t\t\t<a:schemeClr val=\"phClr\"/>\r\n" +
                    "\t\t\t\t\t</a:solidFill>\r\n" +
                    "\t\t\t\t\t<a:prstDash val=\"solid\"/>\r\n" +
                    "\t\t\t\t</a:ln>\r\n" +
                    "\t\t\t\t<a:ln w=\"38100\" cap=\"flat\" cmpd=\"sng\" algn=\"ctr\">\r\n" +
                    "\t\t\t\t\t<a:solidFill>\r\n" +
                    "\t\t\t\t\t\t<a:schemeClr val=\"phClr\"/>\r\n" +
                    "\t\t\t\t\t</a:solidFill>\r\n" +
                    "\t\t\t\t\t<a:prstDash val=\"solid\"/>\r\n" +
                    "\t\t\t\t</a:ln>\r\n" +
                    "\t\t\t</a:lnStyleLst>\r\n" +
                    "\t\t\t<a:effectStyleLst>\r\n" +
                    "\t\t\t\t<a:effectStyle>\r\n" +
                    "\t\t\t\t\t<a:effectLst>\r\n" +
                    "\t\t\t\t\t\t<a:outerShdw blurRad=\"40000\" dist=\"20000\" dir=\"5400000\" rotWithShape=\"0\">\r\n" +
                    "\t\t\t\t\t\t\t<a:srgbClr val=\"000000\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:alpha val=\"38000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:srgbClr>\r\n" +
                    "\t\t\t\t\t\t</a:outerShdw>\r\n" +
                    "\t\t\t\t\t</a:effectLst>\r\n" +
                    "\t\t\t\t</a:effectStyle>\r\n" +
                    "\t\t\t\t<a:effectStyle>\r\n" +
                    "\t\t\t\t\t<a:effectLst>\r\n" +
                    "\t\t\t\t\t\t<a:outerShdw blurRad=\"40000\" dist=\"23000\" dir=\"5400000\" rotWithShape=\"0\">\r\n" +
                    "\t\t\t\t\t\t\t<a:srgbClr val=\"000000\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:alpha val=\"35000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:srgbClr>\r\n" +
                    "\t\t\t\t\t\t</a:outerShdw>\r\n" +
                    "\t\t\t\t\t</a:effectLst>\r\n" +
                    "\t\t\t\t</a:effectStyle>\r\n" +
                    "\t\t\t\t<a:effectStyle>\r\n" +
                    "\t\t\t\t\t<a:effectLst>\r\n" +
                    "\t\t\t\t\t\t<a:outerShdw blurRad=\"40000\" dist=\"23000\" dir=\"5400000\" rotWithShape=\"0\">\r\n" +
                    "\t\t\t\t\t\t\t<a:srgbClr val=\"000000\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:alpha val=\"35000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:srgbClr>\r\n" +
                    "\t\t\t\t\t\t</a:outerShdw>\r\n" +
                    "\t\t\t\t\t</a:effectLst>\r\n" +
                    "\t\t\t\t\t<a:scene3d>\r\n" +
                    "\t\t\t\t\t\t<a:camera prst=\"orthographicFront\">\r\n" +
                    "\t\t\t\t\t\t\t<a:rot lat=\"0\" lon=\"0\" rev=\"0\"/>\r\n" +
                    "\t\t\t\t\t\t</a:camera>\r\n" +
                    "\t\t\t\t\t\t<a:lightRig rig=\"threePt\" dir=\"t\">\r\n" +
                    "\t\t\t\t\t\t\t<a:rot lat=\"0\" lon=\"0\" rev=\"1200000\"/>\r\n" +
                    "\t\t\t\t\t\t</a:lightRig>\r\n" +
                    "\t\t\t\t\t</a:scene3d>\r\n" +
                    "\t\t\t\t\t<a:sp3d>\r\n" +
                    "\t\t\t\t\t<a:bevelT w=\"63500\" h=\"25400\"/>\r\n" +
                    "\t\t\t\t\t</a:sp3d>\r\n" +
                    "\t\t\t\t</a:effectStyle>\r\n" +
                    "\t\t\t</a:effectStyleLst>\r\n" +
                    "\t\t\t<a:bgFillStyleLst>\r\n" +
                    "\t\t\t\t<a:solidFill>\r\n" +
                    "\t\t\t\t\t<a:schemeClr val=\"phClr\"/>\r\n" +
                    "\t\t\t\t</a:solidFill>\r\n" +
                    "\t\t\t\t<a:gradFill rotWithShape=\"1\">\r\n" +
                    "\t\t\t\t\t<a:gsLst>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"0\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t<a:tint val=\"40000\"/>\r\n" +
                    "\t\t\t\t\t\t\t<a:satMod val=\"350000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"40000\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:tint val=\"45000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:shade val=\"99000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:satMod val=\"350000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"100000\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:shade val=\"20000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:satMod val=\"255000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t</a:gsLst>\r\n" +
                    "\t\t\t\t\t<a:path path=\"circle\">\r\n" +
                    "\t\t\t\t\t\t<a:fillToRect l=\"50000\" t=\"-80000\" r=\"50000\" b=\"180000\"/>\r\n" +
                    "\t\t\t\t\t</a:path>\r\n" +
                    "\t\t\t\t</a:gradFill>\r\n" +
                    "\t\t\t\t<a:gradFill rotWithShape=\"1\">\r\n" +
                    "\t\t\t\t\t<a:gsLst>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"0\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:tint val=\"80000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:satMod val=\"300000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t\t<a:gs pos=\"100000\">\r\n" +
                    "\t\t\t\t\t\t\t<a:schemeClr val=\"phClr\">\r\n" +
                    "\t\t\t\t\t\t\t\t<a:shade val=\"30000\"/>\r\n" +
                    "\t\t\t\t\t\t\t\t<a:satMod val=\"200000\"/>\r\n" +
                    "\t\t\t\t\t\t\t</a:schemeClr>\r\n" +
                    "\t\t\t\t\t\t</a:gs>\r\n" +
                    "\t\t\t\t\t</a:gsLst>\r\n" +
                    "\t\t\t\t\t<a:path path=\"circle\">\r\n" +
                    "\t\t\t\t\t\t<a:fillToRect l=\"50000\" t=\"50000\" r=\"50000\" b=\"50000\"/>\r\n" +
                    "\t\t\t\t\t</a:path>\r\n" +
                    "\t\t\t\t</a:gradFill>\r\n" +
                    "\t\t\t</a:bgFillStyleLst>\r\n" +
                    "\t\t</a:fmtScheme>\r\n" +
                    "\t</a:themeElements>\r\n" +
                    "\t<a:objectDefaults/>\r\n" +
                    "\t<a:extraClrSchemeLst/>\r\n" +
                    "</a:theme>"
                );

                return stringBuilder.ToString();
            }
            private string GetFileContentForStyles()
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
			        "<styleSheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" mc:Ignorable=\"x14ac\" xmlns:x14ac=\"http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac\">\r\n"
                );

                // Number formats
                stringBuilder.Append($"\t<numFmts count=\"{SortedSharedNumberFormats.Count}\" x14ac:knownFonts=\"1\">\r\n");
                for (int sharedNumberFormatsIndex = 0; sharedNumberFormatsIndex < SortedSharedNumberFormats.Count; sharedNumberFormatsIndex++)
                {
                    ExcelNumberFormat sharedNumberFormats = SortedSharedNumberFormats[sharedNumberFormatsIndex];
			        stringBuilder.Append($"\t\t<numFmt numFmtId=\"{sharedNumberFormatsIndex + 164 /* Shared number format ids start with 164 as lower values are reserved for built-in formats */}\" formatCode=\"{sharedNumberFormats.FormatCode}\">\r\n");
                }
			    stringBuilder.Append("\t</numFmts>\r\n");

                // Fonts
                stringBuilder.Append($"\t<fonts count=\"{SortedSharedFonts.Count}\" x14ac:knownFonts=\"1\">\r\n");
			    //stringBuilder.Append(
			    //    "\t\t<font>\r\n" +
			    //    "\t\t\t<sz val=\"11\"/>\r\n" +
			    //    "\t\t\t<color theme=\"1\"/>\r\n" +
			    //    "\t\t\t<name val=\"Calibri\"/>\r\n" +
			    //    "\t\t\t<family val=\"2\"/>\r\n" +
			    //    "\t\t\t<charset val=\"204\"/>\r\n" +
			    //    "\t\t\t<scheme val=\"minor\"/>\r\n" +
			    //    "\t\t</font>\r\n" +
			    //    "\t\t<font>\r\n" +
			    //    "\t\t\t<b/>\r\n" +
			    //    "\t\t\t<vertAlign val=\"subscript\"/>\r\n" +
			    //    "\t\t\t<sz val=\"11\"/>\r\n" +
			    //    "\t\t\t<color theme=\"1\"/>\r\n" +
			    //    "\t\t\t<name val=\"Calibri\"/>\r\n" +
			    //    "\t\t\t<family val=\"2\"/>\r\n" +
			    //    "\t\t\t<charset val=\"204\"/>\r\n" +
			    //    "\t\t\t<scheme val=\"minor\"/>\r\n" +
			    //    "\t\t</font>\r\n"
       //         );
                foreach (ExcelSharedFont font in SortedSharedFonts)
                {
			        stringBuilder.Append
                    (
                        "\t\t<font>\r\n" +
                        ((font.Style & FontStyle.Bold) != 0 ? "\t\t\t<b/>\r\n" : "") +
                        ((font.Style & FontStyle.Italic) != 0 ? "\t\t\t<i/>\r\n" : "") +
                        ((font.Style & FontStyle.Underline) != 0 ? "\t\t\t<u/>\r\n" : "") +
                        ((font.Style & FontStyle.Strikeout) != 0 ? "\t\t\t<strike/>\r\n" : "") +
                        $"\t\t\t<sz val=\"{font.Size}\"/>\r\n" +
                        $"\t\t\t<name val=\"{font.Family}\"/>\r\n" +
                        $"\t\t\t<color rgb=\"{font.Color.ToHexArgb()}\"/>\r\n" +
                        //$"\t\t\t<family val=\"{}\"/>" +
                        //$"\t\t\t<charset val=\"{}\"/>" +
			            "\t\t</font>\r\n"
                    );
                }
                stringBuilder.Append("\t</fonts>\r\n");

                // Fills
                stringBuilder.Append($"\t<fills count=\"{SortedSharedBackgrounds.Count}\">\r\n");
                foreach (ExcelSharedBackground background in SortedSharedBackgrounds)
                {
			        stringBuilder.Append("\t\t<fill>\r\n");
               //     if (background.Color == Color.Transparent && background.Pattern != ExcelBackgroundPattern.GRAY125)
               //     {
			            //stringBuilder.Append("\t\t\t<patternFill patternType=\"none\"/>\r\n");
               //     }
               //     else if (background.Pattern == ExcelBackgroundPattern.GRAY125)
               //     {
			            //stringBuilder.Append("\t\t\t<patternFill patternType=\"gray125\"/>\r\n");
               //     }
               //     else if (background.Pattern == ExcelBackgroundPattern.SOLID)
               //     {
			            //stringBuilder.Append
               //         (
               //             "\t\t\t<patternFill patternType=\"solid\">\r\n" +
               //             $"\t\t\t\t<fgColor rgb=\"{background.Color.ToHexArgb()}\"/>\r\n" +
               //             "\t\t\t</patternFill>\r\n"
               //         );
               //     }
               //     else
               //     {
               //         throw new Exception($"Background pattern {background.Pattern} is not supported.");
               //     }
                    switch (background.Pattern)
                    {
                        case ExcelBackgroundPattern.NONE:
                            stringBuilder.Append("\t\t\t<patternFill patternType=\"none\"/>\r\n");
                            break;
                        case ExcelBackgroundPattern.GRAY125:
                            stringBuilder.Append("\t\t\t<patternFill patternType=\"gray125\"/>\r\n");
                            break;
                        case ExcelBackgroundPattern.SOLID:
                            stringBuilder.Append
                            (
                                "\t\t\t<patternFill patternType=\"solid\">\r\n" +
                                $"\t\t\t\t<fgColor rgb=\"{background.Color.ToHexArgb()}\"/>\r\n" +
                                "\t\t\t</patternFill>\r\n"
                            );
                            break;
                        default:
                            throw new Exception($"Background pattern {background.Pattern} is not supported.");
                    }
                    stringBuilder.Append("\t\t</fill>\r\n");
                }
                stringBuilder.Append("\t</fills>\r\n");

                // Borders
                stringBuilder.Append($"\t<borders count=\"{SortedSharedBorders.Count}\">\r\n");
                foreach (ExcelSharedBorder border in SortedSharedBorders)
                {
                    stringBuilder.Append("\t\t<border>\r\n");
                    stringBuilder.Append(border.Left != null   && border.Left.Style   != ExcelBorderLineStyle.NONE ? $"\t\t\t<left style=\"{ExcelBorderStyleToString(border.Left.Style)}\">\r\n\t\t\t\t<color rgb=\"{border.Left.Color.ToHexArgb()}\"/>\r\n\t\t\t</left>\r\n"         : "\t\t\t<left/>\r\n");
                    stringBuilder.Append(border.Right != null  && border.Right.Style  != ExcelBorderLineStyle.NONE ? $"\t\t\t<right style=\"{ExcelBorderStyleToString(border.Right.Style)}\">\r\n\t\t\t\t<color rgb=\"{border.Right.Color.ToHexArgb()}\"/>\r\n\t\t\t</right>\r\n"     : "\t\t\t<right/>\r\n");
                    stringBuilder.Append(border.Top != null    && border.Top.Style    != ExcelBorderLineStyle.NONE ? $"\t\t\t<top style=\"{ExcelBorderStyleToString(border.Top.Style)}\">\r\n\t\t\t\t<color rgb=\"{border.Top.Color.ToHexArgb()}\"/>\r\n\t\t\t</top>\r\n"             : "\t\t\t<top/>\r\n");
                    stringBuilder.Append(border.Bottom != null && border.Bottom.Style != ExcelBorderLineStyle.NONE ? $"\t\t\t<bottom style=\"{ExcelBorderStyleToString(border.Bottom.Style)}\">\r\n\t\t\t\t<color rgb=\"{border.Bottom.Color.ToHexArgb()}\"/>\r\n\t\t\t</bottom>\r\n" : "\t\t\t<bottom/>\r\n");
                    stringBuilder.Append(
                        "\t\t\t<diagonal/>\r\n" +
                        "\t\t</border>\r\n"
                    );
                }
                stringBuilder.Append("\t</borders>\r\n");

                // Master styles
                stringBuilder.Append("\t<cellStyleXfs count=\"1\">\r\n");
                stringBuilder.Append("\t\t<xf numFmtId=\"0\" fontId=\"0\" fillId=\"0\" borderId=\"0\" applyNumberFormat=\"false\" applyFont=\"false\"  applyFill=\"false\" applyBorder=\"false\" applyAlignment=\"false\" applyProtection=\"false\"/>\r\n");
                stringBuilder.Append("\t</cellStyleXfs>\r\n");

                // Direct tyles
                stringBuilder.Append($"\t<cellXfs count=\"{SortedSharedStyles.Count}\">\r\n");
                for (int index = 0; index < SortedSharedStyles.Count; index++)
                {
                    ExcelSharedStyle style = SortedSharedStyles[index];
                    // xfId is a reference to a master style
                    stringBuilder.Append($"\t\t<xf numFmtId=\"0\" fontId=\"{style.Font.IndexInTempArray}\" fillId=\"{style.Background.IndexInTempArray}\" borderId=\"{style.Border.IndexInTempArray}\" xfId=\"0\">\r\n");
                    stringBuilder.Append($"\t\t\t<alignment horizontal=\"{Excel.ToString(style.Font.AlignmentHorizontal)}\" vertical=\"{Excel.ToString(style.Font.AlignmentVertical)}\" textRotation=\"0\" wrapText=\"{(style.Font.Wrap ? "true" : "false")}\" indent=\"0\" shrinkToFit=\"false\"/>\r\n");
                    stringBuilder.Append($"\t\t</xf>\r\n");
                }
                stringBuilder.Append("\t</cellXfs>\r\n");

                // Named styles
			    stringBuilder.Append($"\t<cellStyles count=\"1\">\r\n");
                stringBuilder.Append($"\t\t<cellStyle name=\"Default\" xfId=\"0\" builtinId=\"0\"/>\r\n");
                //for (int index = 0; index < SharedBorders.Count; index++)
                //{
                //    stringBuilder.Append($"\t\t<cellStyle name=\"Обычный\" xfId=\"0\" builtinId=\"{index}\"/>\r\n");
                //}
                stringBuilder.Append("\t</cellStyles>\r\n");

                // Rest
                stringBuilder.Append(
			        "\t<dxfs count=\"0\"/>\r\n" +
			        "\t<tableStyles count=\"0\" defaultTableStyle=\"TableStyleMedium2\" defaultPivotStyle=\"PivotStyleLight16\"/>\r\n" +
			        "\t<extLst>\r\n" +
			        "\t\t<ext uri=\"{EB79DEF2-80B8-43e5-95BD-54CBDDF9020C}\" xmlns:x14=\"http://schemas.microsoft.com/office/spreadsheetml/2009/9/main\">\r\n" +
			        "\t\t\t<x14:slicerStyles defaultSlicerStyle=\"SlicerStyleLight1\"/>\r\n" +
			        "\t\t</ext>\r\n" +
			        "\t</extLst>\r\n" +
			        "</styleSheet>"
                );

                return stringBuilder.ToString();
            }
            private string GetFileContentForContentTypes()
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
			        "<Types xmlns=\"http://schemas.openxmlformats.org/package/2006/content-types\">\r\n" +
                    //"\t<Default Extension=\"bin\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.printerSettings\"/>\r\n" +
			        "\t<Default Extension=\"rels\" ContentType=\"application/vnd.openxmlformats-package.relationships+xml\"/>\r\n" +
			        "\t<Default Extension=\"xml\" ContentType=\"application/xml\"/>\r\n" +
			        "\t<Override PartName=\"/xl/workbook.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml\"/>\r\n"
                );
                for (int index = 0; index < Sheets.Count; index++)
                {
                    ExcelSheet sheet = Sheets[index];
                    stringBuilder.Append($"\t<Override PartName=\"/xl/worksheets/sheet{index + 1}.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml\"/>\r\n");
                }
                stringBuilder.Append(
                    "\t<Override PartName=\"/xl/theme/theme1.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.theme+xml\"/>\r\n" +
			        "\t<Override PartName=\"/xl/styles.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml\"/>\r\n" +
			        "\t<Override PartName=\"/xl/sharedStrings.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml\"/>\r\n" +
			        "\t<Override PartName=\"/docProps/core.xml\" ContentType=\"application/vnd.openxmlformats-package.core-properties+xml\"/>\r\n" +
			        "\t<Override PartName=\"/docProps/app.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.extended-properties+xml\"/>\r\n" +
			        "</Types>"
                );

                return stringBuilder.ToString();
            }
            private string GetFileContentForRels()
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
			        "<Relationships xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\">\r\n" +
			        "\t<Relationship Id=\"rId3\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties\" Target=\"docProps/app.xml\"/>\r\n" +
			        "\t<Relationship Id=\"rId2\" Type=\"http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties\" Target=\"docProps/core.xml\"/>\r\n" +
			        "\t<Relationship Id=\"rId1\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument\" Target=\"xl/workbook.xml\"/>\r\n" +
			        "</Relationships>"
                );

                return stringBuilder.ToString();
            }
            private string GetFileContentForBook()
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
			        "<workbook xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\r\n" +
			        "\t<fileVersion appName=\"xl\" lastEdited=\"5\" lowestEdited=\"5\" rupBuild=\"9303\"/>\r\n" +
			        "\t<workbookPr defaultThemeVersion=\"124226\"/>\r\n" +
			        //"\t<bookViews>\r\n" +
			        //"\t\t<workbookView xWindow=\"360\" yWindow=\"75\" windowWidth=\"22995\" windowHeight=\"13605\" activeTab=\"3\"/>\r\n" +
			        //"\t</bookViews>\r\n" +
			        "\t<sheets>\r\n"
                );
                for (int index = 0; index < Sheets.Count; index++)
                {
                    ExcelSheet sheet = Sheets[index];
                    stringBuilder.Append($"\t\t<sheet name=\"{sheet.Name}\" sheetId=\"{index + 1}\" r:id=\"rId{index + 1}\"/>\r\n"); // here sheetId started with 4: 4, 5, 6 and 7
                }
                stringBuilder.Append(
                    "\t</sheets>\r\n" +
			        "\t<calcPr calcId=\"145621\"/>\r\n" +
			        "</workbook>"
                );

                return stringBuilder.ToString();
            }
            private string GetFileContentForBookRels()
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
                    "<Relationships xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\">\r\n"
                );
                for (int index = 0; index < Sheets.Count; index++)
                {
                    ExcelSheet sheet = Sheets[index];
                    stringBuilder.Append($"<Relationship Id=\"rId{index + 1}\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet\" Target=\"worksheets/sheet{index + 1}.xml\"/>\r\n");
                }
                int count = Sheets.Count + 1;
                stringBuilder.Append(
                    //$"<Relationship Id=\"rId{count++}\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme\" Target=\"theme/theme1.xml\"/>\r\n" +
			        $"<Relationship Id=\"rId{count++}\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles\" Target=\"styles.xml\"/>\r\n" +
			        $"<Relationship Id=\"rId{count++}\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings\" Target=\"sharedStrings.xml\"/>\r\n" +
			        "</Relationships>"
                );

                return stringBuilder.ToString();
            }
            private string GetFileContentForApp()
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
			        "<Properties xmlns=\"http://schemas.openxmlformats.org/officeDocument/2006/extended-properties\" xmlns:vt=\"http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes\">\r\n" +
			        "\t<Application>Microsoft Excel</Application>\r\n" +
			        "\t<DocSecurity>0</DocSecurity>\r\n" +
			        "\t<ScaleCrop>false</ScaleCrop>\r\n" +
			        "\t<HeadingPairs>\r\n" +
			        "\t\t<vt:vector size=\"2\" baseType=\"variant\">\r\n" +
			        "\t\t\t<vt:variant><vt:lpstr>Листы</vt:lpstr></vt:variant>\r\n" +
			        $"\t\t\t<vt:variant><vt:i4>{Sheets.Count}</vt:i4></vt:variant>\r\n" +
			        "\t\t</vt:vector>\r\n" +
			        "\t</HeadingPairs>\r\n" +
			        "\t<TitlesOfParts>\r\n" +
			        $"\t\t<vt:vector size=\"{Sheets.Count}\" baseType=\"lpstr\">\r\n"
                );
                for (int index = 0; index < Sheets.Count; index++)
                {
                    ExcelSheet sheet = Sheets[index];
                    stringBuilder.Append($"\t\t\t<vt:lpstr>{sheet.Name}</vt:lpstr>\r\n");
                }
                stringBuilder.Append(
                    "\t\t</vt:vector>\r\n" +
			        "\t</TitlesOfParts>\r\n" +
			        "\t<Company>Irkut Corporation</Company>\r\n" +
			        "\t<LinksUpToDate>false</LinksUpToDate>\r\n" +
			        "\t<SharedDoc>false</SharedDoc>\r\n" +
			        "\t<HyperlinksChanged>false</HyperlinksChanged>\r\n" +
			        "\t<AppVersion>14.0300</AppVersion>\r\n" +
			        "</Properties>"
                );

                return stringBuilder.ToString();
            }
            private string GetFileContentForCore()
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
			        "<cp:coreProperties xmlns:cp=\"http://schemas.openxmlformats.org/package/2006/metadata/core-properties\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:dcterms=\"http://purl.org/dc/terms/\" xmlns:dcmitype=\"http://purl.org/dc/dcmitype/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\r\n" +
			        $"\t<dc:creator>{CreatorName}</dc:creator>\r\n" +
			        $"\t<cp:lastModifiedBy>{CreatorName}</cp:lastModifiedBy>\r\n" +
			        $"\t<dcterms:created xsi:type=\"dcterms:W3CDTF\">{DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'")}</dcterms:created>\r\n" +
			        $"\t<dcterms:modified xsi:type=\"dcterms:W3CDTF\">{DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'")}</dcterms:modified>\r\n" +
			        "</cp:coreProperties>"
                );

                return stringBuilder.ToString();
            }

            private string GetFileContentForSharedStrings()
            {
                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.Append(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" +
                    $"<sst xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">\r\n"// count=\"{SharedStrings.Sum(_sharedString => _sharedString.Length)}\" uniqueCount=\"{SharedStrings.Count}\">\r\n"
                );
                int counter = 0;
                foreach (KeyValuePair<string, ExcelSharedString> stringAndSharedString in StringToSharedString)
                {
                    stringAndSharedString.Value.IndexInTempArray = counter++;
                    stringBuilder.Append($"<si><t>{stringAndSharedString.Value.String}</t></si>\r\n");
                }
                stringBuilder.Append("</sst>");

                return stringBuilder.ToString();
            }
            #endregion

            #region Shared
            //private void UpdateSharedStrings()
            //{
            //    SharedStringToIndex.Clear();
            //    SharedStrings.Clear();
            //    foreach (ExcelSheet sheet in Sheets)
            //    {
            //        foreach (ExcelCell cell in sheet.Cells)
            //        {
            //            if (cell.Text == null)
            //            {
            //                cell.TextSharedIndex = -1;
            //                continue;
            //            }
            //            if (!SharedStringToIndex.TryGetValue(cell.Text, out int index))
            //            {
            //                index = SharedStrings.Count;
            //                SharedStrings.Add(cell.Text);
            //                SharedStringToIndex.Add(cell.Text, index);
            //            }
            //            cell.TextSharedIndex = index;
            //        }
            //    }
            //}
            //private void UpdateSharedBorders()
            //{
            //    SharedBorders.Clear();
            //    // No-border entry
            //    SharedBorders.Add(new ExcelBorder()
            //    {
            //        Left   = null,
            //        Top    = null,
            //        Right  = null,
            //        Bottom = null,
            //        //Left   = new ExcelBorderLine(ExcelBorderStyle.NONE, Color.Black),
            //        //Top    = new ExcelBorderLine(ExcelBorderStyle.NONE, Color.Black),
            //        //Right  = new ExcelBorderLine(ExcelBorderStyle.NONE, Color.Black),
            //        //Bottom = new ExcelBorderLine(ExcelBorderStyle.NONE, Color.Black),
            //    });

            //    foreach (ExcelSheet sheet in Sheets)
            //    {
            //        foreach (ExcelCell cell in sheet.Cells)
            //        {
            //            ExcelBorder cellBorder = sheet.GetBorder(cell);
            //            int index = SharedBorders.FindIndex(_border => _border == cellBorder);
            //            if (index < 0)
            //            {
            //                index = SharedBorders.Count;
            //                SharedBorders.Add(cellBorder);
            //            }
            //            cell.StyleSharedIndex = index;
            //        }
            //    }
            //}
            internal ExcelSharedString GetSharedText(string @string)
            {
                if (!StringToSharedString.TryGetValue(@string, out ExcelSharedString sharedString))
                {
                    sharedString = new ExcelSharedString(@string);
                    StringToSharedString.Add(@string, sharedString);
                }
                sharedString.Counter++;
                return sharedString;
            }
            internal void UngetSharedText(ExcelSharedString sharedString)
            {
                sharedString.Counter--;
                if (sharedString.Counter <= 0)
                {
                    StringToSharedString.Remove(sharedString.String);
                }
            }
            internal ExcelSharedFont GetSharedFont(string family, int size, Color color, FontStyle style, TextAlignmentHorizontal alignmentHorizontal, TextAlignmentVertical alignmentVertical, bool wrap)
            {
                foreach (ExcelSharedFont sharedFont in SharedFonts)
                {
                    if (sharedFont.Family == family &&
                        sharedFont.Size == size &&
                        sharedFont.Color == color &&
                        sharedFont.Style == style &&
                        sharedFont.AlignmentHorizontal == alignmentHorizontal &&
                        sharedFont.AlignmentVertical == alignmentVertical &&
                        sharedFont.Wrap == wrap)
                    {
                        sharedFont.Counter--;
                        return sharedFont;
                    }
                }
                ExcelSharedFont font = new ExcelSharedFont(family, size, color, style, alignmentHorizontal, alignmentVertical, wrap);
                font.Counter++;
                SharedFonts.Add(font);
                return font;
            }
            internal void UngetSharedFont(ExcelSharedFont font)
            {
                font.Counter--;
                if (font.Counter <= 0)
                {
                    SharedFonts.Remove(font);
                }
            }
            internal ExcelSharedBackground GetSharedBackground(Color color, ExcelBackgroundPattern pattern)
            {
                foreach (ExcelSharedBackground sharedBackground in SharedBackgrounds)
                {
                    if (sharedBackground.Color == color &&
                        sharedBackground.Pattern == pattern)
                    {
                        sharedBackground.Counter++;
                        return sharedBackground;
                    }
                }
                ExcelSharedBackground background = new ExcelSharedBackground(color, pattern);
                background.Counter++;
                SharedBackgrounds.Add(background);
                return background;
            }
            internal void UngetSharedBackground(ExcelSharedBackground background)
            {
                background.Counter--;
                if (background.Counter <= 0)
                {
                    SharedBackgrounds.Remove(background);
                }
            }
            internal ExcelSharedBorder GetSharedBorder(ExcelBorderLine left, ExcelBorderLine top, ExcelBorderLine right, ExcelBorderLine bottom)
            {
                foreach (ExcelSharedBorder sharedBorder in SharedBorders)
                {
                    if (sharedBorder.Left == left &&
                        sharedBorder.Top == top &&
                        sharedBorder.Right == right &&
                        sharedBorder.Bottom == bottom)
                    {
                        sharedBorder.Counter++;
                        return sharedBorder;
                    }
                }
                ExcelSharedBorder border = new ExcelSharedBorder(left, top, right, bottom);
                border.Counter++;
                SharedBorders.Add(border);
                return border;
            }
            internal void UngetSharedBorder(ExcelSharedBorder border)
            {
                border.Counter--;
                if (border.Counter <= 0)
                {
                    SharedBorders.Remove(border);
                }
            }
            internal ExcelSharedStyle GetSharedStyle(ExcelSharedFont font, ExcelSharedBackground background, ExcelSharedBorder border)
            {
                foreach (ExcelSharedStyle sharedStyle in SharedStyles)
                {
                    if (sharedStyle.Font == font &&
                        sharedStyle.Background == background &&
                        sharedStyle.Border == border)
                    {
                        return sharedStyle;
                    }
                }
                ExcelSharedStyle style = new ExcelSharedStyle(font, background, border);
                SharedStyles.Add(style);
                return style;
            }
            internal void UngetSharedStyle(ExcelSharedStyle style)
            {
                style.Counter--;
                if (style.Counter <= 0)
                {
                    SharedStyles.Remove(style);
                }
            }
            #endregion

            #region Util
            #endregion
        }
    }
}


#endif