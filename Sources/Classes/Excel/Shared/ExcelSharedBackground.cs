﻿#if !UNITY_ENGINE

using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        internal class ExcelSharedBackground : ExcelSharedItem
        {
            internal Color Color;
            internal ExcelBackgroundPattern Pattern;

            public static readonly Color ColorDefault = Color.Transparent;
            public static readonly ExcelBackgroundPattern PatternDefault = ExcelBackgroundPattern.SOLID;

            internal ExcelSharedBackground()
                : this (ColorDefault, PatternDefault) { }
            internal ExcelSharedBackground(Color color)
                : this (color, PatternDefault) { }
            internal ExcelSharedBackground(ExcelBackgroundPattern pattern)
                : this (ColorDefault, pattern) { }
            internal ExcelSharedBackground(Color color, ExcelBackgroundPattern pattern)
            {
                Color = color;
                Pattern = pattern;
            }
        }
    }
}

#endif