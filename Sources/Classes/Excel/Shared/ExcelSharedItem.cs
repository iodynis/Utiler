﻿#if !UNITY_ENGINE

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        internal class ExcelSharedItem
        {
            internal int IndexInTempArray = -1;
            internal int Counter = 0;
        }
    }
}

#endif