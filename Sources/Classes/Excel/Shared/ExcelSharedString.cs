﻿#if !UNITY_ENGINE

namespace Iodynis.Libraries.Utility
{
    public partial class Excel
    {
        internal class ExcelSharedString : ExcelSharedItem
        {
            internal string String;

            internal ExcelSharedString(string @string)
            {
                String = @string;
            }
        }
    }
}

#endif