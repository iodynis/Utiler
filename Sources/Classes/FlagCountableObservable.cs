﻿using System;

namespace Iodynis.Libraries.Utility
{
    /// <summary>
    /// Countable observable flag.
    /// </summary>
    public sealed class FlagCountableObservable
    {
        private sealed class Wrapper : IDisposable
        {
            private FlagCountableObservable Flag;

            /// <summary>
            /// Save and set the flag
            /// </summary>
            public Wrapper(FlagCountableObservable flag)
            {
                if (flag == null)
                {
                    throw new ArgumentNullException(nameof(flag));
                }

                Flag = flag;
                Flag.Set();
            }

            /// <summary>
            /// Unset the flag and dispose
            /// </summary>
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// <summary>
            /// Dispose
            /// </summary>
            public void Dispose(bool disposing)
            {
                if (disposing)
                {
                    Flag.Unset();
                }
            }
        }

        /// <summary>
        /// Flag set/unset counter.
        /// </summary>
        public int Counter { get; private set; } = 0;
        private bool AllowCountGoNegative;

        /// <summary>
        /// Fires when the flag is set.
        /// </summary>
        public event Action OnSet;
        /// <summary>
        /// Fires when the flag is unset or reset.
        /// </summary>
        public event Action OnUnSet;

        /// <summary>
        /// Create a countable observable flag.
        /// </summary>
        /// <param name="allowCountGoNegative">Allow the <see cref="Counter"/> to be negative.</param>
        public FlagCountableObservable(bool allowCountGoNegative = false)
        {
            AllowCountGoNegative = allowCountGoNegative;
        }

        /// <summary>
        /// Use the flag in a scope.
        /// </summary>
        public IDisposable Using
        {
            get
            {
                return new Wrapper(this);
            }
        }

        /// <summary>
        /// Set the flag.
        /// </summary>
        public void Set()
        {
            Counter++;

            if (Counter == 1)
            {
                OnSet?.Invoke();
            }
        }

        /// <summary>
        /// Unset the flag.
        /// </summary>
        public void Unset()
        {
            if (!AllowCountGoNegative && Counter == 0)
            {
                return;
            }

            Counter--;

            if (Counter == 0)
            {
                OnUnSet?.Invoke();
            }
        }

        /// <summary>
        /// Reset the flag.
        /// </summary>
        public void Reset()
        {
            if (Counter == 0)
            {
                return;
            }

            Counter = 0;

            OnUnSet?.Invoke();
        }

        /// <summary>
        /// If the <see cref="Counter"/> is 0.
        /// </summary>
        /// <param name="flag">The flag.</param>
        public static implicit operator bool(FlagCountableObservable flag)
        {
            return flag.Counter > 0;
        }
    }
}
