﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iodynis.Libraries.Utility
{
    public class SortedObservableCollection<T> : ObservableCollection<T>
    {
        private IComparer<T> comparer = Comparer<T>.Default;
        private Comparison<T> comparison = null;
        public SortedObservableCollection()
            : base()
        {
            ;
        }
        public SortedObservableCollection(IComparer<T> comparer)
            : base()
        {
            this.comparer = comparer;
        }
        public SortedObservableCollection(Comparison<T> comparison)
            : base()
        {
            this.comparison = comparison;
        }
        public new void Add(T item)
        {
            int index = 0;
            if (comparison != null)
            {
                while (index < Count && comparison.Invoke(item, Items[index]) >= 0)
                {
                    index++;
                }
            }
            else
            {
                while (index < Count && comparer.Compare(item, Items[index]) >= 0)
                {
                    index++;
                }
            }
            InsertItem(index, item);
        }
        public new void Move(int indexOld, int indexNew)
        {
            throw new InvalidOperationException("Cannot move items in a sorted collection.");
        }
        public new void Insert(int index, T item)
        {
            throw new InvalidOperationException("Cannot insert items at specified index in a sorted collection. Use Add method instead.");
        }
    }
}
