﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        public class RtfText : RtfGroup
        {
            public string Text;
            public RtfText(RtfDocument document, string text)
                : base(document)
            {
                Text = text;
            }
            public override string ToString()
            {
                return Text.Replace(@"\", @"\\");
            }
        }

        /// <summary>
        /// Add text.
        /// </summary>
        /// <param name="group">The element group.</param>
        /// <param name="text">The text.</param>
        public static void Text(this RtfGroup group, string text)
        {
            group.Add(new RtfText(group.Document, text));
        }
    }
}
