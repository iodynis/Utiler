﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// Section footer.
        /// </summary>
        public class RtfFooter : RtfGroup
        {
            /// <summary>
            /// Section footer.
            /// </summary>
            /// <param name="document">The document.</param>
            public RtfFooter(RtfDocument document)
                : base(document)
            {
                ;
            }
            public override string ToString()
            {
                return $@"{{\hdrctl\footer {base.ToString()}}}";
            }
        }
    }
}
