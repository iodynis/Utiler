﻿using System;

namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// Paragraph horizontal alignment.
        /// </summary>
        public enum RtfParagraphAlignment
        {
            /// <summary>
            /// Left.
            /// </summary>
            LEFT,
            /// <summary>
            /// Right.
            /// </summary>
            RIGHT,
            /// <summary>
            /// Center.
            /// </summary>
            CENTER,
            /// <summary>
            /// Justified.
            /// </summary>
            JUSTIFIED,
        }
        /// <summary>
        /// Document paragraph.
        /// </summary>
        public class RtfParagraph : RtfGroup
        {

            /// <summary>
            /// Reset the paragraph formatting. If false then the current paragraph inherits all paragraph properties defined in the previous paragraph.
            /// </summary>
            public bool ResetFormatting;
            /// <summary>
            /// Paragraph horizontal alignment.
            /// </summary>
            public RtfParagraphAlignment Alignment;
            /// <summary>
            /// Document paragraph.
            /// </summary>
            /// <param name="document">The document.</param>
            /// <param name="resetFormatting">Reset the paragraph formatting. If false then the current paragraph inherits all paragraph properties defined in the previous paragraph.</param>
            public RtfParagraph(RtfDocument document, bool resetFormatting = true)
                : base(document)
            {
                ResetFormatting = resetFormatting;
            }

            public override string ToString()
            {
                return $@"{{{(ResetFormatting ? @"\pard " : "")}{ToString(Alignment)} {base.ToString()} \par}}";
            }
            private string ToString(RtfParagraphAlignment alignment)
            {
                switch (alignment)
                {
                    case RtfParagraphAlignment.LEFT:
                        return @"\ql";
                    case RtfParagraphAlignment.RIGHT:
                        return @"\qr";
                    case RtfParagraphAlignment.CENTER:
                        return @"\qc";
                    case RtfParagraphAlignment.JUSTIFIED:
                        return @"\qj";
                    default:
                        throw new NotImplementedException($"{nameof(RtfParagraphAlignment)}.{alignment} is not supported.");
                }
            }
        }

        /// <summary>
        /// Add paragraph.
        /// </summary>
        /// <param name="group">The group.</param>
        /// <param name="resetFormatting">Reset paragraph formatting. If false then the current paragraph inherits all paragraph properties defined in the previous paragraph.</param>
        public static RtfParagraph Paragraph(this RtfGroup group, bool resetFormatting = true)
        {
            RtfParagraph paragraph = new RtfParagraph(group.Document, resetFormatting);
            group.Add(paragraph);
            return paragraph;
        }
    }
}
