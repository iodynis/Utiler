﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// Section header.
        /// </summary>
        public class RtfHeader : RtfGroup
        {
            /// <summary>
            /// Section header.
            /// </summary>
            public RtfHeader(RtfDocument document)
                : base(document)
            {
                ;
            }
            public override string ToString()
            {
                return $@"{{\hdrctl\header {base.ToString()}}}";
            }
        }
    }
}
