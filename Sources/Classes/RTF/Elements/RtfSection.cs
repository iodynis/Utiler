﻿using System;

namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// Section vertical alignment.
        /// </summary>
        public enum RtfSectionAlignment
        {
            /// <summary>
            /// Top.
            /// </summary>
            TOP,
            /// <summary>
            /// Bottom.
            /// </summary>
            BOTTOM,
            /// <summary>
            /// Center.
            /// </summary>
            CENTER,
            /// <summary>
            /// Justified.
            /// </summary>
            JUSTIFIED,
        }
        /// <summary>
        /// Document section.
        /// </summary>
        public class RtfSection : RtfGroup
        {
            /// <summary>
            /// Reset the paragraph formatting. If false then the current section inherits all section properties defined in the previous section.
            /// </summary>
            public bool ResetFormatting;
            /// <summary>
            /// Section vertical alignment.
            /// </summary>
            public RtfSectionAlignment Alignment;
            /// <summary>
            /// If true the first page has a special format.
            /// </summary>
            public bool TitlePage = false;
            /// <summary>
            /// Number of columns for "snaking".
            /// </summary>
            public int? ColumnsCount;
            /// <summary>
            /// Space between columns in millimeters.
            /// </summary>
            public int? ColumnsDistance;
            /// <summary>
            /// Header for the section.
            /// </summary>
            internal RtfHeader Header;
            /// <summary>
            /// Footer for the section.
            /// </summary>
            internal RtfFooter Footer;

            /// <summary>
            /// Document section.
            /// </summary>
            /// <param name="document">The document.</param>
            /// <param name="resetFormatting">Reset the paragraph formatting. If false then the current section inherits all section properties defined in the previous section.</param>
            public RtfSection(RtfDocument document, bool resetFormatting = true)
                : base(document)
            {
                ResetFormatting = resetFormatting;
            }
            public override string ToString()
            {
                return
                    // Open
                    $@"{{" +
                    // Header
                    $@"{(Header == null ? "" : $@"{Header} ")}" +
                    // Footer
                    $@"{(Footer == null ? "" : $@"{Footer} ")}" +
                    // Title page
                    $@"{(TitlePage ? $@"\titlepg " : "")}" +
                    // Columns
                    $@"{(ColumnsCount == null ? "" : $@"\cols{ColumnsCount} ")}" +
                    $@"{(ColumnsDistance == null ? "" : $@"\colsx{ColumnsDistance} ")}" +
                    // Vertical alignment
                    $@"{ToString(Alignment)} " +
                    // Content
                    $@"{base.ToString()} " +
                    // Close
                    $@"\sect}}";
            }
            private string ToString(RtfSectionAlignment alignment)
            {
                switch (alignment)
                {
                    case RtfSectionAlignment.TOP:
                        return @"\vertalt";
                    case RtfSectionAlignment.BOTTOM:
                        return @"\vertalb";
                    case RtfSectionAlignment.CENTER:
                        return @"\vertalc";
                    case RtfSectionAlignment.JUSTIFIED:
                        return @"\vertalj";
                    default:
                        throw new NotImplementedException($"{nameof(RtfSectionAlignment)}.{alignment} is not supported.");
                }
            }
        }

        /// <summary>
        /// Add section.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="resetFormatting">Reset section formatting. If false then the current section inherits all section properties defined in the previous section.</param>
        public static RtfSection Section(this RtfDocument document, bool resetFormatting = true)
        {
            RtfSection section = new RtfSection(document, resetFormatting);
            document.Add(section);
            return section;
        }

        /// <summary>
        /// Set a header.
        /// </summary>
        /// <param name="section">The section.</param>
        public static RtfHeader Header(this RtfSection section)
        {
            RtfHeader header = new RtfHeader(section.Document);
            section.Header = header;
            return header;
        }

        /// <summary>
        /// Set a footer.
        /// </summary>
        /// <param name="section">The section.</param>
        public static RtfFooter Footer(this RtfSection section)
        {
            RtfFooter footer = new RtfFooter(section.Document);
            section.Footer = footer;
            return footer;
        }
    }
}
