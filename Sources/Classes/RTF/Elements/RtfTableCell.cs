﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        public class RtfTableCell : RtfElement
        {
            private RtfTable Table;
            public RtfTableCell(RtfTable table)
                : base(table.Document)
            {
                Table = table;
            }
        }
    }
}
