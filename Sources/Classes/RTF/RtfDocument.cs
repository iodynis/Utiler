﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// The RTF document.
        /// </summary>
        public class RtfDocument : RtfGroup
        {
            /// <summary>
            /// Page orientations.
            /// </summary>
            public enum Orientaions
            {
                /// <summary>
                /// Vertical, default orientation.
                /// </summary>
                PORTRAIT,
                /// <summary>
                /// Horizontal orientation.
                /// </summary>
                LANDSCAPE,
            }

            /// <summary>
            /// RTF page size is in twips, one twip is approximately 0.017638888888889 mm, 1 mm is approximately 56.6929 twips.
            /// 1 Twip is exactly one twentieth (1/20) of a PostScript point.
            /// A PostScript point itself being 1/72 of an inch, making a twip 1/1440 inch.
            /// </summary>
            private const float TwipsInOneMillimeter = 56.6929f;

            //private Dictionary<string, int> FontFamilyToId = new Dictionary<string, int>();
            private Dictionary<Color, int> ColorToId = new Dictionary<Color, int>();
            private List<Color> IdToColor = new List<Color>();

            private Dictionary<RtfFont, int> FontToId = new Dictionary<RtfFont, int>();
            private List<RtfFont> IdToFont = new List<RtfFont>();

            private Dictionary<RtfStylesheet, int> StylesheetToId = new Dictionary<RtfStylesheet, int>();
            private List<RtfStylesheet> IdToStylesheet = new List<RtfStylesheet>();

            /// <summary>
            /// Paper width in mm.
            /// </summary>
            public int Width = 210;
            /// <summary>
            /// Paper height in mm.
            /// </summary>
            public int Height = 297;

            /// <summary>
            /// Paper left margin in mm.
            /// </summary>
            public int MarginLeft = 20;
            /// <summary>
            /// Paper top margin in mm.
            /// </summary>
            public int MarginTop = 20;
            /// <summary>
            /// Paper right margin in mm.
            /// </summary>
            public int MarginRight = 20;
            /// <summary>
            /// Paper bottom margin in mm.
            /// </summary>
            public int MarginBottom = 20;

            /// <summary>
            /// Header height in mm.
            /// </summary>
            public int HeaderHeigh;
            /// <summary>
            /// Footer height in mm.
            /// </summary>
            public int FooterHeigh;

            /// <summary>
            /// Default font color.
            /// </summary>
            public Color FontColor = Color.Black;
            /// <summary>
            /// Default font size.
            /// </summary>
            public int FontSize = 16;
            /// <summary>
            /// Default font family.
            /// </summary>
            public string FontFamily = "Times New Roman";

            internal int GetColorIndex(Color color)
            {
                if (!ColorToId.TryGetValue(color, out int colorIndex))
                {
                    colorIndex = IdToColor.Count;
                    IdToColor.Add(color);
                    ColorToId.Add(color, colorIndex);
                }

                return colorIndex;
            }
            internal int GetFontIndex(RtfFont font)
            {
                if (!FontToId.TryGetValue(font, out int fontIndex))
                {
                    fontIndex = IdToColor.Count;
                    IdToFont.Add(font);
                    FontToId.Add(font, fontIndex);
                }

                return fontIndex;
            }

            /// <summary>
            /// Page orientation.
            /// </summary>
            public Orientaions Orientaion;

            /// <summary>
            /// RTF document.
            /// </summary>
            public RtfDocument()
                : base()
            {
                Document = this;
            }
            /// <summary>
            /// RTF document.
            /// </summary>
            /// <param name="document"></param>
            public RtfDocument(RtfDocument document)
                : base(document) { }

            private int MillimetersToTwips(int millimeters)
            {
                return (int)(millimeters * TwipsInOneMillimeter);
            }
            private void Reset()
            {
                // Reset color cache
                ColorToId.Clear();
                IdToColor.Clear();

                // Reset font cache
                FontToId.Clear();
                IdToFont.Clear();

                // Reset stylesheet cache
                StylesheetToId.Clear();
                IdToStylesheet.Clear();

                // Set defaults
                GetColorIndex(FontColor);
                GetFontIndex(new RtfFont(this) { FontSize = FontSize, FontFamily = FontFamily } );
            }

            /// <summary>
            /// Write to a string.
            /// </summary>
            /// <returns>The content of the document.</returns>
            public override string ToString()
            {
                // Reset cached tables
                Reset();

                StringBuilder stringBuilder = new StringBuilder();

                // Prepare the content. This will fill the colors/fonts/stylesheets tables as well.
                foreach (RtfElement element in Elements)
                {
                    stringBuilder.AppendLine(element.ToString());
                }
                string content = stringBuilder.ToString();
                stringBuilder.Clear();

                // Start the document
                stringBuilder.Append($@"{{\rtf1\ansi\deff0");

                // Orientation
                stringBuilder.Append($@"\{(Orientaion == Orientaions.LANDSCAPE ? "landscape" : "portrait")}");

                // Page size
                stringBuilder.Append($@"\paperw{MillimetersToTwips(Width)}\paperh{MillimetersToTwips(Height)}");

                // Page margins
                stringBuilder.Append($@"\margl{MillimetersToTwips(MarginLeft)}\margr{MillimetersToTwips(MarginRight)}\margt{MillimetersToTwips(MarginTop)}\margb{MillimetersToTwips(MarginBottom)}");

                // Header and footer size
                stringBuilder.AppendLine($@"\headery{MillimetersToTwips(HeaderHeigh)}\footery{MillimetersToTwips(FooterHeigh)}");

                // Fonts
                stringBuilder.Append($@"{{\fonttbl;");
                for (int index = 0; index < IdToFont.Count; index++)
                {
                    RtfFont font = IdToFont[index];
                    stringBuilder.Append($@"{{\f{index + 1}\fcharset0 {font.FontFamily};}}");
                }
                stringBuilder.AppendLine($@"}}");

                // Colors
                stringBuilder.Append($@"{{\colortbl;");
                for (int index = 0; index < IdToColor.Count; index++)
                {
                    Color color = IdToColor[index];
                    stringBuilder.Append($@"\red{color.R}\green{color.G}\blue{color.B};");
                }
                stringBuilder.AppendLine($@"}}");

                // Stylesheets
                stringBuilder.Append($@"{{\stylesheet");
                for (int index = 0; index < IdToStylesheet.Count; index++)
                {
                    RtfStylesheet stylesheet = IdToStylesheet[index];
                    stringBuilder.Append(stylesheet.ToString());
                }
                stringBuilder.AppendLine($@"}}");

                //// Defaults
                //stringBuilder.AppendLine($@"\fs{FontSize}");

                // Content
                stringBuilder.Append(content);

                // Finish the document
                stringBuilder.AppendLine($@"}}");

                return stringBuilder.ToString();
            }

            /// <summary>
            /// Write to the file.
            /// </summary>
            /// <param name="path">The path to the file.</param>
            public void Write(string path)
            {
                File.WriteAllText(path, ToString());
            }
        }
    }
}
