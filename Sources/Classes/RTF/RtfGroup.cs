﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A group of elements.
        /// </summary>
        public class RtfGroup : RtfElement
        {
            /// <summary>
            /// Child elements.
            /// </summary>
            protected List<RtfElement> Elements = new List<RtfElement>();

            /// <summary>
            /// A group of elements.
            /// </summary>
            public RtfGroup()
                : base() { }
            /// <summary>
            /// A group of elements.
            /// </summary>
            public RtfGroup(RtfDocument document)
                : base(document) { }

            /// <summary>
            /// Add a new element to the group.
            /// </summary>
            public void Add(RtfElement elemet)
            {
                Elements.Add(elemet);
            }
            public override string ToString()
            {
                return String.Join(" ", Elements);
            }
        }
    }
}
