﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A non-breaking hyphen \_ command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup HyphenNonBreaking(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\_"));
            return group;
        }
    }
}
