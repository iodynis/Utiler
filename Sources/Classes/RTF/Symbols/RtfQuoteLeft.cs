﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A left quote \lquote command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup QuoteLeft(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\lquote"));
            return group;
        }
    }
}
