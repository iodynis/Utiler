﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A long dash \emdash command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup DashLong(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\emdash"));
            return group;
        }
    }
}
