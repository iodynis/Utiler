﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A right quote \rquote command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup QuoteRight(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\rquote"));
            return group;
        }
    }
}
