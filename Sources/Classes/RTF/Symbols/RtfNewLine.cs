﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A new line \line command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup NewLine(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\line"));
            return group;
        }
    }
}
