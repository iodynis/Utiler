﻿namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A bullet \bullet command.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup Bullet(this RtfGroup group)
        {
            group.Add(new RtfCommand(group.Document, @"\bullet"));
            return group;
        }
    }
}
