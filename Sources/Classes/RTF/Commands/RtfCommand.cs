﻿using System;

namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// A custom command.
        /// </summary>
        public class RtfCommand : RtfElement
        {
            /// <summary>
            /// Command text.
            /// </summary>
            public readonly string Text;

            /// <summary>
            /// A custom command.
            /// </summary>
            /// <param name="document">The document.</param>
            /// <param name="text">The command text. It is automatically appended with a slash if it does not begin with one.</param>
            public RtfCommand(RtfDocument document, string text)
                : base(document)
            {
                if (text == null)
                {
                    throw new ArgumentNullException(nameof(text));
                }
                if (text.Length == 0)
                {
                    throw new ArgumentException("Text cannot be empty", nameof(text));
                }

                Text = $"{(text[0] != '\\' ? "\\" : "")}{text}";
            }

            public override string ToString()
            {
                return Text;
            }
        }

        /// <summary>
        /// Append a command.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="text">The command text. It is automatically appended with a slash if it does not begin with one.</param>
        public static RtfGroup Command(this RtfGroup group, string text)
        {
            group.Add(new RtfCommand(group.Document, text));
            return group;
        }
    }
}
