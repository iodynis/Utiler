﻿using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// Background color.
        /// </summary>
        public class RtfBackgroundColor : RtfElement
        {
            /// <summary>
            /// The color.
            /// </summary>
            public Color? Color;

            /// <summary>
            /// Background color.
            /// </summary>
            /// <param name="document">The document.</param>
            /// <param name="color">The background color or null to reset the color to the default.</param>
            public RtfBackgroundColor(RtfDocument document, Color? color)
                : base(document)
            {
                Color = color;
            }

            public override string ToString()
            {
                int index = Color.HasValue ? Document.GetColorIndex(Color.Value) : 0;
                return $@"\cb{index + 1}";
            }
        }

        /// <summary>
        /// Set the background color.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="color">The color.</param>
        public static RtfGroup BackgroundColor(this RtfGroup group, Color color)
        {
            group.Add(new RtfBackgroundColor(group.Document, color));
            return group;
        }
        /// <summary>
        /// Set the background color to default.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup BackgroundColorDefault(this RtfGroup group)
        {
            group.Add(new RtfBackgroundColor(group.Document, null));
            return group;
        }
    }
}
