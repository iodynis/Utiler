﻿using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// Foreground color.
        /// </summary>
        public class RtfForegroundColor : RtfElement
        {
            /// <summary>
            /// The color.
            /// </summary>
            public Color? Color;

            /// <summary>
            /// Foreground color.
            /// </summary>
            /// <param name="document">The document.</param>
            /// <param name="color">The foreground color or null to reset the color to the default.</param>
            public RtfForegroundColor(RtfDocument document, Color? color)
                : base(document)
            {
                Color = color;
            }

            public override string ToString()
            {
                int index = Color.HasValue ? Document.GetColorIndex(Color.Value) : 0;
                return $@"\cf{index + 1}";
            }
        }

        /// <summary>
        /// Set the font color.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="color">The color.</param>
        public static RtfGroup ForegroundColor(this RtfGroup group, Color color)
        {
            group.Add(new RtfForegroundColor(group.Document, color));
            return group;
        }
        /// <summary>
        /// Set the font color to default.
        /// </summary>
        /// <param name="group"></param>
        public static RtfGroup ForegroundColorDefault(this RtfGroup group)
        {
            group.Add(new RtfForegroundColor(group.Document, null));
            return group;
        }
    }
}
