﻿using System.Collections.Generic;
using System.Drawing;

namespace Iodynis.Libraries.Utility
{
    public static partial class Rtf
    {
        /// <summary>
        /// Document element base.
        /// </summary>
        public class RtfElement
        {
            /// <summary>
            /// The document.
            /// </summary>
            internal RtfDocument Document;

            /// <summary>
            /// Document element base.
            /// </summary>
            public RtfElement()
            {
                ;
            }
            /// <summary>
            /// Document element base.
            /// </summary>
            /// <param name="document">The document.</param>
            public RtfElement(RtfDocument document)
            {
                Document = document;
            }
        }
    }
}
