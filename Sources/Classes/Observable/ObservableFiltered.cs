﻿namespace Iodynis.Libraries.Utility.Observables
{
    public class ObservableFiltered<T> : Observable<T>
    {
        public delegate T FilterDelegate(T value);
        private readonly FilterDelegate Filter;

        public override T Value
        {
            get => base.Value;
            set => base.Value = Filter(value);
        }
        public ObservableFiltered(FilterDelegate filter)
            : this(default(T), filter) { }
        public ObservableFiltered(T value, FilterDelegate filter)
            : base(value)
        {
            Filter = filter;
        }
    }
}
