﻿using System.ComponentModel;

namespace Iodynis.Libraries.Utility.Observables
{
    //public abstract class ObservableBase : INotifyPropertyChanging, INotifyPropertyChanged
    //{
    //}
    public abstract class ObservableBase<T> : INotifyPropertyChanging, INotifyPropertyChanged
    {
        /// <summary>
        /// <see cref="Changing"/> event delegate.
        /// </summary>
        /// <param name="current">Current value.</param>
        /// <param name="next">Next value.</param>
        public delegate void ChangingEventHandler(T current, T next);
        /// <summary>
        /// The <see cref="Value"/> property is going to change.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;
        /// <summary>
        /// The <see cref="Value"/> is going to change.
        /// </summary>
        public event ChangingEventHandler Changing;

        /// <summary>
        /// <see cref="Changed"/> event delegate.
        /// </summary>
        /// <param name="previous">Previous value.</param>
        /// <param name="current">Current value.</param>
        public delegate void ChangedEventHandler(T previous, T current);
        /// <summary>
        /// The <see cref="Value"/> property has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// The <see cref="Value"/> has changed.
        /// </summary>
        public event ChangedEventHandler Changed;

        /// <summary>
        /// The internal value.
        /// </summary>
        protected T ValueCurrent;
        /// <summary>
        /// The value.
        /// </summary>
        public virtual T Value
        {
            get
            {
                return ValueCurrent;
            }
            set
            {
                ValueCurrent = value;
            }
        }

        /// <summary>
        /// Invoke <see cref="Changing"/> and <see cref="PropertyChanging"/> events.
        /// </summary>
        /// <param name="current">Current value.</param>
        /// <param name="next">Next value.</param>
        protected void InvokeChanging(T current, T next)
        {
            if (Changing != null)
            {
                ChangingEventHandler changing = Changing;
                changing?.Invoke(current, next);
            }
            if (PropertyChanging != null)
            {
                PropertyChangingEventHandler changing = PropertyChanging;
                changing?.Invoke(this, new PropertyChangingEventArgs(nameof(Value)));
            }
        }
        /// <summary>
        /// Invoke <see cref="Changed"/> and <see cref="PropertyChanged"/> events.
        /// </summary>
        /// <param name="previous">Previous value.</param>
        /// <param name="current">Current value.</param>
        protected void InvokeChanged(T previous, T current)
        {
            if (Changed != null)
            {
                ChangedEventHandler changed = Changed;
                changed?.Invoke(previous, current);
            }
            if (PropertyChanged != null)
            {
                PropertyChangedEventHandler changed = PropertyChanged;
                changed?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
            }
        }

        public ObservableBase()
            : this(default(T)) { }
        public ObservableBase(T value)
        {
            ValueCurrent = value;
        }

        #region Operators, Equals, GetHashCode

        public static implicit operator T (ObservableBase<T> observable) => observable == null ? default(T) : observable.ValueCurrent;
        public static bool operator == (ObservableBase<T> observable, T value) => Equals(observable == null ? default(T) : observable.ValueCurrent, value);
        public static bool operator != (ObservableBase<T> observable, T value) => !(observable == value);
        public override bool Equals(object @object) => @object is ObservableBase<T> observable && observable == this;
        public override int GetHashCode() => base.GetHashCode();

        #endregion
    }
}
