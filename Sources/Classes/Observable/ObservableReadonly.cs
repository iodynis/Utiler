﻿namespace Iodynis.Libraries.Utility.Observables
{
    public class ObservableReadonly<T> : ObservableBase<T>
    {
        public override T Value
        {
            get
            {
                return ValueCurrent;
            }
            set
            {
                return;
            }
        }
    }
}
