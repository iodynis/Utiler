﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Observables
{
    public class Observable<T> : ObservableBase<T>
    {
        private bool IsChanged = false;
        private bool IsSuppressed = false;
        private bool IsUpdating = false;

        private T ValuePrevious;
        private T ValueNext;
        public override T Value
        {
            get
            {
                return ValueCurrent;
            }
            set
            {
                // Check if already there
                if (Equals(ValueCurrent, value))
                {
                    return;
                }
                // Check if already updating to that exact value
                if (Equals(ValueNext, value))
                {
                    return;
                }
                // Check for a loop (already updating to another value)
                if (IsUpdating)
                {
                    throw new Exception($"Loop deteced in {nameof(ObservableBase<T>)} set method.");
                }

                //// Retain the loop prevention flag
                //IsUpdating = true;

                if (IsSuppressed)
                {
                    ValuePrevious = Equals(ValuePrevious, default) ? ValuePrevious : ValueCurrent;
                    ValueCurrent = value;
                }
                else
                {
                    ValuePrevious = ValueCurrent;
                    ValueNext = value;

                    InvokeChanging();
                    InvokeChange();
                    InvokeChanged();

                    ValuePrevious = default;
                    ValueNext = default;
                }

                //// Update the value
                //InvokeChanging(ValuePrevious, value);
                //ValueCurrent = value;
                //InvokeChanged(ValuePrevious, value);


                //// Release the loop prevention flag
                //IsUpdating = false;
            }
        }

        private void InvokeChanging()
        {
            IsUpdating = true;
            InvokeChanging(ValuePrevious, ValueNext);

            if (Chained != null)
            {
                for (int i = 0; i < Chained.Count; i++)
                {
                    Chained[i].ValuePrevious = Chained[i].ValueCurrent;
                    //Chained[i].ValueNext = Convert(Value);
                }
            }
        }
        private void InvokeChange()
        {
            ValueCurrent = ValueNext;
        }
        private void InvokeChanged()
        {
            InvokeChanged(ValuePrevious, ValueNext);
            IsUpdating = false;
        }

        public Observable()
            : this(default(T)) { }
        public Observable(T value)
            : base(value) { }

        public static implicit operator Observable<T>(T value) => new Observable<T>(value);

        private List<Observable<T>> Chained;
        public Observable<T> Bind(Observable<T> observable)
        {
            ;

            return this;
        }
        public Observable<T> Chain<K>(Observable<K> observable, Func<T, K, K> forward, Func<K, T, T> backward)
        {
            //Others.Add(new Other(
            //    value => observable.InvokeChanging(observable.Value, forward(value)),
            //    value => observable.ValueCurrent = forward(value),
            //    null
            //    //value => observable.InvokeChanged(value, observable.Value)
            //    ));

            return this;
        }

        public void SuppressEvents()
        {
            IsSuppressed = true;
        }
        public void ResumeEvents()
        {
            IsSuppressed = false;
        }

        private List<Other> Others = new List<Other>();
        private struct Other
        {
            public Action<T> Changin;
            public Action<T> Changer;
            public Action<T> Changed;

            public Other(Action<T> changin, Action<T> changer, Action<T> changed)
            {
                Changin = changin;
                Changer = changer;
                Changed = changed;
            }
        }
    }
}
