﻿using System;

namespace Iodynis.Libraries.Utility
{
    /// <summary>
    /// Program version
    /// </summary>
    public struct ProgramVersion
    {
        /// <summary>
        /// Version suffix.
        /// </summary>
        public enum ProgramVersionSuffix : byte
        {
            /// <summary>
            /// No version with no suffix.
            /// </summary>
            NONE = 0,

            /// <summary>
            /// Alpha version with suffix "a".
            /// </summary>
            ALPHA = 1 << 0,

            /// <summary>
            /// Beta version with suffix "b".
            /// </summary>
            BETA = 1 << 1,

            /// <summary>
            /// Release candidate version with suffix "c".
            /// </summary>
            CANDIDATE = 1 << 2,

            /// <summary>
            /// Draft version with suffix "d".
            /// </summary>
            DRAFT = 1 << 3,

            ///// <summary>
            ///// Reserved for something with suffix "e".
            ///// </summary>
            //E = 1 << 4,

            /// <summary>
            /// Final release version with suffix "f".
            /// </summary>
            RELEASE = 1 << 5,

            ///// <summary>
            ///// Reserved for something with suffix "g".
            ///// </summary>
            //G = 1 << 6,

            ///// <summary>
            ///// Reserved for something with suffix "h".
            ///// </summary>
            //H = 1 << 7,
        }

        /// <summary>
        /// Major version number.
        /// </summary>
        public readonly uint Major;
        /// <summary>
        /// Minor version number.
        /// </summary>
        public readonly uint Minor;
        /// <summary>
        /// Patch number.
        /// </summary>
        public readonly uint Patch;
        /// <summary>
        /// Build number.
        /// </summary>
        public readonly uint Build;
        /// <summary>
        /// Version suffix.
        /// </summary>
        public readonly ProgramVersionSuffix Suffix;

        public readonly static ProgramVersion Zero = new ProgramVersion(0, 0, 0, 0);

        /// <summary>
        /// Program version
        /// </summary>
        /// <param name="major">Major version.</param>
        /// <param name="minor">Minor version.</param>
        /// <param name="patch">Patch version.</param>
        /// <param name="build">Build version.</param>
        /// <param name="suffix">Version suffix.</param>
        public ProgramVersion(uint major, uint minor = 0, uint patch = 0, uint build = 0, ProgramVersionSuffix suffix = ProgramVersionSuffix.NONE)
        {
            Suffix = suffix;
            Major = major;
            Minor = minor;
            Patch = patch;
            Build = build;
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="string"></param>
        /// <returns>A version in case of a valid version. A 0.0.0.0 version in case of a null or empty string. Throws exceptions otherwise.</returns>
        public static ProgramVersion Parse(string @string)
        {
            if (@string == null || @string.Length == 0)
            {
                return new ProgramVersion();
            }

            ProgramVersionSuffix status = GetProgramVersionSuffix(@string, out string suffix);
            @string = @string.Substring(0, @string.Length - suffix.Length);
            
            string[] parts = @string.Split('.');
            if (parts.Length > 4)
            {
                throw new Exception($"Program version of {@string} is invalid.");
            }

            uint major = 0;
            if (parts.Length >= 1 && !UInt32.TryParse(parts[0], out major))
            {
                throw new Exception($"Program version of {@string} has invalid major section.");
            }
            uint minor = 0;
            if (parts.Length >= 2 && !UInt32.TryParse(parts[1], out minor))
            {
                throw new Exception($"Program version of {@string} has invalid minor section.");
            }
            uint patch = 0;
            if (parts.Length >= 3 && !UInt32.TryParse(parts[2], out patch))
            {
                throw new Exception($"Program version of {@string} has invalid patch section.");
            }
            uint build = 0;
            if (parts.Length >= 4 && !UInt32.TryParse(parts[3], out build))
            {
                throw new Exception($"Program version of {@string} has invalid build section.");
            }

            return new ProgramVersion(major, minor, patch, build, status);
        }

        /// <summary>
        /// Try to parse a string as a version.
        /// </summary>
        /// <param name="string">String representation of a version.</param>
        /// <param name="version">Version if parsing succeeded.</param>
        /// <returns>True if parse successfully, false otherwise.</returns>
        public static bool TryParse(string @string, out ProgramVersion version)
        {
            if (@string == null || @string.Length == 0)
            {
                version = new ProgramVersion();
                return false;
            }

            ProgramVersionSuffix status = GetProgramVersionSuffix(@string, out string suffix);
            @string = @string.Substring(0, @string.Length - suffix.Length);

            string[] parts = @string.Split('.');
            if (parts.Length > 4)
            {
                version = new ProgramVersion();
                return false;
            }

            uint major = 0;
            if (parts.Length >= 1 && !UInt32.TryParse(parts[0], out major))
            {
                version = new ProgramVersion();
                return false;
            }
            uint minor = 0;
            if (parts.Length >= 2 && !UInt32.TryParse(parts[1], out minor))
            {
                version = new ProgramVersion();
                return false;
            }
            uint patch = 0;
            if (parts.Length >= 3 && !UInt32.TryParse(parts[2], out patch))
            {
                version = new ProgramVersion();
                return false;
            }
            uint build = 0;
            if (parts.Length >= 4 && !UInt32.TryParse(parts[3], out build))
            {
                version = new ProgramVersion();
                return false;
            }

            version = new ProgramVersion(major, minor, patch, build, status);
            return true;
        }

        private static ProgramVersionSuffix GetProgramVersionSuffix(string @string, out string suffix)
        {
            if (@string.Length == 0)
            {
                suffix = "";
                return ProgramVersionSuffix.NONE;
            }

            if (@string.Length > 1)
            {
                char characterLast = @string[@string.Length - 1];
                if (characterLast < 'a' || 'z' < characterLast)
                {
                    suffix = "";
                    return ProgramVersionSuffix.NONE;
                }

                suffix = characterLast.ToString();
                switch (characterLast)
                {
                    case 'a':
                        return ProgramVersionSuffix.ALPHA;
                    case 'b':
                        return ProgramVersionSuffix.BETA;
                    case 'c':
                        return ProgramVersionSuffix.CANDIDATE;
                    case 'd':
                        return ProgramVersionSuffix.DRAFT;
                    case 'f':
                        return ProgramVersionSuffix.RELEASE;
                    default:
                        break;
                }
            }

            suffix = "";
            return ProgramVersionSuffix.NONE;
        }

        public static bool operator == (ProgramVersion programVersionOne, ProgramVersion programVersionTwo)
        {
            return programVersionOne.Major == programVersionTwo.Major
                && programVersionOne.Minor == programVersionTwo.Minor
                && programVersionOne.Patch == programVersionTwo.Patch
                && programVersionOne.Build == programVersionTwo.Build
                && programVersionOne.Suffix == programVersionTwo.Suffix;
        }
        public static bool operator != (ProgramVersion programVersionOne, ProgramVersion programVersionTwo)
        {
            return !(programVersionOne == programVersionTwo);
        }
        public static bool operator > (ProgramVersion programVersionOne, ProgramVersion programVersionTwo)
        {
            if (programVersionOne.Major > programVersionTwo.Major) return true;
            if (programVersionOne.Major < programVersionTwo.Major) return false;
            if (programVersionOne.Minor > programVersionTwo.Minor) return true;
            if (programVersionOne.Minor < programVersionTwo.Minor) return false;
            if (programVersionOne.Patch > programVersionTwo.Patch) return true;
            if (programVersionOne.Patch < programVersionTwo.Patch) return false;
            if (programVersionOne.Build > programVersionTwo.Build) return true;
            if (programVersionOne.Build < programVersionTwo.Build) return false;
            if (programVersionOne.Suffix > programVersionTwo.Suffix) return true;
            if (programVersionOne.Suffix < programVersionTwo.Suffix) return false;
            return false;
        }
        public static bool operator < (ProgramVersion programVersionOne, ProgramVersion programVersionTwo)
        {
            return programVersionTwo > programVersionOne;
        }
        public static bool operator >= (ProgramVersion programVersionOne, ProgramVersion programVersionTwo)
        {
            return programVersionOne < programVersionTwo;
        }
        public static bool operator <= (ProgramVersion programVersionOne, ProgramVersion programVersionTwo)
        {
            return programVersionOne > programVersionTwo;
        }
        public override bool Equals(object version)
        {
            return version is ProgramVersion && (ProgramVersion)version == this;
        }
        public override int GetHashCode()
        {
            return (int)((Major << 28) + (Minor << 22) + (Patch << 16) + (Build << 2) + (int)Suffix);
        }

        public override string ToString()
        {
            return Major +
                ((Minor == 0 && Patch == 0 && Build == 0) ? "" : ("." + Minor)) +
                ((Patch == 0 && Build == 0) ? "" : ("." + Patch)) +
                (Build == 0 ? "" : ("." + Build)) +
                ToString(Suffix);
        }
        private static string ToString(ProgramVersionSuffix status)
        {
            switch (status)
            {
                case ProgramVersionSuffix.NONE:
                    return "";
                case ProgramVersionSuffix.ALPHA:
                    return "a";
                case ProgramVersionSuffix.BETA:
                    return "b";
                case ProgramVersionSuffix.CANDIDATE:
                    return "c";
                case ProgramVersionSuffix.RELEASE:
                    return "f";
                default:
                    throw new NotImplementedException($"{nameof(ProgramVersionSuffix)}.{status} is not supported.");
            }
        }
    }
}
