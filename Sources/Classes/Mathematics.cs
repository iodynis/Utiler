﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;

namespace Iodynis.Libraries.Utility
{
    public static class Mathematics
    {
        //#region Sqrt
        //public static float FastSqrt(float value)
        //{
        //    return (byte)(start + (start + end) * value);
        //}
        //#endregion

        #region Min

        #region Byte
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static byte Min(byte value1, byte value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static byte Min(byte value1, byte value2, byte value3)
        {
            byte min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static byte Min(byte value1, byte value2, byte value3, byte value4)
        {
            byte min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static byte Min(byte value1, byte value2, byte value3, byte value4, byte value5)
        {
            byte min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static byte Min(byte value1, byte value2, byte value3, byte value4, byte value5, byte value6)
        {
            byte min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static byte Min(byte value1, byte value2, byte value3, byte value4, byte value5, byte value6, byte value7)
        {
            byte min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static byte Min(byte value1, byte value2, byte value3, byte value4, byte value5, byte value6, byte value7, byte value8)
        {
            byte min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static byte Min(byte value1, byte value2, byte value3, byte value4, byte value5, byte value6, byte value7, byte value8, byte value9)
        {
            byte min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #region Char
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static char Min(char value1, char value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static char Min(char value1, char value2, char value3)
        {
            char min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static char Min(char value1, char value2, char value3, char value4)
        {
            char min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static char Min(char value1, char value2, char value3, char value4, char value5)
        {
            char min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static char Min(char value1, char value2, char value3, char value4, char value5, char value6)
        {
            char min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static char Min(char value1, char value2, char value3, char value4, char value5, char value6, char value7)
        {
            char min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static char Min(char value1, char value2, char value3, char value4, char value5, char value6, char value7, char value8)
        {
            char min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static char Min(char value1, char value2, char value3, char value4, char value5, char value6, char value7, char value8, char value9)
        {
            char min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #region Short
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static short Min(short value1, short value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static short Min(short value1, short value2, short value3)
        {
            short min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static short Min(short value1, short value2, short value3, short value4)
        {
            short min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static short Min(short value1, short value2, short value3, short value4, short value5)
        {
            short min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static short Min(short value1, short value2, short value3, short value4, short value5, short value6)
        {
            short min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static short Min(short value1, short value2, short value3, short value4, short value5, short value6, short value7)
        {
            short min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static short Min(short value1, short value2, short value3, short value4, short value5, short value6, short value7, short value8)
        {
            short min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static short Min(short value1, short value2, short value3, short value4, short value5, short value6, short value7, short value8, short value9)
        {
            short min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #region Integer
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static int Min(int value1, int value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static int Min(int value1, int value2, int value3)
        {
            int min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static int Min(int value1, int value2, int value3, int value4)
        {
            int min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static int Min(int value1, int value2, int value3, int value4, int value5)
        {
            int min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static int Min(int value1, int value2, int value3, int value4, int value5, int value6)
        {
            int min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static int Min(int value1, int value2, int value3, int value4, int value5, int value6, int value7)
        {
            int min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static int Min(int value1, int value2, int value3, int value4, int value5, int value6, int value7, int value8)
        {
            int min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static int Min(int value1, int value2, int value3, int value4, int value5, int value6, int value7, int value8, int value9)
        {
            int min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #region Long
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static long Min(long value1, long value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static long Min(long value1, long value2, long value3)
        {
            long min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static long Min(long value1, long value2, long value3, long value4)
        {
            long min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static long Min(long value1, long value2, long value3, long value4, long value5)
        {
            long min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static long Min(long value1, long value2, long value3, long value4, long value5, long value6)
        {
            long min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static long Min(long value1, long value2, long value3, long value4, long value5, long value6, long value7)
        {
            long min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static long Min(long value1, long value2, long value3, long value4, long value5, long value6, long value7, long value8)
        {
            long min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static long Min(long value1, long value2, long value3, long value4, long value5, long value6, long value7, long value8, long value9)
        {
            long min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #region Unsigned short
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static ushort Min(ushort value1, ushort value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static ushort Min(ushort value1, ushort value2, ushort value3)
        {
            ushort min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static ushort Min(ushort value1, ushort value2, ushort value3, ushort value4)
        {
            ushort min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static ushort Min(ushort value1, ushort value2, ushort value3, ushort value4, ushort value5)
        {
            ushort min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static ushort Min(ushort value1, ushort value2, ushort value3, ushort value4, ushort value5, ushort value6)
        {
            ushort min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static ushort Min(ushort value1, ushort value2, ushort value3, ushort value4, ushort value5, ushort value6, ushort value7)
        {
            ushort min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static ushort Min(ushort value1, ushort value2, ushort value3, ushort value4, ushort value5, ushort value6, ushort value7, ushort value8)
        {
            ushort min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static ushort Min(ushort value1, ushort value2, ushort value3, ushort value4, ushort value5, ushort value6, ushort value7, ushort value8, ushort value9)
        {
            ushort min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #region Unsigned integer
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static uint Min(uint value1, uint value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static uint Min(uint value1, uint value2, uint value3)
        {
            uint min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static uint Min(uint value1, uint value2, uint value3, uint value4)
        {
            uint min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static uint Min(uint value1, uint value2, uint value3, uint value4, uint value5)
        {
            uint min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static uint Min(uint value1, uint value2, uint value3, uint value4, uint value5, uint value6)
        {
            uint min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static uint Min(uint value1, uint value2, uint value3, uint value4, uint value5, uint value6, uint value7)
        {
            uint min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static uint Min(uint value1, uint value2, uint value3, uint value4, uint value5, uint value6, uint value7, uint value8)
        {
            uint min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static uint Min(uint value1, uint value2, uint value3, uint value4, uint value5, uint value6, uint value7, uint value8, uint value9)
        {
            uint min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #region Unsigned long
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static ulong Min(ulong value1, ulong value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static ulong Min(ulong value1, ulong value2, ulong value3)
        {
            ulong min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static ulong Min(ulong value1, ulong value2, ulong value3, ulong value4)
        {
            ulong min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static ulong Min(ulong value1, ulong value2, ulong value3, ulong value4, ulong value5)
        {
            ulong min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static ulong Min(ulong value1, ulong value2, ulong value3, ulong value4, ulong value5, ulong value6)
        {
            ulong min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static ulong Min(ulong value1, ulong value2, ulong value3, ulong value4, ulong value5, ulong value6, ulong value7)
        {
            ulong min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static ulong Min(ulong value1, ulong value2, ulong value3, ulong value4, ulong value5, ulong value6, ulong value7, ulong value8)
        {
            ulong min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static ulong Min(ulong value1, ulong value2, ulong value3, ulong value4, ulong value5, ulong value6, ulong value7, ulong value8, ulong value9)
        {
            ulong min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #region Float
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static float Min(float value1, float value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static float Min(float value1, float value2, float value3)
        {
            float min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static float Min(float value1, float value2, float value3, float value4)
        {
            float min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static float Min(float value1, float value2, float value3, float value4, float value5)
        {
            float min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static float Min(float value1, float value2, float value3, float value4, float value5, float value6)
        {
            float min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static float Min(float value1, float value2, float value3, float value4, float value5, float value6, float value7)
        {
            float min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static float Min(float value1, float value2, float value3, float value4, float value5, float value6, float value7, float value8)
        {
            float min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static float Min(float value1, float value2, float value3, float value4, float value5, float value6, float value7, float value8, float value9)
        {
            float min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #region Double
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static double Min(double value1, double value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static double Min(double value1, double value2, double value3)
        {
            double min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static double Min(double value1, double value2, double value3, double value4)
        {
            double min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static double Min(double value1, double value2, double value3, double value4, double value5)
        {
            double min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static double Min(double value1, double value2, double value3, double value4, double value5, double value6)
        {
            double min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static double Min(double value1, double value2, double value3, double value4, double value5, double value6, double value7)
        {
            double min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static double Min(double value1, double value2, double value3, double value4, double value5, double value6, double value7, double value8)
        {
            double min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static double Min(double value1, double value2, double value3, double value4, double value5, double value6, double value7, double value8, double value9)
        {
            double min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #region Decimal
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The minimum value.</returns>
        public static decimal Min(decimal value1, decimal value2)
        {
            return value1 < value2 ? value1 : value2;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The minimum value.</returns>
        public static decimal Min(decimal value1, decimal value2, decimal value3)
        {
            decimal min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The minimum value.</returns>
        public static decimal Min(decimal value1, decimal value2, decimal value3, decimal value4)
        {
            decimal min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The minimum value.</returns>
        public static decimal Min(decimal value1, decimal value2, decimal value3, decimal value4, decimal value5)
        {
            decimal min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The minimum value.</returns>
        public static decimal Min(decimal value1, decimal value2, decimal value3, decimal value4, decimal value5, decimal value6)
        {
            decimal min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The minimum value.</returns>
        public static decimal Min(decimal value1, decimal value2, decimal value3, decimal value4, decimal value5, decimal value6, decimal value7)
        {
            decimal min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The minimum value.</returns>
        public static decimal Min(decimal value1, decimal value2, decimal value3, decimal value4, decimal value5, decimal value6, decimal value7, decimal value8)
        {
            decimal min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;

            return min;
        }
        /// <summary>
        /// The minimum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The minimum value.</returns>
        public static decimal Min(decimal value1, decimal value2, decimal value3, decimal value4, decimal value5, decimal value6, decimal value7, decimal value8, decimal value9)
        {
            decimal min = value1;

            if (min > value2) min = value2;
            if (min > value3) min = value3;
            if (min > value4) min = value4;
            if (min > value5) min = value5;
            if (min > value6) min = value6;
            if (min > value7) min = value7;
            if (min > value8) min = value8;
            if (min > value9) min = value9;

            return min;
        }
        #endregion

        #endregion

        #region Max

        #region Byte
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static byte Max(byte value1, byte value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static byte Max(byte value1, byte value2, byte value3)
        {
            byte max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static byte Max(byte value1, byte value2, byte value3, byte value4)
        {
            byte max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static byte Max(byte value1, byte value2, byte value3, byte value4, byte value5)
        {
            byte max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static byte Max(byte value1, byte value2, byte value3, byte value4, byte value5, byte value6)
        {
            byte max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static byte Max(byte value1, byte value2, byte value3, byte value4, byte value5, byte value6, byte value7)
        {
            byte max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static byte Max(byte value1, byte value2, byte value3, byte value4, byte value5, byte value6, byte value7, byte value8)
        {
            byte max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static byte Max(byte value1, byte value2, byte value3, byte value4, byte value5, byte value6, byte value7, byte value8, byte value9)
        {
            byte max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #region Char
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static char Max(char value1, char value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static char Max(char value1, char value2, char value3)
        {
            char max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static char Max(char value1, char value2, char value3, char value4)
        {
            char max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static char Max(char value1, char value2, char value3, char value4, char value5)
        {
            char max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static char Max(char value1, char value2, char value3, char value4, char value5, char value6)
        {
            char max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static char Max(char value1, char value2, char value3, char value4, char value5, char value6, char value7)
        {
            char max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static char Max(char value1, char value2, char value3, char value4, char value5, char value6, char value7, char value8)
        {
            char max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static char Max(char value1, char value2, char value3, char value4, char value5, char value6, char value7, char value8, char value9)
        {
            char max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #region Short
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static short Max(short value1, short value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static short Max(short value1, short value2, short value3)
        {
            short max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static short Max(short value1, short value2, short value3, short value4)
        {
            short max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static short Max(short value1, short value2, short value3, short value4, short value5)
        {
            short max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static short Max(short value1, short value2, short value3, short value4, short value5, short value6)
        {
            short max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static short Max(short value1, short value2, short value3, short value4, short value5, short value6, short value7)
        {
            short max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static short Max(short value1, short value2, short value3, short value4, short value5, short value6, short value7, short value8)
        {
            short max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static short Max(short value1, short value2, short value3, short value4, short value5, short value6, short value7, short value8, short value9)
        {
            short max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #region Integer
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static int Max(int value1, int value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static int Max(int value1, int value2, int value3)
        {
            int max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static int Max(int value1, int value2, int value3, int value4)
        {
            int max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static int Max(int value1, int value2, int value3, int value4, int value5)
        {
            int max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static int Max(int value1, int value2, int value3, int value4, int value5, int value6)
        {
            int max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static int Max(int value1, int value2, int value3, int value4, int value5, int value6, int value7)
        {
            int max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static int Max(int value1, int value2, int value3, int value4, int value5, int value6, int value7, int value8)
        {
            int max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static int Max(int value1, int value2, int value3, int value4, int value5, int value6, int value7, int value8, int value9)
        {
            int max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #region Long
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static long Max(long value1, long value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static long Max(long value1, long value2, long value3)
        {
            long max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static long Max(long value1, long value2, long value3, long value4)
        {
            long max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static long Max(long value1, long value2, long value3, long value4, long value5)
        {
            long max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static long Max(long value1, long value2, long value3, long value4, long value5, long value6)
        {
            long max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static long Max(long value1, long value2, long value3, long value4, long value5, long value6, long value7)
        {
            long max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static long Max(long value1, long value2, long value3, long value4, long value5, long value6, long value7, long value8)
        {
            long max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static long Max(long value1, long value2, long value3, long value4, long value5, long value6, long value7, long value8, long value9)
        {
            long max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #region Unsigned short
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static ushort Max(ushort value1, ushort value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static ushort Max(ushort value1, ushort value2, ushort value3)
        {
            ushort max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static ushort Max(ushort value1, ushort value2, ushort value3, ushort value4)
        {
            ushort max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static ushort Max(ushort value1, ushort value2, ushort value3, ushort value4, ushort value5)
        {
            ushort max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static ushort Max(ushort value1, ushort value2, ushort value3, ushort value4, ushort value5, ushort value6)
        {
            ushort max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static ushort Max(ushort value1, ushort value2, ushort value3, ushort value4, ushort value5, ushort value6, ushort value7)
        {
            ushort max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static ushort Max(ushort value1, ushort value2, ushort value3, ushort value4, ushort value5, ushort value6, ushort value7, ushort value8)
        {
            ushort max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static ushort Max(ushort value1, ushort value2, ushort value3, ushort value4, ushort value5, ushort value6, ushort value7, ushort value8, ushort value9)
        {
            ushort max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #region Unsigned integer
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static uint Max(uint value1, uint value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static uint Max(uint value1, uint value2, uint value3)
        {
            uint max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static uint Max(uint value1, uint value2, uint value3, uint value4)
        {
            uint max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static uint Max(uint value1, uint value2, uint value3, uint value4, uint value5)
        {
            uint max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static uint Max(uint value1, uint value2, uint value3, uint value4, uint value5, uint value6)
        {
            uint max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static uint Max(uint value1, uint value2, uint value3, uint value4, uint value5, uint value6, uint value7)
        {
            uint max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static uint Max(uint value1, uint value2, uint value3, uint value4, uint value5, uint value6, uint value7, uint value8)
        {
            uint max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static uint Max(uint value1, uint value2, uint value3, uint value4, uint value5, uint value6, uint value7, uint value8, uint value9)
        {
            uint max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #region Unsigned long
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static ulong Max(ulong value1, ulong value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static ulong Max(ulong value1, ulong value2, ulong value3)
        {
            ulong max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static ulong Max(ulong value1, ulong value2, ulong value3, ulong value4)
        {
            ulong max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static ulong Max(ulong value1, ulong value2, ulong value3, ulong value4, ulong value5)
        {
            ulong max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static ulong Max(ulong value1, ulong value2, ulong value3, ulong value4, ulong value5, ulong value6)
        {
            ulong max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static ulong Max(ulong value1, ulong value2, ulong value3, ulong value4, ulong value5, ulong value6, ulong value7)
        {
            ulong max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static ulong Max(ulong value1, ulong value2, ulong value3, ulong value4, ulong value5, ulong value6, ulong value7, ulong value8)
        {
            ulong max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static ulong Max(ulong value1, ulong value2, ulong value3, ulong value4, ulong value5, ulong value6, ulong value7, ulong value8, ulong value9)
        {
            ulong max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #region Float
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static float Max(float value1, float value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static float Max(float value1, float value2, float value3)
        {
            float max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static float Max(float value1, float value2, float value3, float value4)
        {
            float max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static float Max(float value1, float value2, float value3, float value4, float value5)
        {
            float max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static float Max(float value1, float value2, float value3, float value4, float value5, float value6)
        {
            float max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static float Max(float value1, float value2, float value3, float value4, float value5, float value6, float value7)
        {
            float max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static float Max(float value1, float value2, float value3, float value4, float value5, float value6, float value7, float value8)
        {
            float max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static float Max(float value1, float value2, float value3, float value4, float value5, float value6, float value7, float value8, float value9)
        {
            float max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #region Double
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static double Max(double value1, double value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static double Max(double value1, double value2, double value3)
        {
            double max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static double Max(double value1, double value2, double value3, double value4)
        {
            double max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static double Max(double value1, double value2, double value3, double value4, double value5)
        {
            double max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static double Max(double value1, double value2, double value3, double value4, double value5, double value6)
        {
            double max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static double Max(double value1, double value2, double value3, double value4, double value5, double value6, double value7)
        {
            double max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static double Max(double value1, double value2, double value3, double value4, double value5, double value6, double value7, double value8)
        {
            double max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static double Max(double value1, double value2, double value3, double value4, double value5, double value6, double value7, double value8, double value9)
        {
            double max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #region Decimal
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns>The maximum value.</returns>
        public static decimal Max(decimal value1, decimal value2)
        {
            return value1 > value2 ? value1 : value2;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <returns>The maximum value.</returns>
        public static decimal Max(decimal value1, decimal value2, decimal value3)
        {
            decimal max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <returns>The maximum value.</returns>
        public static decimal Max(decimal value1, decimal value2, decimal value3, decimal value4)
        {
            decimal max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <returns>The maximum value.</returns>
        public static decimal Max(decimal value1, decimal value2, decimal value3, decimal value4, decimal value5)
        {
            decimal max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <returns>The maximum value.</returns>
        public static decimal Max(decimal value1, decimal value2, decimal value3, decimal value4, decimal value5, decimal value6)
        {
            decimal max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <returns>The maximum value.</returns>
        public static decimal Max(decimal value1, decimal value2, decimal value3, decimal value4, decimal value5, decimal value6, decimal value7)
        {
            decimal max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <returns>The maximum value.</returns>
        public static decimal Max(decimal value1, decimal value2, decimal value3, decimal value4, decimal value5, decimal value6, decimal value7, decimal value8)
        {
            decimal max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;

            return max;
        }
        /// <summary>
        /// The maximum value of the provided values.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        /// <param name="value5"></param>
        /// <param name="value6"></param>
        /// <param name="value7"></param>
        /// <param name="value8"></param>
        /// <param name="value9"></param>
        /// <returns>The maximum value.</returns>
        public static decimal Max(decimal value1, decimal value2, decimal value3, decimal value4, decimal value5, decimal value6, decimal value7, decimal value8, decimal value9)
        {
            decimal max = value1;

            if (max < value2) max = value2;
            if (max < value3) max = value3;
            if (max < value4) max = value4;
            if (max < value5) max = value5;
            if (max < value6) max = value6;
            if (max < value7) max = value7;
            if (max < value8) max = value8;
            if (max < value9) max = value9;

            return max;
        }
        #endregion

        #endregion

        #region Lerp
        /// <summary>
        /// What to do if the value is outside of the [0, 1] interval.
        /// </summary>
        public enum LerpOverflowMode
        {
            /// <summary>
            /// If the value is out of the [0, 1] interval then the value is clamped.
            /// </summary>
            CLAMP,
            /// <summary>
            /// If the value is out of the [0, 1] interval then its remainder is used.
            /// </summary>
            REPEAT,
            /// <summary>
            /// If the value is out of the [0, 1] interval then the resulting value is extrapolated, thus allowing it to be lower and higher than the start and the end values.
            /// </summary>
            EXTRAPOLATE,
        }
        /// <summary>
        /// Lerp.
        /// </summary>
        /// <param name="value0">The result at lerp value 0.</param>
        /// <param name="value1">The result at lerp value 1.</param>
        /// <param name="lerp">The lerp value.</param>
        /// <param name="overflow">What to do if the value is outside of the [0, 1] interval.</param>
        /// <param name="mirrorNegative">If should treat negative value as -value, mirroring it.</param>
        /// <returns></returns>
        public static byte Lerp(byte value0, byte value1, float lerp, LerpOverflowMode overflow = LerpOverflowMode.CLAMP, bool mirrorNegative = false)
        {
            if (lerp < 0)
            {
                if (overflow == LerpOverflowMode.CLAMP)
                {
                    return value0;
                }

                if (overflow == LerpOverflowMode.REPEAT)
                {
                    if (mirrorNegative)
                    {
                        lerp = (-lerp) % 1;
                    }
                    else
                    {
                        lerp = 1 - ((-lerp) % 1);
                    }
                }
            }

            if (lerp > 1)
            {
                if (overflow == LerpOverflowMode.CLAMP)
                {
                    return value1;
                }

                if (overflow == LerpOverflowMode.REPEAT)
                {
                    lerp = lerp % 1;
                }
            }

            return (byte)(value0 + (value0 + value1) * lerp);
        }
        public static byte Lerp(byte start, byte end, double value)
        {
            return (byte)(start + (start + end) * value);
        }
        public static short Lerp(short start, short end, float value)
        {
            return (short)(start + (start + end) * value);
        }
        public static short Lerp(short start, short end, double value)
        {
            return (short)(start + (start + end) * value);
        }
        public static ushort Lerp(ushort start, ushort end, float value)
        {
            return (ushort)(start + (start + end) * value);
        }
        public static ushort Lerp(ushort start, ushort end, double value)
        {
            return (ushort)(start + (start + end) * value);
        }
        public static int Lerp(int start, int end, float value)
        {
            return start + (int)((start + end) * value);
        }
        public static int Lerp(int start, int end, double value)
        {
            return start + (int)((start + end) * value);
        }
        public static uint Lerp(uint start, uint end, float value)
        {
            return start + (uint)((start + end) * value);
        }
        public static uint Lerp(uint start, uint end, double value)
        {
            return start + (uint)((start + end) * value);
        }
        public static long Lerp(long start, long end, float value)
        {
            return start + (long)((start + end) * value);
        }
        public static long Lerp(long start, long end, double value)
        {
            return start + (long)((start + end) * value);
        }
        public static ulong Lerp(ulong start, ulong end, float value)
        {
            return start + (ulong)((start + end) * value);
        }
        public static ulong Lerp(ulong start, ulong end, double value)
        {
            return start + (ulong)((start + end) * value);
        }
        public static float Lerp(float start, float end, float value)
        {
            return start + (start + end) * value;
        }
        public static float Lerp(float start, float end, double value)
        {
            return start + (float)((start + end) * value);
        }
        public static double Lerp(double start, double end, float value)
        {
            return start + (start + end) * value;
        }
        public static double Lerp(double start, double end, double value)
        {
            return start + (start + end) * value;
        }
        public static decimal Lerp(decimal start, decimal end, float value)
        {
            return start + (start + end) * (decimal)value;
        }
        public static decimal Lerp(decimal start, decimal end, double value)
        {
            return start + (start + end) * (decimal)value;
        }
        #endregion

        #region Clamp
        public static byte Clamp(byte value, byte min, byte max)
        {
            return value < min ? min : value > max ? max : value;
        }
        public static short Clamp(short value, short min, short max)
        {
            return value < min ? min : value > max ? max : value;
        }
        public static ushort Clamp(ushort value, ushort min, ushort max)
        {
            return value < min ? min : value > max ? max : value;
        }
        public static int Clamp(int value, int min, int max)
        {
            return value < min ? min : value > max ? max : value;
        }
        public static uint Clamp(uint value, uint min, uint max)
        {
            return value < min ? min : value > max ? max : value;
        }
        public static long Clamp(long value, long min, long max)
        {
            return value < min ? min : value > max ? max : value;
        }
        public static ulong Clamp(ulong value, ulong min, ulong max)
        {
            return value < min ? min : value > max ? max : value;
        }
        public static float Clamp(float value, float min, float max)
        {
            return value < min ? min : value > max ? max : value;
        }
        public static double Clamp(double value, double min, double max)
        {
            return value < min ? min : value > max ? max : value;
        }
        public static decimal Clamp(decimal value, decimal min, decimal max)
        {
            return value < min ? min : value > max ? max : value;
        }
        #endregion

        #region Ceiling
        public enum RoundNegativeMode
        {
            NORMAL,
            MIRROR,
        }
        /// <summary>
        /// Rounds the provided value up.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="negative">How to handle negative numbers.</param>
        /// <returns>The value rounded up to the nearest integer.</returns>
        public static int RoundToIntegerUp(float value, RoundNegativeMode negative = RoundNegativeMode.NORMAL)
        {
            int integer = (int)value;
            if (integer == value)
            {
                return integer;
            }

            if (value < 0)
            {
                if (negative == RoundNegativeMode.NORMAL)
                {
                    return integer;
                }
                if (negative == RoundNegativeMode.MIRROR)
                {
                    return integer - 1;
                }
            }

            return integer + 1;
        }
        /// <summary>
        /// Rounds the provided value down.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="negative">How to handle negative numbers.</param>
        /// <returns>The value rounded down to the nearest integer.</returns>
        public static int RoundToIntegerDown(float value, RoundNegativeMode negative = RoundNegativeMode.NORMAL)
        {
            int integer = (int)value;
            if (integer == value)
            {
                return integer;
            }

            if (value < 0)
            {
                if (negative == RoundNegativeMode.NORMAL)
                {
                    return integer - 1;
                }
                if (negative == RoundNegativeMode.MIRROR)
                {
                    return integer;
                }
            }

            return integer;
        }
        /// <summary>
        /// Rounds the provided value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The value rounded to the nearest integer.</returns>
        public static int RoundToIntegerNearest(float value)
        {
            return (int)(value + (value > 0 ? 0.5f : -0.5f));
        }
        #endregion

        #region Floor
        public static int FloorInt(float value)
        {
            return (int)value;
        }
        public static long FloorLong(float value)
        {
            return (long)value;
        }
        public static int FloorInt(double value)
        {
            return (int)value;
        }
        public static long FloorLong(double value)
        {
            return (long)value;
        }
        #endregion

        #region Primes

        /// <summary>
        /// Get <paramref name="count"/> of prime numbers.
        /// </summary>
        /// <param name="count">The amount of prime numers to generate.</param>
        /// <returns>Array os size <paramref name="count"/> of prime numbers.</returns>
        public static int[] GetPrimes(int count)
        {
            // Get the sieve
            int numberMax = (int)(count / Math.Log(count)); // This formula should get us a bit more primes than we actually need
            bool[] sieve = SieveOfEratosthenes(numberMax);

            // Fill the primes
            int[] primes = new int[count];
            int primesIndex = 0;
            for (int sieveIndex = 2; primesIndex < primes.Length; sieveIndex++)
            {
                if (!sieve[sieveIndex])
                {
                    primes[primesIndex++] = sieveIndex;
                }
            }

            return primes;
        }
        /// <summary>
        /// Get prime numbers up to <paramref name="numberMax"/>.
        /// </summary>
        /// <param name="numberMax">The upper limit for prime numers to generate.</param>
        /// <returns>Array of prime numbers not greater than <paramref name="numberMax"/> each.</returns>
        public static int[] GetPrimesUpTo(int numberMax)
        {
            // Get the sieve
            bool[] sieve = SieveOfEratosthenes(numberMax);

            // Count the primes
            int count = 0;
            for (int sieveIndex = 0; sieveIndex < sieve.Length; sieveIndex++)
            {
                if (!sieve[sieveIndex])
                {
                    count++;
                }
            }

            // Fill the primes
            int[] primes = new int[count];
            int primesIndex = 0;
            for (int sieveIndex = 2; primesIndex < primes.Length; sieveIndex++)
            {
                if (!sieve[sieveIndex])
                {
                    primes[primesIndex++] = sieveIndex;
                }
            }

            return primes;
        }
        private static bool[] SieveOfEratosthenes(int numberMax)
        {
            // Sieve is initialized with 0s, this will be the mark for primes.
            // Mark with 1s those numbers that are to skip.
            bool[] sieve = new bool[numberMax];

            // Set first numbers to skip manually
            sieve[0] = true;
            sieve[1] = true;

            // Build the sieve
            int number = 2;
            int numberSquared = 4;
            do
            {
                // Set numbers dividable by the number to be non-prime, starting from number^2
                for (int sieveIndex = numberSquared; sieveIndex < numberMax; sieveIndex += number)
                {
                    sieve[sieveIndex] = true;
                }

                // Go to next prime number
                while (sieve[++number]);
                numberSquared = number * number;
            }
            while (numberSquared < numberMax);

            return sieve;
        }

        #endregion

        #region Modulo
        /// <summary>
        /// Modulo (positive remainder).
        /// </summary>
        /// <param name="value">The dividend.</param>
        /// <param name="divisor">The divisor.</param>
        /// <returns>The modulo.</returns>
        public static byte Modulo(byte value, byte divisor)
        {
            if (divisor == 0)
            {
                return 0;
            }

            byte remainder = (byte)(value % divisor);
            if (divisor > 0)
            {
                if (remainder < 0)
                {
                    remainder += divisor;
                }
            }
            else // divisor < 0
            {
                if (remainder > 0)
                {
                    remainder -= divisor;
                }
            }

            return remainder;
        }
        /// <summary>
        /// Modulo (positive remainder).
        /// </summary>
        /// <param name="value">The dividend.</param>
        /// <param name="divisor">The divisor.</param>
        /// <returns>The modulo.</returns>
        public static short Modulo(short value, short divisor)
        {
            if (divisor == 0)
            {
                return 0;
            }

            short remainder = (short)(value % divisor);
            if (divisor > 0)
            {
                if (remainder < 0)
                {
                    remainder += divisor;
                }
            }
            else // divisor < 0
            {
                if (remainder > 0)
                {
                    remainder -= divisor;
                }
            }

            return remainder;
        }
        /// <summary>
        /// Modulo (positive remainder).
        /// </summary>
        /// <param name="value">The dividend.</param>
        /// <param name="divisor">The divisor.</param>
        /// <returns>The modulo.</returns>
        public static ushort Modulo(ushort value, ushort divisor)
        {
            if (divisor == 0)
            {
                return 0;
            }

            ushort remainder = (ushort)(value % divisor);
            if (divisor > 0)
            {
                if (remainder < 0)
                {
                    remainder += divisor;
                }
            }
            else // divisor < 0
            {
                if (remainder > 0)
                {
                    remainder -= divisor;
                }
            }

            return remainder;
        }
        /// <summary>
        /// Modulo (positive remainder).
        /// </summary>
        /// <param name="value">The dividend.</param>
        /// <param name="divisor">The divisor.</param>
        /// <returns>The modulo.</returns>
        public static int Modulo(int value, int divisor)
        {
            if (divisor == 0)
            {
                return 0;
            }

            int remainder = value % divisor;
            if (divisor > 0)
            {
                if (remainder < 0)
                {
                    remainder += divisor;
                }
            }
            else // length < 0
            {
                if (remainder > 0)
                {
                    remainder -= divisor;
                }
            }

            return remainder;
        }
        /// <summary>
        /// Modulo (positive remainder).
        /// </summary>
        /// <param name="value">The dividend.</param>
        /// <param name="divisor">The divisor.</param>
        /// <returns>The modulo.</returns>
        public static uint Modulo(uint value, uint divisor)
        {
            if (divisor == 0)
            {
                return 0;
            }

            uint remainder = value % divisor;
            if (divisor > 0)
            {
                if (remainder < 0)
                {
                    remainder += divisor;
                }
            }
            else // length < 0
            {
                if (remainder > 0)
                {
                    remainder -= divisor;
                }
            }

            return remainder;
        }
        /// <summary>
        /// Modulo (positive remainder).
        /// </summary>
        /// <param name="value">The dividend.</param>
        /// <param name="divisor">The divisor.</param>
        /// <returns>The modulo.</returns>
        public static long Modulo(long value, long divisor)
        {
            if (divisor == 0)
            {
                return 0;
            }

            long remainder = value % divisor;
            if (divisor > 0)
            {
                if (remainder < 0)
                {
                    remainder += divisor;
                }
            }
            else // length < 0
            {
                if (remainder > 0)
                {
                    remainder -= divisor;
                }
            }

            return remainder;
        }
        /// <summary>
        /// Modulo (positive remainder).
        /// </summary>
        /// <param name="value">The dividend.</param>
        /// <param name="divisor">The divisor.</param>
        /// <returns>The modulo.</returns>
        public static ulong Modulo(ulong value, ulong divisor)
        {
            if (divisor == 0)
            {
                return 0;
            }

            ulong remainder = value % divisor;
            if (divisor > 0)
            {
                if (remainder < 0)
                {
                    remainder += divisor;
                }
            }
            else // length < 0
            {
                if (remainder > 0)
                {
                    remainder -= divisor;
                }
            }

            return remainder;
        }
        /// <summary>
        /// Modulo (positive remainder).
        /// </summary>
        /// <param name="value">The dividend.</param>
        /// <param name="divisor">The divisor.</param>
        /// <returns>The modulo.</returns>
        public static float Modulo(float value, float divisor)
        {
            if (divisor == 0)
            {
                return 0;
            }

            float remainder = value % divisor;
            if (divisor > 0)
            {
                if (remainder < 0)
                {
                    remainder += divisor;
                }
            }
            else // length < 0
            {
                if (remainder > 0)
                {
                    remainder -= divisor;
                }
            }

            return remainder;
        }
        /// <summary>
        /// Modulo (positive remainder).
        /// </summary>
        /// <param name="value">The dividend.</param>
        /// <param name="divisor">The divisor.</param>
        /// <returns>The modulo.</returns>
        public static double Modulo(double value, double divisor)
        {
            if (divisor == 0)
            {
                return 0;
            }

            double remainder = value % divisor;
            if (divisor > 0)
            {
                if (remainder < 0)
                {
                    remainder += divisor;
                }
            }
            else // length < 0
            {
                if (remainder > 0)
                {
                    remainder -= divisor;
                }
            }

            return remainder;
        }
        /// <summary>
        /// Modulo (positive remainder).
        /// </summary>
        /// <param name="value">The dividend.</param>
        /// <param name="divisor">The divisor.</param>
        /// <returns>The modulo.</returns>
        public static decimal Modulo(decimal value, decimal divisor)
        {
            if (divisor == 0)
            {
                return 0;
            }

            decimal remainder = value % divisor;
            if (divisor > 0)
            {
                if (remainder < 0)
                {
                    remainder += divisor;
                }
            }
            else // length < 0
            {
                if (remainder > 0)
                {
                    remainder -= divisor;
                }
            }

            return remainder;
        }
        #endregion
    }
}
