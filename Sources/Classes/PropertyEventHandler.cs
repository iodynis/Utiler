﻿namespace Iodynis.Libraries.Utility
{
    public delegate void PropertyEventHandler<TObject, TValue>(TObject sender, TValue valueOld, TValue valueNew);
}
