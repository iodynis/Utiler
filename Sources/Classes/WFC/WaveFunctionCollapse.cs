﻿using Iodynis.Libraries.Utility.Extensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Iodynis.Libraries.Utility
{
    public static partial class WaveFunctionCollapse
    {
        public class Map<T>
        {
            private const byte STATE_NONE = 0;
            private const byte X_POSITIVE_INDEX = 0;
            private const byte X_NEGATIVE_INDEX = 1;
            private const byte Y_POSITIVE_INDEX = 2;
            private const byte Y_NEGATIVE_INDEX = 3;
            private const byte Z_POSITIVE_INDEX = 4;
            private const byte Z_NEGATIVE_INDEX = 5;
            private const byte X_POSITIVE_FLAG = 1 << X_POSITIVE_INDEX;
            private const byte X_NEGATIVE_FLAG = 1 << X_NEGATIVE_INDEX;
            private const byte Y_POSITIVE_FLAG = 1 << Y_POSITIVE_INDEX;
            private const byte Y_NEGATIVE_FLAG = 1 << Y_NEGATIVE_INDEX;
            private const byte Z_POSITIVE_FLAG = 1 << Z_POSITIVE_INDEX;
            private const byte Z_NEGATIVE_FLAG = 1 << Z_NEGATIVE_INDEX;
            private const byte STATE_COLLAPSED = 1 << 6;

            private class Rule
            {
                public ulong Variant;
                public ulong[] VariantsPossible;
                public Rule(ulong variant)
                {
                    Variant = variant;
                    VariantsPossible = new ulong[6];
                }
            }

            private bool[] Random;
            private const int RandomCount = 256;
            private int RandomIndex = 0;

            public int SizeX;
            public int SizeY;
            public int SizeZ;

            public int[] ExpandedCountX;
            //public int[] ExpandedY;
            //public int[] ExpandedZ;

            public int[,] ExpandedCountXY;
            //public int[,] ExpandedYZ;
            //public int[,] ExpandedXZ;

            public int ExpandedCount;

            private Stack<int> DirtyX = new Stack<int>();
            private Stack<int> DirtyY = new Stack<int>();
            private Stack<int> DirtyZ = new Stack<int>();

            private ulong[,,] CellsVariants;
            /// <summary>
            /// 
            /// </summary>
            private byte[,,] CellsState;
            private Rule[] Rules;
            private T[] Prefabs;
            private T Error;

            PersistentRandom PersistentRandom;

            public Map(int sizeX, int sizeY, int sizeZ, long seed, T[,,] example, T error)
            {
                PersistentRandom = new PersistentRandom(seed);

                SizeX = sizeX;
                SizeY = sizeY;
                SizeZ = sizeZ;

                CellsVariants = new ulong[SizeX,SizeY, SizeZ];
                CellsState = new byte[SizeX, SizeY, SizeZ];
                Rules = new Rule[64];
                Error = error;

                ExpandedCountX = new int[SizeX];
                ExpandedCountX.Initialize(SizeY * SizeZ);
                //ExpandedY = new int[SizeY];
                //ExpandedZ = new int[SizeZ];

                ExpandedCountXY = new int[SizeX, SizeY];
                ExpandedCountXY.Initialize(SizeZ);
                //ExpandedYZ = new int[SizeY, SizeZ];
                //ExpandedXZ = new int[SizeX, SizeZ];

                ExpandedCount = SizeX * SizeY * SizeZ;

                //ExpandedY.Initialize(SizeX * SizeZ);
                //ExpandedZ.Initialize(SizeX * SizeY);

                //ExpandedYZ.Initialize(SizeX);
                //ExpandedXZ.Initialize(SizeY);

                Random = new bool[RandomCount + 6];
                for (int i = 0; i < Random.Length; i++)
                {
                    Random[i] = PersistentRandom.Take();
                }

                ProcessExample(example);

                // Set initial variants value
                ulong variantsInitial = 0;
                for (int i = 0; i < Prefabs.Length; i++)
                {
                    variantsInitial |= 1UL << i;
                }
                for (int x = 0; x < SizeX; x++)
                {
                    for (int y = 0; y < SizeY; y++)
                    {
                        for (int z = 0; z < SizeZ; z++)
                        {
                            CellsVariants[x, y, z] = variantsInitial;
                        }
                    }
                }
            }

            private void ProcessExample(T[,,] example)
            {
                //Dictionary<T, int> prefabToIndex = new Dictionary<T, int>();
                //List<HashSet<T>[]> prefabIndexToCases = new List<HashSet<T>[]>();

                Dictionary<T, HashSet<T>[]> prefabToCases = new Dictionary<T, HashSet<T>[]>();
                prefabToCases.Add(Error, new HashSet<T>[]
                {
                    new HashSet<T>(),
                    new HashSet<T>(),
                    new HashSet<T>(),
                    new HashSet<T>(),
                    new HashSet<T>(),
                    new HashSet<T>(),
                });

                int xMax = example.GetLength(0);
                int yMax = example.GetLength(1);
                int zMax = example.GetLength(2);

                for (int x = 0; x < xMax; x++)
                {
                    for (int y = 0; y < yMax; y++)
                    {
                        for (int z = 0; z < zMax; z++)
                        {
                            T prefab = example[x, y, z];

                            //if (!prefabToIndex.TryGetValue(prefab, out int index))
                            //{
                            //    HashSet<int>[] cases = new HashSet<int>[]
                            //    {
                            //            new HashSet<int>(),
                            //            new HashSet<int>(),
                            //            new HashSet<int>(),
                            //            new HashSet<int>(),
                            //            new HashSet<int>(),
                            //            new HashSet<int>(),
                            //    };
                            //    prefabIndexToCases.Add(cases);
                            //}

                            if (!prefabToCases.TryGetValue(prefab, out HashSet<T>[] cases))
                            {
                                cases = new HashSet<T>[]
                                {
                                    new HashSet<T>(),
                                    new HashSet<T>(),
                                    new HashSet<T>(),
                                    new HashSet<T>(),
                                    new HashSet<T>(),
                                    new HashSet<T>(),
                                };
                                prefabToCases.Add(prefab, cases);
                            }

                            if (x > 0)
                            {
                                cases[X_NEGATIVE_INDEX].Add(example[x - 1, y, z]);
                            }
                            if (x < xMax - 1)
                            {
                                cases[X_POSITIVE_INDEX].Add(example[x + 1, y, z]);
                            }
                            if (y > 0)
                            {
                                cases[Y_NEGATIVE_INDEX].Add(example[x, y - 1, z]);
                            }
                            if (y < yMax - 1)
                            {
                                cases[Y_POSITIVE_INDEX].Add(example[x, y + 1, z]);
                            }
                            if (z > 0)
                            {
                                cases[Y_NEGATIVE_INDEX].Add(example[x, y, z - 1]);
                            }
                            if (z < zMax - 1)
                            {
                                cases[Y_POSITIVE_INDEX].Add(example[x, y, z + 1]);
                            }
                        }
                    }
                }

                //var list = prefabToCases.Keys.ToList();
                //list.Insert(0, Error);
                Prefabs = prefabToCases.Keys.ToArray();

                for (int i = 0; i < Prefabs.Length; i++)
                {
                    Rule rule = new Rule(1UL << i);

                    HashSet<T>[] cases = prefabToCases[Prefabs[i]];

                    foreach (T prefab in cases[X_NEGATIVE_INDEX])
                    {
                        int index = Prefabs.IndexOf(prefab);
                        rule.VariantsPossible[X_NEGATIVE_INDEX] |= 1UL << index;
                    }
                    foreach (T prefab in cases[X_POSITIVE_INDEX])
                    {
                        int index = Prefabs.IndexOf(prefab);
                        rule.VariantsPossible[X_POSITIVE_INDEX] |= 1UL << index;
                    }
                    foreach (T prefab in cases[Y_NEGATIVE_INDEX])
                    {
                        int index = Prefabs.IndexOf(prefab);
                        rule.VariantsPossible[Y_NEGATIVE_INDEX] |= 1UL << index;
                    }
                    foreach (T prefab in cases[Y_POSITIVE_INDEX])
                    {
                        int index = Prefabs.IndexOf(prefab);
                        rule.VariantsPossible[Y_POSITIVE_INDEX] |= 1UL << index;
                    }
                    foreach (T prefab in cases[Z_NEGATIVE_INDEX])
                    {
                        int index = Prefabs.IndexOf(prefab);
                        rule.VariantsPossible[Z_NEGATIVE_INDEX] |= 1UL << index;
                    }
                    foreach (T prefab in cases[Z_POSITIVE_INDEX])
                    {
                        int index = Prefabs.IndexOf(prefab);
                        rule.VariantsPossible[Z_POSITIVE_INDEX] |= 1UL << index;
                    }

                    Rules[i] = rule;
                }
            }

            public T[,,] Run()
            {
                while (ExpandedCount > 0)
                {
                    Iterate();
                }

                T[,,] result = new T[SizeX, SizeY, SizeZ];

                for (int x = 0; x < SizeX; x++)
                {
                    for (int y = 0; y < SizeY; y++)
                    {
                        for (int z = 0; z < SizeZ; z++)
                        {
                            int index = CellsVariants[x, y, z].HighestBitIndex();
                            result[x, y, z] = Prefabs[index];
                        }
                    }
                }

                return result;
            }
            private void Iterate()
            {
                // Find a non-collapsed cell
                int x = PersistentRandom.Take(0, SizeX);
                for (;; x++)
                {
                    if (x >= SizeX)
                    {
                        x = 0;
                    }
                    if (ExpandedCountX[x] > 0)
                    {
                        break;
                    }
                }
                int y = PersistentRandom.Take(0, SizeY);
                for (;; y++)
                {
                    if (y >= SizeY)
                    {
                        y = 0;
                    }
                    if (ExpandedCountXY[x, y] > 0)
                    {
                        break;
                    }
                }
                int z = PersistentRandom.Take(0, SizeZ);
                for (;; z++)
                {
                    if (z >= SizeZ)
                    {
                        z = 0;
                    }
                    if (CellsState[x, y, z] != STATE_COLLAPSED)
                    {
                        break;
                    }
                }

                RandomIndex++;
                if (RandomIndex >= RandomCount)
                {
                    RandomIndex = 0;
                }

                // Collapse
                ulong variant = ChooseRandomVariant(CellsVariants[x, y, z]);
                CellsVariants[x, y, z] = variant;
                CellsState[x, y, z] = STATE_COLLAPSED;
                ExpandedCountXY[x, y]--;
                ExpandedCountX[x]--;
                ExpandedCount--;

                if (variant == 0)
                {
                    ;
                }
                else
                {
                    // Update nearby cells
                    if (x > 0)
                    {
                        CellsState[x - 1, y, z] |= X_POSITIVE_FLAG;
                        DirtyX.Add(x - 1);
                        DirtyY.Add(y);
                        DirtyZ.Add(z);
                    }
                    if (x < SizeX - 1)
                    {
                        CellsState[x + 1, y, z] |= X_NEGATIVE_FLAG;
                        DirtyX.Add(x + 1);
                        DirtyY.Add(y);
                        DirtyZ.Add(z);
                    }
                    if (y > 0)
                    {
                        CellsState[x, y - 1, z] |= Y_POSITIVE_FLAG;
                        DirtyX.Add(x);
                        DirtyY.Add(y - 1);
                        DirtyZ.Add(z);
                    }
                    if (y < SizeY - 1)
                    {
                        CellsState[x, y + 1, z] |= Y_NEGATIVE_FLAG;
                        DirtyX.Add(x);
                        DirtyY.Add(y + 1);
                        DirtyZ.Add(z);
                    }
                    if (z > 0)
                    {
                        CellsState[x, y, z - 1] |= Z_POSITIVE_FLAG;
                        DirtyX.Add(x);
                        DirtyY.Add(y);
                        DirtyZ.Add(z - 1);
                    }
                    if (z < SizeZ - 1)
                    {
                        CellsState[x, y, z + 1] |= Z_NEGATIVE_FLAG;
                        DirtyX.Add(x);
                        DirtyY.Add(y);
                        DirtyZ.Add(z + 1);
                    }
                }

                // Update cells
                while (DirtyX.Count > 0)
                {
                    int x2 = DirtyX.Pop();
                    int y2 = DirtyY.Pop();
                    int z2 = DirtyZ.Pop();

                    ulong variantsBefore = CellsVariants[x2, y2, z2];
                    ulong variantsAfter = variantsBefore;

                    byte state = CellsState[x2, y2, z2];

                    if ((state & X_NEGATIVE_FLAG) != 0)
                    {
                        variantsAfter &= GetAvailableVariantsInDirection(CellsVariants[x2 - 1, y2, z2], X_POSITIVE_INDEX);
                    }
                    if ((state & X_POSITIVE_FLAG) != 0)
                    {
                        variantsAfter &= GetAvailableVariantsInDirection(CellsVariants[x2 + 1, y2, z2], X_NEGATIVE_INDEX);
                    }
                    if ((state & Y_NEGATIVE_FLAG) != 0)
                    {
                        variantsAfter &= GetAvailableVariantsInDirection(CellsVariants[x2, y2 - 1, z2], Y_POSITIVE_INDEX);
                    }
                    if ((state & Y_POSITIVE_FLAG) != 0)
                    {
                        variantsAfter &= GetAvailableVariantsInDirection(CellsVariants[x2, y2 + 1, z2], Y_NEGATIVE_INDEX);
                    }
                    if ((state & Z_NEGATIVE_FLAG) != 0)
                    {
                        variantsAfter &= GetAvailableVariantsInDirection(CellsVariants[x2, y2, z2 - 1], Z_POSITIVE_INDEX);
                    }
                    if ((state & Z_POSITIVE_FLAG) != 0)
                    {
                        variantsAfter &= GetAvailableVariantsInDirection(CellsVariants[x2, y2, z2 + 1], Z_NEGATIVE_INDEX);
                    }

                    // Check if some variants left
                    if (variantsAfter == 0)
                    {
                        //throw new Exception("Not enough blocks.");
                    }

                    if (variantsAfter != variantsBefore)
                    {
                        CellsVariants[x2, y2, z2] = variantsAfter;

                        if (variantsAfter.BitsCount() == 1)
                        {
                            CellsState[x2, y2, z2] = STATE_COLLAPSED;
                            ExpandedCountXY[x2, y2]--;
                            ExpandedCountX[x2]--;
                            ExpandedCount--;
                        }

                        if (variantsAfter == 0)
                        {
                            List<T> xNegative = (state & X_NEGATIVE_FLAG) != 0 ? GetPrefabs(CellsVariants[x2 - 1, y2, z2]) : null;
                            List<T> xPositive = (state & X_POSITIVE_FLAG) != 0 ? GetPrefabs(CellsVariants[x2 + 1, y2, z2]) : null;
                            List<T> yNegative = (state & Y_NEGATIVE_FLAG) != 0 ? GetPrefabs(CellsVariants[x2, y2 - 1, z2]) : null;
                            List<T> yPositive = (state & Y_POSITIVE_FLAG) != 0 ? GetPrefabs(CellsVariants[x2, y2 + 1, z2]) : null;
                            List<T> zNegative = (state & Z_NEGATIVE_FLAG) != 0 ? GetPrefabs(CellsVariants[x2, y2, z2 - 1]) : null;
                            List<T> zPositive = (state & Z_POSITIVE_FLAG) != 0 ? GetPrefabs(CellsVariants[x2, y2, z2 + 1]) : null;
                            List<T> center = GetPrefabs(variantsBefore);


                            ;
                        }
                        else
                        {
                            // Update nearby cells
                            if (x2 > 0 && CellsState[x2 - 1, y2, z2] != STATE_COLLAPSED)
                            {
                                CellsState[x2 - 1, y2, z2] |= X_POSITIVE_FLAG;
                                DirtyX.Add(x2 - 1);
                                DirtyY.Add(y2);
                                DirtyZ.Add(z2);
                            }
                            if (x2 < SizeX - 1 && CellsState[x2 + 1, y2, z2] != STATE_COLLAPSED)
                            {
                                CellsState[x2 + 1, y2, z2] |= X_NEGATIVE_FLAG;
                                DirtyX.Add(x2 + 1);
                                DirtyY.Add(y2);
                                DirtyZ.Add(z2);
                            }
                            if (y2 > 0 && CellsState[x, y2 - 1, z2] != STATE_COLLAPSED)
                            {
                                CellsState[x2, y2 - 1, z2] |= Y_POSITIVE_FLAG;
                                DirtyX.Add(x2);
                                DirtyY.Add(y2 - 1);
                                DirtyZ.Add(z2);
                            }
                            if (y2 < SizeY - 1 && CellsState[x2, y2 + 1, z2] != STATE_COLLAPSED)
                            {
                                CellsState[x2, y2 + 1, z2] |= Y_NEGATIVE_FLAG;
                                DirtyX.Add(x2);
                                DirtyY.Add(y2 + 1);
                                DirtyZ.Add(z2);
                            }
                            if (z2 > 0 && CellsState[x2, y2, z2 - 1] != STATE_COLLAPSED)
                            {
                                CellsState[x2, y2, z2 - 1] |= Z_POSITIVE_FLAG;
                                DirtyX.Add(x2);
                                DirtyY.Add(y2);
                                DirtyZ.Add(z2 - 1);
                            }
                            if (z2 < SizeZ - 1 && CellsState[x2, y2, z2 + 1] != STATE_COLLAPSED)
                            {
                                CellsState[x2, y2, z2 + 1] |= Z_NEGATIVE_FLAG;
                                DirtyX.Add(x2);
                                DirtyY.Add(y2);
                                DirtyZ.Add(z2 + 1);
                            }
                        }
                    }
                }
            }

            private ulong ChooseRandomVariant(ulong variants)
            {
                if ((variants & 0x0000_0000_FFFF_FFFF) != 0 && (Random[RandomIndex] || (variants & 0xFFFF_FFFF_0000_0000) == 0))
                {
                    if ((variants & 0x0000_0000_0000_FFFF) != 0 && (Random[RandomIndex + 1] || (variants & 0x0000_0000_FFFF_0000) == 0))
                    {
                        if ((variants & 0x0000_0000_0000_00FF) != 0 && (Random[RandomIndex + 2] || (variants & 0x0000_0000_0000_FF00) == 0))
                        {
                            if ((variants & 0x0000_0000_0000_000F) != 0 && (Random[RandomIndex + 3] || (variants & 0x0000_0000_0000_00F0) == 0))
                            {
                                if ((variants & 0x0000_0000_0000_0003) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_0000_0000_000B) == 0))
                                {
                                    if ((variants & 0x0000_0000_0000_0001) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0000_0002) == 0))
                                    {
                                        return 0x0000_0000_0000_0001;
                                    }
                                    else // ((variants & 0x0000_0000_0000_0002) != 0)
                                    {
                                        return 0x0000_0000_0000_0002;
                                    }
                                }
                                else // ((variants & 0x0000_0000_0000_000B) != 0)
                                {
                                    if ((variants & 0x0000_0000_0000_0004) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0000_0008) == 0))
                                    {
                                        return 0x0000_0000_0000_0004;
                                    }
                                    else // ((variants & 0x0000_0000_0000_0008) != 0)
                                    {
                                        return 0x0000_0000_0000_0008;
                                    }
                                }
                            }
                            else // ((variants & 0x0000_0000_0000_00F0) != 0)
                            {
                                if ((variants & 0x0000_0000_0000_0030) != 0)
                                {
                                    if ((variants & 0x0000_0000_0000_0010) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0000_0020) == 0))
                                    {
                                        return 0x0000_0000_0000_0010;
                                    }
                                    else // ((variants & 0x0000_0000_0000_0020) != 0)
                                    {
                                        return 0x0000_0000_0000_0020;
                                    }
                                }
                                else // ((variants & 0x0000_0000_0000_00B0) != 0)
                                {
                                    if ((variants & 0x0000_0000_0000_0040) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0000_0080) == 0))
                                    {
                                        return 0x0000_0000_0000_0040;
                                    }
                                    else // ((variants & 0x0000_0000_0000_0080) != 0)
                                    {
                                        return 0x0000_0000_0000_0080;
                                    }
                                }
                            }
                        }
                        else // ((variants & 0x0000_0000_0000_FF00) != 0)
                        {
                            if ((variants & 0x0000_0000_0000_0F00) != 0 && (Random[RandomIndex + 3] || (variants & 0x0000_0000_0000_F000) == 0))
                            {
                                if ((variants & 0x0000_0000_0000_0300) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_0000_0000_0B00) == 0))
                                {
                                    if ((variants & 0x0000_0000_0000_0100) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0000_0200) == 0))
                                    {
                                        return 0x0000_0000_0000_0100;
                                    }
                                    else // ((variants & 0x0000_0000_0000_0200) != 0)
                                    {
                                        return 0x0000_0000_0000_0200;
                                    }
                                }
                                else // ((variants & 0x0000_0000_0000_0B00) != 0)
                                {
                                    if ((variants & 0x0000_0000_0000_0400) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0000_0800) == 0))
                                    {
                                        return 0x0000_0000_0000_0400;
                                    }
                                    else // ((variants & 0x0000_0000_0000_0800) != 0)
                                    {
                                        return 0x0000_0000_0000_0800;
                                    }
                                }
                            }
                            else // ((variants & 0x0000_0000_0000_F000) != 0)
                            {
                                if ((variants & 0x0000_0000_0000_3000) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_0000_0000_B000) == 0))
                                {
                                    if ((variants & 0x0000_0000_0000_1000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0000_2000) == 0))
                                    {
                                        return 0x0000_0000_0000_1000;
                                    }
                                    else // ((variants & 0x0000_0000_0000_2000) != 0)
                                    {
                                        return 0x0000_0000_0000_2000;
                                    }
                                }
                                else // ((variants & 0x0000_0000_0000_B000) != 0)
                                {
                                    if ((variants & 0x0000_0000_0000_4000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0000_8000) == 0))
                                    {
                                        return 0x0000_0000_0000_4000;
                                    }
                                    else // ((variants & 0x0000_0000_0000_8000) != 0)
                                    {
                                        return 0x0000_0000_0000_8000;
                                    }
                                }
                            }
                        }
                    }
                    else // ((variants & 0x0000_0000_FFFF_0000) != 0)
                    {
                        if ((variants & 0x0000_0000_00FF_0000) != 0 && (Random[RandomIndex + 2] || (variants & 0x0000_0000_FF00_0000) == 0))
                        {
                            if ((variants & 0x0000_0000_000F_0000) != 0 && (Random[RandomIndex + 3] || (variants & 0x0000_0000_00F0_0000) == 0))
                            {
                                if ((variants & 0x0000_0000_0003_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_0000_000B_0000) == 0))
                                {
                                    if ((variants & 0x0000_0000_0001_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0002_0000) == 0))
                                    {
                                        return 0x0000_0000_0001_0000;
                                    }
                                    else // ((variants & 0x0000_0000_0002_0000) != 0)
                                    {
                                        return 0x0000_0000_0002_0000;
                                    }
                                }
                                else // ((variants & 0x0000_0000_000B_0000) != 0)
                                {
                                    if ((variants & 0x0000_0000_0004_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0008_0000) == 0))
                                    {
                                        return 0x0000_0000_0004_0000;
                                    }
                                    else // ((variants & 0x0000_0000_0008_0000) != 0)
                                    {
                                        return 0x0000_0000_0008_0000;
                                    }
                                }
                            }
                            else // ((variants & 0x0000_0000_00F0_0000) != 0)
                            {
                                if ((variants & 0x0000_0000_0030_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_0000_00B0_0000) == 0))
                                {
                                    if ((variants & 0x0000_0000_0010_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0020_0000) == 0))
                                    {
                                        return 0x0000_0000_0010_0000;
                                    }
                                    else // ((variants & 0x0000_0000_0020_0000) != 0)
                                    {
                                        return 0x0000_0000_0020_0000;
                                    }
                                }
                                else // ((variants & 0x0000_0000_00B0_0000) != 0)
                                {
                                    if ((variants & 0x0000_0000_0040_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0080_0000) == 0))
                                    {
                                        return 0x0000_0000_0040_0000;
                                    }
                                    else // ((variants & 0x0000_0000_0080_0000) != 0)
                                    {
                                        return 0x0000_0000_0080_0000;
                                    }
                                }
                            }
                        }
                        else // ((variants & 0x0000_0000_FF00_0000) != 0)
                        {
                            if ((variants & 0x0000_0000_0F00_0000) != 0 && (Random[RandomIndex + 3] || (variants & 0x0000_0000_F000_0000) == 0))
                            {
                                if ((variants & 0x0000_0000_0300_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_0000_0B00_0000) == 0))
                                {
                                    if ((variants & 0x0000_0000_0100_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0200_0000) == 0))
                                    {
                                        return 0x0000_0000_0100_0000;
                                    }
                                    else // ((variants & 0x0000_0000_0200_0000) != 0)
                                    {
                                        return 0x0000_0000_0200_0000;
                                    }
                                }
                                else // ((variants & 0x0000_0000_0B00_0000) != 0)
                                {
                                    if ((variants & 0x0000_0000_0400_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_0800_0000) == 0))
                                    {
                                        return 0x0000_0000_0400_0000;
                                    }
                                    else // ((variants & 0x0000_0000_0800_0000) != 0)
                                    {
                                        return 0x0000_0000_0800_0000;
                                    }
                                }
                            }
                            else // ((variants & 0x0000_0000_F000_0000) != 0)
                            {
                                if ((variants & 0x0000_0000_3000_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_0000_B000_0000) == 0))
                                {
                                    if ((variants & 0x0000_0000_1000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_2000_0000) == 0))
                                    {
                                        return 0x0000_0000_1000_0000;
                                    }
                                    else // ((variants & 0x0000_0000_2000_0000) != 0)
                                    {
                                        return 0x0000_0000_2000_0000;
                                    }
                                }
                                else // ((variants & 0x0000_0000_B000_0000) != 0)
                                {
                                    if ((variants & 0x0000_0000_4000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0000_8000_0000) == 0))
                                    {
                                        return 0x0000_0000_4000_0000;
                                    }
                                    else // ((variants & 0x0000_0000_8000_0000) != 0)
                                    {
                                        return 0x0000_0000_8000_0000;
                                    }
                                }
                            }
                        }
                    }
                }
                else // ((variants & 0xFFFF_FFFF_0000_0000_0000_0000) != 0)
                {
                    if ((variants & 0x0000_FFFF_0000_0000) != 0 && (Random[RandomIndex + 1] || (variants & 0xFFFF_0000_0000_0000) == 0))
                    {
                        if ((variants & 0x0000_00FF_0000_0000) != 0 && (Random[RandomIndex + 2] || (variants & 0x0000_FF00_0000_0000) == 0))
                        {
                            if ((variants & 0x0000_000F_0000_0000) != 0 && (Random[RandomIndex + 3] || (variants & 0x0000_00F0_0000_0000) == 0))
                            {
                                if ((variants & 0x0000_0003_0000_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_000B_0000_0000) == 0))
                                {
                                    if ((variants & 0x0000_0001_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0002_0000_0000) == 0))
                                    {
                                        return 0x0000_0001_0000_0000;
                                    }
                                    else // ((variants & 0x0000_0002_0000_0000) != 0)
                                    {
                                        return 0x0000_0002_0000_0000;
                                    }
                                }
                                else // ((variants & 0x0000_000B_0000_0000) != 0)
                                {
                                    if ((variants & 0x0000_0004_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0008_0000_0000) == 0))
                                    {
                                        return 0x0000_0004_0000_0000;
                                    }
                                    else // ((variants & 0x0000_0008_0000_0000) != 0)
                                    {
                                        return 0x0000_0008_0000_0000;
                                    }
                                }
                            }
                            else // ((variants & 0x0000_00F0_0000_0000) != 0)
                            {
                                if ((variants & 0x0000_0030_0000_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_00B0_0000_0000) == 0))
                                {
                                    if ((variants & 0x0000_0010_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0020_0000_0000) == 0))
                                    {
                                        return 0x0000_0010_0000_0000;
                                    }
                                    else // ((variants & 0x0000_0020_0000_0000) != 0)
                                    {
                                        return 0x0000_0020_0000_0000;
                                    }
                                }
                                else // ((variants & 0x0000_00B0_0000_0000) != 0)
                                {
                                    if ((variants & 0x0000_0040_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0080_0000_0000) == 0))
                                    {
                                        return 0x0000_0040_0000_0000;
                                    }
                                    else // ((variants & 0x0000_0080_0000_0000) != 0)
                                    {
                                        return 0x0000_0080_0000_0000;
                                    }
                                }
                            }
                        }
                        else // ((variants & 0x0000_FF00_0000_0000) != 0)
                        {
                            if ((variants & 0x0000_0F00_0000_0000) != 0 && (Random[RandomIndex + 3] || (variants & 0x0000_F000_0000_0000) == 0))
                            {
                                if ((variants & 0x0000_0300_0000_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_0B00_0000_0000) == 0))
                                {
                                    if ((variants & 0x0000_0100_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0200_0000_0000) == 0))
                                    {
                                        return 0x0000_0100_0000_0000;
                                    }
                                    else // ((variants & 0x0000_0200_0000_0000) != 0)
                                    {
                                        return 0x0000_0200_0000_0000;
                                    }
                                }
                                else // ((variants & 0x0000_0B00_0000_0000) != 0)
                                {
                                    if ((variants & 0x0000_0400_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_0800_0000_0000) == 0))
                                    {
                                        return 0x0000_0400_0000_0000;
                                    }
                                    else // ((variants & 0x0000_0800_0000_0000) != 0)
                                    {
                                        return 0x0000_0800_0000_0000;
                                    }
                                }
                            }
                            else // ((variants & 0x0000_F000_0000_0000) != 0)
                            {
                                if ((variants & 0x0000_3000_0000_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x0000_B000_0000_0000) == 0))
                                {
                                    if ((variants & 0x0000_1000_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_2000_0000_0000) == 0))
                                    {
                                        return 0x0000_1000_0000_0000;
                                    }
                                    else // ((variants & 0x0000_2000_0000_0000) != 0)
                                    {
                                        return 0x0000_2000_0000_0000;
                                    }
                                }
                                else // ((variants & 0x0000_B000_0000_0000) != 0)
                                {
                                    if ((variants & 0x0000_4000_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0000_8000_0000_0000) == 0))
                                    {
                                        return 0x0000_4000_0000_0000;
                                    }
                                    else // ((variants & 0x0000_8000_0000_0000) != 0)
                                    {
                                        return 0x0000_8000_0000_0000;
                                    }
                                }
                            }
                        }
                    }
                    else // ((variants & 0xFFFF_0000_0000_0000) != 0)
                    {
                        if ((variants & 0x00FF_0000_0000_0000) != 0 && (Random[RandomIndex + 2] || (variants & 0xFF00_0000_0000_0000) == 0))
                        {
                            if ((variants & 0x000F_0000_0000_0000) != 0 && (Random[RandomIndex + 3] || (variants & 0x00F0_0000_0000_0000) == 0))
                            {
                                if ((variants & 0x0003_0000_0000_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x000B_0000_0000_0000) == 0))
                                {
                                    if ((variants & 0x0001_0000_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0002_0000_0000_0000) == 0))
                                    {
                                        return 0x0001_0000_0000_0000;
                                    }
                                    else // ((variants & 0x0002_0000_0000_0000) != 0)
                                    {
                                        return 0x0002_0000_0000_0000;
                                    }
                                }
                                else // ((variants & 0x000B_0000_0000_0000) != 0)
                                {
                                    if ((variants & 0x0004_0000_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0008_0000_0000_0000) == 0))
                                    {
                                        return 0x0004_0000_0000_0000;
                                    }
                                    else // ((variants & 0x0008_0000_0000_0000) != 0)
                                    {
                                        return 0x0008_0000_0000_0000;
                                    }
                                }
                            }
                            else // ((variants & 0x00F0_0000_0000_0000) != 0)
                            {
                                if ((variants & 0x0030_0000_0000_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x00B0_0000_0000_0000) == 0))
                                {
                                    if ((variants & 0x0010_0000_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0020_0000_0000_0000) == 0))
                                    {
                                        return 0x0010_0000_0000_0000;
                                    }
                                    else // ((variants & 0x0020_0000_0000_0000) != 0)
                                    {
                                        return 0x0020_0000_0000_0000;
                                    }
                                }
                                else // ((variants & 0x00B0_0000_0000_0000) != 0)
                                {
                                    if ((variants & 0x0040_0000_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0080_0000_0000_0000) == 0))
                                    {
                                        return 0x0040_0000_0000_0000;
                                    }
                                    else // ((variants & 0x0080_0000_0000_0000) != 0)
                                    {
                                        return 0x0080_0000_0000_0000;
                                    }
                                }
                            }
                        }
                        else // ((variants & 0xFF00_0000_0000_0000) != 0)
                        {
                            if ((variants & 0x0F00_0000_0000_0000) != 0 && (Random[RandomIndex + 3] || (variants & 0xF000_0000_0000_0000) == 0))
                            {
                                if ((variants & 0x0300_0000_0000_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0x0B00_0000_0000_0000) == 0))
                                {
                                    if ((variants & 0x0100_0000_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0200_0000_0000_0000) == 0))
                                    {
                                        return 0x0100_0000_0000_0000;
                                    }
                                    else // ((variants & 0x0200_0000_0000_0000) != 0)
                                    {
                                        return 0x0200_0000_0000_0000;
                                    }
                                }
                                else // ((variants & 0x0B00_0000_0000_0000) != 0)
                                {
                                    if ((variants & 0x0400_0000_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x0800_0000_0000_0000) == 0))
                                    {
                                        return 0x0400_0000_0000_0000;
                                    }
                                    else // ((variants & 0x0800_0000_0000_0000) != 0)
                                    {
                                        return 0x0800_0000_0000_0000;
                                    }
                                }
                            }
                            else // ((variants & 0xF000_0000_0000_0000) != 0)
                            {
                                if ((variants & 0x3000_0000_0000_0000) != 0 && (Random[RandomIndex + 4] || (variants & 0xB000_0000_0000_0000) == 0))
                                {
                                    if ((variants & 0x1000_0000_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x2000_0000_0000_0000) == 0))
                                    {
                                        return 0x1000_0000_0000_0000;
                                    }
                                    else // ((variants & 0x2000_0000_0000_0000) != 0)
                                    {
                                        return 0x2000_0000_0000_0000;
                                    }
                                }
                                else // ((variants & 0xB000_0000_0000_0000) != 0)
                                {
                                    if ((variants & 0x4000_0000_0000_0000) != 0 && (Random[RandomIndex + 5] || (variants & 0x8000_0000_0000_0000) == 0))
                                    {
                                        return 0x4000_0000_0000_0000;
                                    }
                                    else // ((variants & 0x8000_0000_0000_0000) != 0)
                                    {
                                        if (variants == 0)
                                        {
                                            return 0;
                                        }

                                        return 0x8000_0000_0000_0000;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            private List<T> GetPrefabs(ulong variants)
            {
                List<T> list = new List<T>();

                for (int i = 0; i < 64; i++)
                {
                    if ((variants & 0x1) != 0)
                    {
                        list.Add(Prefabs[i]);
                    }

                    variants = variants >> 1;
                }

                return list;
            }
            private ulong GetAvailableVariantsInDirection(ulong variants, byte direction)
            {
                ulong variantsAvailable = 0;

                if ((variants & 0x0000_0000_0000_FFFF) != 0)
                {
                    if ((variants & 0x0000_0000_0000_000F) != 0)
                    {
                        if ((variants & 0x0000_0000_0000_0001) != 0)
                        {
                            variantsAvailable |= Rules[0].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_0002) != 0)
                        {
                            variantsAvailable |= Rules[1].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_0004) != 0)
                        {
                            variantsAvailable |= Rules[2].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_0008) != 0)
                        {
                            variantsAvailable |= Rules[3].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x0000_0000_0000_00F0) != 0)
                    {
                        if ((variants & 0x0000_0000_0000_0010) != 0)
                        {
                            variantsAvailable |= Rules[4].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_0020) != 0)
                        {
                            variantsAvailable |= Rules[5].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_0040) != 0)
                        {
                            variantsAvailable |= Rules[6].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_0080) != 0)
                        {
                            variantsAvailable |= Rules[7].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x0000_0000_0000_0F00) != 0)
                    {
                        if ((variants & 0x0000_0000_0000_0100) != 0)
                        {
                            variantsAvailable |= Rules[8].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_0200) != 0)
                        {
                            variantsAvailable |= Rules[9].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_0400) != 0)
                        {
                            variantsAvailable |= Rules[10].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_0800) != 0)
                        {
                            variantsAvailable |= Rules[11].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x0000_0000_0000_F000) != 0)
                    {
                        if ((variants & 0x0000_0000_0000_1000) != 0)
                        {
                            variantsAvailable |= Rules[12].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_2000) != 0)
                        {
                            variantsAvailable |= Rules[13].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_4000) != 0)
                        {
                            variantsAvailable |= Rules[14].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0000_8000) != 0)
                        {
                            variantsAvailable |= Rules[15].VariantsPossible[direction];
                        }
                    }
                }
                if ((variants & 0x0000_0000_FFFF_0000) != 0)
                {
                    if ((variants & 0x0000_0000_000F_0000) != 0)
                    {
                        if ((variants & 0x0000_0000_0001_0000) != 0)
                        {
                            variantsAvailable |= Rules[16].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0002_0000) != 0)
                        {
                            variantsAvailable |= Rules[17].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0004_0000) != 0)
                        {
                            variantsAvailable |= Rules[18].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0008_0000) != 0)
                        {
                            variantsAvailable |= Rules[19].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x0000_0000_00F0_0000) != 0)
                    {
                        if ((variants & 0x0000_0000_0010_0000) != 0)
                        {
                            variantsAvailable |= Rules[20].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0020_0000) != 0)
                        {
                            variantsAvailable |= Rules[21].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0040_0000) != 0)
                        {
                            variantsAvailable |= Rules[22].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0080_0000) != 0)
                        {
                            variantsAvailable |= Rules[23].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x0000_0000_0F00_0000) != 0)
                    {
                        if ((variants & 0x0000_0000_0100_0000) != 0)
                        {
                            variantsAvailable |= Rules[24].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0200_0000) != 0)
                        {
                            variantsAvailable |= Rules[25].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0400_0000) != 0)
                        {
                            variantsAvailable |= Rules[26].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_0800_0000) != 0)
                        {
                            variantsAvailable |= Rules[27].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x0000_0000_F000_0000) != 0)
                    {
                        if ((variants & 0x0000_0000_1000_0000) != 0)
                        {
                            variantsAvailable |= Rules[28].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_2000_0000) != 0)
                        {
                            variantsAvailable |= Rules[29].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_4000_0000) != 0)
                        {
                            variantsAvailable |= Rules[30].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0000_8000_0000) != 0)
                        {
                            variantsAvailable |= Rules[31].VariantsPossible[direction];
                        }
                    }
                }
                if ((variants & 0x0000_FFFF_0000_0000) != 0)
                {
                    if ((variants & 0x0000_000F_0000_0000) != 0)
                    {
                        if ((variants & 0x0000_0001_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[32].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0002_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[33].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0004_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[34].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0008_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[35].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x0000_00F0_0000_0000) != 0)
                    {
                        if ((variants & 0x0000_0010_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[36].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0020_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[37].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0040_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[38].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0080_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[39].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x0000_0F00_0000_0000) != 0)
                    {
                        if ((variants & 0x0000_0100_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[40].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0200_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[41].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0400_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[42].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_0800_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[43].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x0000_F000_0000_0000) != 0)
                    {
                        if ((variants & 0x0000_1000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[44].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_2000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[45].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_4000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[46].VariantsPossible[direction];
                        }
                        if ((variants & 0x0000_8000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[47].VariantsPossible[direction];
                        }
                    }
                }
                if ((variants & 0xFFFF_0000_0000_0000) != 0)
                {
                    if ((variants & 0x000F_0000_0000_0000) != 0)
                    {
                        if ((variants & 0x0001_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[48].VariantsPossible[direction];
                        }
                        if ((variants & 0x0002_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[49].VariantsPossible[direction];
                        }
                        if ((variants & 0x0004_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[50].VariantsPossible[direction];
                        }
                        if ((variants & 0x0008_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[51].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x00F0_0000_0000_0000) != 0)
                    {
                        if ((variants & 0x0010_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[52].VariantsPossible[direction];
                        }
                        if ((variants & 0x0020_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[53].VariantsPossible[direction];
                        }
                        if ((variants & 0x0040_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[54].VariantsPossible[direction];
                        }
                        if ((variants & 0x0080_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[55].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0x0F00_0000_0000_0000) != 0)
                    {
                        if ((variants & 0x0100_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[56].VariantsPossible[direction];
                        }
                        if ((variants & 0x0200_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[57].VariantsPossible[direction];
                        }
                        if ((variants & 0x0400_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[58].VariantsPossible[direction];
                        }
                        if ((variants & 0x0800_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[59].VariantsPossible[direction];
                        }
                    }
                    if ((variants & 0xF000_0000_0000_0000) != 0)
                    {
                        if ((variants & 0x1000_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[60].VariantsPossible[direction];
                        }
                        if ((variants & 0x2000_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[61].VariantsPossible[direction];
                        }
                        if ((variants & 0x4000_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[62].VariantsPossible[direction];
                        }
                        if ((variants & 0x8000_0000_0000_0000) != 0)
                        {
                            variantsAvailable |= Rules[63].VariantsPossible[direction];
                        }
                    }
                }

                return variantsAvailable;
            }
        }
    }
}
