﻿using System;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Check if the character is a digit.
        /// </summary>
        /// <param name="character">The character to check.</param>
        /// <returns>True if the character is a digit. False otherwise.</returns>
        public static bool IsDigit(this char character)
        {
            return 0x30 /* 0 */ <= character && character <= 0x39 /* 9 */;
        }
        /// <summary>
        /// Check if the character is an english letter (a-z or A-Z).
        /// </summary>
        /// <param name="character">The character to check.</param>
        /// <returns>True if the character is an english letter (a-z or A-Z). False otherwise.</returns>
        public static bool IsAlphabet(this char character)
        {
            return character.IsAlphabetLowercase() || character.IsAlphabetUppercase();
        }
        /// <summary>
        /// Check if the character is an english uppercase letter (A-Z).
        /// </summary>
        /// <param name="character">The character to check.</param>
        /// <returns>True if the character is an english lowercase letter (A-Z). False otherwise.</returns>
        public static bool IsAlphabetUppercase(this char character)
        {
            return 0x41 /* A */ <= character && character <= 0x5A /* Z */;
        }
        /// <summary>
        /// Check if the character is an english uppercase letter (a-z).
        /// </summary>
        /// <param name="character">The character to check.</param>
        /// <returns>True if the character is an english lowercase letter (a-z). False otherwise.</returns>
        public static bool IsAlphabetLowercase(this char character)
        {
            return 0x61 /* a */ <= character && character <= 0x7A /* z */;
        }
        /// <summary>
        /// Converts the value of a Unicode character to its uppercase equivalent.
        /// </summary>
        /// <param name="character">The Unicode character to convert.</param>
        /// <returns>The uppercase equivalent of the character, or the unchanged value of the character if it is already uppercase, has no uppercase equivalent, or is not alphabetic.</returns>
        public static char ToUpper(this char character)
        {
            return Char.ToUpper(character);
        }
        /// <summary>
        /// Converts the value of a Unicode character to its uppercase equivalent using the casing rules of the invariant culture.
        /// </summary>
        /// <param name="character">The Unicode character to convert.</param>
        /// <returns>The uppercase equivalent of the character, or the unchanged value of the character, if it is already uppercase or not alphabetic.</returns>
        public static char ToUpperInvariant(this char character)
        {
            return Char.ToUpperInvariant(character);
        }
        /// <summary>
        /// Converts the value of a Unicode character to its lowercase equivalent.
        /// </summary>
        /// <param name="character">The Unicode character to convert.</param>
        /// <returns>The lowercase equivalent of the character, or the unchanged value of the character if it is already lowercase, has no lowercase equivalent, or is not alphabetic.</returns>
        public static char ToLower(this char character)
        {
            return Char.ToLower(character);
        }
        /// <summary>
        /// Converts the value of a Unicode character to its lowercase equivalent using the casing rules of the invariant culture.
        /// </summary>
        /// <param name="character">The Unicode character to convert.</param>
        /// <returns>The lowercase equivalent of the character, or the unchanged value of the character, if it is already lowercase or not alphabetic.</returns>
        public static char ToLowerInvariant(this char character)
        {
            return Char.ToLowerInvariant(character);
        }
    }
}