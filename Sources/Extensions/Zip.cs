﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static string UnZipToString(this Stream stream, string entryPath = null, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;

            ZipArchive archive = new ZipArchive(stream);
            if (archive.Entries.Count == 0)
            {
                throw new Exception("Archive does not contain any entries.");
            }
            if (archive.Entries.Count > 1 && entryPath == null)
            {
                throw new Exception($"Entry name was not provided, but archive contains {archive.Entries.Count} entries. Only one entry is expected in this case.");
            }

            ZipArchiveEntry entry = (entryPath == null ?
            archive.Entries.First() :
                archive.Entries.SingleOrDefault(_entry => _entry.FullName == entryPath)) ??
                throw new Exception($"Archive does not contain an entry named {entryPath}.");

            using (Stream entryStream = entry.Open()) using (MemoryStream entryMemoryStream = new MemoryStream())
            {
                entryStream.CopyTo(entryMemoryStream);
                byte[] entryBytes = entryMemoryStream.ToArray();
                string entryText = encoding.GetString(entryBytes);
                return entryText;
            }
        }
        public static string UnZipToString(this byte[] bytes, string entryPath = null, Encoding encoding = null)
        {
            using (MemoryStream archiveStream = new MemoryStream(bytes))
            {
                return UnZipToString(archiveStream, entryPath, encoding);
            }
        }
        public static byte[] UnZipToBytes(this Stream stream, string entryPath = null)
        {
            ZipArchive archive = new ZipArchive(stream);
            if (archive.Entries.Count == 0)
            {
                throw new Exception("Archive does not contain any entries.");
            }
            if (archive.Entries.Count > 1 && entryPath == null)
            {
                throw new Exception($"Entry name was not provided, but archive contains {archive.Entries.Count} entries. Only one entry is expected in this case.");
            }

            ZipArchiveEntry entry = (entryPath == null ?
                archive.Entries.First() :
                archive.Entries.SingleOrDefault(_entry => _entry.FullName == entryPath)) ??
                throw new Exception($"Archive does not contain an entry named {entryPath}.");

            using (Stream entryStream = entry.Open()) using (MemoryStream entryMemoryStream = new MemoryStream())
            {
                entryStream.CopyTo(entryMemoryStream);
                byte[] entryBytes = entryMemoryStream.ToArray();
                return entryBytes;
            }
        }
        public static byte[] UnZipToBytes(this byte[] bytes, string entryPath = null)
        {
            using (MemoryStream archiveStream = new MemoryStream(bytes))
            {
                return UnZipToBytes(archiveStream, entryPath);
            }
        }
        public static void UnZipToFile(this byte[] bytes, string path, string entryPath = null)
        {
            if (path == null)
            {
                throw new ArgumentNullException("Path cannot be null.");
            }
            using (MemoryStream archiveStream = new MemoryStream(bytes))
            {
                ZipArchive archive = new ZipArchive(archiveStream);
                if (archive.Entries.Count == 0)
                {
                    throw new Exception("Archive does not contain any entries.");
                }
                if (archive.Entries.Count > 1 && entryPath == null)
                {
                    throw new Exception($"Entry name was not provided, but archive contains {archive.Entries.Count} entries. Only one entry is expected in this case.");
                }

                ZipArchiveEntry entry = (entryPath == null ?
                    archive.Entries.First() :
                    archive.Entries.SingleOrDefault(_entry => _entry.FullName == entryPath)) ??
                    throw new Exception($"Archive does not contain an entry named {entryPath}.");

                entry.ExtractToFile(path);
            }
        }
        public static void UnZipToDirectory(this byte[] bytes, string directory)
        {
            if (directory == null)
            {
                throw new ArgumentNullException("Directory cannot be null.");
            }
            using (MemoryStream archiveStream = new MemoryStream(bytes))
            {
                ZipArchive archive = new ZipArchive(archiveStream);
                archive.ExtractToDirectory(directory);
            }
        }
    }
}
