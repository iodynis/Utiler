﻿using System;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Limit the value from above.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="max">The maximum for the value.</param>
        /// <returns>Minimum of <paramref name="value"/> and <paramref name="max"/>.</returns>
        public static void Max(this ref short value, short max)
        {
            if (value > max)
            {
                value = max;
            }
        }
        /// <summary>
        /// Limit the value from below.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="min">The minimaum for the value.</param>
        /// <returns>Maximum of <paramref name="value"/> and <paramref name="min"/>.</returns>
        public static void Min(this ref short value, short min)
        {
            if (value < min)
            {
                value = min;
            }
        }
        /// <summary>
        /// Limit the value.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="min">The minimaum for the value.</param>
        /// <param name="max">The maximum for the value.</param>
        /// <returns>Value limited to the interval [<paramref name="min"/>, <paramref name="max"/>].</returns>
        public static void Limit(this ref short value, short min, short max)
        {
            value.Min(min);
            value.Max(max);
        }
        /// <summary>
        /// Make the value positive.
        /// </summary>
        /// <param name="value">The value to make positive.</param>
        /// <returns>The absolute value.</returns>
        public static void Abs(this ref short value)
        {
            if (value < 0)
            {
                value = (short)-value;
            }
        }
        /// <summary>
        /// Count 1 bits in the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Number of 1 bits in the value.</returns>
        public static int BitsCount(this Int16 value)
        {
            return
                BitsCountInByte[(value) & 0xFF] +
                BitsCountInByte[(value >> 8) & 0xFF];
        }
        public static int HighestBitIndex(this Int16 value)
        {
            if (value < 0)
                return 31;
            if (value == 0)
                return 0;

            if (value < 1 << 8)
                if (value < 1 << 4)
                    if (value < 1 << 2)
                        if (value < 1 << 1) return  0;
                        else return  1;
                    else
                        if (value < 1 << 3) return  2;
                        else return  3;
                else
                    if (value < 1 << 6)
                        if (value < 1 << 5) return  4;
                        else return  5;
                    else
                        if (value < 1 << 7) return  6;
                        else return  7;
            else
                if (value < 1 << 12)
                    if (value < 1 << 10)
                        if (value < 1 << 9) return  8;
                        else return  9;
                    else
                        if (value < 1 << 11) return  10;
                        else return  11;
                else
                    if (value < 1 << 14)
                        if (value < 1 << 13) return  12;
                        else return  13;
                    else
                        if (value == 1 << 14) return  14;
                        else return  15;

            throw new Exception();
        }
        /// <summary>
        /// Loop the value in the [0, <paramref name="threshold"/>) range for positiive <paramref name="threshold"/>
        /// or in the (<paramref name="threshold"/>, 0] range for negative one.
        /// </summary>
        /// <param name="value">The value to loop.</param>
        /// <param name="threshold">The threshold to limit the value.</param>
        public static void Loop(this ref Int16 value, Int16 threshold)
        {
            if (threshold == 0)
            {
                value = 0;
            }

            if (threshold > 0)
            {
                value.Loop(0, threshold);
            }
            else
            {
                value.Loop(threshold, 0);

                // Change the behaviour for the border value to fit the positive threshold behaviour.
                if (value == threshold)
                {
                    value = 0;
                }
            }
        }
        /// <summary>
        /// Loop the value in the [<paramref name="min"/>, <paramref name="max"/>) range.
        /// </summary>
        /// <param name="value">The value to loop.</param>
        /// <param name="min">The minimum threshold to limit the value.</param>
        /// <param name="max">The maximum threshold to limit the value.</param>
        public static void Loop(this ref Int16 value, Int16 min, Int16 max)
        {
            if (min == max)
            {
                value = min;
                return;
            }

            // Fix the reverse
            if (min > max)
            {
                Utiler.Swap(ref min, ref max);
            }

            Int16 length = (Int16)(max - min);

            // Go to relative coordinates
            value -= min;
            if (value < 0)
            {
                value = (Int16)(length - value);
            }

            // Clamp
            value %= length;

            // Go back to absolute coordinates
            value += min;
        }
        /// <summary>
        /// Reverse the bits in the value.
        /// </summary>
        /// <param name="value">The value to reverse the bits in.</param>
        /// <returns>The value with all the bits reversed.</returns>
        public static Int16 Reverse(this Int16 value)
        {
            return (Int16)(
                (((int)ReverseTable[value      & 0xFF]) << 8) |
                (((int)ReverseTable[value >> 8 & 0xFF])     ));
        }
    }
}