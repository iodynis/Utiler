﻿#if !UNITY_ENGINE

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static void InvokeIfRequired(this ISynchronizeInvoke @object, MethodInvoker action)
        {
            if (@object.InvokeRequired)
            {
                var args = new object[0];
                @object.Invoke(action, args);
            }
            else
            {
                action();
            }
        }
    }
}

#endif