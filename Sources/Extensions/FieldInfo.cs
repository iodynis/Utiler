﻿using System;
using System.Reflection;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static object GetFieldValue(this object @object, string fieldName)
        {
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }
            if (fieldName == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }

            Type type = @object.GetType();
            FieldInfo fieldInfo = type.GetField(fieldName);
            if (fieldInfo == null)
            {
                throw new Exception($"Property {fieldName} is not presented in {type}.");
            }
            object value = fieldInfo.GetValue(@object);
            return value;
        }
        public static T GetFieldValue<T>(this object @object, string fieldName)
        {
            return (T)GetFieldValue(@object, fieldName);
        }
    }
}
