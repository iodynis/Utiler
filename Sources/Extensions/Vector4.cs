﻿#if UNITY_ENGINE

using UnityEngine;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static Vector4 Invert(this Vector4 vector)
        {
            return new Vector4(1f / vector.x, 1f / vector.y, 1f / vector.z, 1f / vector.w);
        }
    }
}

#endif