﻿
namespace Iodynis.Libraries.Utility.Extensions
{
    /// <summary>
    /// Camel case to snake case conversion mode.
    /// </summary>
    public enum ConvertCamelCaseToSnakeCaseMode : byte
    {
        /// <summary>
        /// Case is not modified.
        /// </summary>
        MIXED_CASE = 0,
        /// <summary>
        /// All letters are in upper case.
        /// </summary>
        UPPER_CASE = 1,
        /// <summary>
        /// All letters are in lower case.
        /// </summary>
        LOWER_CASE = 2,
        /// <summary>
        /// First letter of every word is in upper case, the rest is in lower case.
        /// </summary>
        CAMEL_CASE = 3,
        /// <summary>
        /// Only the first letter of the very first word is in upper case, the rest is in lower case.
        /// </summary>
        FIRST_UPPER_REST_LOWER = 4,
    }
}
