﻿using System;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Round the value to the specified count of significant digits.
        /// </summary>
        /// <param name="value">Value to round.</param>
        /// <param name="count">Significant digits count.</param>
        /// <returns>Value rounded to specified count of significant digits.</returns>
        public static double RoundToSignificantDigits(this double value, int count)
        {
            if (Math.Abs(value) <= Double.Epsilon)
            {
                return 0;
            }

            double scale = Math.Pow(10, Math.Floor(Math.Log10(Math.Abs(value))) + 1);
            return scale * Math.Round(value / scale, count);
        }
        ///// <summary>
        ///// Format as string with the specified count of significant digits.
        ///// </summary>
        ///// <param name="value">Value to format as string.</param>
        ///// <param name="count">Significant digits count.</param>
        ///// <returns>String value rounded to specified count of significant digits.</returns>
        //public static string ToStringOnlySignificantDigits(this double value, int count)
        //{
        //    double testing = value;
        //    if (testing < 0)
        //    {
        //        testing = -testing;
        //    }
        //    if (testing <= Double.Epsilon)
        //    {
        //        return "0";
        //    }

        //    if (value > 1)
        //    {
        //        return value.ToString("F" + count);
        //    }
        //    else
        //    {
        //        int zeros = -1;
        //        while (testing < 1)
        //        {
        //            testing *= 10;
        //            zeros++;
        //        }
        //        return value.ToString("F" + (zeros + count));
        //    }
        //}
        public static long GetMantise(this double value)
        {
            long bits = BitConverter.DoubleToInt64Bits(value);
            return bits & 0x000F_FFFF_FFFF_FFFFL;
        }
        public static int GetExponent(this double value)
        {
            long bits = BitConverter.DoubleToInt64Bits(value);
            return (int) ((bits >> 52) & 0x7FFL);
        }
        public static void GetExponentAndMantise(this double value, out bool sign, out int exponent, out long mantissa)
        {
            if (Double.IsPositiveInfinity(value))
            {
                sign = false;
                exponent = 2047;
                mantissa = 0;
                return;
            }
            if (Double.IsNegativeInfinity(value))
            {
                sign = true;
                exponent = 2047;
                mantissa = 0;
                return;
            }
            if (Double.IsNaN(value))
            {
                sign = false;
                exponent = 2047;
                mantissa = 0x000F_FFFF_FFFF_FFFFL;
                return;
            }

            // Translate the double into sign, exponent and mantissa.
            long bits = BitConverter.DoubleToInt64Bits(value);
            // Note that the shift is sign-extended, hence the test against -1 not 1
            sign = bits < 0;
            exponent = (int)((bits >> 52) & 0x7FFL);
            mantissa = bits & 0x000F_FFFF_FFFF_FFFFL;

            // Subnormal numbers; exponent is effectively one higher,
            // but there's no extra normalisation bit in the mantissa
            if (exponent == 0)
            {
                exponent = 1;
            }
            // Normal numbers; leave exponent as it is but add extra
            // bit to the front of the mantissa
            else
            {
                mantissa |= 1L<<52;
            }

            // Bias the exponent. It's actually biased by 1023, but we're
            // treating the mantissa as m.0 rather than 0.m, so we need
            // to subtract another 52 from it (mantissa is 52bits).
            exponent -= 1075;

            if (mantissa == 0)
            {
                return;
            }

            /* Normalize */
            while ((mantissa & 1) == 0)
            {    /*  i.e., Mantissa is even */
                mantissa >>= 1;
                exponent++;
            }

            ///// Construct a new decimal expansion with the mantissa
            //ArbitraryDecimal ad = new ArbitraryDecimal (param_mantissa);

            //// If the exponent is less than 0, we need to repeatedly
            //// divide by 2 - which is the equivalent of multiplying
            //// by 5 and dividing by 10.
            //if (exponent < 0)
            //{
            //    for (int i=0; i < -exponent; i++)
            //        ad.MultiplyBy(5);
            //    ad.Shift(-exponent);
            //}
            //// Otherwise, we need to repeatedly multiply by 2
            //else
            //{
            //    for (int i=0; i < exponent; i++)
            //        ad.MultiplyBy(2);
            //}

            ////// Finally, return the string with an appropriate sign
            ////if (negative)
            ////    return "-"+ad.ToString();
            ////else
            ////    return ad.ToString();
        }
        //public static double TruncateToSignificantDigits(this double value, int count)
        //{
        //    if (count < 0)
        //    {
        //        throw new ArgumentException("Count cannot be negative.", nameof(count));
        //    }
        //    if (count == 0)
        //    {
        //        throw new ArgumentException("Count cannot be zero.", nameof(count));
        //    }

        //    //
        //    // Double in-memory bit representation accoring to IEEE 754 standard:
        //    //
        //    //        ____ Sign, 1 bit
        //    //       /
        //    //      /
        //    //     /     ____ Exponent, 11 bits
        //    //    /     /
        //    //   /     /                                   ____ Mantissa, 52 bits
        //    //  /     /                                   /
        //    // SEEEEEEE EEEEMMMM MMMMMMMM MMMMMMMM MMMMMMMM MMMMMMMM MMMMMMMM MMMMMMMM
        //    //

        //    long integer = BitConverter.ToInt64(BitConverter.GetBytes(value), 0);
        //    long mantissa = integer & 0x000FFFFFFFFFFFFF;
        //    mantissa = mantissa.TruncateToSignificantDigits(count);
        //    integer = (integer & unchecked((long)0xFFF0000000000000)) + mantissa;
        //    value = BitConverter.ToSingle(BitConverter.GetBytes(integer), 0);
        //    BitConverter.ToSingle(BitConverter.GetBytes(value), 0);
        //    return value;
        //}
        ///// <summary>Private class used for manipulating
        //private class ArbitraryDecimal
        //{
        //    /// <summary>Digits in the decimal expansion, one byte per digit
        //    byte[] digits;
        //    /// <summary>
        //    /// How many digits are *after* the decimal point
        //    /// </summary>
        //    int decimalPoint=0;

        //    /// <summary>
        //    /// Constructs an arbitrary decimal expansion from the given long.
        //    /// The long must not be negative.
        //    /// </summary>
        //    internal ArbitraryDecimal (long x)
        //    {
        //        string tmp = x.ToString(CultureInfo.InvariantCulture);
        //        digits = new byte[tmp.Length];
        //        for (int i=0; i < tmp.Length; i++)
        //            digits[i] = (byte) (tmp[i]-'0');
        //        Normalize();
        //    }

        //    /// <summary>
        //    /// Multiplies the current expansion by the given amount, which should
        //    /// only be 2 or 5.
        //    /// </summary>
        //    internal void MultiplyBy(int amount)
        //    {
        //        byte[] result = new byte[digits.Length+1];
        //        for (int i=digits.Length-1; i >= 0; i--)
        //        {
        //            int resultDigit = digits[i]*amount+result[i+1];
        //            result[i]=(byte)(resultDigit/10);
        //            result[i+1]=(byte)(resultDigit%10);
        //        }
        //        if (result[0] != 0)
        //        {
        //            digits=result;
        //        }
        //        else
        //        {
        //            Array.Copy (result, 1, digits, 0, digits.Length);
        //        }
        //        Normalize();
        //    }

        //    /// <summary>
        //    /// Shifts the decimal point; a negative value makes
        //    /// the decimal expansion bigger (as fewer digits come after the
        //    /// decimal place) and a positive value makes the decimal
        //    /// expansion smaller.
        //    /// </summary>
        //    internal void Shift (int amount)
        //    {
        //        decimalPoint += amount;
        //    }

        //    /// <summary>
        //    /// Removes leading/trailing zeroes from the expansion.
        //    /// </summary>
        //    internal void Normalize()
        //    {
        //        int first;
        //        for (first=0; first < digits.Length; first++)
        //            if (digits[first]!=0)
        //                break;
        //        int last;
        //        for (last=digits.Length-1; last >= 0; last--)
        //            if (digits[last]!=0)
        //                break;

        //        if (first==0 && last==digits.Length-1)
        //            return;

        //        byte[] tmp = new byte[last-first+1];
        //        for (int i=0; i < tmp.Length; i++)
        //            tmp[i]=digits[i+first];

        //        decimalPoint -= digits.Length-(last+1);
        //        digits=tmp;
        //    }

        //    /// <summary>
        //    /// Converts the value to a proper decimal string representation.
        //    /// </summary>
        //    public override String ToString()
        //    {
        //        char[] digitString = new char[digits.Length];
        //        for (int i=0; i < digits.Length; i++)
        //            digitString[i] = (char)(digits[i]+'0');

        //        // Simplest case - nothing after the decimal point,
        //        // and last real digit is non-zero, eg value=35
        //        if (decimalPoint==0)
        //        {
        //            return new string (digitString);
        //        }

        //        // Fairly simple case - nothing after the decimal
        //        // point, but some 0s to add, eg value=350
        //        if (decimalPoint < 0)
        //        {
        //            return new string (digitString)+
        //                   new string ('0', -decimalPoint);
        //        }

        //        // Nothing before the decimal point, eg 0.035
        //        if (decimalPoint >= digitString.Length)
        //        {
        //            return "0."+
        //                new string ('0',(decimalPoint-digitString.Length))+
        //                new string (digitString);
        //        }

        //        // Most complicated case - part of the string comes
        //        // before the decimal point, part comes after it,
        //        // eg 3.5
        //        return new string (digitString, 0,
        //                           digitString.Length-decimalPoint)+
        //            "."+
        //            new string (digitString,
        //                        digitString.Length-decimalPoint,
        //                        decimalPoint);
        //    }
        //}
        /// <summary>
        /// Limit the value from above.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="max">The maximum for the value.</param>
        /// <returns>Minimum of <paramref name="value"/> and <paramref name="max"/>.</returns>
        public static void Max(this ref double value, double max)
        {
            if (value > max)
            {
                value = max;
            }
        }
        /// <summary>
        /// Limit the value from below.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="min">The minimaum for the value.</param>
        /// <returns>Maximum of <paramref name="value"/> and <paramref name="min"/>.</returns>
        public static void Min(this ref double value, double min)
        {
            if (value < min)
            {
                value = min;
            }
        }
        /// <summary>
        /// Limit the value.
        /// </summary>
        /// <param name="value">The value to limit.</param>
        /// <param name="min">The minimaum for the value.</param>
        /// <param name="max">The maximum for the value.</param>
        /// <returns>Value limited to the interval [<paramref name="min"/>, <paramref name="max"/>].</returns>
        public static void Limit(this ref double value, double min, double max)
        {
            value.Min(min);
            value.Max(max);
        }
        /// <summary>
        /// Make the value positive.
        /// </summary>
        /// <param name="value">The value to make positive.</param>
        /// <returns>The absolute value.</returns>
        public static void Abs(this ref double value)
        {
            if (value < 0)
            {
                value = -value;
            }
        }
    }
}