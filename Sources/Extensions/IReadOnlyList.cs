﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Get the index the of specified element.
        /// If multiple elements are presented in the list, the index of the first one will be returned.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static int IndexOf<T>(this IReadOnlyList<T> list, T item)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list), "List cannot be null.");
            }

            for (int index = 0; index < list.Count; index++)
            {
                if (Equals(list[index], item))
                {
                    return index;
                }
            }
            return -1;
        }
    }
}
