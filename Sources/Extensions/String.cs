﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Iodynis.Libraries.Utility.Extensions
{
    // TODO: Check if String.Normalize() is anywhere needed.
    public static partial class Extensions
    {
        private static readonly Dictionary<char, string> MakeSpecialSymbolsVisibleDictionary = new Dictionary<char, string>()
        {
            {(char)0x00, @"\0"},   // Null
            {(char)0x01, @"\SOH"}, // Start of heading
            {(char)0x02, @"\STX"}, // Start of text
            {(char)0x03, @"\ETX"}, // End of text
            {(char)0x04, @"\EOT"}, // End of transmission
            {(char)0x05, @"\ENQ"}, // Enquiry
            {(char)0x06, @"\ACK"}, // Acknowledge
            {(char)0x07, @"\a"},   // Alert (beep, bell)
            {(char)0x08, @"\b"},   // Backspace
            {(char)0x09, @"\t"},   // Horizontal tab
            {(char)0x0A, @"\n"},   // New line
            {(char)0x0B, @"\v"},   // Vertical tab
            {(char)0x0C, @"\f"},   // New page
            {(char)0x0D, @"\r"},   // Carriage return
            {(char)0x0E, @"\SO"},  // Shift out
            {(char)0x0F, @"\SI"},  // Shift in
            {(char)0x10, @"\DLE"}, // Data link escape
            {(char)0x11, @"\DC1"}, // Device control 1
            {(char)0x12, @"\DC2"}, // Device control 2
            {(char)0x13, @"\DC3"}, // Device control 3
            {(char)0x14, @"\DC4"}, // Device control 4
            {(char)0x15, @"\NAK"}, // Negative acknowledge
            {(char)0x16, @"\SYN"}, // Synchronous idle
            {(char)0x17, @"\ETB"}, // End of transaction block
            {(char)0x18, @"\CAN"}, // Cancel
            {(char)0x19, @"\EM"},  // End of medium
            {(char)0x1A, @"\SUB"}, // Substitute
            {(char)0x1B, @"\ESC"}, // Escape
            {(char)0x1C, @"\FS"},  // File separator
            {(char)0x1D, @"\GS"},  // Group separator
            {(char)0x1E, @"\RS"},  // Record separator
            {(char)0x1F, @"\US"},  // Unit separator
            {(char)0x7F, @"\DEL"}, //
        };
        public static string MakeSpecialSymbolsVisible(this string @string)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int index = 0; index < @string.Length; index++)
            {
                char character = @string[index];
                // If part of double-byte codepoint
                if (char.IsHighSurrogate(character))
                {
                    stringBuilder.Append(character);
                    index++;
                    if (index < @string.Length)
                    {
                        character = @string[index];
                        stringBuilder.Append(character);
                    };
                }
                // If one-byte codepoint that has a replacement
                else if (MakeSpecialSymbolsVisibleDictionary.TryGetValue(character, out string replacement))
                {
                    stringBuilder.Append(replacement);
                }
                // If one-byte codepoint to use as is
                else
                {
                    stringBuilder.Append(character);
                }
            }
            return stringBuilder.ToString();
        }
        /// <summary>
        /// Trim one or several extensions from a path.
        /// </summary>
        /// <param name="path">Path.</param>
        /// <param name="count">Number of extensions to trim.</param>
        /// <returns>Path with one or more extensions trimmed.</returns>
        public static string TrimExtension(this string path, int count = 1)
        {
            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count), "Extensions count cannot be negative.");
            }
            if (count == 0)
            {
                return path;
            }
            // Determine last slash position to operate only on the file name
            int indexSlash = Math.Max(path.LastIndexOf('/'), path.LastIndexOf('\\'));
            while (count-- > 0)
            {
                int indexDot = path.LastIndexOf('.');
                if (indexDot < indexSlash)
                {
                    return path;
                }
                if (indexDot < 0)
                {
                    return path;
                }
                path = path.Substring(0, indexDot);
            }
            return path;
        }
        /// <summary>
        /// Get a substring starting at the specified index and ending at the first occurence of the specified char.
        /// </summary>
        /// <param name="string">Original string.</param>
        /// <param name="index">Index to start the substring from.</param>
        /// <param name="char">Character to search for.</param>
        /// <param name="includeChar">Include the character in the resulting string.</param>
        /// <returns></returns>
        public static string Substring(this string @string, int index, char @char, bool includeChar)
        {
            if (index < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative.");
            }
            if (index >= @string.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(index), "Index cannot greater or equal to the string length.");
            }
            int indexOfChar = @string.IndexOf(@char);
            return indexOfChar < 0 ? @string.Substring(index) : @string.Substring(index, indexOfChar - index + (includeChar ? 1 : 0));
        }
        /// <summary>
        /// Return a substring beginning at the first occurance of the specified first character and ending at the next occurence (from that position) of the specified last char.
        /// </summary>
        /// <param name="string">Original string.</param>
        /// <param name="charFirst">A starting character to search for.</param>
        /// <param name="includeCharFirst">Include the starting character in the resulting string.</param>
        /// <param name="charLast">An ending character to search for.</param>
        /// <param name="includeCharLast">Include the ending character in the resulting string.</param>
        /// <returns></returns>
        public static string Substring(this string @string, char charFirst, bool includeCharFirst, char charLast, bool includeCharLast)
        {
            int indexOfCharFirst = @string.IndexOf(charFirst);
            int indexOfCharLast  = @string.IndexOf(charLast, indexOfCharFirst < 0 ? 0 : indexOfCharFirst);

            int start = indexOfCharFirst < 0 ? 0 : (indexOfCharFirst + (includeCharFirst ? 0 : 1));
            int end   = indexOfCharLast < 0 ? @string.Length : (indexOfCharLast + (includeCharLast ? 1 : 0));

            return @string.Substring(start, end - start);
        }
        /// <summary>
        /// Return a substring beginning at the first occurance of the specified first character and ending at the next occurence (from that position) of the specified last char.
        /// </summary>
        /// <param name="string">Original string.</param>
        /// <param name="characterOne">A starting character to search for.</param>
        /// <param name="characterOneStart">.</param>
        /// <param name="characterOneIsIncluded">Include the starting character in the resulting string.</param>
        /// <param name="characterTwo">An ending character to search for.</param>
        /// <param name="characterTwoStart">.</param>
        /// <param name="characterTwoIsIncluded">Include the ending character in the resulting string.</param>
        /// <returns></returns>
        public static string Substring(this string @string, char characterOne, int characterOneStart, bool characterOneIsIncluded, char characterTwo, int characterTwoStart, bool characterTwoIsIncluded)
        {
            if (characterOneStart == 0)
            {
                throw new ArgumentException("Index of the character cannot be zero, it must be positive if counting from the string start or negative if counting from the string end.", nameof(characterOneStart));
            }
            if (characterTwoStart == 0)
            {
                throw new ArgumentException("Index of the character cannot be zero, it must be positive if counting from the string start or negative if counting from the string end.", nameof(characterTwoStart));
            }
            if (@string.Length == 0)
            {
                return "";
            }
            int characterOneIndex = SubstringIndexOf(@string, characterOne, characterOneStart >= 0 ? 0 : @string.Length - 1, characterOneStart);
            if (characterOneIndex < 0)
            {
                return "";
            }
            // TODO: This seems invalid
            int characterTwoIndex = SubstringIndexOf(@string, characterTwo, characterOneIndex + (characterTwoStart > 0 ? 1 : -1), characterTwoStart);

            int start, end;
            //
            // ````````O````````X`````````
            //   one _/          \_ two
            //
            if (characterTwoStart > 0)
            {
                start = characterOneIndex + (characterOneIsIncluded ? 0 : 1);
                end   = characterTwoIndex < 0 ? @string.Length - 1 : (characterTwoIndex - (characterTwoIsIncluded ? 0 : 1));
            }
            //
            // ````````X````````o`````````
            //   two _/          \_ one
            //
            else
            {
                start = characterTwoIndex < 0 ? @string.Length - 1 : (characterTwoIndex + (characterTwoIsIncluded ? 0 : 1));
                end   = characterOneIndex - (characterOneIsIncluded ? 0 : 1);
            }
            return @string.Substring(start, end - start + 1);
        }
        private static int SubstringIndexOf(string @string, char character, int start, int count)
        {
            if (count == 0)
            {
                return @string.IndexOf(character);
            }
            else if (count > 0)
            {
                for (int index = start; index < @string.Length; index++)
                {
                    if (@string[index] == character && --count == 0)
                    {
                        return index;
                    }
                }
            }
            else // (count < 0)
            {
                for (int index = start; index >= 0; index--)
                {
                    if (@string[index] == character && ++count == 0)
                    {
                        return index;
                    }
                }
            }
            return -1;
        }
        /// <summary>
        /// Return a substring beginning at the first occurance of the specified character and of specified length or shorter.
        /// </summary>
        /// <param name="string">Original string.</param>
        /// <param name="character">Character to search for.</param>
        /// <param name="includeCharecter">Include the character in the resulting string.</param>
        /// <param name="length">Maximal length of the substring.</param>
        /// <returns></returns>
        public static string Substring(this string @string, char character, bool includeCharecter, int length)
        {
            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length), "Length cannot be negative.");
            }
            //if (param_length >= param_string.Length)
            //{
            //    throw new ArgumentOutOfRangeException(nameof(param_length), "Length cannot greater or equal to the string length.");
            //}
            if (length == 0)
            {
                return "";
            }

            int indexOfChar = @string.IndexOf(character);
            if (indexOfChar < 0)
            {
                if (length >= @string.Length)
                {
                    return @string;
                }
                else
                {
                    return @string.Substring(0, length);
                }
            }
            else
            {
                indexOfChar += (includeCharecter ? 0 : 1);
                if (indexOfChar + length >= @string.Length)
                {
                    return @string.Substring(indexOfChar);
                }
                else
                {
                    return @string.Substring(indexOfChar, length);
                }
            }
            //return indexOfChar < 0 ? param_string.Substring(0, param_length) : param_string.Substring(indexOfChar + (param_includeChar ? 0 : 1), param_length);
        }
        /// <summary>
        /// Return a substring beginning at the first occurance of the specified character.
        /// </summary>
        /// <param name="string">Original string.</param>
        /// <param name="character">Character to search for.</param>
        /// <param name="includeCharacter">Include the character in the resulting string.</param>
        /// <returns></returns>
        public static string Substring(this string @string, char character, bool includeCharacter)
        {
            int indexOfChar = @string.IndexOf(character);
            return indexOfChar < 0 ? @string : @string.Substring(indexOfChar + (includeCharacter ? 0 : 1));
        }
        /// <summary>
        /// Remove specified character form the string.
        /// </summary>
        /// <param name="string">Original string.</param>
        /// <param name="characters">Characters to remove.</param>
        /// <returns>String with specified characters removed.</returns>
        public static string RemoveCharacters(this string @string, params char[] characters)
        {
            if (characters == null)
            {
                throw new ArgumentNullException(nameof(characters), "Characters array cannot be null.");
            }
            StringBuilder stringBuilder = new StringBuilder();
            foreach (char character in @string)
            {
                if (characters.Contains(character))
                {
                    continue;
                }
                stringBuilder.Append(character);
            }
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Leave only specified character in the string.
        /// </summary>
        /// <param name="string">Original string.</param>
        /// <param name="leaveLowercase">If leave lowercase a-z letters.</param>
        /// <param name="leaveUppercase">If leave uppercase A-Z letters</param>
        /// <param name="leaveDigits">If leave digits 0-9.</param>
        /// <param name="leaveCharacters">Characters to leave.</param>
        /// <returns>String with all other characters removed.</returns>
        public static string LeaveCharacters(this string @string, bool leaveLowercase, bool leaveUppercase, bool leaveDigits, params char[] leaveCharacters)
        {
            if (leaveCharacters == null)
            {
                throw new ArgumentNullException(nameof(leaveCharacters), "Characters array cannot be null.");
            }
            StringBuilder stringBuilder = new StringBuilder();
            foreach (char character in @string)
            {
                if ((leaveLowercase && 'a' <= character && character <= 'z') ||
                    (leaveUppercase && 'A' <= character && character <= 'Z') ||
                    (leaveDigits    && '0' <= character && character <= '9') ||
                    leaveCharacters.Contains(character))
                {
                    stringBuilder.Append(character);
                }
            }
            return stringBuilder.ToString();
        }
        public static int Count(this string @string, char character)
        {
            int count = 0;
            for (int i = 0; i < @string.Length; i++)
            {
                if (@string[i] == character)
                {
                    count++;
                }
            }
            return count;
        }
        public static int Count(this string @string, string substring, bool overlap = true)
        {
            int count = 0;
            // If the substring consists of just one symbol
            if (substring.Length == 1)
            {
                for (int indexString = 0; indexString <= @string.Length - substring.Length; indexString++)
                {
                    if (@string[indexString] == substring[0])
                    {
                        count++;
                    }
                }
                return count;
            }
            // If substring is longer then just one symbol
            for (int indexString = 0; indexString <= @string.Length - substring.Length; indexString++)
            {
                // Try first symbol
                if (@string[indexString] != substring[0])
                {
                    continue;
                }
                // Try other symbols
                for (int indexSubstring = 1; indexSubstring < substring.Length; indexSubstring++)
                {
                    if (@string[indexString + indexSubstring] != substring[indexSubstring])
                    {
                        break;
                    }
                    // If the last symbol was matched -- a full match is found
                    if (indexSubstring == substring.Length - 1)
                    {
                        count++;
                        // Shift the index if overlapping is forbidden
                        if (!overlap)
                        {
                            indexString += indexSubstring;
                        }
                        break;
                    }
                }
            }
            return count;
        }
        public static bool ContainsAny(this string @string, char[] characters)
        {
            foreach (char character in characters)
            {
                if (@string.Contains(character))
                {
                    return true;
                }
            }
            return false;
        }
        public static bool ContainsAll(this string @string, char[] characters)
        {
            foreach (char character in characters)
            {
                if (!@string.Contains(character))
                {
                    return false;
                }
            }
            return true;
        }
        public static string Times(this string @string, int count)
        {
            // Check simple cases
            if (count < 0)
            {
                throw new Exception("Count cannot be negative.");
            }
            if (count == 0)
            {
                return "";
            }
            if (count == 1)
            {
                return @string;
            }

            // Stack the string into builder
            StringBuilder stringBuilder = new StringBuilder();
            int counter = count;
            while (counter-- > 0)
            {
                stringBuilder.Append(@string);
            }
            return stringBuilder.ToString();
        }

        public static string ReplaceOnce(this string @string, string substringOld, string substringNew)
        {
            if (substringOld == null)
            {
                throw new ArgumentNullException(nameof(substringOld), "String to search cannot be null.");
            }
            if (substringNew == null)
            {
                throw new ArgumentNullException(nameof(substringNew), "String to replace cannot be null.");
            }
            if (substringOld == "")
            {
                throw new ArgumentException("String to search cannot be empty.", nameof(substringOld));
            }
            if (substringOld.Length > @string.Length)
            {
                return @string;
            }
            int position = @string.IndexOf(substringOld, StringComparison.Ordinal);
            if (position < 0)
            {
                return @string;
            }
            return @string.Substring(0, position) + substringNew + @string.Substring(position + substringOld.Length);
        }
        //// TODO: Support UTF16 multi-byte symbols
        //public static string Replace(this string param_text, string param_search, string param_replacement)
        //{
        //    if (param_search == null)
        //    {
        //        throw new NullReferenceException("String to search cannot be null.");
        //    }
        //    if (param_replacement == null)
        //    {
        //        throw new NullReferenceException("Replacement string cannot be null.");
        //    }
        //    if (param_search == "")
        //    {
        //        throw new ArgumentException("String to search cannot be empty.", nameof(param_search));
        //    }
        //    if (param_search.Length > param_text.Length)
        //    {
        //        return param_text;
        //    }

        //    StringBuilder stringBuilder = new StringBuilder();
        //    int index = 0;
        //    for (; index <= param_text.Length - param_search.Length; index++)
        //    {
        //        for (int j = 0; j < param_search.Length; j++)
        //        {
        //            // Check if the sliding substring and the search string are different at the current symbol
        //            if (param_text[index] != param_search[j])
        //            {
        //                stringBuilder.Append(param_text[index]);
        //                break;
        //            }
        //            // Check if it is the end
        //            if (j == param_search.Length)
        //            {
        //                stringBuilder.Append(param_replacement);
        //                index += param_search.Length - 1 /* there will be +1 because of "for" operator */;
        //            }
        //        }
        //    }
        //    // Collect what's left
        //    while (index < param_text.Length)
        //    {
        //        stringBuilder.Append(param_text[index]);
        //        index++;
        //    }
        //    return stringBuilder.ToString();
        //}
        public static int IndexOf(this string @string, string[] substrings)
        {
            return @string.IndexOf(substrings, 0);
        }
        public static int IndexOf(this string @string, string[] substrings, int start)
        {
            int index = -1;
            foreach (string substring in substrings)
            {
                int temp = @string.IndexOf(substring, start);
                if (index < 0 || (temp >= 0 && index > temp))
                {
                    index = temp;
                }
            }
            return index;
        }
        public static string Sanitize(this string @string, StringSanitizeMode keep, string keepThese = null)
        {
            return @string.Sanitize((keep & StringSanitizeMode.KEEP_LOWER) != 0, (keep & StringSanitizeMode.KEEP_UPPER) != 0, (keep & StringSanitizeMode.KEEP_DIGITS) != 0, (keep & StringSanitizeMode.KEEP_SPACES) != 0, keepThese);
        }
        public static string CapitalizeFirstLetter(this string @string)
        {
            return @string = @string.Substring(0, 1).ToUpper() + @string.Substring(1);
        }
        public static string DecapitalizeFirstLetter(this string @string)
        {
            return @string = @string.Substring(0, 1).ToLower() + @string.Substring(1);
        }
        public static string ToUpperCase(this string @string, int index)
        {
            int start = index < 0 ? (@string.Length - index) : index;
            if (start < 0 || start >= @string.Length)
            {
                throw new ArgumentOutOfRangeException($"Start index {index} is out of range.", nameof(index));
            }
            return @string.Substring(0, start) + @string.Substring(start).ToUpperInvariant();
        }
        public static string ToUpperCase(this string @string, int index, int length)
        {
            if (length == 0)
            {
                return @string;
            }

            int start = index < 0 ? (@string.Length - index) : index;
            if (start < 0 || start >= @string.Length)
            {
                throw new ArgumentOutOfRangeException($"Start index {index} is out of range.", nameof(index));
            }
            if (length < 0)
            {
                throw new ArgumentException("Length cannot be negative.");
            }
            if (start + length > @string.Length)
            {
                throw new ArgumentOutOfRangeException($"End index {index} is out of range.");
            }
            return @string.Substring(0, start) + @string.Substring(start, length).ToUpperInvariant() + @string.Substring(start + length);
        }
        public static string ToLowerCase(this string @string, int index)
        {
            int start = index < 0 ? (@string.Length - index) : index;
            if (start < 0 || start >= @string.Length)
            {
                throw new ArgumentOutOfRangeException($"Start index {index} is out of range.", nameof(index));
            }
            return @string.Substring(0, start) + @string.Substring(start).ToLowerInvariant();
        }
        public static string ToLowerCase(this string @string, int index, int length)
        {
            if (length == 0)
            {
                return @string;
            }

            int start = index < 0 ? (@string.Length - index) : index;
            if (start < 0 || start >= @string.Length)
            {
                throw new ArgumentOutOfRangeException($"Start index {index} is out of range.", nameof(index));
            }
            if (length < 0)
            {
                throw new ArgumentException("Length cannot be negative.");
            }
            if (start + length > @string.Length)
            {
                throw new ArgumentOutOfRangeException($"End index {index} is out of range.");
            }
            return @string.Substring(0, start) + @string.Substring(start, length).ToLowerInvariant() + @string.Substring(start + length);
        }
        /// <summary>
        /// Convert snake case to camel case. First letters are converted to uppercase. Parts that contain mixed case are left as is unless <paramref name="forceFirstUpperRestLower"/> is set.
        /// </summary>
        /// <param name="string">The string to convert.</param>
        /// <param name="delimiter">Snake case delimiter, usually "_" or " ".</param>
        /// <param name="forceFirstUpperRestLower">Force parts that contain mixed case to have first letter in upper case and the rest in lower case.</param>
        /// <param name="keepDelimiters">If to keep delimiters.</param>
        /// <returns>The string in camel case.</returns>
        public static string ConvertSnakeCaseToCamelCase(this string @string, char delimiter, bool forceFirstUpperRestLower = false, bool keepDelimiters = false)
        {
            return ConvertSnakeCaseToCamelCase(@string, new char[] { delimiter }, new string[0], new string[0], forceFirstUpperRestLower, keepDelimiters);
        }
        /// <summary>
        /// Convert snake case to camel case. First letters are converted to uppercase. Parts that contain mixed case are left as is unless <paramref name="forceFirstUpperRestLower"/> is set.
        /// </summary>
        /// <param name="string">The string to convert.</param>
        /// <param name="delimiters">Snake case delimiters, usually "_" or " ".</param>
        /// <param name="forceFirstUpperRestLower">Force parts that contain mixed case to have first letter in upper case and the rest in lower case.</param>
        /// <param name="keepDelimiters">If to keep delimiters.</param>
        /// <returns>The string in camel case.</returns>
        public static string ConvertSnakeCaseToCamelCase(this string @string, char[] delimiters, bool forceFirstUpperRestLower = false, bool keepDelimiters = false)
        {
            return ConvertSnakeCaseToCamelCase(@string, delimiters, new string[0], new string[0], forceFirstUpperRestLower, keepDelimiters);
        }
        /// <summary>
        /// Convert snake case to camel case. First letters are converted to uppercase. Parts that contain mixed case are left as is unless <paramref name="forceFirstUpperRestLower"/> is set.
        /// </summary>
        /// <param name="string">The string to convert.</param>
        /// <param name="delimiter">Snake case delimiter, usually "_" or " ".</param>
        /// <param name="forceUpperCase">Words to forcibly convert into uppercase.</param>
        /// <param name="forceLowerCase">Words to forcibly convert into lowercase including the first letter.</param>
        /// <param name="forceFirstUpperRestLower">Force parts that contain mixed case to have first letter in upper case and the rest in lower case.</param>
        /// <param name="keepDelimiters">If to keep delimiters.</param>
        /// <returns>The string in camel case.</returns>
        public static string ConvertSnakeCaseToCamelCase(this string @string, char delimiter, string[] forceUpperCase, string[] forceLowerCase, bool forceFirstUpperRestLower = false, bool keepDelimiters = false)
        {
            return ConvertSnakeCaseToCamelCase(@string, new char[] { delimiter }, forceUpperCase, forceLowerCase, forceFirstUpperRestLower, keepDelimiters);
        }
        /// <summary>
        /// Convert snake case to camel case. First letters are converted to uppercase. Parts that contain mixed case are left as is unless <paramref name="forceFirstUpperRestLower"/> is set.
        /// </summary>
        /// <param name="string">The string to convert.</param>
        /// <param name="delimiters">Snake case delimiters, usually "_" or " ".</param>
        /// <param name="forceUpperCase">Words to forcibly convert into uppercase.</param>
        /// <param name="forceLowerCase">Words to forcibly convert into lowercase including the first letter.</param>
        /// <param name="forceFirstUpperRestLower">Force parts that contain mixed case to have first letter in upper case and the rest in lower case.</param>
        /// <param name="keepDelimiters">If to keep delimiters.</param>
        /// <returns>The string in camel case.</returns>
        public static string ConvertSnakeCaseToCamelCase(this string @string, char[] delimiters, string[] forceUpperCase, string[] forceLowerCase, bool forceFirstUpperRestLower = false, bool keepDelimiters = false)
        {
            StringBuilder stringBuilder = new StringBuilder();

            string lowerCase = @string.ToLowerInvariant();
            string upperCase = @string.ToUpperInvariant();
            bool isMixedCase = @string != lowerCase && @string != upperCase;

            delimiters = delimiters ?? new char[] { ' ', '\t', '\r', '\t' };
            string[] parts = @string.Split(delimiters);
            // This contains separator position
            int separatorIndex = 0;

            foreach (string part in parts)
            {
                if (part.Length == 0)
                {
                    ;
                }
                else if (part.Length == 1)
                {
                    if (forceLowerCase.Contains(part, StringComparer.OrdinalIgnoreCase))
                    {
                        stringBuilder.Append(part.ToLowerInvariant());
                    }
                    else if (forceUpperCase.Contains(part, StringComparer.OrdinalIgnoreCase))
                    {
                        stringBuilder.Append(part.ToUpperInvariant());
                    }
                    else // Default behaviour
                    {
                       stringBuilder.Append(part.ToUpperInvariant());
                    }
                }
                else
                {
                    if (forceLowerCase.Contains(part, StringComparer.OrdinalIgnoreCase))
                    {
                        stringBuilder.Append(part.ToLowerInvariant());
                    }
                    else if (forceUpperCase.Contains(part, StringComparer.OrdinalIgnoreCase))
                    {
                        stringBuilder.Append(part.ToUpperInvariant());
                    }
                    else
                    {
                        string first = part.Substring(0, 1);
                        string rest = part.Substring(1);
                        string restInLower = rest.ToLowerInvariant();

                        // If there are upper case characters in the middle - do not apply CamelCase. It may be an acronym or smth else
                        if (!forceFirstUpperRestLower && isMixedCase && !rest.Equals(restInLower, StringComparison.Ordinal))
                        {
                            stringBuilder.Append(part);
                        }
                        else
                        {
                            stringBuilder.Append(first.ToUpperInvariant());
                            stringBuilder.Append(restInLower);
                        }
                    }
                }
                if (keepDelimiters)
                {
                    // Move the length of the part just processed
                    separatorIndex += part.Length;
                    if (separatorIndex < @string.Length - 1)
                    {
                        // Add the separator
                        stringBuilder.Append(@string.Substring(separatorIndex, 1));
                    }
                    // Move the length of the separator - always 1
                    separatorIndex++;
                }
            }

            return stringBuilder.ToString();
        }
        public static string ConvertCamelCaseToSnakeCase(this string @string, string delimiter, ConvertCamelCaseToSnakeCaseMode mode)
        {
            StringBuilder stringBuilder = new StringBuilder();

            bool isPreviousCharacterLowerCase = true;
            for (int index = 0; index < @string.Length; index++)
            {
                char character = @string[index];
                if (character.IsAlphabetLowercase())
                {
                    if (mode == ConvertCamelCaseToSnakeCaseMode.UPPER_CASE)
                    {
                        character = character.ToUpperInvariant();
                    }
                    isPreviousCharacterLowerCase = true;
                }
                else if (character.IsAlphabetUppercase())
                {
                    if (mode == ConvertCamelCaseToSnakeCaseMode.LOWER_CASE || (mode == ConvertCamelCaseToSnakeCaseMode.FIRST_UPPER_REST_LOWER && index != 0))
                    {
                        character = character.ToLowerInvariant();
                    }
                    if (isPreviousCharacterLowerCase)
                    {
                        // Do not add the delimiter in front of the first character
                        if (stringBuilder.Length > 0)
                        {
                            stringBuilder.Append(delimiter);
                        }
                        isPreviousCharacterLowerCase = false;
                    }
                }
                else
                {
                    ; // Digit, symbol, etc.
                }

                stringBuilder.Append(character);
            }

            return stringBuilder.ToString();
        }
        public static string ToUpperFirstLowerRest(this string @string)
        {
            if (@string.Length == 0)
            {
                return "";
            }
            if (@string.Length == 1)
            {
                return @string.ToUpper();
            }
            return @string[0].ToUpper() + @string.Substring(1).ToLower();
        }
        public static string ToUpperFirst(this string @string)
        {
            if (@string.Length == 0)
            {
                return "";
            }
            if (@string.Length == 1)
            {
                return @string.ToUpper();
            }
            return @string[0].ToUpper() + @string.Substring(1);
        }
        public static string ToLowerFirst(this string @string)
        {
            if (@string.Length == 0)
            {
                return "";
            }
            if (@string.Length == 1)
            {
                return @string.ToLower();
            }
            return @string[0].ToLower() + @string.Substring(1);
        }
        public static string ToUpperFirstLowerRestInvariant(this string @string)
        {
            if (@string.Length == 0)
            {
                return "";
            }
            if (@string.Length == 1)
            {
                return @string.ToUpperInvariant();
            }
            return @string[0].ToUpperInvariant() + @string.Substring(1).ToLowerInvariant();
        }
        public static string Mask(this string @string, int countToSkipAtBeginning = 0, int countToSkipAtEnding = 0)
        {
            return (@string.Length <= countToSkipAtBeginning + countToSkipAtEnding) ? @string :
                (@string.Substring(0, countToSkipAtBeginning) + (@string.Length - countToSkipAtBeginning - countToSkipAtEnding).Times("*") + @string.Substring(@string.Length - countToSkipAtEnding, countToSkipAtEnding));
        }
        public static string Sanitize(this string @string, bool keepAlphabetLowercase, bool keepAlphabetUppercase, bool keepDigits, bool keepSpaces, string keepThese = null)
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (char @char in @string)
            {
                if (keepAlphabetLowercase && 0x61 /* a */ <= @char && @char <= 0x7A /* z */)
                {
                    stringBuilder.Append(@char);
                }
                else if (keepAlphabetUppercase && 0x41 /* A */ <= @char && @char <= 0x5A /* Z */)
                {
                    stringBuilder.Append(@char);
                }
                else if (keepDigits && 0x30 /* 0 */ <= @char && @char <= 0x39 /* 9 */)
                {
                    stringBuilder.Append(@char);
                }
                else if (keepSpaces && @char == ' ')
                {
                    stringBuilder.Append(@char);
                }
                else if (keepThese != null && keepThese.Contains(@char))
                {
                    stringBuilder.Append(@char);
                }
            }

            return stringBuilder.ToString();
        }
        public static string Format(this string @string, params string[] values)
        {
            return String.Format(@string, values);
        }
        public static string EnglishPluralize(this string @string)
        {
            if (@string == "")
            {
                return "";
            }
            if (@string.EndsWith("y"))
            {
                return @string.Substring(0, @string.Length - 1) + "ies";
            }
            if (@string.EndsWith("s"))
            {
                return @string.Substring(0, @string.Length - 1) + "es";
            }
            return @string + "s";
        }
        public static string EnglishPluralize(this string @string, int count)
        {
            return (count == 1 || count == -1) ? @string : @string.EnglishPluralize();
        }
        public static int FromRoman(this string @string)
        {
            int result = 0;
            for (int i = @string.Length - 1; i >= 0; i--)
            {
                char character = @string[i].ToUpperInvariant();
                if (character ==  'I')
                {
                    if      (result == 5 || result == 10)  result -= 1;
                    else if (result <= 2)                  result += 1;
                    else throw new ArgumentException(nameof(@string), $"String {@string} is not a valid Roman number.");
                }
                else if (character ==  'V')
                {
                    if (result <= 3) result += 5;
                    else throw new ArgumentException(nameof(@string), $"String {@string} is not a valid Roman number.");
                }
                else if (character ==  'X')
                {
                    if      ((50 <= result && result <= 59) || (100 <= result && result <= 109)) result -= 10;
                    else if (result <= 29) result += 10;
                    else throw new ArgumentException(nameof(@string), $"String {@string} is not a valid Roman number.");
                }
                else if (character ==  'L')
                {
                    if (result <= 39) result += 50;
                    else throw new ArgumentException(nameof(@string), $"String {@string} is not a valid Roman number.");
                }
                else if (character ==  'C')
                {
                    if      ((500 <= result && result <= 599) || (1000 <= result && result <= 1099)) result -= 100;
                    else if (result <= 299) result += 100;
                    else throw new ArgumentException(nameof(@string), $"String {@string} is not a valid Roman number.");
                }
                else if (character ==  'D')
                {
                    if (result <= 399) result += 500;
                    else throw new ArgumentException(nameof(@string), $"String {@string} is not a valid Roman number.");
                }
                else if (character ==  'M')
                {
                    if (result <= 2999) result += 1000;
                    else throw new ArgumentException(nameof(@string), $"String {@string} is not a valid Roman number.");
                }
            }
            return result;
        }
    }
}