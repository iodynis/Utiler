﻿using System;
using System.Collections;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Find position of the needle array inside the array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="needle">The needle array to find.</param>
        /// <returns>The position of the needle or -1 if did not find it.</returns>
        public static int Find<T>(this T[] array, T[] needle) where T : IEquatable<T>
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
            if (array.Length == 0)
            {
                throw new IndexOutOfRangeException("Array to search is empty.");
            }
            if (needle == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
            if (needle.Length == 0)
            {
                throw new IndexOutOfRangeException("Array to find is empty.");
            }

            if (needle.Length > array.Length)
            {
                return -1;
            }

            int indexStartLast = array.Length - needle.Length;
            for (int indexStart = 0; indexStart < indexStartLast; indexStart++)
            {
                bool isMatch = true;
                for (int index = 0; index < needle.Length; index++)
                {
                    if (needle == null)
                    {
                        if (array == null)
                        {
                            continue;
                        }
                        else
                        {
                            isMatch = false;
                            break;
                        }
                    }
                    if (array == null)
                    {
                        isMatch = false;
                        break;
                    }

                    if (!needle[index].Equals(array[index + indexStart]))
                    {
                        isMatch = false;
                        break;
                    }
                }

                if (isMatch)
                {
                    return indexStart;
                }
            }

            return -1;
        }
        /// <summary>
        /// Get the element of the array at the specified index.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="index">The index.</param>
        /// <returns>The element at the specified index if the index falls in the correct range.
        /// First or last element if it does not, depending if the index is lower than zero or greater or equal to the array length.</returns>
        public static T GetClamped<T>(this T[] array, int index)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
            if (array.Length == 0)
            {
                throw new IndexOutOfRangeException("Array is empty.");
            }

            if (index < 0)
            {
                return array[0];
            }
            if (index >= array.Length)
            {
                return array[array.Length - 1];
            }
            return array[index];
        }
        /// <summary>
        /// Create a new array that contains elements after the specified index, including the one at the index.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array to slice.</param>
        /// <param name="index">The starting index.</param>
        /// <returns>A new array that contains elements after the specified index, including the one at the index.</returns>
        public static T[] Slice<T>(this T[] array, int index)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
            if (index < 0)
            {
                throw new ArgumentException("Index cannot be negative.", nameof(index));
            }
            if (index >= array.Length)
            {
                throw new IndexOutOfRangeException($"Start index {index} is out of the range of the array. Array size is {array.Length}.");
            }

            int length = array.Length - index;
            T[] arraySliced = new T[length];
            Array.Copy(array, index, arraySliced, 0, length);
            return arraySliced;
        }
        /// <summary>
        /// Create a new array that contains elements after the specified index, including the one at the index, and of the specified length.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array to slice.</param>
        /// <param name="index">The starting index.</param>
        /// <param name="length">The length of of the new array.</param>
        /// <returns>A new array that contains elements after the specified index, including the one at the index, and of the specified length.</returns>
        public static T[] Slice<T>(this T[] array, int index, int length)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
            if (index < 0)
            {
                throw new ArgumentException("Index cannot be negative.", nameof(index));
            }
            if (index >= array.Length)
            {
                throw new IndexOutOfRangeException($"Start index {index} is out of the range of the array. Array size is {array.Length}.");
            }
            if (length < 0)
            {
                throw new ArgumentException("Length cannot be negative.", nameof(length));
            }
            if (index + length >= array.Length)
            {
                throw new IndexOutOfRangeException($"End index {index + length} is out of the range of the array. Array size is {array.Length}.");
            }

            T[] arraySliced = new T[length];
            Array.Copy(array, index, arraySliced, 0, length);
            return arraySliced;
        }
        /// <summary>
        /// Make a shallow copy of the array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <returns>A shallow copy of the array.</returns>
	    public static T[] CopyShallow<T>(this T[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            T[] clone = new T[array.LongLength];
            Array.Copy(array, clone, array.LongLength);
            return clone;
        }
        /// <summary>
        /// Clones the array and sorts the cloned one.
        /// </summary>
        /// <param name="array"></param>
        /// <param name="comparer"></param>
        /// <returns></returns>
        public static Array CloneAndSort(this Array array, IComparer comparer)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }
            if (comparer == null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }

            Array arraySorted = (Array)array.Clone();
            Array.Sort(arraySorted, comparer);
            return arraySorted;
        }

        /// <summary>
        /// Initialize the array with the specified value;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="value">The default value.</param>
        public static void Initialize<T>(this T[] array, T value)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (long x = 0; x < array.LongLength; x++)
            {
                array[x] = value;
            }
        }
        /// <summary>
        /// Initialize the array with the specified value;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="value">The default value.</param>
        public static void Initialize<T>(this T[][] array, T value)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (long x = 0; x < array.LongLength; x++)
            {
                for (long y = 0; y < array[x].LongLength; y++)
                {
                    array[x][y] = value;
                }
            }
        }
        /// <summary>
        /// Initialize the array with the specified value;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="value">The default value.</param>
        public static void Initialize<T>(this T[,] array, T value)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (long x = 0; x < array.GetLongLength(0); x++)
            {
                for (long y = 0; y < array.GetLongLength(1); y++)
                {
                    array[x, y] = value;
                }
            }
        }
        /// <summary>
        /// Initialize the array with the specified value;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="value">The default value.</param>
        public static void Initialize<T>(this T[][][] array, T value)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (long x = 0; x < array.LongLength; x++)
            {
                for (long y = 0; y < array[x].LongLength; y++)
                {
                    for (long z = 0; z < array[x][y].LongLength; z++)
                    {
                        array[x][y][z] = value;
                    }
                }
            }
        }
        /// <summary>
        /// Initialize the array with the specified value;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="value">The default value.</param>
        public static void Initialize<T>(this T[,,] array, T value)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            for (long x = 0; x < array.GetLongLength(0); x++)
            {
                for (long y = 0; y < array.GetLongLength(1); y++)
                {
                    for (long z = 0; z < array.GetLongLength(2); z++)
                    {
                        array[x, y, z] = value;
                    }
                }
            }
        }
        /// <summary>
        /// Check if all elements of the array satisfy the provided condition.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="condition">The condition to test the array elements against.</param>
        /// <returns>True if every element of the array satisfies the provided condition. False otherwise.</returns>
        public static bool All<T>(this T[,] array, Func<T, bool> condition)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int iMax = array.GetLength(0);
            int jMax = array.GetLength(1);
            for (int i = 0; i < iMax; i++)
            {
                for (int j = 0; j < jMax; j++)
                {
                    if (!condition(array[i, j]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        /// <summary>
        /// Check if any element of the array satisfy the provided condition.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array">The array.</param>
        /// <param name="condition">The condition to test the array elements against.</param>
        /// <returns>True if any element of the array satisfies the provided condition. False otherwise.</returns>
        public static bool Any<T>(this T[,] array, Func<T, bool> condition)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int iMax = array.GetLength(0);
            int jMax = array.GetLength(1);
            for (int i = 0; i < iMax; i++)
            {
                for (int j = 0; j < jMax; j++)
                {
                    if (condition(array[i, j]))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}