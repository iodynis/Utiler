﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Convert Enum.XXX_YYY to "Xxx yyy" string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumeration">Enum.</param>
        /// <returns>Pretty string.</returns>
        public static string ToPrettyString<T>(this T enumeration) where T : Enum
        {
            return enumeration.ToString().Replace('_', ' ').ToUpperFirstLowerRest();
        }

        private static Dictionary<Enum, Enum> EnumerationNext = new Dictionary<Enum, Enum>();
        /// <summary>
        /// Get next enumeration value. This is looped -- after the last value comes the first one.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumeration">Enum.</param>
        /// <returns>Next value.</returns>
        public static T Next<T>(this T enumeration) where T : Enum
        {
            if (!EnumerationNext.TryGetValue(enumeration, out Enum next))
            {
                Array array = Enum.GetValues(typeof(T));
                for (int index = 0; index < array.Length; index++)
                {
                    object value = array.GetValue(index);
                    if (enumeration.Equals(value))
                    {
                        if (index == array.Length - 1)
                        {
                            EnumerationNext[enumeration] = (T)array.GetValue(0);
                        }
                        else
                        {
                            EnumerationNext[enumeration] = (T)array.GetValue(index + 1);
                        }
                    }
                }
            }

            return (T)next;
        }

        private static Dictionary<Enum, Enum> EnumerationPrevious = new Dictionary<Enum, Enum>();
        /// <summary>
        /// Get previous enumeration value. This is looped -- before the first value comes the last one.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumeration">Enum.</param>
        /// <returns>Previous value.</returns>
        public static T Previous<T>(this T enumeration) where T : Enum
        {
            if (!EnumerationPrevious.TryGetValue(enumeration, out Enum previous))
            {
                Array array = Enum.GetValues(typeof(T));
                for (int index = 0; index < array.Length; index++)
                {
                    object value = array.GetValue(index);
                    if (enumeration.Equals(value))
                    {
                        if (index == 0)
                        {
                            EnumerationPrevious[enumeration] = (T)array.GetValue(array.Length - 1);
                        }
                        else
                        {
                            EnumerationPrevious[enumeration] = (T)array.GetValue(index - 1);
                        }
                    }
                }
            }

            return (T)previous;
        }
    }
}
