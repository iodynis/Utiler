﻿#if !UNITY_ENGINE

using System.Drawing;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Check if the Alpha, Red, Green and Blue values are equal.
        /// </summary>
        /// <param name="one">The color.</param>
        /// <param name="two">The color.</param>
        /// <returns>True if the Alpha, Red, Green and Blue values are equal. False otherwise.</returns>
        public static bool EqualsInArgb(this Color one, Color two)
        {
            return one.A == two.A && one.R == two.R && one.G == two.G && one.B == two.B;
        }
        /// <summary>
        /// Check if the Red, Green and Blue values are equal. Alpha channel is ignored.
        /// </summary>
        /// <param name="one">The color.</param>
        /// <param name="two">The color.</param>
        /// <returns>True if the Red, Green and Blue values are equal. False otherwise.</returns>
        public static bool EqualsInRgb(this Color one, Color two)
        {
            return one.R == two.R && one.G == two.G && one.B == two.B;
        }
        /// <summary>
        /// Convert the color to a string in ARGB format.
        /// </summary>
        /// <param name="color">The color.</param>
        /// <returns>ARGB string.</returns>
        public static string ToHexArgb(this Color color)
        {
            return color.A.ToString("X2") + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
        }
        /// <summary>
        /// Convert the color to a string in RGBA format.
        /// </summary>
        /// <param name="color">The color.</param>
        /// <returns>RGBA string.</returns>
        public static string ToHexRgba(this Color color)
        {
            return color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2") + color.A.ToString("X2");
        }
        /// <summary>
        /// Convert the color to a string in RGB format.
        /// </summary>
        /// <param name="color">The color.</param>
        /// <returns>RGB string.</returns>
        public static string ToHexRgb(this Color color)
        {
            return color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
        }
    }
}

#endif