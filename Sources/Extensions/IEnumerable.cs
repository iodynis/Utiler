﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Find first element that satisfies the condition.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static T FindFirst<T>(this IEnumerable<T> enumerable, Func<T, bool> condition) where T : class
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (condition == null)
            {
                throw new ArgumentNullException(nameof(condition));
            }

            foreach (var item in enumerable)
            {
                if (condition(item))
                {
                    return item;
                }
            }
            return null;
        }
        /// <summary>
        /// Find all element that satisfy the condition.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public static List<T> FindAll<T>(this IEnumerable<T> enumerable, Func<T, bool> condition) where T : class
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (condition == null)
            {
                throw new ArgumentNullException(nameof(condition));
            }

            List<T> items = new List<T>();
            foreach (var item in enumerable)
            {
                if (condition(item))
                {
                    items.Add(item);
                }
            }
            return items;
        }
        public static T FindFirst<T>(this IEnumerable<T> enumerable, T item) where T : struct
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }

            foreach (var itemToTest in enumerable)
            {
                if (item.Equals(itemToTest))
                {
                    return itemToTest;
                }
            }
            return default(T);
        }
        public static T FindMax<T>(this IEnumerable<T> enumerable, Func<T, int> measure) where T : class
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (measure == null)
            {
                throw new ArgumentNullException(nameof(measure));
            }

            T itemMax = default(T);
            int measurementMax = int.MinValue;

            foreach (var item in enumerable)
            {
                int measurement = measure(item);
                if (measurement > measurementMax)
                {
                    itemMax = item;
                    measurementMax = measurement;
                }
            }

            return itemMax;
        }
        public static T FindMin<T>(this IEnumerable<T> enumerable, Func<T, int> measure) where T : class
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (measure == null)
            {
                throw new ArgumentNullException(nameof(measure));
            }

            T itemMax = default(T);
            int measurementMax = int.MaxValue;

            foreach (var item in enumerable)
            {
                int measurement = measure(item);
                if (measurement < measurementMax)
                {
                    itemMax = item;
                    measurementMax = measurement;
                }
            }

            return itemMax;
        }
        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            foreach (var item in enumerable)
            {
                action(item);
            }
        }
        public static void ForEach(this IEnumerable enumerable, Action<object> action)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            foreach (object item in enumerable)
            {
                action(item);
            }
        }
        /// <summary>
        /// Check if enumerable has no elements.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns>True if empty, false otherwise.</returns>
        public static bool IsEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }

            return !enumerable.Any();
        }
        /// <summary>
        /// Check if enumerable is null or has no elements.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns>True if null or empty, false otherwise.</returns>
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }

            return enumerable == null || !enumerable.Any();
        }
        /// <summary>
        /// In case of null returns a new empty enumerable of cthe correct type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns>Empty IEnumerable if it's null.</returns>
        public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }

            return enumerable ?? Enumerable.Empty<T>();
        }
        /// <summary>
        /// Skip null elements.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable"></param>
        /// <returns>IEnumerable without null elements.</returns>
        public static IEnumerable<T> SkipNulls<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }

            return enumerable.Where(_item => _item != null);
        }
        public static IEnumerable<(T item, int index)> WithIndex<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }

            return enumerable.Select((item, index) => (item, index));
        }
        public static IEnumerable<T> Concat<T>(this IEnumerable<T> enumerable, T item)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }

            foreach (T enumerableItem in enumerable)
            {
                yield return enumerableItem;
            }
            yield return item;
        }
        public static short Multiply<T>(this IEnumerable<T> enumerable, Func<T, short> getter)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (getter == null)
            {
                throw new ArgumentNullException(nameof(getter));
            }

            IEnumerator<T> enumerator = enumerable.GetEnumerator();
            short result = 1;
            while (enumerator.MoveNext())
            {
                result *= getter(enumerator.Current);
            }
            return result;
        }
        public static ushort Multiply<T>(this IEnumerable<T> enumerable, Func<T, ushort> getter)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (getter == null)
            {
                throw new ArgumentNullException(nameof(getter));
            }

            IEnumerator<T> enumerator = enumerable.GetEnumerator();
            ushort result = 1;
            while (enumerator.MoveNext())
            {
                result *= getter(enumerator.Current);
            }
            return result;
        }
        public static int Multiply<T>(this IEnumerable<T> enumerable, Func<T, int> getter)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (getter == null)
            {
                throw new ArgumentNullException(nameof(getter));
            }

            IEnumerator<T> enumerator = enumerable.GetEnumerator();
            int result = 1;
            while (enumerator.MoveNext())
            {
                result *= getter(enumerator.Current);
            }
            return result;
        }
        public static uint Multiply<T>(this IEnumerable<T> enumerable, Func<T, uint> getter)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (getter == null)
            {
                throw new ArgumentNullException(nameof(getter));
            }

            IEnumerator<T> enumerator = enumerable.GetEnumerator();
            uint result = 1;
            while (enumerator.MoveNext())
            {
                result *= getter(enumerator.Current);
            }
            return result;
        }
        public static long Multiply<T>(this IEnumerable<T> enumerable, Func<T, long> getter)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (getter == null)
            {
                throw new ArgumentNullException(nameof(getter));
            }

            IEnumerator<T> enumerator = enumerable.GetEnumerator();
            long result = 1;
            while (enumerator.MoveNext())
            {
                result *= getter(enumerator.Current);
            }
            return result;
        }
        public static ulong Multiply<T>(this IEnumerable<T> enumerable, Func<T, ulong> getter)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (getter == null)
            {
                throw new ArgumentNullException(nameof(getter));
            }

            IEnumerator<T> enumerator = enumerable.GetEnumerator();
            ulong result = 1;
            while (enumerator.MoveNext())
            {
                result *= getter(enumerator.Current);
            }
            return result;
        }
        public static decimal Multiply<T>(this IEnumerable<T> enumerable, Func<T, decimal> getter)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (getter == null)
            {
                throw new ArgumentNullException(nameof(getter));
            }

            IEnumerator<T> enumerator = enumerable.GetEnumerator();
            decimal result = 1;
            while (enumerator.MoveNext())
            {
                result *= getter(enumerator.Current);
            }
            return result;
        }
        public static float Multiply<T>(this IEnumerable<T> enumerable, Func<T, float> getter)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (getter == null)
            {
                throw new ArgumentNullException(nameof(getter));
            }

            IEnumerator<T> enumerator = enumerable.GetEnumerator();
            float result = 1f;
            while (enumerator.MoveNext())
            {
                result *= getter(enumerator.Current);
            }
            return result;
        }
        public static double Multiply<T>(this IEnumerable<T> enumerable, Func<T, double> getter)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (getter == null)
            {
                throw new ArgumentNullException(nameof(getter));
            }

            IEnumerator<T> enumerator = enumerable.GetEnumerator();
            double result = 1f;
            while (enumerator.MoveNext())
            {
                result *= getter(enumerator.Current);
            }
            return result;
        }
        public static bool ContainsAny<T>(this IEnumerable<T> enumerable, IEnumerable<T> items)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (items == null)
            {
                throw new ArgumentNullException(nameof(items));
            }

            foreach (T one in enumerable)
            {
                foreach (T two in items)
                {
                    if (Equals(one, two))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public static bool ContainsAll<T>(this IEnumerable<T> enumerable, IEnumerable<T> items)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (items == null)
            {
                throw new ArgumentNullException(nameof(items));
            }

            foreach (T one in enumerable)
            {
                bool contains = false;
                foreach (T two in items)
                {
                    if (Equals(one, two))
                    {
                        contains = true;
                        break;
                    }
                }
                if (!contains)
                {
                    return false;
                }
            }
            return true;
        }
        public static bool Contains<T>(this IEnumerable<T> enumerable, Func<T, bool> filter)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }
            if (filter == null)
            {
                throw new ArgumentNullException(nameof(filter));
            }

            foreach (T item in enumerable)
            {
                if (filter(item))
                {
                    return true;
                }
            }
            return false;
        }
        public static IEnumerable<T> Except<T>(this IEnumerable<T> enumerable, T exclude)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }

            foreach (T item in enumerable)
            {
                if (Equals(item, exclude))
                {
                    continue;
                }
                yield return item;
            }
        }
        public static T Y<T>(this IEnumerable<T> enumerable, Func<T, T, T> function)
        {
            T result = default(T);

            foreach (T item in enumerable)
            {
                result = function(item, result);
            }

            return result;
        }

        #region Temp collections

        private static class TempListPool<T>
        {
            private static Stack<List<T>> Lists = new Stack<List<T>>();
            public static List<T> Take()
            {
                if (Lists.Count > 0)
                {
                    return Lists.Pop();
                }

                return new List<T>();
            }
            public static void Put(List<T> list)
            {
                list.Clear();
                Lists.Push(list);
            }
        }
        public static IEnumerable<T> ToTempList<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                throw new ArgumentNullException(nameof(enumerable));
            }

            List<T> list = TempListPool<T>.Take();
            list.AddRange(enumerable);

            for (int i = 0; i < list.Count; i++)
            {
                yield return list[i];
            }

            TempListPool<T>.Put(list);
        }

        #endregion
    }
}