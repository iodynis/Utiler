﻿using System;
using System.Reflection;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static object GetValue(this MemberInfo memberInfo, object @object)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }

            switch (memberInfo)
            {
                case PropertyInfo propertyInfo:
                    return propertyInfo.GetValue(@object);
                case FieldInfo fieldInfo:
                    return fieldInfo.GetValue(@object);
                default:
                    throw new NotSupportedException($"MemebrType of {memberInfo.MemberType} is not supported.");
            }
        }
        public static void SetValue(this MemberInfo memberInfo, object @object, object value)
        {
            if (memberInfo == null)
            {
                throw new ArgumentNullException(nameof(memberInfo));
            }
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }

            switch (memberInfo)
            {
                case PropertyInfo propertyInfo:
                    propertyInfo.SetValue(@object, value);
                    break;
                case FieldInfo fieldInfo:
                    fieldInfo.SetValue(@object, value);
                    break;
                default:
                    throw new NotSupportedException($"MemebrType of {memberInfo.MemberType} is not supported.");
            }
        }
    }
}
