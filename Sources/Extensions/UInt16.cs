﻿using System;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        /// <summary>
        /// Count 1 bits in the value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Number of 1 bits in the value.</returns>
        public static int BitsCount(this UInt16 value)
        {
            return
                BitsCountInByte[(value) & 0xFF] +
                BitsCountInByte[(value >> 8) & 0xFF];
        }
        public static int HighestBitIndex(this UInt16 value)
        {
            if (value < 0)
                return 31;
            if (value == 0)
                return 0;

            if (value < 1 << 8)
                if (value < 1 << 4)
                    if (value < 1 << 2)
                        if (value < 1 << 1) return  0;
                        else return  1;
                    else
                        if (value < 1 << 3) return  2;
                        else return  3;
                else
                    if (value < 1 << 6)
                        if (value < 1 << 5) return  4;
                        else return  5;
                    else
                        if (value < 1 << 7) return  6;
                        else return  7;
            else
                if (value < 1 << 12)
                    if (value < 1 << 10)
                        if (value < 1 << 9) return  8;
                        else return  9;
                    else
                        if (value < 1 << 11) return  10;
                        else return  11;
                else
                    if (value < 1 << 14)
                        if (value < 1 << 13) return  12;
                        else return  13;
                    else
                        if (value == 1 << 14) return  14;
                        else return  15;

            throw new Exception();
        }
        /// <summary>
        /// Reverse the bits in the value.
        /// </summary>
        /// <param name="value">The value to reverse the bits in.</param>
        /// <returns>The value with all the bits reversed.</returns>
        public static UInt16 Reverse(this UInt16 value)
        {
            return (UInt16)(
                (((int)ReverseTable[value      & 0xFF]) << 8) |
                (((int)ReverseTable[value >> 8       ])));
        }
    }
}
