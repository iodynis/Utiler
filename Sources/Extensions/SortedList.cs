﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static int GetNearestDown<T>(this SortedList<int, T> sortedList, int key)
        {
            if (sortedList == null)
            {
                throw new ArgumentNullException(nameof(sortedList));
            }
            if (sortedList.Count == 0)
            {
                throw new ArgumentException("SortedList cannot be empty.", nameof(sortedList));
            }
            // Check outer borders and precise values
            {
                int keyFirst = sortedList.Keys[0];
                if (key <= keyFirst)
                {
                    return keyFirst;
                }
                int keyLast = sortedList.Keys[sortedList.Keys.Count - 1];
                if (keyLast <= key)
                {
                    return keyLast;
                }
                if (sortedList.ContainsKey(key))
                {
                    return key;
                }
            }

            int keyIndexMinimum = 0;
            int keyIndexMaximum = sortedList.Count - 1;
            int keyIndexCurrent = 0;
            while ((keyIndexCurrent = (keyIndexMinimum + keyIndexMaximum) / 2) != keyIndexMinimum)
            {
                //keyIndexCurrent = (keyIndexMinimum + keyIndexMaximum) / 2;
                if (sortedList.Keys[keyIndexCurrent] < key)
                {
                    keyIndexMinimum = keyIndexCurrent;
                }
                else if (sortedList.Keys[keyIndexCurrent] > key)
                {
                    keyIndexMaximum = keyIndexCurrent;
                }
                else
                {
                    break;
                }
            }
            return sortedList.Keys[keyIndexCurrent];
        }
        //public static int GetNearestUp(SortedList<int, int> sortedList, int key)
        //{
        //    int nearsetDown = GetNearestDown(sortedList, key);

        //}
    }
}
