﻿using System;
using System.Reflection;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static object GetPropertyValue(this object @object, string propertyName)
        {
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }

            Type type = @object.GetType();
            PropertyInfo propertyInfo = type.GetProperty(propertyName);
            if (propertyInfo == null)
            {
                throw new Exception($"Property {propertyName} is not presented in {type}.");
            }
            object value = propertyInfo.GetValue(@object);
            return value;
        }
        public static T GetPropertyValue<T>(this object @object, string propertyName)
        {
            return (T)GetPropertyValue(@object, propertyName);
        }
        public static object GetStaticPropertyValue(this object @object, string propertyName)
        {
            return @object.GetType().GetProperty(propertyName).GetValue(null);
        }
        public static T GetStaticPropertyValue<T>(this object @object, string propertyName)
        {
            return (T)@object.GetType().GetProperty(propertyName).GetValue(null);
        }
        //private static Dictionary<Func<TObject, TProperty>> getters;
        /// <summary>
        /// Compiles a property getter.
        /// </summary>
        /// <typeparam name="TObject">Object type.</typeparam>
        /// <typeparam name="TProperty">Property type.</typeparam>
        /// <param name="object">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <returns>Property getter.</returns>
        public static TProperty GetProperty<TObject, TProperty>(this TObject @object, string propertyName)
        {
            return Utiler.GetPropertyGetter<TObject, TProperty>(propertyName)(@object);
        }
        /// <summary>
        /// Compiles a property setter.
        /// </summary>
        /// <typeparam name="TObject">Object type.</typeparam>
        /// <typeparam name="TProperty">Property type.</typeparam>
        /// <param name="object">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="value">Property value.</param>
        /// <returns>Property setter.</returns>
        public static void SetProperty<TObject, TProperty>(this TObject @object, string propertyName, TProperty value)
        {
            Utiler.GetPropertySetter<TObject, TProperty>(propertyName)(@object, value);
        }
    }
}
