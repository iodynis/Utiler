﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Iodynis.Libraries.Utility.Extensions
{
    public static partial class Extensions
    {
        public static void Add(this Stack stack, object @object)
        {
            stack.Push(@object);
        }
        public static void Add<T>(this Stack<T> stack, T @object)
        {
            stack.Push(@object);
        }
        public static void Push(this Stack stack, IEnumerable @object)
        {
            @object.ForEach(_object => stack.Push(_object));
        }
        public static void Push<T>(this Stack<T> stack, IEnumerable<T> @object)
        {
            @object.ForEach(_object => stack.Push(_object));
        }
        public static object[] Pop(this Stack stack, int count)
        {
            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }
            if (count == 0)
            {
                return new object[0];
            }
            object[] items = new object[count];
            for (int i = 0; i < count; i++)
            {
                items[i] = stack.Pop();
            }
            return items;
        }
        public static T[] Pop<T>(this Stack<T> stack, int count)
        {
            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }
            if (count == 0)
            {
                return new T[0];
            }
            T[] items = new T[count];
            for (int i = 0; i < count; i++)
            {
                items[i] = stack.Pop();
            }
            return items;
        }
        public static bool TryPeek(this Stack stack, out object item)
        {
            if (stack.Count == 0)
            {
                item = null;
                return false;
            }

            item = stack.Peek();
            return true;
        }
        public static bool TryPeek<T>(this Stack<T> stack, out T item)
        {
            if (stack.Count == 0)
            {
                item = default;
                return false;
            }

            item = stack.Peek();
            return true;
        }
    }
}
