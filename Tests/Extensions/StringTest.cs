﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Iodynis.Libraries.Utility.Extensions;

namespace Iodynis.Libraries.Utility.Extensions.Test
{
	[TestClass]
	public class StringTest
	{
	    [TestMethod]
	    public void ReplaceOnce()
	    {
	        Assert.AreEqual("alaza", "azaza".ReplaceOnce("za", "la"));
	        Assert.AreEqual("alala", "alaza".ReplaceOnce("za", "la"));
	        Assert.AreEqual("alala", "alala".ReplaceOnce("za", "la"));

	        Assert.ThrowsException<ArgumentNullException>(() => "test".ReplaceOnce(null ,"test"));
	        Assert.ThrowsException<ArgumentNullException>(() => "test".ReplaceOnce("test" , null));
	    }
	    [TestMethod]
	    public void TrimExtension()
	    {
	        Assert.AreEqual("aaa", "aaa.bbb".TrimExtension());
	        Assert.AreEqual("aaa.bbb", "aaa.bbb.ccc".TrimExtension());
	        Assert.AreEqual("aaa.bbb/ccc", "aaa.bbb/ccc.ddd".TrimExtension(2));
	        Assert.AreEqual("aaa.bbb/ccc", "aaa.bbb/ccc.ddd".TrimExtension(2222222));
	        Assert.AreEqual("aaa.bbb/ccc", "aaa.bbb/ccc..ddd".TrimExtension(2));
	        Assert.AreEqual("aaa.bbb/ccc.", "aaa.bbb/ccc...ddd".TrimExtension(2));

	        Assert.ThrowsException<ArgumentOutOfRangeException>(() => "test".TrimExtension(-1));
	    }
	    [TestMethod]
	    public void Substring()
	    {
	        Assert.AreEqual("cdef", "abcdef".Substring('c', true));
	        Assert.AreEqual("def", "abcdef".Substring('c', false));
	        Assert.AreEqual("f", "abcdef".Substring('f', true));
	        Assert.AreEqual("", "abcdef".Substring('f', false));
	        Assert.AreEqual("ccddeeff", "aabbccddeeff".Substring('c', true));
	        Assert.AreEqual("cddeeff", "aabbccddeeff".Substring('c', false));

	        Assert.AreEqual("cd", "abcdef".Substring('c', true, 2));
	        Assert.AreEqual("de", "abcdef".Substring('c', false, 2));
	        Assert.AreEqual("", "abcdef".Substring('c', false, 0));
	        Assert.AreEqual("", "abcdef".Substring('x', false, 0));
	        Assert.AreEqual("ccddeeff", "aabbccddeeff".Substring('c', true, 9000));
	        Assert.AreEqual("cddee", "aabbccddeeff".Substring('c', false, 5));

	        Assert.ThrowsException<ArgumentOutOfRangeException>(() => "test".Substring('t', true, -1));

	        Assert.AreEqual("cde", "abcdef".Substring(2, 'e', true));
	        Assert.AreEqual("cd", "abcdef".Substring(2, 'e', false));
	        Assert.AreEqual("abcdef", "abcdef".Substring(0, 'x', false));
	        Assert.AreEqual("bbccdde", "aabbccddeeff".Substring(2, 'e', true));
	        Assert.AreEqual("bbccdd", "aabbccddeeff".Substring(2, 'e', false));
	        Assert.AreEqual("f", "aabbccddeeff".Substring("aabbccddeeff".Length - 1, 'x', false));

	        Assert.ThrowsException<ArgumentOutOfRangeException>(() => "test".Substring(-1, 't', true));
	        Assert.ThrowsException<ArgumentOutOfRangeException>(() => "test".Substring(9000, 'z', true));

	        Assert.AreEqual("cde", "abcdef".Substring('c', true, 'e', true));
	        Assert.AreEqual("d", "abcdef".Substring('c', false, 'e', false));
	        Assert.AreEqual("ccdde", "aabbccddeeff".Substring('c', true, 'e', true));
	        Assert.AreEqual("cdd", "aabbccddeeff".Substring('c', false, 'e', false));
	        Assert.AreEqual("cddeeff", "aabbccddeeff".Substring('c', false, 'x', false));
	        Assert.AreEqual("aabbccddeeff", "aabbccddeeff".Substring('x', false, 'y', false));
	    }
	    [TestMethod]
	    public void RemoveCharacters()
        {
	        Assert.AreEqual("bcdef", "abcdef".RemoveCharacters(new char[] { 'a' }));
	        Assert.AreEqual("bbccddeeff", "aabbccddeeff".RemoveCharacters(new char[] { 'a' }));
	        Assert.AreEqual("bbccddff", "aabbccddeeff".RemoveCharacters(new char[] { 'a', 'e' }));
	        Assert.AreEqual("bbccdd", "aabbccddeeff".RemoveCharacters(new char[] { 'a', 'e', 'f' }));

	        Assert.ThrowsException<ArgumentNullException>(() => "test".RemoveCharacters(null));
        }
	    [TestMethod]
	    public void LeaveCharacters()
        {
	        Assert.AreEqual("_abc___", "_abc_DEF_123_".LeaveCharacters(true, false, false, new char[] { '_' }));
	        Assert.AreEqual("__DEF__", "_abc_DEF_123_".LeaveCharacters(false, true, false, new char[] { '_' }));
	        Assert.AreEqual("___123_", "_abc_DEF_123_".LeaveCharacters(false, false, true, new char[] { '_' }));
	        Assert.AreEqual("aaaaaa", "abacadaeafa".LeaveCharacters(false, false, false, new char[] { 'a' }));
	        Assert.AreEqual("aaaa", "aabbaa".LeaveCharacters(false, false, false, new char[] { 'a' }));
	        Assert.AreEqual("aaee", "aabbccddeeff".LeaveCharacters(false, false, false, new char[] { 'a', 'e' }));
	        Assert.AreEqual("aaeeff", "aabbccddeeff".LeaveCharacters(false, false, false, new char[] { 'a', 'e', 'f' }));

	        Assert.ThrowsException<ArgumentNullException>(() => "test".LeaveCharacters(false, false, false, null));
        }
	    [TestMethod]
	    public void Count()
        {
	        Assert.AreEqual(0, "bcdef".Count('a'));
	        Assert.AreEqual(1, "abcdef".Count('a'));
	        Assert.AreEqual(3, "abacad".Count('a'));

	        Assert.AreEqual(3, "aaabcd".Count("a", false));
	        Assert.AreEqual(3, "aaabcd".Count("a", true));
	        Assert.AreEqual(1, "aaabcd".Count("aa", false));
	        Assert.AreEqual(2, "aaabcd".Count("aa", true));
	        Assert.AreEqual(1, "aaabcd".Count("aaa", false));
	        Assert.AreEqual(1, "aaabcd".Count("aaa", true));
	        Assert.AreEqual(0, "aaabcd".Count("aaaaaaaaa", true));

        }
    }
}
