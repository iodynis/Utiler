﻿using System;
using System.Collections.Generic;
using Iodynis.Libraries.Utility.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace Iodynis.Libraries.Utility.Extensions.Test
{
    [TestClass]
    public class ILIstTest
    {
	    [TestMethod]
	    public void Test()
	    {
            List<int> list = new List<int> { 0, 1, 2, 3, 4 };

            for (int i = 0; i <= 20; i++)
            {
	            Assert.AreEqual(i % 5, list.ElementAtCycled(i));
            }
            for (int i = -20; i <= 0; i++)
            {
                int j = i;
                while (j < 0) j+= list.Count;

	            Assert.AreEqual(j, list.ElementAtCycled(i));
            }
	        Assert.AreEqual(4, new List<int>() { 1, 2, 3, 4 }.ElementAtCycled(15));
	        Assert.AreEqual(4, new List<int>() { 1, 2, 3, 4 }.ElementAtCycled(3));
	        Assert.AreEqual(1, new List<int>() { 1, 2, 3, 4 }.ElementAtCycled(0));
	        Assert.AreEqual(4, new List<int>() { 1, 2, 3, 4 }.ElementAtCycled(-1));
	        Assert.AreEqual(1, new List<int>() { 1, 2, 3, 4 }.ElementAtCycled(-4));
	        Assert.AreEqual(2, new List<int>() { 1, 2, 3, 4 }.ElementAtCycled(-15));
	        Assert.AreEqual(1, new List<int>() { 1, 2, 3, 4 }.ElementAtCycled(-16));
        }
    }
}
