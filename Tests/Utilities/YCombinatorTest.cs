﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Iodynis.Libraries.Utility.Test
{
    [TestClass]
    public class YCombinatorTest
    {
		[TestMethod]
		public void Y()
		{
			Assert.AreEqual(120, Utiler.Y<int>(_ => x => x <= 1 ? 1 : x * _(x - 1))(5));
		}
    }
}
