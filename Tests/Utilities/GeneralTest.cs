﻿using System;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Iodynis.Libraries.Utility;

namespace Test
{
	[TestClass]
	public class GeneralTest
	{
		[TestMethod]
		public void Base64ToBytes()
		{
			string text = "Nun liebe Kinder gebt fein Acht\r\n" +
			              "Ich bin die Stimme aus dem Kissen\r\n" +
			              "Ich hab euch etwas mitgebracht\r\n" +
			              "Hab es aus meiner Brust gerissen\r\n" +
			              "Mit diesem Herz hab ich die Macht\r\n" +
			              "Die Augenlider zu erpressen\r\n" +
			              "Ich singe bis der Tag erwacht\r\n" +
			              "Ein heller Schein am Firmament\r\n";
			byte[] bytes = System.Text.Encoding.UTF8.GetBytes(text);
			string base64 = "TnVuIGxpZWJlIEtpbmRlciBnZWJ0IGZlaW4gQWNodA0KSWNoIGJpbiBkaWUgU3RpbW1lIGF1cyBkZW0gS2lzc2VuDQpJY2ggaGFiIGV1Y2ggZXR3YXMgbWl0Z2VicmFjaHQNCkhhYiBlcyBhdXMgbWVpbmVyIEJydXN0IGdlcmlzc2VuDQpNaXQgZGllc2VtIEhlcnogaGFiIGljaCBkaWUgTWFjaHQNCkRpZSBBdWdlbmxpZGVyIHp1IGVycHJlc3Nlbg0KSWNoIHNpbmdlIGJpcyBkZXIgVGFnIGVyd2FjaHQNCkVpbiBoZWxsZXIgU2NoZWluIGFtIEZpcm1hbWVudA0K";

			CollectionAssert.AreEqual(bytes, Utiler.Base64ToBytes(base64));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.Base64ToBytes(null));
		}
		[TestMethod]
		public void BytesToBase64()
		{
			string text = "Nun liebe Kinder gebt fein Acht\r\n" +
			              "Ich bin die Stimme aus dem Kissen\r\n" +
			              "Ich hab euch etwas mitgebracht\r\n" +
			              "Hab es aus meiner Brust gerissen\r\n" +
			              "Mit diesem Herz hab ich die Macht\r\n" +
			              "Die Augenlider zu erpressen\r\n" +
			              "Ich singe bis der Tag erwacht\r\n" +
			              "Ein heller Schein am Firmament\r\n";
			byte[] bytes = System.Text.Encoding.UTF8.GetBytes(text);
			string base64 = "TnVuIGxpZWJlIEtpbmRlciBnZWJ0IGZlaW4gQWNodA0KSWNoIGJpbiBkaWUgU3RpbW1lIGF1cyBkZW0gS2lzc2VuDQpJY2ggaGFiIGV1Y2ggZXR3YXMgbWl0Z2VicmFjaHQNCkhhYiBlcyBhdXMgbWVpbmVyIEJydXN0IGdlcmlzc2VuDQpNaXQgZGllc2VtIEhlcnogaGFiIGljaCBkaWUgTWFjaHQNCkRpZSBBdWdlbmxpZGVyIHp1IGVycHJlc3Nlbg0KSWNoIHNpbmdlIGJpcyBkZXIgVGFnIGVyd2FjaHQNCkVpbiBoZWxsZXIgU2NoZWluIGFtIEZpcm1hbWVudA0K";

			Assert.AreEqual(base64, Utiler.BytesToBase64(bytes));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.BytesToBase64(null));
		}
		[TestMethod]
		public void BytesHashMd5()
		{
			byte[] bytes = new byte[] { 0x4E, 0x75, 0x6E, 0x20, 0x6C, 0x69, 0x65, 0x62, 0x65, 0x20, 0x4B, 0x69, 0x6E, 0x64, 0x65, 0x72, 0x20, 0x67, 0x65, 0x62, 0x74, 0x20, 0x66, 0x65, 0x69, 0x6E, 0x20, 0x41, 0x63, 0x68, 0x74 };
			byte[] hash  = new byte[] { 0x2B, 0x8B, 0x79, 0x80, 0xEC, 0xCE, 0x17, 0x74, 0x87, 0x1C, 0x88, 0x8F, 0xB6, 0xD6, 0xFE, 0x7B };

			CollectionAssert.AreEqual(hash, Utiler.BytesHashMd5(bytes));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.BytesHashMd5(null));
		}
		[TestMethod]
		public void StringHashMd5()
		{
			string @string = "Nun liebe Kinder gebt fein Acht";
			byte[] hash  = new byte[] { 0x2B, 0x8B, 0x79, 0x80, 0xEC, 0xCE, 0x17, 0x74, 0x87, 0x1C, 0x88, 0x8F, 0xB6, 0xD6, 0xFE, 0x7B };

			CollectionAssert.AreEqual(hash, Utiler.StringHashMd5(@string));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.StringHashMd5(null));
		}
		[TestMethod]
		public void BytesExtractByStartAndLength()
		{
			byte[] bytes = new byte[] { 0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0xFF };

			CollectionAssert.AreEqual(new byte[] { 0x00 }, Utiler.BytesExtractByStartAndLength(bytes, 0, 1));
			CollectionAssert.AreEqual(new byte[] { 0x01, 0x02, 0x04, 0x08, 0x10 }, Utiler.BytesExtractByStartAndLength(bytes, 1, 5));
			CollectionAssert.AreEqual(new byte[] { 0x80, 0xFF }, Utiler.BytesExtractByStartAndLength(bytes, 8, 2));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.BytesExtractByStartAndLength(null, 0, 1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndLength(bytes, -1, 0));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndLength(bytes, 100, 0));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndLength(bytes, 0, -1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndLength(bytes, 0, 100));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndLength(new byte[] { }, 0, 0));
		}
		[TestMethod]
		public void BytesExtractByStartAndEnd()
		{
			byte[] bytes = new byte[] { 0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0xFF };

			CollectionAssert.AreEqual(new byte[] { 0x00 }, Utiler.BytesExtractByStartAndEnd(bytes, 0, 0));
			CollectionAssert.AreEqual(new byte[] { 0x01, 0x02, 0x04, 0x08, 0x10 }, Utiler.BytesExtractByStartAndEnd(bytes, 1, 5));
			CollectionAssert.AreEqual(new byte[] { 0x80, 0xFF }, Utiler.BytesExtractByStartAndEnd(bytes, 8, 9));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.BytesExtractByStartAndEnd(null, 0, 1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndEnd(bytes, -1, 9));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndEnd(bytes, 100, 9));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndEnd(bytes, 0, -1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndEnd(bytes, 0, 100));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndEnd(new byte[] { }, 0, 0));

			CollectionAssert.AreEqual(new byte[] { 0xFF }, Utiler.BytesExtractByStartAndEnd(bytes, 9));
			CollectionAssert.AreEqual(new byte[] { 0x10, 0x20, 0x40, 0x80, 0xFF }, Utiler.BytesExtractByStartAndEnd(bytes, 5));
			CollectionAssert.AreEqual(new byte[] { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0xFF }, Utiler.BytesExtractByStartAndEnd(bytes, 1));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.BytesExtractByStartAndEnd(null, 0));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndEnd(bytes, -1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesExtractByStartAndEnd(bytes, 100));
		}
		[TestMethod]
		public void BytesReadByte()
		{
			byte[] bytes = new byte[] { 0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0xFF };

			Assert.AreEqual(0x00, Utiler.BytesReadByte(bytes, 0));
			Assert.AreEqual(0x01, Utiler.BytesReadByte(bytes, 1));
			Assert.AreEqual(0x02, Utiler.BytesReadByte(bytes, 2));
			Assert.AreEqual(0xFF, Utiler.BytesReadByte(bytes, 9));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.BytesReadByte(null, 0));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesReadByte(bytes, -1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesReadByte(bytes, 100));
		}
		[TestMethod]
		public void BytesReadShort()
		{
			byte[] bytes = new byte[] { 0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0xFF };

			Assert.AreEqual(0x0001, Utiler.BytesReadShort(bytes, 0));
			Assert.AreEqual(0x0102, Utiler.BytesReadShort(bytes, 1));
			Assert.AreEqual(0x0204, Utiler.BytesReadShort(bytes, 2));
			Assert.AreEqual(0x80FF, Utiler.BytesReadShort(bytes, 8));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.BytesReadShort(null, 0));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesReadShort(bytes, -1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesReadShort(bytes, 100));
		}
		[TestMethod]
		public void BytesReadInt()
		{
			byte[] bytes = new byte[] { 0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0xFF };

			Assert.AreEqual(0x00010204, Utiler.BytesReadInt(bytes, 0));
			Assert.AreEqual(0x01020408, Utiler.BytesReadInt(bytes, 1));
			Assert.AreEqual(0x02040810, Utiler.BytesReadInt(bytes, 2));
			Assert.AreEqual(0x204080FF, Utiler.BytesReadInt(bytes, 6));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.BytesReadInt(null, 0));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesReadInt(bytes, -1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesReadInt(bytes, 100));
		}
		[TestMethod]
		public void BytesReadLong()
		{
			byte[] bytes = new byte[] { 0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0xFF };

			Assert.AreEqual(0x0001020408102040, Utiler.BytesReadLong(bytes, 0));
			Assert.AreEqual(0x0102040810204080, Utiler.BytesReadLong(bytes, 1));
			Assert.AreEqual(0x02040810204080FF, Utiler.BytesReadLong(bytes, 2));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.BytesReadLong(null, 0));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesReadLong(bytes, -1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => Utiler.BytesReadLong(bytes, 100));
		}
		[TestMethod]
		public void Utf8ToHex()
		{
			string @string = "Nun liebe Kinder gebt fein Acht";
			string hex = "4E756E206C69656265204B696E6465722067656274206665696E2041636874";

			Assert.AreEqual(hex, Utiler.Utf8ToHex(@string));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.Utf8ToHex(null));
		}
		[TestMethod]
		public void HexToUtf8()
		{
			string @string = "Nun liebe Kinder gebt fein Acht";
			string hex = "4E756E206C69656265204B696E6465722067656274206665696E2041636874";

			Assert.AreEqual(@string, Utiler.HexToUtf8(hex));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.HexToUtf8(null));
		}
		[TestMethod]
		public void BytesToHex()
		{
			string @string = "Nun liebe Kinder gebt fein Acht";
			byte[] bytes = Encoding.UTF8.GetBytes(@string);
			string hex = "4E756E206C69656265204B696E6465722067656274206665696E2041636874";

			Assert.AreEqual("", Utiler.BytesToHex(new byte[] { }));
			Assert.AreEqual(hex, Utiler.BytesToHex(bytes));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.BytesToHex(null));
		}
		[TestMethod]
		public void HexToBytes()
		{
			string @string = "Nun liebe Kinder gebt fein Acht";
			byte[] bytes = Encoding.UTF8.GetBytes(@string);
			string hex = "4E756E206C69656265204B696E6465722067656274206665696E2041636874";

			CollectionAssert.AreEqual(new byte[] { }, Utiler.HexToBytes(""));
			CollectionAssert.AreEqual(bytes, Utiler.HexToBytes(hex));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.HexToBytes(null));
		}
		[TestMethod]
		public void Xor()
		{
			CollectionAssert.AreEqual(new byte[] { 0xFF, 0xFF, 0xFF, 0xFF }, Utiler.Xor(new byte[] { 0xFF, 0xF0, 0x0F, 0x00 }, new byte[] { 0x00, 0x0F, 0xF0, 0xFF }));
			CollectionAssert.AreEqual(new byte[] { 0x01, 0x03, 0x07, 0x0A }, Utiler.Xor(new byte[] { 0x01, 0x02, 0x03, 0x03 }, new byte[] { 0x00, 0x01, 0x04, 0x09 }));

			Assert.ThrowsException<ArgumentNullException>(() => Utiler.Xor(null, null));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.Xor(null, new byte[] { 0x00, 0x0F, 0xF0, 0xFF }));
			Assert.ThrowsException<ArgumentNullException>(() => Utiler.Xor(new byte[] { 0x00, 0x0F, 0xF0, 0xFF }, null));
			Assert.ThrowsException<ArgumentException>(() => Utiler.Xor(new byte[] { 0x00 }, new byte[] { 0x00, 0x0F, 0xF0, 0xFF }));
			Assert.ThrowsException<ArgumentException>(() => Utiler.Xor(new byte[4], new byte[8]));
		}
		[TestMethod]
		public void GetBytes()
		{
			CollectionAssert.AreEqual(new byte[] { 0x61, 0x00, 0x62, 0x00, 0x63, 0x00 }, Utiler.GetBytes("abc"));
			CollectionAssert.AreEqual(new byte[] { }, Utiler.GetBytes(""));

			Assert.ThrowsException<ArgumentNullException>(() => Utiler.GetBytes(null));
		}
		[TestMethod]
		public void GetString()
		{
			Assert.AreEqual("abc", Utiler.GetString(new byte[] { 0x61, 0x00, 0x62, 0x00, 0x63, 0x00 }));
			Assert.AreEqual("", Utiler.GetString(new byte[] { }));

			Assert.ThrowsException<ArgumentNullException>(() => Utiler.GetString(null));
		}
	}
}
