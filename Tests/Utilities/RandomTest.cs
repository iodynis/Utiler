﻿using System;
using Iodynis.Libraries.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
	public class RandomTest
	{
        public enum Randoms
        {
            RANDOM_INTEGER,
        }

		[TestMethod]
		public void PersistentRandomInteger()
		{
            var generator = new PersistentRandom(123);

            // Edge cases
            PersistentRandomInteger(int.MinValue, 0, 100);
            PersistentRandomInteger(0, 0, 100);
            PersistentRandomInteger(int.MaxValue, 0, 100);
            PersistentRandomInteger(13666, -10000, 1000);
            PersistentRandomInteger(758, 100, 101);
            PersistentRandomInteger(1, 1, 1);
            PersistentRandomInteger(0, 0, 0);
            Assert.ThrowsException<ArgumentException>(() => generator.Take(1, -1));

            // Distribution
            for (int i = -100; i <= 100; i++)
            {
                PersistentRandomInteger(i * 127 + 23 + i * i * 17, i, i * i + 56);
            }

            // Interval length of 1
            Assert.IsTrue(generator.Take(1, 1) == 1);

            // Preview and skip
            int min = 0;
            int max = 1000000;
            int one = generator.Take(min, max);
            int threePreview1 = generator.Preview(min, max, 1);
            int two = generator.Take(min, max);
            int threePreview2 = generator.Preview(min, max);
            int three = generator.Take(min, max);
            generator.Skip(-1);
            int threeAgain = generator.Take(min, max);

            Assert.IsTrue(one != two);
            Assert.IsTrue(two != three);
            Assert.IsTrue(three == threePreview1);
            Assert.IsTrue(three == threePreview2);
            Assert.IsTrue(three == threeAgain);
        }
		public void PersistentRandomInteger(int seed, int min, int max)
        {
            int length = max - min + 1;
            int[] array = new int[length];
            var generator = new PersistentRandom(seed);

            long sum = 0;
            int count = 1000;
            for (int i = 0; i < count; i++)
            {
                int random = generator.Take(min, max);
                sum += random;
                array[random - min]++;
            }

            if (max != min)
            {
                for (int i = 0; i < length - 1; i++)
                {
                    Assert.IsTrue(array[i] >= count / length / 1.5f - 5);
                }

                long avg = sum / count;
                long diffAbsolute = Math.Abs(avg - (max + min) / 2);
                float diffRelative = (float)diffAbsolute / Math.Abs(max - min);
                Assert.IsTrue(diffRelative < 0.05f);
            }
        }
    }
}
